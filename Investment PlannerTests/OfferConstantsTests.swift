//
//  OfferConstantsTests.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 3/11/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import XCTest

class OfferConstantsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOfferCostMultiplier(){
        let printCost = 0.11
        let handlingFee = 0.14
        let costPerRedemption = 0.50
        let redemptionRate = 0.06
        
        let testProgram = JSONProgram(type: .Custom)
        testProgram.printCostId = printCost
        testProgram.handlingFee = handlingFee
        
        let testOffer = JSONOffer()
        testOffer.parentProgram = testProgram
        testOffer.costPerRedemption = costPerRedemption
        testOffer.redemptionRate = redemptionRate
        testProgram.offersArray.append(testOffer)
        
        assert(((handlingFee + costPerRedemption) * redemptionRate) + printCost == 0.1484)
        let offerCostMultiplier = MathController.sharedMathController.getOfferCostMultiplierForOffer(testOffer)
        assert(offerCostMultiplier == 0.1484)
        
    }
    func testCPUMOffer(){
        let printCost = 0.11
        let handlingFee = 0.14
        let unitsPerOffer = 1.0
        let costPerRedemption = 0.50
        let redemptionRate = 0.06
        let repeatFactor = 1.1
        
        
        let testProgram = JSONProgram(type: .Custom)
        testProgram.printCostId = printCost
        testProgram.handlingFee = handlingFee
        testProgram.shortTermROI = false
        
        let testOffer = JSONOffer()
        testOffer.parentProgram = testProgram
        testOffer.unitsPerOffer = unitsPerOffer
        testOffer.repeatFactor = repeatFactor
        testOffer.costPerRedemption = costPerRedemption
        testOffer.redemptionRate = redemptionRate
        testOffer.tlvType = "loyalty"
        testProgram.offersArray.append(testOffer)
        
        /*
        let a = getPrintCostIdForOffer(offer)
        let b = getHandlingFeeForOffer(offer)
        let c = getOfferQTYForOffer(offer)
        let d = getOfferCostPerRedemptionForOffer(offer)
        let e = getRedemptionRateForOffer(offer)
        let n = getRepeatFactorForOffer(offer)
*/
    }
    
    
}

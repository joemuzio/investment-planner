//
//  BrandPotentialViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/24/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class BrandPotentialViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    var ROIMin: Float = 0.25
    var ROIMax: Float = 5.00
    var maxedOutTrial: Double = 15000000
    var maxedOutLoyalty: Double = 15000000
    var maxedOutVolume: Double = 15000000
    
    var infoButton: UIButton!
    var programDataButton: UIButton!
    var financialDataButton: UIButton!
    var potentialStatementLabel: UILabel!
    var roiButton: UIButton!
    var roiSlider: UISlider!
    var roiValueLabel: UILabel!
    var footnoteLabel: UILabel!
    
    var currentDollarsBar: BarChartBarLayer!
    var trialBar: BarChartBarLayer!
    var loyaltyBar: BarChartBarLayer!
    var volumeBar: BarChartBarLayer!
    var potentialDollarsBar: BarChartBarLayer!
    var barChart: BarChart!
    
    var header: PortfolioHeaderView!
    var footer: GradientView!
    
    var scenario:IPScenario!
    
    var isUpdating: Bool = false
    var isOptimizingProfitROI = true
    var popoverTable = BrandPotentialPopoverTable()
    
    let pageDefinitions = ["Current Dollars": "Total dollars based on the current complete 13 cycles thus equaling 1 year.  Transactional data", "Loyalty": "Incremental Loyalty Program Dollars", "Potential Dollars": "Current Dollars + Trial + Loyalty + Volume", "Profit ROI": "Total incremental profit driven per dollar invested", "Trial": "Incremental Trial Program Dollars", "Volume": "Incremental Volume Program Dollars"]
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        NotificationCenter.default.addObserver(self, selector: #selector(popoverDismissed), name:NSNotification.Name(rawValue: "dismissPopoverTable"), object: nil)
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "icon-page-definitions-light-bg"), for: UIControlState())
        infoButton.sizeToFit()
        infoButton.addTarget(self, action: #selector(BrandPotentialViewController.infoButtonPressed), for: .touchUpInside)
        
        programDataButton = UIButton()
        programDataButton.setImage(UIImage(named: "icon-view-program-data-light-bg"), for: UIControlState())
        programDataButton.sizeToFit()
        programDataButton.addTarget(self, action: #selector(BrandPotentialViewController.programDataButtonPressed), for: .touchUpInside)
        
        financialDataButton = UIButton()
        financialDataButton.setImage(UIImage(named: "roi-chart-blue-icon"), for: UIControlState())
        financialDataButton.sizeToFit()
        financialDataButton.addTarget(self, action: #selector(BrandPotentialViewController.financialDataButtonPressed), for: .touchUpInside)
        
        potentialStatementLabel = UILabel()
        potentialStatementLabel.frame = CGRect(x: 0, y: 80, width: self.view.bounds.width - 160, height: 100)
        potentialStatementLabel.numberOfLines = 2
        
        header = PortfolioHeaderView()
        header.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 0)
        header.titleLabel.text = "" //late decision to remove titles
        header.rightButtons = [infoButton, programDataButton, financialDataButton]
        header.supplementaryViews = [potentialStatementLabel]
        header.sizeToFit()
        self.view.addSubview(header)
        
        footer = GradientView()
        footer.colors = [UIColor(hexColor: 0x284C7E).cgColor, UIColor(hexColor: 0x1F78AF).cgColor]
        footer.locations = [0,1]
        footer.backgroundColor = UIColor.catalinaDarkBlueColor()
        footer.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(footer)
        footer.addConstraint(NSLayoutConstraint(item: footer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 100))
        self.view.addConstraint(NSLayoutConstraint(item: footer, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: footer, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: footer, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        
        roiButton = UIButton(type: UIButtonType.custom)
        roiButton.setTitle("Profit ROI", for: UIControlState())
        roiButton.setTitleColor(UIColor.catalinaLightGreenColor(), for: UIControlState())
        roiButton.titleLabel?.font = UIFont.catalinaMediumFontOfSize(24)
        roiButton.addTarget(self, action: #selector(roiButtonPressed), for: .touchUpInside)
        roiButton.sizeToFit()
        roiButton.translatesAutoresizingMaskIntoConstraints = false
        footer.addSubview(roiButton)
        footer.addConstraint(NSLayoutConstraint(item: roiButton, attribute: .top, relatedBy: .equal, toItem: footer, attribute: .top, multiplier: 1, constant: 20))
        footer.addConstraint(NSLayoutConstraint(item: roiButton, attribute: .centerX, relatedBy: .equal, toItem: footer, attribute: .centerX, multiplier: 1, constant: 0))
        
        roiSlider = UISlider()
        roiSlider.setThumbImage(UIImage(named: "deactivated-slider-top.png"), for: UIControlState())
        roiSlider.setMinimumTrackImage(UIImage(named: "deactivated-slider-bottom.png"), for: UIControlState())
        roiSlider.setMaximumTrackImage(UIImage(named: "deactivated-slider-bottom.png"), for: UIControlState())
        roiSlider.minimumValue = ROIMin
        roiSlider.maximumValue = ROIMax
        roiSlider.value = 1.00
        roiSlider.addTarget(self, action: #selector(BrandPotentialViewController.roiSliderValueChanged), for: .valueChanged)
        roiSlider.translatesAutoresizingMaskIntoConstraints = false
        footer.addSubview(roiSlider)
        roiSlider.addConstraint(NSLayoutConstraint(item: roiSlider, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        footer.addConstraint(NSLayoutConstraint(item: roiSlider, attribute: .top, relatedBy: .equal, toItem: roiButton, attribute: .bottom, multiplier: 1, constant: 0))
        footer.addConstraint(NSLayoutConstraint(item: roiSlider, attribute: .centerX, relatedBy: .equal, toItem: footer, attribute: .centerX, multiplier: 1, constant: 0))
        
        roiValueLabel = UILabel()
        roiValueLabel.text = "$XXX"
        roiValueLabel.font = UIFont.catalinaMediumFontOfSize(16)
        roiValueLabel.textColor = UIColor.catalinaWhiteColor()
        roiValueLabel.sizeToFit()
        roiValueLabel.translatesAutoresizingMaskIntoConstraints = false
        footer.addSubview(roiValueLabel)
        footer.addConstraint(NSLayoutConstraint(item: roiValueLabel, attribute: .left, relatedBy: .equal, toItem: roiSlider, attribute: .right, multiplier: 1, constant: 20))
        footer.addConstraint(NSLayoutConstraint(item: roiValueLabel, attribute: .centerY, relatedBy: .equal, toItem: roiSlider, attribute: .centerY, multiplier: 1, constant: 0))
        
        footnoteLabel = UILabel()
        footnoteLabel.text = "* Simulation based upon investment of $XXX"
        footnoteLabel.font = UIFont.catalinaHeavyFontOfSize(12)
        footnoteLabel.textColor = UIColor.catalinaDarkBlueColor()
        footnoteLabel.sizeToFit()
        footnoteLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(footnoteLabel)
        self.view.addConstraint(NSLayoutConstraint(item: footnoteLabel, attribute: .bottom, relatedBy: .equal, toItem: footer, attribute: .top, multiplier: 1, constant: -20))
        self.view.addConstraint(NSLayoutConstraint(item: footnoteLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        
        barChart = BarChart()
        barChart.titlePadding = 20
        barChart.titleColor = UIColor.catalinaDarkBlueColor()
        barChart.titleFont = UIFont.catalinaMediumFontOfSize(14)
        barChart.textPadding = 2
        barChart.textColor = UIColor.catalinaDarkBlueColor()
        barChart.textFont = UIFont.catalinaHeavyFontOfSize(14)
        barChart.barSpacing = 40
        barChart.baselineWidth = 2.0
        barChart.baselineColor = UIColor.catalinaDarkBlueColor()
        barChart.shouldAnimateUpdates = true
        barChart.shouldUseHighestBarValueForMax = false
        barChart.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(barChart)
        self.view.addConstraint(NSLayoutConstraint(item: barChart, attribute: .top, relatedBy: .equal, toItem: header, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: barChart, attribute: .bottom, relatedBy: .equal, toItem: footnoteLabel, attribute: .top, multiplier: 1, constant: -40))
        self.view.addConstraint(NSLayoutConstraint(item: barChart, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 40))
        self.view.addConstraint(NSLayoutConstraint(item: barChart, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: -40))
        
        let currentDollarsValue: Double = 50000000
        currentDollarsBar = barChart.addBarWithValue(currentDollarsValue, color: UIColor(white: 0.9, alpha: 1), title: "CURRENT DOLLARS", text: currentDollarsValue.abbreviatedCurrencyString())
        trialBar = barChart.addBarWithValue(0, color: UIColor.catalinaDarkBlueColor(), title: "TRIAL", text: nil)
        loyaltyBar = barChart.addBarWithValue(0, color: UIColor.catalinaLightBlueColor(), title: "LOYALTY", text: nil)
        volumeBar = barChart.addBarWithValue(0, color: UIColor.catalinaLightGreenColor(), title: "VOLUME", text: nil)
        potentialDollarsBar = barChart.addBarWithValue(0, color: UIColor(hexColor: 0x87DCFA), title: "POTENTIAL DOLLARS", text: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if scenario == nil || scenario.program.report != SettingsController.sharedSettingsController.selectedReport {
            
            if let program = SettingsController.sharedSettingsController.selectedReport?.program{
               scenario = program.createNewScenario()
            }
            
            let currentDollarsValue = scenario.program.report.data.mpiTotal.dollars
            currentDollarsBar.value = Double(currentDollarsValue)
            currentDollarsBar.text = currentDollarsBar.value.abbreviatedCurrencyString()
            
            let max = scenario.program.templateScenario!.totalDollars
            barChart.maxBarValue = currentDollarsBar.value + max
            
            SolverController.sharedSolverController.optimalInvestmentForConstraints(scenario)
            
            if(isOptimizingProfitROI){
                roiSlider.minimumValue = Float(scenario.minProfitRoi)
                roiSlider.maximumValue = Float(scenario.maxProfitRoi)
                roiSlider.setValue(Float(scenario.profitRoi), animated:true)
            }
            else{
                roiSlider.minimumValue = Float(scenario.minRetailRoi)
                roiSlider.maximumValue = Float(scenario.maxRetailRoi)
                roiSlider.setValue(Float(scenario.retailRoi), animated:true)
            }
        }
        
        self.update(false)
    }
    
    func infoButtonPressed() {
        let defSection = DefinitionsSection(name: "Definitions", terms: pageDefinitions)
        self.showPageDefinitionsWithSections([defSection], animated: true)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Info Button Pressed", action: "Brand Potential Definitions Shown", label: "", value: 0)
    }
    
    func programDataButtonPressed() {
        let designInputs = DesignInputsViewController(scenario: scenario)
        self.present(designInputs, animated: true, completion: nil)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Program Data Button Pressed", action: "Brand Potential Program Data Shown", label: "", value: 0)
    }
    
    func financialDataButtonPressed() {
        let financialSummary = FinancialSummaryViewController()
        financialSummary.scenario = scenario
        self.present(financialSummary, animated: true, completion: nil)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Financial Data Button Pressed", action: "Brand Potential Financial Data Shown", label: "", value: 0)
    }
    
    func roiSliderValueChanged() {
        if !isUpdating {
            self.update(true)
        }
    }
    
    func update(_ needsSolve: Bool) {
        
        isUpdating = true
        let roiSliderValue = Double(round(roiSlider.value * 100) / 100)
        
        if needsSolve {
            if(isOptimizingProfitROI){
                SolverController.sharedSolverController.optimalProfitRoiForConstraints(scenario, profitRoi: roiSliderValue, trialPercent: nil, loyaltyPercent: nil, volumePercent: nil, investment: nil)
            }
            else{
                SolverController.sharedSolverController.optimalRetailRoiForConstraints(scenario, retailRoi: roiSliderValue, trialPercent: nil, loyaltyPercent: nil, volumePercent: nil, investment: nil)
            }
        }

    //    roiSlider.setValue(Float(scenario.profitRoi), animated: true)
    
        if(isOptimizingProfitROI){
            roiValueLabel.text = "$\(scenario.profitRoi.format(".2"))"
        }
        else{
            roiValueLabel.text = "$\(scenario.retailRoi.format(".2"))"
        }

        let trialValue = scenario.getTotalDollarsForType(.Trial)
        let loyaltyValue = scenario.getTotalDollarsForType(.Loyalty)
        let volumeValue = scenario.getTotalDollarsForType(.Volume)
        //trial
        trialBar.value = trialValue
        trialBar.offset = currentDollarsBar.value
        trialBar.text = trialBar.value.abbreviatedCurrencyString()
        
        //loyalty
        loyaltyBar.value = loyaltyValue
        loyaltyBar.offset = trialBar.offset + trialBar.value
        loyaltyBar.text = loyaltyBar.value.abbreviatedCurrencyString()
        
        //volume
        volumeBar.value = volumeValue
        volumeBar.offset = loyaltyBar.offset + loyaltyBar.value
        volumeBar.text = volumeBar.value.abbreviatedCurrencyString()
        
        //potential
        potentialDollarsBar.value = currentDollarsBar.value + trialBar.value + loyaltyBar.value + volumeBar.value
        potentialDollarsBar.text = potentialDollarsBar.value.abbreviatedCurrencyString()
        
        barChart.update()
        
        footnoteLabel.text = "* Simulation based on investment of \(scenario.totalBudget.abbreviatedCurrencyString())"
        let growthPotential = trialBar.value + loyaltyBar.value + volumeBar.value
        let growthPotentialText = growthPotential.abbreviatedCurrencyString()
        self.updatePotentialStatementLabel("\(scenario.program.focusBrand) has the potential to grow \(growthPotentialText) \ntotal dollars in the next year.")
        
        isUpdating = false
    }
    
    func updatePotentialStatementLabel(_ text: String) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = .center
        let attributedText = NSAttributedString(string: text, attributes:
            [NSParagraphStyleAttributeName : paragraphStyle,
                NSFontAttributeName : UIFont.catalinaLightFontOfSize(36),
                NSForegroundColorAttributeName : UIColor.catalinaDarkBlueColor()])
        potentialStatementLabel.attributedText = attributedText
    }
    
    func roiButtonPressed(_ sender:UIButton) {
        popoverTable.modalPresentationStyle = .popover
        popoverTable.setupTable(["Profit ROI", "Retail ROI"])
        if let popover = popoverTable.popoverPresentationController {
            let buttonPoint = sender.superview?.convert(sender.center, to: self.view)
            popover.delegate = self
            popover.sourceRect = CGRect(x: buttonPoint!.x, y: buttonPoint!.y, width: 0, height: 0)
            popover.sourceView = self.view
            popover.permittedArrowDirections = .down
            self.present(popoverTable, animated:true, completion: nil)
        }
    }
    
    func popoverDismissed(){
        let unit = popoverTable.selectedEfficiencyString
        if(unit == "Profit ROI"){
            isOptimizingProfitROI = true
            roiButton.setTitle("Profit ROI", for: UIControlState())
            roiSlider.minimumValue = Float(scenario.minProfitRoi)
            roiSlider.maximumValue = Float(scenario.maxProfitRoi)
            roiSlider.setValue(Float(scenario.profitRoi), animated:true)
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("ROI Type Changed", action: "Brand Potential ROI Changed To: Profit ROI", label: "", value: 0)
        }
        else{
            isOptimizingProfitROI = false
            roiButton.setTitle("Retail ROI", for: UIControlState())
            roiSlider.minimumValue = Float(scenario.minRetailRoi)
            roiSlider.maximumValue = Float(scenario.maxRetailRoi)
            roiSlider.setValue(Float(scenario.retailRoi), animated:true)
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("ROI Type Changed", action: "Brand Potential ROI Changed To: Retail ROI", label: "", value: 0)
        }
        self.update(true)
        popoverTable.dismiss(animated: true, completion: nil)
    }
    
}
class BrandPotentialPopoverTable: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var cellsArray:[String]?
    var tableView = UITableView()
    var selectedEfficiencyString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
    }
    func setupTable(_ array:[String]){
        cellsArray = array
        let tableHeight = ((self.cellsArray!.count) * 44 <= 600) ? (self.cellsArray!.count) * 44 : 600
        let viewRect = CGRect(x: 0, y: 0, width: 300, height: CGFloat(tableHeight))
        self.preferredContentSize = CGSize(width: viewRect.size.width, height: viewRect.size.height)
        tableView.frame = viewRect
        tableView.delegate = self
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
        tableView.reloadData()
    }
    // use tag for scenario number or program number
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.textLabel?.text = self.cellsArray![(indexPath as NSIndexPath).row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let cellText = cell!.textLabel!.text
        selectedEfficiencyString = cellText!
        NotificationCenter.default.post(name: Notification.Name(rawValue: "dismissPopoverTable"), object: nil)
    }
    func numberOfSections(in tableView:UITableView)->Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsArray!.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //Removes bottom space on tableview
        return CGFloat.leastNormalMagnitude
    }
}

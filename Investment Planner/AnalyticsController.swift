//
//  AnalyticsController.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 10/6/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

private let SharedAnalyticsInstance = AnalyticsController()

class AnalyticsController: NSObject {
    
    class var sharedAnalyticsController: AnalyticsController {
        return SharedAnalyticsInstance
    }
    func setupDefaults(){
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()!
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        gai.logger.logLevel = GAILogLevel.verbose
    }
    func sendEventToAnalytics(_ event: String, action: String, label: String, value: NSNumber){
        if let tracker = GAI.sharedInstance().defaultTracker{
        tracker.set(kGAIEvent, value: action)
            let build = GAIDictionaryBuilder.createEvent(withCategory: event, action: action, label: label, value: value).build() as [NSObject: AnyObject]
       //    let build = GAIDictionaryBuilder.createEventWithCategory(event, action: action, label: label, value: value).build() as [NSObject: AnyObject]
        tracker.send(build)

        }
    }
    func sendScreenDataToAnalytics(_ screen: String){
        if let tracker = GAI.sharedInstance().defaultTracker{
            tracker.set(kGAIScreenName, value: screen)
            let build = GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject]
            tracker.send(build)
            
        }
    }
    func sendUserIdToAnalytics(_ userId:String){
        if let tracker = GAI.sharedInstance().defaultTracker{
            tracker.set(GAIFields.customDimension(for: 1), value: userId)
        tracker.set(kGAIUserId, value: userId)
       let build = GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject]
        tracker.send(build)
        }
    }
    func getCurrentUserID()->String?{
        if let tracker = GAI.sharedInstance().defaultTracker{
            return tracker.get(kGAIUserId)
        }
        return nil
    }
}

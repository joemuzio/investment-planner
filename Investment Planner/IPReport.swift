//
//  IPReport.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/18/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum IPReportType: String {
    
    case Standard = "STANDARD"
    case Custom = "CUSTOM"
    case Shared = "SHARED"
    static let allTypes = [Shared, Custom, Standard]
    static let searchableTypes = [Shared, Custom, Standard]
    static let userSpecificTypes = [Shared, Custom]
    static let listManagerTypes = [Shared, Custom]
    static let brandCategoryTypes = [Standard]
}

class IPReport: IPModelObject {
    
    var id: String = ""
    var displayName: String = ""
    var displayType: IPReportType = .Custom
    var reportName: String = ""
    var reportType: String = ""
    var reportDate: String = ""
    var reportNumber: String = ""
    var category: Int = 0
    var categoryDescription: String = ""
    var brand: String = ""
    var productList: String = ""
    var owner: String = ""
    var createdBy: String = ""
    var createdDate: Date = Date()
    var lastModifiedBy: String = ""
    var lastModifiedDate: Date = Date()
    var fileName: String = ""
    var fileExtension: String = ""
    var filePath: String = ""
    var fileLastModified: Date = Date.distantPast
    
    var data: IPReportData = IPReportData()
    var program: IPProgram?
    var prompts: [[String]] = [[String]]()
    
    var prettyPrompts: [String: String]{
        get {
            return makePrettyPrmopts(prompts)
        }
    }
    
    convenience init(type: IPReportType) {
        self.init()
        self.displayType = type
    }
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "id",
            "displayType",
            "displayName",
            "reportName",
            "reportType",
            "reportDate",
            "reportNumber",
            "category",
            "categoryDescription",
            "brand",
            "productList",
            "owner",
            "createdBy",
            "createdDate",
            "lastModifiedBy",
            "lastModifiedDate",
            "fileName",
            "fileExtension",
            "filePath",
            "fileLastModified",
            "data",
            "program",
            "prompts"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "displayType" {
                if let stringValue = value as? String {
                    if let type = IPReportType(rawValue: stringValue) {
                        self.displayType = type
                        return
                    }
                }
                self.displayType = .Custom
                return
            }
            if key == "fileLastModified" {
                if let stringValue = value as? String {
                    if let date = IPModelObject.standardUTCDateFormatter.date(from: stringValue) {
                        self.fileLastModified = date
                    }
                }
                return
            }
            if key == "createdDate" {
                if let stringValue = value as? String {
                    if let date = IPModelObject.standardUTCDateFormatter.date(from: stringValue) {
                        self.createdDate = date
                    }
                }
                return
            }
            if key == "lastModifiedDate" {
                if let stringValue = value as? String {
                    if let date = IPModelObject.standardUTCDateFormatter.date(from: stringValue) {
                        self.lastModifiedDate = date
                    }
                }
                return
            }
            if key == "data" {
                if let JSONDictionary = value as? [String: AnyObject] {
                    let data = IPReportData()
                    data.updateWithJSONObject(JSONDictionary)
                    self.data = data
                }
                return
            }
            if key == "program" {
                if let JSONDictionary = value as? [String: AnyObject] {
                    let program = IPProgram(report: self)
                    program.updateWithJSONObject(JSONDictionary)
                    self.program = program
                }
                return
            }
            if key == "prompts" {
                if let JSONDictionary = value as? [[AnyObject]] {
                    self.prompts = cleaningUpJasonsGarbage(JSONDictionary)
                }
                return
            }
        }
        super.setValue(value, forKey: key)
    }
    
    fileprivate func cleaningUpJasonsGarbage(_ raw: [[AnyObject]]) -> [[String]] {
        var temp: [[String]] = [[String]]()
        for i in raw {
            var w0 = ""
            var w1 = ""
            if let n0 = i[0] as? String {
                if(n0 != "") {
                    w0 = n0
                }
            }
            if let n1 = i[1] as? String {
                if(n1 != "") {
                    w1 = n1
                }
            }
            temp.append([w0, w1])
        }
        
        return temp
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "id" {
                if self.id.characters.count == 0 {
                    return nil
                }
                return self.id
            }
            if key == "displayType" {
                return self.displayType.rawValue
            }
            if key == "fileLastModified" {
                return IPModelObject.standardUTCDateFormatter.string(from: self.fileLastModified)
            }
            if key == "createdDate" {
                return IPModelObject.standardUTCDateFormatter.string(from: self.createdDate)
            }
            if key == "lastModifiedDate" {
                return IPModelObject.standardUTCDateFormatter.string(from: self.lastModifiedDate)
            }
            if key == "data" {
                return data.JSONObject()
            }
            if key == "program" {
                if let program = self.program {
                    return program.JSONObject()
                }
                return nil
            }
            
        }
        return super.value(forKey: key)
    }
    
    fileprivate func makePrettyPrmopts(_ raw: [[String]]) -> [String: String] {
        var prompts: [String: String] = [String: String]()
        
        var tempVals: String = ""
        var tempKey: String = ""
        for block in raw {
            let key = block[0]
            let val = block[1]
            // Check the key
            if(key == "") {
                //  We don't want to store a double blank
                if(val == "") {
                    continue
                }
                // Check if we have had any valid keys yet
                if(tempKey == "") {
                    continue
                }
                // Append to the temp vals
                if(tempVals == "") {
                    tempVals = val
                } else {
                    tempVals += "\n\(val)"
                }
            } else {
                // Key is equal to something.  Append any existing temps to the prompts.  Reset for the next accumulation.
                if(tempKey != "") {
                    prompts[tempKey] = tempVals
                }
                // Clear out the temps and set to new key val
                tempKey = key
                tempVals = val
            }
        }
        // Make sure to add any residual values after loop finishes
        if(tempKey != "") {
            prompts[tempKey] = tempVals
        }
        return prompts
    }
    
    func createProgram() -> IPProgram {
        
        let program = IPProgram(report: self)
        program.clientName = self.displayName
        program.focusBrand = self.brand
        let userDictionary = SettingsController.sharedSettingsController.userMathConstants
        var constantsDictionary = data.constants
        if(userDictionary?.count > 0){
        for(key, value) in userDictionary!{
            constantsDictionary[key] = value
        }
        }
        program.setValuesForKeys(constantsDictionary)
        // override defaults if user has new default saved
        if let startDateString = data.setup["Current Period - Start Date"] as? String {
            program.beginDate = IPModelObject.standardUTCDateFormatter.date(from: startDateString)!
            program.beginDate = program.beginDate.addingTimeInterval(31536000)
        }
        if let endDateString = data.setup["Current Period - End Date"] as? String {
            program.endDate = IPModelObject.standardUTCDateFormatter.date(from: endDateString)!
            program.endDate = program.endDate.addingTimeInterval(31536000)
        }
        for mpiValue in data.mpi.reversed() {
            if mpiValue.cycle.uppercased() == "TOTAL" {
                program.retailPricePerUnit = mpiValue.averagePrice
                break
            }
        }
        
        program.templateScenario = program.createTemplateScenario()
        let defaultScenario = program.createNewScenario()
        program.scenarios.append(defaultScenario)
        
        return program
    }
    
    func save() -> Bool {
        
        return DataController.sharedDataController.saveReport(self)
    }
    
    func duplicate() -> IPReport {
        
        let JSON = self.JSONObject()
        let newReport = IPReport()
        newReport.updateWithJSONObject(JSON)
        return newReport
    }
}

class IPReportData: IPModelObject {
    
    var constants: [String: AnyObject] = [String: AnyObject]()
    var results: [IPResult] = [IPResult]()
    var setup: [String: AnyObject] = [String: AnyObject]()
    var retailers: [IPRetailer] = [IPRetailer]()
    var mpi: [IPMPI] = [IPMPI]()
    var mpiCcm: [IPMPICCM] = [IPMPICCM]()
    var loyaltyDefection: [String: AnyObject] = [String: AnyObject]()
    var consumerTruth: [String: AnyObject] = [String: AnyObject]()
    var lapsedBuyerBgt: [String: AnyObject] = [String: AnyObject]()
    
    var mpiTotal: IPMPI {
        get {
            
            for mpiLine in mpi {
                if mpiLine.cycle == "Total" {
                    return mpiLine
                }
            }
            
            return IPMPI()
        }
    }
    
    var mpiCcmTotal: IPMPI {
        get {
            
            for mpiLine in mpiCcm {
                if mpiLine.cycle == "Total" {
                    return mpiLine
                }
            }
            
            return IPMPI()
        }
    }
    
    func getOrderedCycles() -> [MPICycle] {
        var unorderedCycles = Set<MPICycle>()
        
        for mpiLine in mpi {
            if let cycleObject = mpiLine.getCycleObject() {
                unorderedCycles.insert(cycleObject)
            }
        }
        var firstYear: Int = Int.max
        
        for mpiCycle in unorderedCycles {
            if mpiCycle.year < firstYear {
                firstYear = mpiCycle.year
            }
        }
        
        var firstYearCycles = [MPICycle]()
        var secondYearCycles = [MPICycle]()
        
        for mpiCycle in unorderedCycles {
            if mpiCycle.year == firstYear {
                firstYearCycles.append(mpiCycle)
            }
            else {
                secondYearCycles.append(mpiCycle)
            }
        }
        
        firstYearCycles.sort(by: { $0.cycle < $1.cycle })
        secondYearCycles.sort(by: { $0.cycle < $1.cycle })
        
        var sortedCycles = [MPICycle]()
        sortedCycles.append(contentsOf: firstYearCycles)
        sortedCycles.append(contentsOf: secondYearCycles)
        
        return sortedCycles
    }
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "constants",
            "results",
            "setup",
            "retailers",
            "mpi",
            "mpiCcm",
            "loyaltyDefection",
            "consumerTruth",
            "lapsedBuyerBgt"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "results" {
                var results = [IPResult]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let result = IPResult()
                        result.updateWithJSONObject(item)
                        results.append(result)
                    }
                }
                self.results = results
                return
            }
            if key == "retailers" {
                var mpi = [IPRetailer]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let mpiValue = IPRetailer()
                        mpiValue.updateWithJSONObject(item)
                        mpi.append(mpiValue)
                    }
                }
                self.retailers = mpi
                return
            }
            if key == "mpi" {
                var mpi = [IPMPI]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let mpiValue = IPMPI()
                        mpiValue.updateWithJSONObject(item)
                        mpi.append(mpiValue)
                    }
                }
                self.mpi = mpi
                return
            }
            if key == "mpiCcm" {
                var mpiCcm = [IPMPICCM]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let mpiValue = IPMPICCM()
                        mpiValue.updateWithJSONObject(item)
                        mpiCcm.append(mpiValue)
                    }
                }
                self.mpiCcm = mpiCcm
                return
            }
        }
        super.setValue(value, forKey: key)
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "results" {
                var array = [[String: AnyObject]]()
                for result in self.results {
                    array.append(result.JSONObject())
                }
                return array
            }
            if key == "retailers" {
                var array = [[String : AnyObject]]()
                for mpiValue in self.retailers {
                    array.append(mpiValue.JSONObject())
                }
                return array
            }
            if key == "mpi" {
                var array = [[String : AnyObject]]()
                for mpiValue in self.mpi {
                    array.append(mpiValue.JSONObject())
                }
                return array
            }
            if key == "mpiCcm" {
                var array = [[String : AnyObject]]()
                for mpiValue in self.mpiCcm {
                    array.append(mpiValue.JSONObject())
                }
                return array
            }
        }
        return super.value(forKey: key)
    }
    
    
}

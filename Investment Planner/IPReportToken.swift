//
//  IPReportToken.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/19/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPReportToken: IPModelObject {
    
    var id: String = ""
    var type: IPReportType = .Custom
    var name: String = ""
    var brand: String = ""
    var lastModified: Date = Date.distantPast
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "id",
            "type",
            "name",
            "brand",
            "lastModified"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "type" {
                if let stringValue = value as? String {
                    if let type = IPReportType(rawValue: stringValue) {
                        self.type = type
                        return
                    }
                }
                self.type = .Custom
                return
            }
            if key == "lastModified" {
                if let stringValue = value as? String {
                    if let date = IPModelObject.standardUTCDateFormatter.date(from: stringValue) {
                        self.lastModified = date
                    }
                }
                return
            }
        }
        super.setValue(value, forKey: key)
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "type" {
                return self.type.rawValue
            }
            if key == "lastModified" {
                return IPModelObject.standardUTCDateFormatter.string(from: self.lastModified)
            }
        }
        return super.value(forKey: key)
    }
}

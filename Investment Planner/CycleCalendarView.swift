//
//  CycleCalendarView.swift
//  Investment Planner
//
//  Created by Douglas Wall on 8/29/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class CycleCalendarView: UIView {
    
    var scenario: IPScenario!
    var titleRow: UIView!
    var lapsedBuyerRow:CalendarLineView!
    var dataMiningRow: CalendarLineView!
    var heavyCategoryNeverBuyRow: CalendarLineView!
    var expandConsumptionRow: CalendarLineView!
    var ccmRow: CalendarLineView!
    
    init(scenario: IPScenario){
        super.init(frame: CGRect.zero)
        self.scenario = scenario
        let orderedCycles = scenario.program.report.data.getOrderedCycles()
        let rowHeightMultiplier:CGFloat = 1/8
        let cellWidthMultiplier: CGFloat = 1/13
        
        let cycleLabel = UILabel()
        cycleLabel.translatesAutoresizingMaskIntoConstraints = false
        cycleLabel.textColor = UIColor.catalinaLightBlueColor()
        cycleLabel.text = "Cycles"
        cycleLabel.font = UIFont.catalinaMediumFontOfSize(20)
        self.addSubview(cycleLabel)
        
        self.addConstraint(NSLayoutConstraint(item: cycleLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: cycleLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: cycleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: cycleLabel, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        titleRow = UIView()
        titleRow.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleRow)
        
        self.addConstraint(NSLayoutConstraint(item: titleRow, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: titleRow, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: titleRow, attribute: .top, relatedBy: .equal, toItem: cycleLabel, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: titleRow, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        lapsedBuyerRow = CalendarLineView(cycleType: "LB", orderedCycles: orderedCycles, maxButtonCount: 2)
        lapsedBuyerRow.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lapsedBuyerRow)
        
        self.addConstraint(NSLayoutConstraint(item: lapsedBuyerRow, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: lapsedBuyerRow, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: lapsedBuyerRow, attribute: .top, relatedBy: .equal, toItem: titleRow, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: lapsedBuyerRow, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        dataMiningRow = CalendarLineView(cycleType: "DM", orderedCycles: orderedCycles, maxButtonCount: 2)
        dataMiningRow.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(dataMiningRow)
        
        self.addConstraint(NSLayoutConstraint(item: dataMiningRow, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: dataMiningRow, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: dataMiningRow, attribute: .top, relatedBy: .equal, toItem: lapsedBuyerRow, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: dataMiningRow, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        heavyCategoryNeverBuyRow = CalendarLineView(cycleType: "HCNB", orderedCycles: orderedCycles, maxButtonCount: 2)
        heavyCategoryNeverBuyRow.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(heavyCategoryNeverBuyRow)
        
        self.addConstraint(NSLayoutConstraint(item: heavyCategoryNeverBuyRow, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: heavyCategoryNeverBuyRow, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: heavyCategoryNeverBuyRow, attribute: .top, relatedBy: .equal, toItem: dataMiningRow, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: heavyCategoryNeverBuyRow, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        expandConsumptionRow = CalendarLineView(cycleType: "OUTU", orderedCycles: orderedCycles, maxButtonCount: 13)
        expandConsumptionRow.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(expandConsumptionRow)
        
        self.addConstraint(NSLayoutConstraint(item: expandConsumptionRow, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: expandConsumptionRow, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: expandConsumptionRow, attribute: .top, relatedBy: .equal, toItem: heavyCategoryNeverBuyRow, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: expandConsumptionRow, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        ccmRow = CalendarLineView(cycleType: "CCM", orderedCycles: orderedCycles, maxButtonCount: 4)
        ccmRow.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(ccmRow)
        
        self.addConstraint(NSLayoutConstraint(item: ccmRow, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: ccmRow, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20))
        self.addConstraint(NSLayoutConstraint(item: ccmRow, attribute: .top, relatedBy: .equal, toItem: expandConsumptionRow, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: ccmRow, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: rowHeightMultiplier, constant: 0))
        
        var lastCell: UILabel!
        var currentCycle = 1
        if let firstCycle = orderedCycles.first{
            currentCycle = firstCycle.cycle
        }
        for i in 0..<13{
            let newCell = UILabel()
            newCell.translatesAutoresizingMaskIntoConstraints = false
         //   newCell.backgroundColor = UIColor.init(red: 127.0/255.0, green: 127.0/255.0, blue: 127.0/255.0, alpha: 1.0)
            newCell.textColor = UIColor.catalinaLightBlueColor()
            newCell.font = UIFont.catalinaBoldFontOfSize(16)
            newCell.textAlignment = .center
            newCell.text = "\(currentCycle)"
            titleRow.addSubview(newCell)
            
            titleRow.addConstraint(NSLayoutConstraint(item: newCell, attribute: .top, relatedBy: .equal, toItem: titleRow, attribute: .top, multiplier: 1, constant: 1))
            titleRow.addConstraint(NSLayoutConstraint(item: newCell, attribute: .bottom, relatedBy: .equal, toItem: titleRow, attribute: .bottom, multiplier: 1, constant: -1))
            titleRow.addConstraint(NSLayoutConstraint(item: newCell, attribute: .width, relatedBy: .equal, toItem: titleRow, attribute: .width, multiplier: cellWidthMultiplier, constant: -1))
            if i == 0{
                titleRow.addConstraint(NSLayoutConstraint(item: newCell, attribute: .left, relatedBy: .equal, toItem: titleRow, attribute: .left, multiplier: 1, constant: 1))
            }
            else{
                titleRow.addConstraint(NSLayoutConstraint(item: newCell, attribute: .left, relatedBy: .equal, toItem: lastCell, attribute: .right, multiplier: 1, constant: 1))
            }
            lastCell = newCell
            
            currentCycle += 1
            
            if currentCycle > 13 {
                currentCycle -= 13
            }
        }
        highlightActiveCycles()
        disableInactiveCycles()
        
    }
    func disableInactiveCycles(){
        for offer in scenario.offers{
            for coupon in offer.coupons{
                let templateCoupon = coupon.getTemplateCoupon()
                let distributionCycles = templateCoupon.distributionCycles
                switch offer.name {
                case .LapsedBuyer:
                    lapsedBuyerRow.enableDisableButtonsOnCount()
                case .DataMining:
                    dataMiningRow.enableDisableButtonsOnCount()
                case .HeavyNeverBuy:
                    heavyCategoryNeverBuyRow.enableDisableButtonsOnCount()
                case .CCM:
                    ccmRow.disableInactiveCycleButtons(allowedCycles: distributionCycles)
                default:
                    ()
                }
            }
        }
    }
    func highlightActiveCycles(){
        
        let orderedCycles = scenario.program.report.data.getOrderedCycles()
        for offer in scenario.offers {
            let activeCycles = offer.activeCycles()
            for mpiCycle in orderedCycles{
                for activeMpiCycle in activeCycles{
                    if activeMpiCycle.cycle == mpiCycle.cycle && activeMpiCycle.year == mpiCycle.year {
                        switch offer.name {
                        case .LapsedBuyer:
                            lapsedBuyerRow.highLightButtonWithCycleTag(cycle: activeMpiCycle.cycle, color: UIColor.catalinaDarkBlueColor())
                        case .DataMining:
                            dataMiningRow.highLightButtonWithCycleTag(cycle: activeMpiCycle.cycle, color: UIColor.catalinaDarkBlueColor())
                        case .HeavyNeverBuy:
                            heavyCategoryNeverBuyRow.highLightButtonWithCycleTag(cycle: activeMpiCycle.cycle, color: UIColor.catalinaDarkBlueColor())
                        case .OUTU:
                            expandConsumptionRow.highLightButtonWithCycleTag(cycle: activeMpiCycle.cycle, color: UIColor.catalinaLightBlueColor())
                        case .CCM:
                            ccmRow.highLightButtonWithCycleTag(cycle: activeMpiCycle.cycle, color: UIColor.catalinaLightGreenColor())
                        }
                    }
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CalendarButton: UIButton {
    var isOn = false
    var cycleString = ""
    init(cycle: Int, year: Int){
        super.init(frame: CGRect.zero)
        if cycle < 10{
            cycleString = "\(year) Cyc0\(cycle)"
        }
        else{
            cycleString = "\(year) Cyc\(cycle)"
        }
    }
    func disable(){
        self.alpha = 0.2
        self.isUserInteractionEnabled = false
    }
    func enable(){
        self.alpha = 1
        self.isUserInteractionEnabled = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CalendarLineView: UIView {
    private var buttons: [UIButton]!
    var maxButtonCount: Int!
    init(cycleType: String, orderedCycles: [MPICycle], maxButtonCount: Int) {
        super.init(frame: CGRect.zero)
        self.maxButtonCount = maxButtonCount
        let cellWidthMultiplier: CGFloat = 1/13
        buttons = [UIButton]()
        var lastCell: UIButton!
        
        for i in 0..<13{
            let currentCycle = orderedCycles[i]
            let newCell = CalendarButton(cycle: currentCycle.cycle, year: currentCycle.year)
            newCell.tag = currentCycle.cycle
            newCell.translatesAutoresizingMaskIntoConstraints = false
            newCell.backgroundColor = UIColor.lightGray
            newCell.setTitleColor(UIColor.catalinaWhiteColor(), for: .normal)
            newCell.titleLabel?.font = UIFont.catalinaBoldFontOfSize(16)
            newCell.titleLabel?.textAlignment = .center
            newCell.setTitle(cycleType, for: .normal)
            self.addSubview(newCell)
            buttons.append(newCell)
            
            self.addConstraint(NSLayoutConstraint(item: newCell, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: newCell, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -1))
            self.addConstraint(NSLayoutConstraint(item: newCell, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: cellWidthMultiplier, constant: -1))
            if i == 0{
                self.addConstraint(NSLayoutConstraint(item: newCell, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 1))
            }
            else{
                self.addConstraint(NSLayoutConstraint(item: newCell, attribute: .left, relatedBy: .equal, toItem: lastCell, attribute: .right, multiplier: 1, constant: 1))
            }
            lastCell = newCell
        }
    }
    func addButtonTarget(target: UIViewController, selector: Selector){
        for button in buttons{
            button.addTarget(target, action: selector, for: .touchUpInside)
        }
    }
    func printTags(){
        for i in 0...buttons.count - 1{
            let button = buttons[i] as! CalendarButton
            print(button.cycleString)
        }
    }
    func highLightButtonWithCycleTag(cycle: Int, color: UIColor){
        for i in 0...buttons.count - 1{
            let button = buttons[i] as! CalendarButton
            if button.tag == cycle{
                button.backgroundColor = color
                button.isOn = true
            }
        }
    }
    func highlightOrDisableButtonForActiveCycle(activeCycles: [MPICycle], color: UIColor){
        for i in 0...buttons.count - 1{
            var shouldDisable = true
            let button = buttons[i] as! CalendarButton
            for cycle in activeCycles{
                if cycle.cycle == button.tag{
                    shouldDisable = false
                }
            }
            if shouldDisable{
                button.layer.opacity = 0.25
                button.isUserInteractionEnabled = false
            }
            else{
                button.backgroundColor = color
                button.isOn = true
            }
        }
    }
    func enableDisableButtonsOnCount(){
        if buttonOnCount == maxButtonCount{
            for i in 0...buttons.count - 1{
                let button = buttons[i] as! CalendarButton
                if !button.isOn {
                    button.disable()
                }
            }
        }
        else{
            for i in 0...buttons.count - 1{
                let button = buttons[i] as! CalendarButton
                if !button.isOn {
                    button.enable()
                }
            }
        }
    }
    var buttonOnCount: Int{
        get{
            var buttonOnCount = 0
            for i in 0...buttons.count - 1{
                let button = buttons[i] as! CalendarButton
                if button.isOn {
                    buttonOnCount += 1
                }
            }
            return buttonOnCount
        }
    }
    func disableInactiveCycleButtons(allowedCycles:[String: Double]){
        for i in 0...buttons.count - 1{
            var shouldDisable = true
            let button = buttons[i] as! CalendarButton
            for cycle in allowedCycles{
                if cycle.key.contains(button.cycleString){
                    shouldDisable = false
                }
            }
            if shouldDisable{
                button.layer.opacity = 0.25
                button.isUserInteractionEnabled = false
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

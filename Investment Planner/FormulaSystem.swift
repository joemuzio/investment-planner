class FormulaSystem {
    var constraints = [[String: AnyObject]]()
    //var columnNames = [String]()
    var binaryColumns = Set<String>()
    var constraintTypesDictionary = ["LE": 1, "EQ": 3, "GE": 2, "<=": 1, "=": 3, "==": 3, ">=": 2]
    var objectiveFunction = [String:AnyObject]()
    var sensitivity = 0.05
    var columns: [IPSolverColumn] = []
    
    
    func addColumn(_ columnName:String, isBinary:Bool)->String{
        var varName = columnName
        if(varName == ""){
            varName = "unNamed\(columns.count)"
        }
        
        let column = IPSolverColumn()
        column.name = varName
        
        
        //columnNames.append(varName)
        if(isBinary){
            binaryColumns.insert(varName)
            column.type = GLP_BV
            column.upperBound = 1.0
            column.lowerBound = 0.0
        }
        
        columns.append(column)
        return varName
    }
    
    func getColumnByName(_ columnName: String) -> IPSolverColumn? {
        for column in columns {
            if(column.name == columnName) {
                return column
            }
        }
        return nil
    }
    
    static func roundPrecision(_ value:Double) -> Double {
        //return value
        return Double(round(1000000 * value)/1000000)
    }
    
    func addConstraint(_ row:Row, constraint:String, constant:Double){
        
        let constraintType = constraintTypesDictionary[constraint]!;
        constraints.append(["row": row, "constraint": constraintType as AnyObject, "constant": FormulaSystem.roundPrecision(constant) as AnyObject])
    }
    
    func setObjective(_ row:Row){
        objectiveFunction = ["row": row, "constraint": 3 as AnyObject, "constant": 0 as AnyObject]
    }
    
    func initStructSmcp() -> glp_smcp {
        let struct_pointer = UnsafeMutablePointer<glp_smcp>.allocate(capacity: 1)
        let struct_memory = struct_pointer.pointee
        struct_pointer.deinitialize()
        return struct_memory
    }
    
    func initStructIocp() -> glp_iocp {
        let struct_pointer = UnsafeMutablePointer<glp_iocp>.allocate(capacity: 1)
        let struct_memory = struct_pointer.pointee
        struct_pointer.deinitialize()
        return struct_memory
    }
    
    
    
    func solve() -> (Int, [String: Double]) {
        var orderedColumns : [IPSolverColumn] = columns.sorted(by: { $0.name > $1.name })
        
        //Make GLPK System
        let lp = glp_create_prob()
        glp_set_prob_name(lp, "example")
        glp_set_obj_dir(lp, GLP_MAX)
        
        //Add columns
        glp_add_cols(lp, Int32(orderedColumns.count))
        for i in 1 ... orderedColumns.count {
            let column = orderedColumns[i-1]
            let varName = column.name
            glp_set_col_name(lp, Int32(i), varName)
            glp_set_col_kind(lp, Int32(i), column.type)
            glp_set_col_bnds(lp, Int32(i), column.valueType, column.lowerBound, column.upperBound)
            
        }
        
        //Add rows
        glp_add_rows(lp, Int32(constraints.count))
        for i in 1 ... constraints.count {
            glp_set_row_name(lp, Int32(i), "row\(i)")
        }
        
        //Build matrix
        var rowIndices = [Int32]()
        rowIndices.append(0)
        var colIndices = [Int32]()
        colIndices.append(0)
        var coefficients = [Double]()
        coefficients.append(0)
        
        //Build rows
        for i in 1 ... constraints.count {
            let constraint = constraints[i-1]
            let row = constraint["row"] as! Row
            let equality = constraint["constraint"] as! Int
            let constant = constraint["constant"] as! Double
            for j in 1 ... orderedColumns.count {
                let column = orderedColumns[j-1]
                let varName = column.name
                if let coef = row.a[varName] {
                    if (coef != 0) {
                        rowIndices.append(Int32(i))
                        colIndices.append(Int32(j))
                        coefficients.append(coef)
                    }
                }
            }
            switch equality {
                
            case constraintTypesDictionary["EQ"]!:
                glp_set_row_bnds(lp, Int32(i), GLP_FX, constant, 0.0)
                
            case constraintTypesDictionary["GE"]!:
                glp_set_row_bnds(lp, Int32(i), GLP_LO, constant, 0.0)
                
            case constraintTypesDictionary["LE"]!:
                glp_set_row_bnds(lp, Int32(i), GLP_UP, 0.0, constant)
                
            default: break
                
            }
        }
        
        //Build objective function
        let objRow = objectiveFunction["row"] as! Row
        
        for i in 1 ... orderedColumns.count {
            let column = orderedColumns[i-1]
            let varName = column.name
            let coef = objRow.a[varName] ?? 0
            glp_set_obj_coef(lp, Int32(i), coef)
        }
        
        
        //Load matrix
        glp_load_matrix(lp, Int32(coefficients.count - 1), &rowIndices, &colIndices, &coefficients)
        
        // Create solver parameters
        var params = initStructSmcp()
        glp_init_smcp(&params)
        params.meth = GLP_DUALP
        params.msg_lev = GLP_MSG_OFF
        params.tm_lim = 100
        params.presolve = GLP_ON
        
        var iocp = initStructIocp()
        glp_init_iocp(&iocp)
        iocp.tm_lim = 200  // If it doesnt solve in 100ms, it probably wont find a solution.
        iocp.mip_gap = 1e-11 //Default is 1e-11
        iocp.mir_cuts = GLP_OFF
        iocp.gmi_cuts = GLP_OFF
        iocp.cov_cuts = GLP_OFF
        iocp.clq_cuts = GLP_OFF
        iocp.presolve = GLP_ON
        iocp.msg_lev = GLP_MSG_OFF
        
        // Print the columns
//        for i in 1 ... columns.count {
//            let colName = String.fromCString(glp_get_col_name(lp, Int32(i)))
//            let colType = glp_get_col_kind(lp, Int32(i))
//            print(colName! + " : " + String(colType))
//        }
        
        // Solve the problem
        let simplexStatus = glp_simplex(lp, &params)
//        print("LP Status:  " + getSolverError(simplexStatus))
        
        if(simplexStatus == 0) {
            let bbStatus = glp_intopt(lp, &iocp)
//            print("BB Status:  " + getSolverError(bbStatus))
            if(bbStatus == 0 || bbStatus == GLP_ENOPFS) {
                let mipStatusCode = glp_mip_status(lp)
//                print("MIP Status: " + getMIPStatusCode(mipStatusCode))
                
                if(mipStatusCode == GLP_OPT) {
                    var solution = [String: Double]()
                    let objValue = glp_mip_obj_val(lp)
                    solution["obj"] = objValue
                    for i in 1 ... orderedColumns.count {
                        let column = orderedColumns[i-1]
                        let varName = column.name
                        let value = glp_mip_col_val(lp, Int32(i))
                        solution[varName] = value
                    }
                    return ( 1, solution)
                }
            }
        }
        return ( 0, ["ERROR":0])
    }
    
    // MARK: -- Console Print Functions --
    
    func getMIPStatusCode(_ statusCode:Int32) -> String {
        switch statusCode {
        case GLP_UNDEF:
            return "MIP solution is undefined"
        case GLP_OPT:
            return "MIP solution is integer optimal"
        case GLP_FEAS:
            return "MIP solution is integer feasible but its optimality (or non-optimality) has not been proven, perhaps due to premature termination of the search"
        case GLP_NOFEAS:
            return "problem has no integer feasible solution (proven by the solver)."
        default:
            return "\(statusCode): Unknown Status Code"
        }
    }
    
    
    func getSolverError(_ statusCode:Int32) -> String{
        switch(statusCode) {
        case 0:
            return "0: The LP problem instance has been successfully solved. This code does not necessarily mean that the solver has found optimal solution. It only means that the solution process was successful."
        case GLP_EBADB:
            return "EBADB: Unable to start the search, because the initial basis specified in the problem object is invalid--the number of basic (auxiliary and structural) variables is not the same as the number of rows in the problem object."
        case GLP_ESING:
            return "Unable to start the search, because the basis matrix correspodning to the initial basis is singular within the working precision."
        case GLP_ECOND:
            return "Unable to start the search, because the basis matrix correspodning to the initial basis is ill-conditioned, i.e. its condition number is too large."
        case GLP_EBOUND:
            return "Unable to start the search, because some double-bounded variables have incorrect bounds."
        case GLP_EFAIL:
            return "The search was prematurely terminated due to the solver failure."
        case GLP_EOBJLL:
            return "The search was prematurely terminated, because the objective function being maximized has reached its lower limit and continues decreasing (dual simplex only)."
        case GLP_EOBJUL:
            return "The search was prematurely terminated, because the objective function being minimized has reached its upper limit and continues increasing (dual simplex only)."
        case GLP_EITLIM:
            return "The search was prematurely terminated, because the simplex iteration limit has been exceeded."
        case GLP_ETMLIM:
            return "The search was prematurely terminated, because the time limit has been exceeded."
        case GLP_ENOPFS:
            return "The LP problem instance has no primal feasible solution (only if the LP presolver is used)."
        case GLP_ENODFS:
            return "The LP problem instance has no dual feasible solution (only if the LP presolver is used)."
        default:
            return "\(statusCode): Unknown Status Code"
        }
    }
    
    func printSystemInFriendlyForm() {
        print(self.getObjectiveString())
        for constraint in constraints {
            print(self.getConstraintString(constraint))
        }
        print("")
    }
    
    func getObjectiveString() -> String {
        var result = ""
        result += "Maximize "
        result += "P"
        result += " = "
        
        let orderedColumns : [IPSolverColumn] = columns.sorted(by: { $0.name > $1.name })
        for column in orderedColumns {
            let varName = column.name
            if (varName != "P") {
                let row = objectiveFunction["row"] as! Row
                if let value = row.a[varName] {
                    if (value >= 0) {
                        if (result.hasSuffix(" = ") == false) {
                            result += " + "
                        }
                    }
                    else {
                        result += " - "
                    }
                    if (abs(value) != 1.0) {
                        result += "\(abs(value))"
                    }
                    result += varName.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "?", with: "T").replacingOccurrences(of: "(", with: "_").replacingOccurrences(of: ")", with: "_").replacingOccurrences(of: "-", with: "_").replacingOccurrences(of: "/", with: "_")
                }
                else {
                    if (result.hasSuffix(" = ") == false) {
                        result += " + "
                    }
                    
                    result += "0" + varName.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "?", with: "T").replacingOccurrences(of: "(", with: "_").replacingOccurrences(of: ")", with: "_").replacingOccurrences(of: "-", with: "_").replacingOccurrences(of: "/", with: "_")
                }
            }
            
        }
        result += " subject to"
        return result
    }
    
    func getConstraintString(_ constraint:[String:AnyObject]) -> String {
        var result = ""
        let row = constraint["row"] as! Row
        let constraintEquality = constraint["constraint"] as! Int
        let orderedColumns : [IPSolverColumn] = columns.sorted(by: { $0.name > $1.name })
        for column in orderedColumns {
            let varName = column.name
            
            if let value = row.a[varName] {
                if (value >= 0) {
                    if (result.characters.count > 0) {
                        result += " + "
                    }
                }
                else {
                    result += " - "
                }
                if (abs(value) != 1.0) {
                    result += "\(abs(value))"
                }
                result += varName.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "?", with: "T").replacingOccurrences(of: "(", with: "_").replacingOccurrences(of: ")", with: "_").replacingOccurrences(of: "-", with: "_").replacingOccurrences(of: "/", with: "_")
            }
        }
        if (constraintEquality == 2) {
            result += " >= "
        }
        else if (constraintEquality == 1) {
            result += " <= "
        }
        else if (constraintEquality == 3) {
            result += " = "
        }
        
        result += "\(constraint["constant"]!)"
        return result
    }
    
    
}

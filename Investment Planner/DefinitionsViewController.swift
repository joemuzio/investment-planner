//
//  DefinitionsViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/9/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class DefinitionsSection {
    
    fileprivate(set) var name = ""
    fileprivate(set) var terms: [(String,String)]!

    init(name: String, terms: [String:String]) {
        self.name = name
      // self.terms = terms.sorted { $0 < $1 }
        self.terms = terms.flatMap({$0}).sorted(by: <)
     //   self.terms = terms.sort()
    }
}

class DefinitionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let menuAnimator = SideMenuTransitionAnimator()
    var sections = [DefinitionsSection]()
    
    var headerView: UIView!
    var infoButton: UIButton!
    var titleLabel: UILabel!
    var tableView: UITableView!
    
    // MARK: - UIViewController Overrides
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.frame = CGRect(x: 0, y: 0, width: 666, height: self.view.frame.height)
        
        self.view.backgroundColor = UIColor.catalinaDarkBlueColor().withAlphaComponent(0.8)
        
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 80))
        headerView.backgroundColor = UIColor.catalinaDarkBlueColor()
        self.view.addSubview(headerView)
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "icon-page-definitions-dark-bg"), for: UIControlState())
        infoButton.addTarget(self, action: #selector(DefinitionsViewController.infoButtonPressed), for: .touchUpInside)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(infoButton)
        headerView.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .left, relatedBy: .equal, toItem: headerView, attribute: .left, multiplier: 1, constant:30))
        headerView.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0))
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.catalinaMediumFontOfSize(24)
        titleLabel.textColor = UIColor.catalinaWhiteColor()
        titleLabel.text = "Page Definitions"
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(titleLabel)
        headerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: infoButton, attribute: .right, multiplier: 1, constant: 10))
        headerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0))
        
        tableView = UITableView()
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.register(DefinitionTableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: 10))
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource Implementation
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let section = sections[section]
        return section.terms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DefinitionTableViewCell
        let section = sections[(indexPath as NSIndexPath).section]
        let term = section.terms[(indexPath as NSIndexPath).row]
        cell.keyLabel.text = term.0.uppercased()
        cell.valueLabel.text = term.1
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let section = sections[section]
        return section.name
    }
    
    // MARK: - Actions
    
    func infoButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
}

class DefinitionTableViewCell: UITableViewCell {
    
    var keyLabel: UILabel!
    var valueLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        keyLabel = UILabel()
        keyLabel.font = UIFont.catalinaHeavyFontOfSize(15)
        keyLabel.textColor = UIColor.catalinaWhiteColor()
        keyLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(keyLabel)
        self.contentView.addConstraint(NSLayoutConstraint(item: keyLabel, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: 30))
        self.contentView.addConstraint(NSLayoutConstraint(item: keyLabel, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 20))
        
        valueLabel = UILabel()
        valueLabel.font = UIFont.catalinaMediumFontOfSize(15)
        valueLabel.textColor = UIColor.catalinaWhiteColor()
        valueLabel.numberOfLines = 0
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(valueLabel)
        self.contentView.addConstraint(NSLayoutConstraint(item: valueLabel, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: 30))
        self.contentView.addConstraint(NSLayoutConstraint(item: valueLabel, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1, constant: -30))
        self.contentView.addConstraint(NSLayoutConstraint(item: valueLabel, attribute: .top, relatedBy: .equal, toItem: keyLabel, attribute: .bottom, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: valueLabel, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -10))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

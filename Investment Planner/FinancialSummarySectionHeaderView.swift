//
//  FinancialSummaryHeaderView.swift
//  Investment Planner
//
//  Created by Douglas Wall on 8/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class FinancialSummarySectionHeaderView: UICollectionReusableView {
    
    static let titleHeight: CGFloat = 30
    var accessoryButton: UIButton? {
        
        willSet {
            if let newButton = newValue {
            self.addSubview(newButton)
            self.addConstraint(NSLayoutConstraint(item: newButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: FinancialSummaryCell.cellPadding))
            self.addConstraint(NSLayoutConstraint(item: newButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
            }
            
        }
    }
    
    var sectionLabel: UILabel!
    var propertyValueLabel1: UILabel!
    var propertyValueLabel2: UILabel!
    var propertyValueLabel3: UILabel!
    var propertyValueLabel4: UILabel!
    var bottomBar: UIView!
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.catalinaWhiteColor()
        
        sectionLabel = UILabel()
        sectionLabel.isHidden = true
        sectionLabel.font = UIFont.catalinaBoldFontOfSize(20)
        sectionLabel.backgroundColor = UIColor.clear
        sectionLabel.textColor = UIColor.catalinaBlackColor()
        sectionLabel.textAlignment = .left
        sectionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(sectionLabel)
        
        propertyValueLabel4 = UILabel()
        propertyValueLabel4.isHidden = true
        propertyValueLabel4.text = "Total"
        propertyValueLabel4.textAlignment = .center
        propertyValueLabel4.font = UIFont.catalinaBookFontOfSize(13)
        propertyValueLabel4.backgroundColor = UIColor.lightGray
        propertyValueLabel4.textColor = UIColor.catalinaBlackColor()
        propertyValueLabel4.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(propertyValueLabel4)
        
        propertyValueLabel3 = UILabel()
        propertyValueLabel3.isHidden = true
        propertyValueLabel3.text = "Volume"
        propertyValueLabel3.textAlignment = .center
        propertyValueLabel3.font = UIFont.catalinaBookFontOfSize(13)
        propertyValueLabel3.backgroundColor = UIColor.catalinaLightGreenColor()
        propertyValueLabel3.textColor = UIColor.catalinaWhiteColor()
        propertyValueLabel3.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(propertyValueLabel3)
        
        propertyValueLabel2 = UILabel()
        propertyValueLabel2.isHidden = true
        propertyValueLabel2.text = "Loyalty"
        propertyValueLabel2.textAlignment = .center
        propertyValueLabel2.font = UIFont.catalinaBookFontOfSize(13)
        propertyValueLabel2.backgroundColor = UIColor.catalinaDarkBlueColor()
        propertyValueLabel2.textColor = UIColor.catalinaWhiteColor()
        propertyValueLabel2.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(propertyValueLabel2)
        
        propertyValueLabel1 = UILabel()
        propertyValueLabel1.isHidden = true
        propertyValueLabel1.text = "Trial"
        propertyValueLabel1.textAlignment = .center
        propertyValueLabel1.font = UIFont.catalinaBookFontOfSize(13)
        propertyValueLabel1.backgroundColor = UIColor.catalinaLightBlueColor()
        propertyValueLabel1.textColor = UIColor.catalinaWhiteColor()
        propertyValueLabel1.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(propertyValueLabel1)
        
        bottomBar = UIView()
        bottomBar.isHidden = true
        bottomBar.backgroundColor = UIColor.catalinaBlackColor()
        bottomBar.frame = CGRect(x: FinancialSummaryCell.cellPadding, y: self.frame.height - 1, width: self.frame.width - (FinancialSummaryCell.cellPadding * 2), height: 1)
        self.addSubview(bottomBar)
        
        setConstraintsForCollapsed()

    }
    
    func setupForIndex(_ index: Int) {
        switch index {
        case 0:
            sectionLabel.isHidden = true
            propertyValueLabel4.isHidden = false
            propertyValueLabel3.isHidden = false
            propertyValueLabel2.isHidden = false
            propertyValueLabel1.isHidden = false
            accessoryButton?.isHidden = false
            bottomBar.isHidden = true

        case 1:
            sectionLabel.isHidden = false
            propertyValueLabel4.isHidden = true
            propertyValueLabel3.isHidden = true
            propertyValueLabel2.isHidden = true
            propertyValueLabel1.isHidden = true
            accessoryButton?.isHidden = true
            sectionLabel.text = "Return"
            bottomBar.isHidden = false
        case 2:
            sectionLabel.isHidden = false
            propertyValueLabel4.isHidden = true
            propertyValueLabel3.isHidden = true
            propertyValueLabel2.isHidden = true
            propertyValueLabel1.isHidden = true
            accessoryButton?.isHidden = true
            sectionLabel.text = "Investment"
            bottomBar.isHidden = false
        case 3:
            sectionLabel.isHidden = false
            propertyValueLabel4.isHidden = true
            propertyValueLabel3.isHidden = true
            propertyValueLabel2.isHidden = true
            propertyValueLabel1.isHidden = true
            accessoryButton?.isHidden = true
            sectionLabel.text = "ROI"
            bottomBar.isHidden = false
        default:
            break
        }
    }
    
    
    
    func setConstraintsForExpanded() {
        self.removeConstraints(self.constraints)

        self.addConstraint(NSLayoutConstraint(item: sectionLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: FinancialSummaryCell.cellPadding))
        self.addConstraint(NSLayoutConstraint(item: sectionLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: sectionLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.40, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -FinancialSummaryCell.cellPadding))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.12, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel3, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.12, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel2, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.12, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel1, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.12, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.layoutIfNeeded()
        })
    }
    
    func setConstraintsForCollapsed() {
        self.removeConstraints(self.constraints)

        self.addConstraint(NSLayoutConstraint(item: sectionLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: FinancialSummaryCell.cellPadding))
        self.addConstraint(NSLayoutConstraint(item: sectionLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: sectionLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.40, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -FinancialSummaryCell.cellPadding))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel3, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel2, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel1, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 0.48, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.layoutIfNeeded()
        })
    }
    
    class func requiredHeight() -> CGFloat {
        
        return self.titleHeight
    }
}

//
//  ControlPanelViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/26/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ControlPanelViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var unmodifiedReportCopy: IPReport?
    var selectedReport: IPReport? {
        didSet {
            if let program = selectedReport?.program {
                
                titleLabel.text = "Report:  \(program.report.displayName)"
                cancelButton.isHidden = false
                doneButton.isHidden = false
                
                //update program variables text fields
                clientNameTextField.text = program.clientName
                defaultInvestmentTextField.text = program.defaultInvestment.currencyString(0)
                profitMarginTextField.text = program.profitMargin.percentString()
                focusBrandTextField.text = program.focusBrand
                roiSegmentedControl.selectedSegmentIndex = program.shortTermROI ? 0 : 1
                distributionCostTransactionalTextField.text = program.printCostTransactional.currencyString(2, 3)
                ccmAnnouncementCostTextField.text = program.ccmAnnouncementCost.currencyString(2, 3)
                distributionCostIDTextField.text = program.printCostId.currencyString(2, 3)
                handlingFeeTextField.text = program.handlingFee.currencyString()
                avgRetailCostTextField.text = program.retailPricePerUnit.currencyString()
                
                //select first coupon
                if let coupons = program.templateScenario?.getOrderedCoupons() {
                    selectedCoupon = coupons.first
                } else {
                    selectedCoupon = nil
                }
                offersCollectionView.reloadData()
                
            }
            
            self.selectivelyEnableDoneButton()
        }
    }
    var selectedCoupon: IPCoupon? {
        didSet {
            if let coupon = selectedCoupon {
                //update offer variables text fields
                offerAmountTextField.text = coupon.promotionValue.currencyString()
                purchaseQuantityTextField.text = coupon.purchaseRequirement.numberString()
                estimatedRedemptionTextField.text = coupon.redemptionRate.percentString(1)
                incrementalAssumptionTextField.text = coupon.offer.incrementalRatio.percentString()
                enabledToggleSwitch.isOn = coupon.offer.enabled
                promotionLevelInputsTabView.selectedTab = coupon.offer.type.rawValue.uppercased()
                if(coupon.offer.type == .Volume){
                    purchaseQuantityTextField.isUserInteractionEnabled = false
                    purchaseQuantityTextField.text = "Good on next shopping order"
                
                }
                else{
                    purchaseQuantityTextField.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    var scrollView: UIScrollView!
    var contentView: UIView!
    
    var titleBarContainer: UIView!
    var doneButton: UIButton!
    var cancelButton: UIButton!
    var titleLabel: UILabel!
    
    var reportVariablesScrollView: UIScrollView!
    var reportVariablesContainer: UIView!
    var pageIndicator: UIPageControl!
    
    var globalInputsContainerView: UIView!
    var globalInputsLabel: UILabel!
    var clientNameTextField: UITextField!
//    var programNameTextField: UITextField!
    var defaultInvestmentTextField: UITextField!
    var profitMarginTextField: UITextField!
    var focusBrandTextField: UITextField!
    var roiSegmentedControl: UISegmentedControl!
    var distributionCostTransactionalTextField: UITextField!
    var ccmAnnouncementCostTextField: UITextField!
    var distributionCostIDTextField: UITextField!
    var handlingFeeTextField: UITextField!
    var avgRetailCostTextField: UITextField!
    
    var promotionLevelInputsContainerView: UIView!
    var promotionLevelInputsLabel: UILabel!
    var promotionLevelInputsTabView: TabView!
    var offersCollectionViewLayout: PromotionsCollectionViewLayout!
    var offersCollectionView: UICollectionView!
    var offerAmountTextField: UITextField!
    var purchaseQuantityTextField: UITextField!
    var estimatedRedemptionTextField: UITextField!
    var incrementalAssumptionTextField: UITextField!
    var enabledToggleSwitch: UISwitch!
    
    var restoreDefaultsButton: UIButton!
    
    var goodOnNextOrderLabel:UITextField!
    var keyboardWillShowObserver: NSObjectProtocol?
    var keyboardWillHideObserver: NSObjectProtocol?
    
    var activeTextField: UITextField?
    var lastLoyaltyCoupon:IPCoupon?
    var lastVolumeCoupon:IPCoupon?
    
    var userMathConstants = [String: AnyObject]()
    
    // MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        // MARK: top level view container
        
        scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.white
        scrollView.frame = self.view.bounds
        //        scrollView.alwaysBounceVertical = true
        scrollView.contentSize = self.view.bounds.size
        self.view.addSubview(scrollView)
        
        contentView = UIView()
        contentView.backgroundColor = UIColor.white
        contentView.frame = self.view.bounds
        scrollView.addSubview(contentView)
        
        // MARK: title view - includes title and done/cancel buttons
        
        titleBarContainer = UIView()
        titleBarContainer.backgroundColor = UIColor.catalinaDarkBlueColor()
        titleBarContainer.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(titleBarContainer)
        contentView.addConstraint(NSLayoutConstraint(item: titleBarContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 72))
        contentView.addConstraint(NSLayoutConstraint(item: titleBarContainer, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: titleBarContainer, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: titleBarContainer, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: 0))
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.catalinaLightFontOfSize(24)
        titleLabel.text = "Report Name"
        titleLabel.textColor = UIColor.catalinaWhiteColor()
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleBarContainer.addSubview(titleLabel)
        titleBarContainer.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: titleBarContainer, attribute: .centerX, multiplier: 1, constant: 0))
        titleBarContainer.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: titleBarContainer, attribute: .centerY, multiplier: 1, constant: 0))
        
        cancelButton = UIButton()
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.setTitleColor(UIColor.catalinaWhiteColor(), for: UIControlState())
        cancelButton.sizeToFit()
        cancelButton.addTarget(self, action: #selector(ControlPanelViewController.cancelButtonPressed), for: .touchUpInside)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        titleBarContainer.addSubview(cancelButton)
        titleBarContainer.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .left, relatedBy: .equal, toItem: titleBarContainer, attribute: .left, multiplier: 1, constant: 20))
        titleBarContainer.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .centerY, relatedBy: .equal, toItem: titleBarContainer, attribute: .centerY, multiplier: 1, constant: 0))
        
        doneButton = UIButton()
        doneButton.setTitle("Done", for: UIControlState())
        doneButton.setTitleColor(UIColor.catalinaWhiteColor(), for: UIControlState())
        doneButton.setTitleColor(UIColor.lightGray, for: .disabled)
        doneButton.sizeToFit()
        doneButton.addTarget(self, action: #selector(ControlPanelViewController.doneButtonPressed), for: .touchUpInside)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        titleBarContainer.addSubview(doneButton)
        titleBarContainer.addConstraint(NSLayoutConstraint(item: doneButton, attribute: .right, relatedBy: .equal, toItem: titleBarContainer, attribute: .right, multiplier: 1, constant: -20))
        titleBarContainer.addConstraint(NSLayoutConstraint(item: doneButton, attribute: .centerY, relatedBy: .equal, toItem: titleBarContainer, attribute: .centerY, multiplier: 1, constant: 0))
        
        // MARK: report variables view - includes all editable report variables
        
        reportVariablesScrollView = UIScrollView()
        reportVariablesScrollView.backgroundColor = UIColor.catalinaWhiteColor()
        reportVariablesScrollView.isPagingEnabled = true
        reportVariablesScrollView.bounces = true
        reportVariablesScrollView.delegate = self
        reportVariablesScrollView.showsHorizontalScrollIndicator = false
        reportVariablesScrollView.alwaysBounceHorizontal = true
        reportVariablesScrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reportVariablesScrollView)
        contentView.addConstraint(NSLayoutConstraint(item: reportVariablesScrollView, attribute: .top, relatedBy: .equal, toItem: titleBarContainer, attribute: .bottom, multiplier: 1, constant: 20))
        contentView.addConstraint(NSLayoutConstraint(item: reportVariablesScrollView, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: reportVariablesScrollView, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: reportVariablesScrollView, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -40))
        
        reportVariablesContainer = UIView()
        reportVariablesContainer.backgroundColor = UIColor.catalinaWhiteColor()
        reportVariablesContainer.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width * 2, height: self.view.bounds.height - 132)
        reportVariablesScrollView.addSubview(reportVariablesContainer)
        reportVariablesScrollView.contentSize = reportVariablesContainer.bounds.size
        
        pageIndicator = UIPageControl()
        pageIndicator.numberOfPages = 2
        pageIndicator.currentPage = 0
        pageIndicator.backgroundColor = UIColor.white
        pageIndicator.pageIndicatorTintColor = UIColor.lightGray
        pageIndicator.currentPageIndicatorTintColor = UIColor.darkGray
        pageIndicator.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(pageIndicator)
        contentView.addConstraint(NSLayoutConstraint(item: pageIndicator, attribute: .top, relatedBy: .equal, toItem: reportVariablesScrollView, attribute: .bottom, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: pageIndicator, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: pageIndicator, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: pageIndicator, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        // MARK: global inputs
        
        globalInputsContainerView = UIView()
        globalInputsContainerView.translatesAutoresizingMaskIntoConstraints = false
        reportVariablesContainer.addSubview(globalInputsContainerView)
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: globalInputsContainerView, attribute: .top, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .top, multiplier: 1, constant: 0))
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: globalInputsContainerView, attribute: .left, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .left, multiplier: 1, constant: 0))
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: globalInputsContainerView, attribute: .width, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .width, multiplier: 0.5, constant: 0))
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: globalInputsContainerView, attribute: .bottom, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .bottom, multiplier: 1, constant: 0))
        
        globalInputsLabel = UILabel()
        globalInputsLabel.text = "Global Inputs"
        globalInputsLabel.font = UIFont.catalinaMediumFontOfSize(28)
        globalInputsLabel.textColor = UIColor.catalinaLightBlueColor()
        globalInputsLabel.sizeToFit()
        globalInputsLabel.translatesAutoresizingMaskIntoConstraints = false
        globalInputsContainerView.addSubview(globalInputsLabel)
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: globalInputsLabel, attribute: .top, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .top, multiplier: 1, constant: 20))
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: globalInputsLabel, attribute: .centerX, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .centerX, multiplier: 1, constant: 0))
        
        clientNameTextField = UITextField()
        clientNameTextField.placeholder = "client name"
        clientNameTextField.clearButtonMode = .whileEditing
        clientNameTextField.borderStyle = .roundedRect
        clientNameTextField.delegate = self
        clientNameTextField.keyboardType = .default
        self.configureLayoutForGlobalInputEditor(clientNameTextField, underOtherView: globalInputsLabel, withLabelTitle: "Client Name", asRequiredField: false, withAdditionalPadding: 20)
        
//        programNameTextField = UITextField()
//        programNameTextField.placeholder = "program name"
//        programNameTextField.clearButtonMode = .WhileEditing
//        programNameTextField.borderStyle = .RoundedRect
//        programNameTextField.delegate = self
//        programNameTextField.keyboardType = .Default
//        self.configureLayoutForGlobalInputEditor(programNameTextField, underOtherView: clientNameTextField, withLabelTitle: "Program Name", asRequiredField: false)
        
        defaultInvestmentTextField = UITextField()
        defaultInvestmentTextField.placeholder = "$1,000,000"
        defaultInvestmentTextField.clearButtonMode = .whileEditing
        defaultInvestmentTextField.borderStyle = .roundedRect
        defaultInvestmentTextField.delegate = self
        defaultInvestmentTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(defaultInvestmentTextField, underOtherView: clientNameTextField, withLabelTitle: "Default Investment", asRequiredField: false)
        
        focusBrandTextField = UITextField()
        focusBrandTextField.placeholder = "focus brand"
        focusBrandTextField.clearButtonMode = .whileEditing
        focusBrandTextField.borderStyle = .roundedRect
        focusBrandTextField.delegate = self
        focusBrandTextField.keyboardType = .default
        self.configureLayoutForGlobalInputEditor(focusBrandTextField, underOtherView: defaultInvestmentTextField, withLabelTitle: "Focus Brand", asRequiredField: false)
        
        profitMarginTextField = UITextField()
        profitMarginTextField.placeholder = "30%"
        profitMarginTextField.clearButtonMode = .whileEditing
        profitMarginTextField.borderStyle = .roundedRect
        profitMarginTextField.delegate = self
        profitMarginTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(profitMarginTextField, underOtherView: focusBrandTextField, withLabelTitle: "Profit Margin", asRequiredField: false)
        
        roiSegmentedControl = UISegmentedControl(items: ["Short Term", "Long Term"])
        roiSegmentedControl.addTarget(self, action: #selector(ControlPanelViewController.roiSegmentedControlValueChanged), for: .valueChanged)
        roiSegmentedControl.selectedSegmentIndex = 0
        roiSegmentedControl.tintColor = UIColor.catalinaDarkBlueColor()
        self.configureLayoutForGlobalInputEditor(roiSegmentedControl, underOtherView: profitMarginTextField, withLabelTitle: "Trial ROI", asRequiredField: false)
        
        distributionCostTransactionalTextField = UITextField()
        distributionCostTransactionalTextField.clearButtonMode = .whileEditing
        distributionCostTransactionalTextField.borderStyle = .roundedRect
        distributionCostTransactionalTextField.delegate = self
        distributionCostTransactionalTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(distributionCostTransactionalTextField, underOtherView: roiSegmentedControl, withLabelTitle: "Distribution Cost (Transactional)", asRequiredField: false)
        
        ccmAnnouncementCostTextField = UITextField()
        ccmAnnouncementCostTextField.clearButtonMode = .whileEditing
        ccmAnnouncementCostTextField.borderStyle = .roundedRect
        ccmAnnouncementCostTextField.delegate = self
        ccmAnnouncementCostTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(ccmAnnouncementCostTextField, underOtherView: distributionCostTransactionalTextField, withLabelTitle: "CCM Announcement Cost", asRequiredField: false)
        
        distributionCostIDTextField = UITextField()
        distributionCostIDTextField.clearButtonMode = .whileEditing
        distributionCostIDTextField.borderStyle = .roundedRect
        distributionCostIDTextField.delegate = self
        distributionCostIDTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(distributionCostIDTextField, underOtherView: ccmAnnouncementCostTextField, withLabelTitle: "Distribution Cost (ID Based)", asRequiredField: false)
        
        handlingFeeTextField = UITextField()
        handlingFeeTextField.clearButtonMode = .whileEditing
        handlingFeeTextField.borderStyle = .roundedRect
        handlingFeeTextField.delegate = self
        handlingFeeTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(handlingFeeTextField, underOtherView: distributionCostIDTextField, withLabelTitle: "Handling Fee", asRequiredField: false)
        
        avgRetailCostTextField = UITextField()
        avgRetailCostTextField.clearButtonMode = .whileEditing
        avgRetailCostTextField.borderStyle = .roundedRect
        avgRetailCostTextField.delegate = self
        avgRetailCostTextField.keyboardType = .decimalPad
        self.configureLayoutForGlobalInputEditor(avgRetailCostTextField, underOtherView: handlingFeeTextField, withLabelTitle: "Average Retail Cost", asRequiredField: false)
        
        restoreDefaultsButton = UIButton(type: UIButtonType.roundedRect)
        restoreDefaultsButton.setTitle("Restore Defaults", for: UIControlState())
        restoreDefaultsButton.addTarget(self, action: #selector(restoreDefaultsButtonPressed), for: .touchUpInside)
        restoreDefaultsButton.sizeToFit()
        restoreDefaultsButton.translatesAutoresizingMaskIntoConstraints = false
        
        globalInputsContainerView.addSubview(restoreDefaultsButton)
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: restoreDefaultsButton, attribute: .top, relatedBy: .equal, toItem: avgRetailCostTextField, attribute: .bottom, multiplier: 1, constant: 20))
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: restoreDefaultsButton, attribute: .centerX, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .centerX, multiplier: 1, constant: 0))
        
        // MARK: promotion level inputs
        
        promotionLevelInputsContainerView = UIView()
        promotionLevelInputsContainerView.translatesAutoresizingMaskIntoConstraints = false
        reportVariablesContainer.addSubview(promotionLevelInputsContainerView)
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: promotionLevelInputsContainerView, attribute: .top, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .top, multiplier: 1, constant: 0))
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: promotionLevelInputsContainerView, attribute: .width, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .width, multiplier: 0.5, constant: 0))
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: promotionLevelInputsContainerView, attribute: .right, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .right, multiplier: 1, constant: 0))
        reportVariablesContainer.addConstraint(NSLayoutConstraint(item: promotionLevelInputsContainerView, attribute: .bottom, relatedBy: .equal, toItem: reportVariablesContainer, attribute: .bottom, multiplier: 1, constant: 0))
        
        promotionLevelInputsLabel = UILabel()
        promotionLevelInputsLabel.text = "Promotion Level Inputs"
        promotionLevelInputsLabel.font = UIFont.catalinaMediumFontOfSize(28)
        promotionLevelInputsLabel.textColor = UIColor.catalinaLightBlueColor()
        promotionLevelInputsLabel.sizeToFit()
        promotionLevelInputsLabel.translatesAutoresizingMaskIntoConstraints = false
        promotionLevelInputsContainerView.addSubview(promotionLevelInputsLabel)
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: promotionLevelInputsLabel, attribute: .top, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .top, multiplier: 1, constant: 20))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: promotionLevelInputsLabel, attribute: .centerX, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .centerX, multiplier: 1, constant: 0))
        
        promotionLevelInputsTabView = TabView()
        promotionLevelInputsTabView.tintColor = UIColor.catalinaDarkBlueColor()
        promotionLevelInputsTabView.tabs = ["TRIAL", "LOYALTY", "VOLUME"]
        promotionLevelInputsTabView.addTarget(self, action: #selector(ControlPanelViewController.selectedTabChanged), for: .valueChanged)
        promotionLevelInputsTabView.translatesAutoresizingMaskIntoConstraints = false
        promotionLevelInputsContainerView.addSubview(promotionLevelInputsTabView)
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: promotionLevelInputsTabView, attribute: .top, relatedBy: .equal, toItem: promotionLevelInputsLabel, attribute: .bottom, multiplier: 1, constant: 10))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: promotionLevelInputsTabView, attribute: .centerX, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .centerX, multiplier: 1, constant: 0))
        
        offersCollectionViewLayout = PromotionsCollectionViewLayout()
        offersCollectionViewLayout.itemSize = CGSize(width: 400, height: 230)
        offersCollectionViewLayout.scrollDirection = .horizontal
        offersCollectionViewLayout.minimumLineSpacing = 0
        offersCollectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 300, bottom: 0, right: 300)
        
        offersCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 1024, height: 230), collectionViewLayout: offersCollectionViewLayout)
        offersCollectionView.register(OfferCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        offersCollectionView.dataSource = self
        offersCollectionView.delegate = self
        offersCollectionView.backgroundColor = UIColor.catalinaWhiteColor()
        offersCollectionView.bounces = true
        offersCollectionView.showsHorizontalScrollIndicator = false
        offersCollectionView.alwaysBounceHorizontal = true
        offersCollectionView.translatesAutoresizingMaskIntoConstraints = false
        promotionLevelInputsContainerView.addSubview(offersCollectionView)
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: offersCollectionView, attribute: .top, relatedBy: .equal, toItem: promotionLevelInputsTabView, attribute: .bottom, multiplier: 1, constant: 10))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: offersCollectionView, attribute: .centerX, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .centerX, multiplier: 1, constant: 0))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: offersCollectionView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 1024))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: offersCollectionView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 230))
        
        offerAmountTextField = UITextField()
        offerAmountTextField.clearButtonMode = .whileEditing
        offerAmountTextField.borderStyle = .roundedRect
        offerAmountTextField.delegate = self
        offerAmountTextField.keyboardType = .decimalPad
        self.configureLayoutForPromotionLevelInputEditor(offerAmountTextField, underOtherView: offersCollectionView, withLabelTitle: "Offer Amount", withAdditionalPadding: 20)
        
        purchaseQuantityTextField = UITextField()
        purchaseQuantityTextField.clearButtonMode = .whileEditing
        purchaseQuantityTextField.borderStyle = .roundedRect
        purchaseQuantityTextField.delegate = self
        purchaseQuantityTextField.keyboardType = .decimalPad
        self.configureLayoutForPromotionLevelInputEditor(purchaseQuantityTextField, underOtherView: offerAmountTextField, withLabelTitle: "Promotion Quantity")
        
        estimatedRedemptionTextField = UITextField()
        estimatedRedemptionTextField.clearButtonMode = .whileEditing
        estimatedRedemptionTextField.borderStyle = .roundedRect
        estimatedRedemptionTextField.delegate = self
        estimatedRedemptionTextField.keyboardType = .decimalPad
        self.configureLayoutForPromotionLevelInputEditor(estimatedRedemptionTextField, underOtherView: purchaseQuantityTextField, withLabelTitle: "Estimated Redemption")
        
        incrementalAssumptionTextField = UITextField()
        incrementalAssumptionTextField.clearButtonMode = .whileEditing
        incrementalAssumptionTextField.borderStyle = .roundedRect
        incrementalAssumptionTextField.delegate = self
        incrementalAssumptionTextField.keyboardType = .decimalPad
        self.configureLayoutForPromotionLevelInputEditor(incrementalAssumptionTextField, underOtherView: estimatedRedemptionTextField, withLabelTitle: "Incremental Assumption")
        
        enabledToggleSwitch = UISwitch()
        enabledToggleSwitch.addTarget(self, action: #selector(self.enabledSwitchChanged(_:)), for: UIControlEvents.valueChanged)
        self.configureLayoutForPromotionLevelInputEditor(enabledToggleSwitch, underOtherView: incrementalAssumptionTextField, withLabelTitle: "Enabled")
        
        
        if let report = SettingsController.sharedSettingsController.selectedReport {
            unmodifiedReportCopy = report.duplicate()
            selectedReport = report.duplicate()
      //      print("report id = \(report.id)")
        } else {
            unmodifiedReportCopy = nil
            selectedReport = nil
        }
    }
    
    // MARK: - Auto-Layout Helper Methods
    
    func configureLayoutForGlobalInputEditor(_ editor: UIView, underOtherView view: UIView, withLabelTitle title: String, asRequiredField required: Bool) {
        
        self.configureLayoutForGlobalInputEditor(editor, underOtherView: view, withLabelTitle: title, asRequiredField: required, withAdditionalPadding: 0)
    }
    
    func configureLayoutForGlobalInputEditor(_ editor: UIView, underOtherView view: UIView, withLabelTitle title: String, asRequiredField required: Bool, withAdditionalPadding padding: CGFloat) {
        
        editor.translatesAutoresizingMaskIntoConstraints = false
        globalInputsContainerView.addSubview(editor)
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: editor, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 20 + padding))
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: editor, attribute: .left, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .centerX, multiplier: 1, constant: 10))
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: editor, attribute: .width, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .width, multiplier: 0.25, constant: 0))
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes:[NSForegroundColorAttributeName:UIColor.catalinaDarkBlueColor(), NSFontAttributeName:UIFont.catalinaMediumFontOfSize(17)])
        if required {
            attributedTitle.append(NSAttributedString(string: "*", attributes: [NSForegroundColorAttributeName:UIColor.catalinaDarkRedColor(), NSFontAttributeName:UIFont.catalinaMediumFontOfSize(17)]))
        }
        let titleLabel = UILabel()
        titleLabel.attributedText = attributedTitle
        titleLabel.textAlignment = .right
        titleLabel.numberOfLines = 0
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        globalInputsContainerView.addSubview(titleLabel)
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: editor, attribute: .centerY, multiplier: 1, constant: 0))
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .centerX, multiplier: 1, constant: -10))
        globalInputsContainerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .width, relatedBy: .equal, toItem: globalInputsContainerView, attribute: .width, multiplier: 0.25, constant: 0))
    }
    
    func configureLayoutForPromotionLevelInputEditor(_ editor: UIView, underOtherView view: UIView, withLabelTitle title: String) {
        
        self.configureLayoutForPromotionLevelInputEditor(editor, underOtherView: view, withLabelTitle: title, withAdditionalPadding: 0)
    }
    
    func configureLayoutForPromotionLevelInputEditor(_ editor: UIView, underOtherView view: UIView, withLabelTitle title: String, withAdditionalPadding padding: CGFloat) {
        
        editor.translatesAutoresizingMaskIntoConstraints = false
        promotionLevelInputsContainerView.addSubview(editor)
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: editor, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 20 + padding))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: editor, attribute: .left, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .centerX, multiplier: 1, constant: 10))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: editor, attribute: .width, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .width, multiplier: 0.25, constant: 0))
        
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textColor = UIColor.catalinaDarkBlueColor()
        titleLabel.textAlignment = .right
        titleLabel.numberOfLines = 0
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        promotionLevelInputsContainerView.addSubview(titleLabel)
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: editor, attribute: .centerY, multiplier: 1, constant: 0))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .centerX, multiplier: 1, constant: -10))
        promotionLevelInputsContainerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .width, relatedBy: .equal, toItem: promotionLevelInputsContainerView, attribute: .width, multiplier: 0.25, constant: 0))
    }
    
    // MARK: - Other UIViewController Overrides
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        // check for new reports
        
        keyboardWillShowObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            if let userInfo = (notification as NSNotification).userInfo {
                
                let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue
                
                //calculate new visible area with shown keyboard
                var visibleAreaRect = self.view.bounds
                visibleAreaRect.size.height = (keyboardEndFrame?.origin.y)!
                //adjust frame of scroll view to reflect the visible area
                self.scrollView.frame = visibleAreaRect
                
                if let activeField = self.activeTextField { //keyboard is being shown for one of out data entry fields
                    
                    //get frame of active text field in the content view's coordinates
                    var activeFieldFrameInContentView = self.contentView.convert(activeField.frame, from: activeField.superview!)
                    //adjust ative text field frame to account for scrollview offset
                    activeFieldFrameInContentView.origin.y -= self.scrollView.contentOffset.y
                    //check to see if the active text field is currently in our visible area
                    if !visibleAreaRect.contains(activeFieldFrameInContentView) {
                        //if not, scroll the scrollview to make the active text field visible
                        var adjustment: CGFloat = 0
                        let center = activeFieldFrameInContentView.midY
                        let padding:CGFloat = 100
                        if center < visibleAreaRect.origin.y {
                            //above the visible area, need to scroll back up to make it visible
                            adjustment = (visibleAreaRect.origin.y - center) + padding
                        } else {
                            //below the visible area, need to scroll down to make it visible
                            adjustment = (visibleAreaRect.origin.y + visibleAreaRect.size.height) - (center + padding)
                        }
                        let offset = CGPoint(x: 0, y: self.scrollView.contentOffset.y - adjustment)
                        self.scrollView.setContentOffset(offset, animated: true)
                    }
                }
            }
        }
        
        keyboardWillHideObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            self.scrollView.frame = self.view.bounds
            self.scrollView.scrollRectToVisible(CGRect.zero, animated: true)
        }
        
        self.layoutOfferCollectionViewCells()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var highestLoyaltyQty = 0
        var highestVolumeQty = 0
        if let coupons = selectedReport?.program?.templateScenario?.getOrderedCoupons(){
            for coupon in coupons{
                if(coupon.offer.type == .Loyalty){
                    if(coupon.triggerQuantity > highestLoyaltyQty){
                        highestLoyaltyQty = coupon.triggerQuantity
                        lastLoyaltyCoupon = coupon
                    }
                }
                else if(coupon.offer.type == .Volume){
                    if(coupon.triggerQuantity > highestVolumeQty){
                        highestVolumeQty = coupon.triggerQuantity
                        lastVolumeCoupon = coupon
                    }
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        if let observer = keyboardWillShowObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        if let observer = keyboardWillHideObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    // MARK: - Actions
    
    func cancelButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func doneButtonPressed() {
        
        self.view.endEditing(true)
        
        
        if !self.programDataIsValid() {
            return
        }
        
        if self.programHasScenarioInvalidatingChanges() {
            
            let alert = UIAlertController(title: "Save Report", message: "The changes you have made to this report will invalidate all existing scenarios. If you choose to save this report, it will be saved as a new report with only the default scenario. The old report with all of your other existing scenarios will also be kept for later reference.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            let saveAction = UIAlertAction(title: "Save", style: .default) { (action) in
                
                if let program = self.selectedReport?.program {
                    
                    let nameTextField = alert.textFields![0] as UITextField
                    let results = DataController.sharedDataController.searchReports(nameTextField.text!)
                    if(results.count > 0){
                        var nameExists = false
                        for result in results{
                            if(result.name == nameTextField.text!){
                                nameExists = true
                            }
                        }
                        if(nameExists){
                            let newAlert = UIAlertController(title: "Unique Name", message: "Report name already exists. Please rename your report.", preferredStyle: .alert)
                            newAlert.addAction(UIAlertAction(title: "Ok", style: .default) {(action) in
                                self.doneButtonPressed()
                                })
                            self.present(newAlert, animated: true, completion: nil)
                        }
                        else{
                            program.report.displayName = nameTextField.text!
                            program.report.id = UUID().uuidString
                            program.scenarios.removeAll()
                            let newScenario = program.createNewScenario()
                            newScenario.name = "Default"
                            program.scenarios.append(newScenario)
                            self.saveAndClose()
                        }
                    }
                    else{
                        program.report.displayName = nameTextField.text!
                        program.report.id = UUID().uuidString
                        program.scenarios.removeAll()
                        let newScenario = program.createNewScenario()
                        newScenario.name = "Default"
                        program.scenarios.append(newScenario)
                        self.saveAndClose()
                    }
                }
            }
            alert.addAction(saveAction)
            
            alert.addTextField(configurationHandler: { (textField) -> Void in
                textField.placeholder = "Report Name"
                textField.text = self.selectedReport?.displayName
                NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main) { (notification) in
                    saveAction.isEnabled = textField.text != ""
                }
            })
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            self.saveAndClose()
        }
    }
    
    func selectedTabChanged() {
        
        if let selectedTab = promotionLevelInputsTabView.selectedTab {
            if let coupons = selectedReport?.program?.templateScenario?.getOrderedCoupons() {
                for coupon in coupons {
                    if coupon.offer.type.rawValue.uppercased() == selectedTab.uppercased() {
                        selectedCoupon = coupon
                        let index = coupons.index(of: coupon)!
                        let indexPath = IndexPath(item: index, section: 0)
                        offersCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                        break
                    }
                }
            }
        }
    }
    
    func roiSegmentedControlValueChanged() {
        
        if let program = selectedReport?.program {
            program.shortTermROI = roiSegmentedControl.selectedSegmentIndex == 0
            if(program.shortTermROI){
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting ROI Short/Long Term Changed", action: "New Value: Short Term", label: "", value: 0)
            }
            else{
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting ROI Short/Long Term Changed", action: "New Value: Long Term", label: "", value: 0)
            }
            self.selectivelyEnableDoneButton()
        }
    }
    
    // MARK: - UIScrollViewDelegate Implementation
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.layoutOfferCollectionViewCells()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == offersCollectionView {
            self.selectVisuallyEmphasizedOffer()
        }
        if scrollView == reportVariablesScrollView {
            self.updatePageIndicator()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == offersCollectionView {
            if !decelerate {
                self.selectVisuallyEmphasizedOffer()
            }
        }
        if scrollView == reportVariablesScrollView {
            if !decelerate {
                self.updatePageIndicator()
            }
        }
    }
    
    // MARK: - UITextFieldDelegate Implementation
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.activeTextField = nil
        
        if let program = selectedReport?.program {
            
            if textField == clientNameTextField {
                program.clientName = textField.text != nil ? textField.text! : ""
                clientNameTextField.text = program.clientName
                if program.clientName != textField.text{
                    AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Client Name Changed", action: "New  Value: \(program.clientName)", label: "", value: 0)
                }
            }
            else if textField == defaultInvestmentTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                program.defaultInvestment = Double(currencyString: stringWithNoCharsOrSymbols)
                defaultInvestmentTextField.text = program.defaultInvestment.currencyString(0)
                if unmodifiedReportCopy!.program!.defaultInvestment.currencyString(0) != textField.text{
                    AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Default Investment Changed", action: "New Value: \(defaultInvestmentTextField.text!)", label: "", value: 0)
                }
                
            }
            else if textField == focusBrandTextField {
                program.focusBrand = textField.text != nil ? textField.text! : ""
                focusBrandTextField.text = program.focusBrand
                if let unmodifiedFocusBrand = unmodifiedReportCopy?.program?.focusBrand{
                    if unmodifiedFocusBrand != program.focusBrand{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Focus Brand Changed", action: "New Value: \(program.focusBrand)", label: "", value: 0)
                    }
                }
            }
            else if textField == profitMarginTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                program.profitMargin = Double(percentString: stringWithNoCharsOrSymbols)
                profitMarginTextField.text = program.profitMargin.percentString()
                if let profitMargin = unmodifiedReportCopy?.program?.profitMargin{
                    if profitMargin != program.profitMargin{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Profit Margin Changed", action: "New Value: \(program.profitMargin.percentString())", label: "", value: 0)
                    }
                }
            }
            else if textField == distributionCostTransactionalTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                if(stringWithNoCharsOrSymbols == "" || stringWithNoCharsOrSymbols == "."){
                    program.printCostTransactional = 0.0
                    distributionCostTransactionalTextField.text = program.printCostTransactional.currencyString()
                }
                else{
                    let amount = stringWithNoCharsOrSymbols?.threeDecmialString()
                    program.printCostTransactional = amount!
                    distributionCostTransactionalTextField.text = program.printCostTransactional.numberWithThreeDecimalsString()
                }
                if let unmodifiedTransactionalPrintCost = unmodifiedReportCopy?.program?.printCostTransactional{
                    if program.printCostTransactional != unmodifiedTransactionalPrintCost{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Distribution Cost (Transactional) Changed", action: "New Value: \(program.printCostTransactional.numberWithThreeDecimalsString())", label: "", value: 0)
                    }
                }
            }
            else if textField == ccmAnnouncementCostTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                if(stringWithNoCharsOrSymbols == "" || stringWithNoCharsOrSymbols == "."){
                    program.ccmAnnouncementCost = 0.0
                    ccmAnnouncementCostTextField.text = program.ccmAnnouncementCost.currencyString()
                }
                else{
                    let amount = stringWithNoCharsOrSymbols?.threeDecmialString()
                    program.ccmAnnouncementCost = amount!
                    ccmAnnouncementCostTextField.text = program.ccmAnnouncementCost.numberWithThreeDecimalsString()
                }
                if let unmodifiedCCMCost = unmodifiedReportCopy?.program?.ccmAnnouncementCost{
                    if unmodifiedCCMCost != program.ccmAnnouncementCost{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting CCM Announcement Cost Changed", action: "New Value: \(program.ccmAnnouncementCost.numberWithThreeDecimalsString())", label: "", value: 0)
                    }
                }
            }
            else if textField == distributionCostIDTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                if(stringWithNoCharsOrSymbols == "" || stringWithNoCharsOrSymbols == "."){
                    program.printCostId = 0.0
                    distributionCostIDTextField.text = program.printCostId.currencyString()
                }
                else{
                    let amount = stringWithNoCharsOrSymbols?.threeDecmialString()
                    program.printCostId = amount!
                    distributionCostIDTextField.text = program.printCostId.numberWithThreeDecimalsString()
                }
                if let unmodifiedIdPrintCost = unmodifiedReportCopy?.program?.printCostId{
                    if program.printCostId != unmodifiedIdPrintCost{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Distribution Cost (ID Based) Changed", action: "New Value: \(program.printCostId.numberWithThreeDecimalsString())", label: "", value: 0)
                    }
                }
            }
            else if textField == handlingFeeTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                program.handlingFee = Double(currencyString: stringWithNoCharsOrSymbols)
                handlingFeeTextField.text = program.handlingFee.currencyString()
                if let unmodifiedHandlingFee = unmodifiedReportCopy?.program?.handlingFee{
                    if program.handlingFee != unmodifiedHandlingFee{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Handling Fee Changed", action: "New Value: \(program.handlingFee.currencyString())", label: "", value: 0)
                    }
                }
            }
            else if textField == avgRetailCostTextField {
                let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                program.retailPricePerUnit = Double(currencyString: stringWithNoCharsOrSymbols)
                avgRetailCostTextField.text = program.retailPricePerUnit.currencyString()
                if let unmodifiedAvgRetailCost = unmodifiedReportCopy?.program?.retailPricePerUnit{
                    if unmodifiedAvgRetailCost != program.retailPricePerUnit{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Average Retail Cost Changed", action: "New Value: \(program.retailPricePerUnit.currencyString())", label: "", value: 0)
                    }
                }
            }
            
            if let coupon = selectedCoupon {
                if textField == offerAmountTextField {
                    let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                    let offerAmount = Double(currencyString: stringWithNoCharsOrSymbols)
                    if offerAmount != coupon.promotionValue{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Offer Amount Changed", action: "New Value: \(offerAmount.currencyString())", label: "", value: 0)
                    }
                    coupon.promotionValue = offerAmount
                    offerAmountTextField.text = coupon.promotionValue.currencyString()
                } else if textField == purchaseQuantityTextField {
                    let stringWithNoCharsOrSymbols = textField.text?.stringWithNoCharsOrSymbols()
                    let purchaseQty = Double(numberString: stringWithNoCharsOrSymbols)
                    if coupon.purchaseRequirement != purchaseQty{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Promotion Qty Changed", action: "New Value: \(purchaseQty.numberString())", label: "", value: 0)
                    }
                    coupon.purchaseRequirement = purchaseQty
                    purchaseQuantityTextField.text = coupon.purchaseRequirement.numberString()
                    
                } else if textField == estimatedRedemptionTextField {
                    var redemptionAmountString = textField.text!.stringWithNoCharsOrSymbols()
                    
                    let redemptionAmount:Double = Double(redemptionAmountString)!
                    let couponRedemptionAmount = Double(round(coupon.redemptionRate * 1000) / 10)
                    if redemptionAmount != couponRedemptionAmount{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Estimated Redemption Changed", action: "New Value: \(redemptionAmount)%", label: "", value: 0)
                    }
                    if(redemptionAmount < 0){
                            redemptionAmountString = "0%"
                    }
                    else if(redemptionAmount > 100){
                        redemptionAmountString = "100.0%"
                    }
                    
                    coupon.redemptionRate = Double(percentString: redemptionAmountString)
                    estimatedRedemptionTextField.text = coupon.redemptionRate.percentString(1)
                    
                } else if textField == incrementalAssumptionTextField {
                    var incrementalAmountString = textField.text!.stringWithNoCharsOrSymbols()
                    let incrementalAmount = Double(incrementalAmountString)!
                    let couponAssumptionAmount = Double(round(coupon.offer.incrementalRatio * 100))
                    print(couponAssumptionAmount)
                    print(incrementalAmount)
                    if incrementalAmount != couponAssumptionAmount{
                        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Incremental Assumption Changed", action: "New Value: \(incrementalAmount)%", label: "", value: 0)
                    }
                    if(incrementalAmount < 0){
                        incrementalAmountString = "0%"
                    }
                    else if(incrementalAmount > 100){
                        if(coupon.offer.type == .Volume){
                            incrementalAmountString = "100.0%"
                        }
                        else{
                            if(incrementalAmount > 200){
                                incrementalAmountString = "200.0"
                            }
                        }
                    }
                    coupon.offer.incrementalRatio = Double(percentString: incrementalAmountString)
                    incrementalAssumptionTextField.text = coupon.offer.incrementalRatio.percentString()
                }
            }
        }
        
        self.selectivelyEnableDoneButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == clientNameTextField {
            defaultInvestmentTextField.becomeFirstResponder()
        } else if textField == defaultInvestmentTextField {
            focusBrandTextField.becomeFirstResponder()
        } else if textField == focusBrandTextField {
            profitMarginTextField.becomeFirstResponder()
        } else if textField == profitMarginTextField {
            distributionCostTransactionalTextField.becomeFirstResponder()
        } else if textField == distributionCostTransactionalTextField {
            ccmAnnouncementCostTextField.becomeFirstResponder()
        } else if textField == ccmAnnouncementCostTextField {
            distributionCostIDTextField.becomeFirstResponder()
        } else if textField == distributionCostIDTextField {
            handlingFeeTextField.becomeFirstResponder()
        } else if textField == handlingFeeTextField {
            avgRetailCostTextField.becomeFirstResponder()
        } else if textField == avgRetailCostTextField {
            clientNameTextField.becomeFirstResponder()
        } else if textField == offerAmountTextField {
            purchaseQuantityTextField.becomeFirstResponder()
        } else if textField == purchaseQuantityTextField {
            estimatedRedemptionTextField.becomeFirstResponder()
        } else if textField == estimatedRedemptionTextField {
            incrementalAssumptionTextField.becomeFirstResponder()
        } else if textField == incrementalAssumptionTextField {
            offerAmountTextField.becomeFirstResponder()
        }
        return true
    }
    
    func enabledSwitchChanged(_ toggleSwitch: UISwitch) {
        if let coupon = selectedCoupon {
            if toggleSwitch.isOn {
                coupon.offer.enabled = true
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Coupon Enabled Changed", action: "New Value: Enabled", label: "", value: 0)
            }
            else {
                coupon.offer.enabled = false
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Setting Coupon Enabled Changed", action: "New Value: Disabled", label: "", value: 0)
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource Implementation
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let scenario = selectedReport?.program?.templateScenario {
            return scenario.getOrderedCoupons().count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! OfferCollectionViewCell
        if let coupon = selectedReport?.program?.templateScenario?.getOrderedCoupons()[(indexPath as NSIndexPath).item] {
            cell.nameLabel.text = coupon.offer.name.rawValue
            if(coupon == lastVolumeCoupon || coupon == lastLoyaltyCoupon){
                cell.promoLabel.text = "Buy \(coupon.triggerQuantity)+"
            }
            else{
                cell.promoLabel.text = "Buy \(coupon.triggerQuantity)"
                }
        } else {
            cell.nameLabel.text = "--"
            cell.promoLabel.text = "--"
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate Implementation
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let coupon = selectedReport?.program?.templateScenario?.getOrderedCoupons()[(indexPath as NSIndexPath).item] {
            selectedCoupon = coupon
        }
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    // MARK: - Helper Methods
    
    func saveAndClose() {
        
        if let report = selectedReport {
            saveSelectedMetrics()
            _ = report.save()
            SettingsController.sharedSettingsController.selectedReport = report
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func updatePageIndicator() {
        
        if reportVariablesScrollView.contentOffset.x > reportVariablesContainer.bounds.width/4 {
            pageIndicator.currentPage = 1
        } else {
            pageIndicator.currentPage = 0
        }
    }
    
    func layoutOfferCollectionViewCells() {
        
        for cell in offersCollectionView.visibleCells {
            let visibleRect = CGRect(x: offersCollectionView.contentOffset.x, y: offersCollectionView.contentOffset.y, width: offersCollectionView.bounds.width, height: offersCollectionView.bounds.height)
            let position = visibleRect.midX - cell.center.x
            let couponCell = cell as! OfferCollectionViewCell
            couponCell.transformContentViewForRelativeXPosition(position)
        }
    }
    
    func selectVisuallyEmphasizedOffer() {
        
        if let coupons = selectedReport?.program?.templateScenario?.getOrderedCoupons() {
            if coupons.count > 0 {
                let center = CGPoint(x: offersCollectionView.contentOffset.x + offersCollectionView.bounds.width/2, y: offersCollectionView.bounds.height/2)
                if let indexPath = offersCollectionView.indexPathForItem(at: center) {
                    selectedCoupon = coupons[(indexPath as NSIndexPath).item]
                }
            }
        }
    }
    
    func selectivelyEnableDoneButton() {
        
        doneButton.isEnabled = self.programDataIsValid()
        if self.programHasScenarioInvalidatingChanges() {
            doneButton.setTitle("Apply", for: UIControlState())
        } else {
            doneButton.setTitle("Done", for: UIControlState())
        }
    }
    
    func programDataIsValid() -> Bool {
        
        if let program = selectedReport?.program {
            if program.clientName.characters.count == 0 {
                return false
            }
            if program.defaultInvestment < 0 {
                return false
            }
            if program.profitMargin < 0 {
                return false
            }
            if program.focusBrand.characters.count == 0 {
                return false
            }
            if program.printCostTransactional < 0 {
                return false
            }
            if program.ccmAnnouncementCost < 0 {
                return false
            }
            if program.printCostId < 0 {
                return false
            }
            if program.handlingFee < 0 {
                return false
            }
            if program.retailPricePerUnit <= 0 {
                return false
            }
            if let coupons = program.templateScenario?.getOrderedCoupons() {
                for coupon in coupons {
                    if coupon.promotionValue < 0 {
                        return false
                    }
                    if coupon.purchaseRequirement <= 0 {
                        return false
                    }
                    if coupon.redemptionRate < 0 {
                        return false
                    }
                    if coupon.offer.incrementalRatio < 0 {
                        return false
                    }
                }
            }
            return true
        } else {
            return false
        }
    }
    
    func programHasScenarioInvalidatingChanges() -> Bool {
        var isInvalid: Bool = false
        if let program = selectedReport?.program {
            
            if program.report.displayType == .Standard {
                isInvalid = false
            }
            if let unmodifiedProgram = unmodifiedReportCopy?.program {
                if program.profitMargin != unmodifiedProgram.profitMargin {
                    userMathConstants["profitMargin"] = program.profitMargin as AnyObject?
                    isInvalid = true
                }
                if program.shortTermROI != unmodifiedProgram.shortTermROI {
                    userMathConstants["shortTermROI"] = program.shortTermROI as AnyObject?
                    isInvalid = true
                }
                if program.printCostTransactional != unmodifiedProgram.printCostTransactional {
                    userMathConstants["printCostTransactional"] = program.printCostTransactional as AnyObject?
                    isInvalid = true
                }
                if program.ccmAnnouncementCost != unmodifiedProgram.ccmAnnouncementCost {
                    userMathConstants["ccmAnnouncementCost"] = program.ccmAnnouncementCost as AnyObject?
                    isInvalid = true
                }
                if program.printCostId != unmodifiedProgram.printCostId {
                    userMathConstants["printCostId"] = program.printCostId as AnyObject?
                    isInvalid = true
                }
                if program.handlingFee != unmodifiedProgram.handlingFee {
                    userMathConstants["handlingFee"] = program.handlingFee as AnyObject?
                    isInvalid = true
                }
                if program.defaultInvestment != unmodifiedProgram.defaultInvestment {
                    userMathConstants["defaultInvestment"] = program.defaultInvestment as AnyObject?
                    isInvalid = true
                }
                if program.retailPricePerUnit != unmodifiedProgram.retailPricePerUnit {
                    isInvalid = true
                }
                if let coupons = program.templateScenario?.getOrderedCoupons() {
                    if let unmodifiedCoupons = unmodifiedProgram.templateScenario?.getOrderedCoupons() {
                        for coupon in coupons {
                            let index = coupons.index(of: coupon)!
                            let unmodifiedCoupon = unmodifiedCoupons[index]
                            if coupon.promotionValue != unmodifiedCoupon.promotionValue {
                                isInvalid = true
                            }
                            if coupon.purchaseRequirement != unmodifiedCoupon.purchaseRequirement {
                                isInvalid = true
                            }
                            if coupon.redemptionRate != unmodifiedCoupon.redemptionRate {
                                isInvalid = true
                            }
                            if coupon.offer.incrementalRatio != unmodifiedCoupon.offer.incrementalRatio {
                                isInvalid = true
                            }
                            if coupon.offer.enabled != unmodifiedCoupon.offer.enabled {
                                isInvalid = true
                            }
                        }
                    }
                }
            }
        }
        return isInvalid
    }
    fileprivate func loadSelectedMetrics() {
        
        if let userConstantsDictionary = SettingsController.sharedSettingsController.userMathConstants {
            if(userConstantsDictionary.count > 0){
                for(key, value) in userConstantsDictionary{
                    userMathConstants[key] = value as? String as AnyObject?
                }
            }
        }
    }
    
    fileprivate func saveSelectedMetrics() {
        SettingsController.sharedSettingsController.userMathConstants = userMathConstants
    }
    
    func restoreDefaultsButtonPressed(){
        if let program = selectedReport?.program {
            program.setValuesForKeys(program.report.data.constants)
            defaultInvestmentTextField.text = program.defaultInvestment.currencyString(0)
            if self.programHasScenarioInvalidatingChanges() {
                doneButton.setTitle("Apply", for: UIControlState())
                clientNameTextField.text = program.clientName
                profitMarginTextField.text = program.profitMargin.percentString()
                focusBrandTextField.text = program.focusBrand
                roiSegmentedControl.selectedSegmentIndex = program.shortTermROI ? 0 : 1
                distributionCostTransactionalTextField.text = program.printCostTransactional.currencyString()
                ccmAnnouncementCostTextField.text = program.ccmAnnouncementCost.currencyString()
                distributionCostIDTextField.text = program.printCostId.currencyString()
                handlingFeeTextField.text = program.handlingFee.currencyString()
                avgRetailCostTextField.text = program.retailPricePerUnit.currencyString()
            }
        }
    }
}

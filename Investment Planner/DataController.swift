//
//  DataController.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/29/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


private let sharedDataControllerInstance = DataController()
private let reportDataPersistenceSerialQueue = DispatchQueue(label: "io.pythagoras.reportDataPersistenceSerialQueue", attributes: [])

enum ReportDataPersistenceAction: String {
    case Create = "POST"
    case Update = "PATCH"
    case Delete = "DELETE"
}

let ReportDataPersistenceNotification = "io.pythagoras.reportDataPersistenceNotification"

let DataErrorDomain = "pythagoras.error.DataAccessErrorDomain"
let CoreDataInitializationErrorCode = 101

class ReportDataPersistenceRecord {
    
    fileprivate(set) var action: ReportDataPersistenceAction
    fileprivate(set) var report: IPReport
    fileprivate(set) var success: Bool
    
    init(action: ReportDataPersistenceAction, report: IPReport, success: Bool) {
        self.action = action
        self.report = report
        self.success = success
    }
}

class DataController: NSObject {
    
    var userName: String?
    
    class var sharedDataController: DataController {
        
        return sharedDataControllerInstance
    }
    
    lazy var documentsDirectory: URL = {
        
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }()
    
    // MARK: - Public Methods
    
    func searchReports(_ searchText: String?) -> [SearchToken] {
        
        return self.searchReports(searchText, types: IPReportType.searchableTypes)
    }
    
    func searchReports(_ searchText: String?, types: [IPReportType]) -> [SearchToken] {
        
        var searchResults = [SearchToken]()
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        let searchString =  String(searchText!.characters.filter {okayChars.contains($0) })
        let arrayOfWords = searchString.components(separatedBy: " ")
        managedObjectContext.performAndWait {
        
            for type in types {
                print(type.rawValue)
               // let fetchRequest = NSFetchRequest(entityName: "SearchToken")
                let fetchRequest: NSFetchRequest<SearchToken>
                if #available(iOS 10.0, OSX 10.12, *) {
                    fetchRequest = SearchToken.fetchRequest() as! NSFetchRequest<SearchToken>
                } else {
                    fetchRequest = NSFetchRequest(entityName: "SearchToken")
                }
                var predicate = NSPredicate(format: "(type == %@)", type.rawValue)
                fetchRequest.fetchLimit = 500
                if(arrayOfWords.count > 0){
                    if(searchText! != ""){
                        var predString = "(type == \"\(type.rawValue)\") "
                        for word in arrayOfWords{
                            let stringToAdd = "AND (name CONTAINS[c] \"\(word)\" OR brand CONTAINS[c] \"\(word)\" OR category CONTAINS[c] \"\(word)\")"
                            predString += stringToAdd
                        }
                        predicate = NSPredicate(format: predString)
                    }
                }
                fetchRequest.predicate = predicate
                
                do {
                    let results =
                        try self.managedObjectContext.fetch(fetchRequest)
                    if(results.count > 0){
                        for token in results{
                            searchResults.append(token)
                        }
                    }
                    
                    
                } catch let error as NSError {
                    print("Could not fetch \(error), \(error.userInfo)")
                }
            }
            
        }
        
        return searchResults
    }
    
    func openReportForToken(_ token: SearchToken) -> IPReport? {
        
        var reportType = IPReportType.Custom
        if(token.type == IPReportType.Standard.rawValue){
            reportType = .Standard
        }
        else if(token.type! == IPReportType.Shared.rawValue){
            reportType = .Shared
        }
        return self.openReportWithID(token.id!, andType: reportType)
    }
    
    func openReportWithID(_ id: String, andType reportType: IPReportType) -> IPReport? {
        
        //look for a file matching the id and report type
        let fileManager = FileManager.default
        var fileURL = self.getReportsDirectory(reportType)
        fileURL = fileURL.appendingPathComponent("\(id).json")
        if fileManager.fileExists(atPath: fileURL.path) {
            if let reportJSON = self.readJSONObjectFromFile(fileURL) as? [String : AnyObject] {
                let report = IPReport()
                report.updateWithJSONObject(reportJSON)
                return report
            }
        }
        
        //not found, return nil
        return nil
    }
    
    func saveReport(_ report: IPReport) -> Bool {
        
        let records = self.saveReports([report])
        if let record = records.first {
            return record.success
        } else {
            return false
        }
    }
    
    func saveReports(_ reports: [IPReport]) -> [ReportDataPersistenceRecord] {
        
        var records = [ReportDataPersistenceRecord]()
        
        managedObjectContext.performAndWait {
            
            for report in reports {
                //save the actual report file and create a record of the save action and result to use in the notification
                let fileURL = self.getReportsDirectory(report.displayType).appendingPathComponent("\(report.id).json")
                let action: ReportDataPersistenceAction = FileManager.default.fileExists(atPath: fileURL.path) ? .Update : .Create
                let success = self.writeJSONObjectToFile(report.JSONObject() as AnyObject, fileURL: fileURL)
                let record = ReportDataPersistenceRecord(action: action, report: report, success: success)
                
                records.append(record)
                
                //now, if the save was successful, make sure the index is updated
                if success {
                    print("isLocked = \(report.program?.scenarios[0].isLocked) for: \(report.program?.scenarios[0].name)")
                    if let token = self.getSearchTokenForId(report.id){
                        token.name = report.displayName
                        token.type = report.displayType.rawValue
                        token.lastModified = report.lastModifiedDate
                        token.user = report.owner
                 //       token.category = report.categoryDescription
                        if let program = report.program {
                            token.brand = program.focusBrand
                        } else {
                            token.brand = report.brand
                        }
                    }
                    else { //otherwise, create a new token for the report and add it to the index
                        let entity =  NSEntityDescription.entity(forEntityName: "SearchToken", in:self.managedObjectContext)
                        let token = SearchToken(entity: entity!, insertInto: self.managedObjectContext)
                        token.id = report.id
                        token.name = report.displayName
                        token.type = report.displayType.rawValue
                        token.lastModified = report.lastModifiedDate
                        token.user = report.owner
                  //      token.category = report.categoryDescription
                        if let program = report.program {
                            token.brand = program.focusBrand
                        } else {
                            token.brand = report.brand
                        }
                    }
                }
            }
            
            //now that all of the reports have been saved, we need to save the indexes
            if self.managedObjectContext.hasChanges {
                do {
                    try self.managedObjectContext.save()
                } catch {
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    abort()
                }
            }
            

        }
        
        //lastly, we need to notify observers
        NotificationCenter.default.post(name: Notification.Name(rawValue: ReportDataPersistenceNotification), object: self, userInfo: ["records":records])
        
        return records
    }
    
    func deleteReport(_ report: IPReport) -> Bool {
        
        var success = false
        
        managedObjectContext.performAndWait {
            
            //delete the report file
            let fileManager = FileManager.default
            var fileURL = self.getReportsDirectory(report.displayType)
            fileURL = fileURL.appendingPathComponent("\(report.id).json")
            
            do {
                //update the search index
                try fileManager.removeItem(at: fileURL)
                if let token = self.getSearchTokenForId(report.id){
                    self.managedObjectContext.delete(token)
                }
                if self.managedObjectContext.hasChanges {
                    do {
                        try self.managedObjectContext.save()
                        success = true
                    } catch {
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                        abort()
                    }
                }
                
            } catch {}
            
            //notify observers
            let record = ReportDataPersistenceRecord(action: .Delete, report: report, success: success)
            NotificationCenter.default.post(name: Notification.Name(rawValue: ReportDataPersistenceNotification), object: self, userInfo: ["records":[record]])
            
        }
        
        return success
    }
    
    func getLastModifiedDateOfMostRecentReportInTypes(_ types: [IPReportType]) -> Date {
        
        var date = Date.distantPast
        
        managedObjectContext.performAndWait {
            
            for type in types {
            //    let fetchRequest = NSFetchRequest(entityName: "SearchToken")
                let fetchRequest: NSFetchRequest<SearchToken>
                if #available(iOS 10.0, OSX 10.12, *) {
                    fetchRequest = SearchToken.fetchRequest() as! NSFetchRequest<SearchToken>
                } else {
                    fetchRequest = NSFetchRequest(entityName: "SearchToken")
                }
                let predicate = NSPredicate(format: "type == %@", type.rawValue)
                fetchRequest.predicate = predicate
                fetchRequest.fetchLimit = 1
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "lastModified", ascending: false)
                fetchRequest.sortDescriptors = [descriptor]
                
                do {
                    let results =
                        try self.managedObjectContext.fetch(fetchRequest)
                    if(results.count > 0){
                        let entity = results[0] 
                        date = entity.lastModified!
                    }
                    
                } catch let error as NSError {
                    print("Could not fetch \(error), \(error.userInfo)")
                }
            }
            
        }
        return date
    }
    
    func replaceCategoryBrandSearchIndexWithNewIndexFromMetadata(_ metadataJSON: [String: AnyObject]) {
        
        managedObjectContext.performAndWait {
       //     let request: NSFetchRequest<SearchToken> = SearchToken.fetchRequest() as! NSFetchRequest<SearchToken>
            
            
            let request: NSFetchRequest<SearchToken>
            if #available(iOS 10.0, OSX 10.12, *) {
                request = SearchToken.fetchRequest() as! NSFetchRequest<SearchToken>
            } else {
                request = NSFetchRequest(entityName: "SearchToken")
            }
            //  let request = fetchRequest(entityName: "SearchToken")
            request.predicate = NSPredicate(format: "type == %@", IPReportType.Standard.rawValue)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
            
            
            
            do {
                try self.persistentStoreCoordinator.execute(deleteRequest, with: self.managedObjectContext)
            } catch let error as NSError {
                print(error)
            }
            
            let content = metadataJSON["content"] as! [[String: AnyObject]]
            for metadataItem in content {
                let entity =  NSEntityDescription.entity(forEntityName: "SearchToken", in:self.managedObjectContext)
                let token = SearchToken(entity: entity!, insertInto: self.managedObjectContext)
                token.id = metadataItem["id"] as? String
                token.category = metadataItem["categoryDescription"] as? String
                token.brand = metadataItem["brand"] as? String
                token.name = metadataItem["reportName"] as? String
                if let timeIntervalMilliseconds = metadataItem["lastModifiedDate"] as? Double {
                    token.lastModified = Date(timeIntervalSince1970: (timeIntervalMilliseconds/1000))
                } else {
                    token.lastModified = Date.distantPast
                }
                token.user = ""
                token.type = IPReportType.Standard.rawValue
            }
            
            if self.managedObjectContext.hasChanges {
                do {
                    try self.managedObjectContext.save()
                } catch {
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    abort()
                }
            }
            
        }
    }
    
    // MARK: - Internal Helper Methods
    
    fileprivate func getReportsDirectory(_ type: IPReportType) -> URL {
        
        var fileURL = self.documentsDirectory
        if IPReportType.userSpecificTypes.contains(type) {
            if let userName = self.userName {
                fileURL = fileURL.appendingPathComponent(userName)
            } else {
                let userName = WebservicesController.sharedWebservicesController.userName!
                fileURL = fileURL.appendingPathComponent(userName)
            }
        }
        fileURL = fileURL.appendingPathComponent("\(type.rawValue) Reports")
        
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: fileURL.path) {
            do {
                try fileManager.createDirectory(at: fileURL, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Unable to create reports Directory for report type \(type). Error Details: \(error.localizedDescription)")
            }
        }
        return fileURL
    }
    
    fileprivate func readJSONObjectFromFile(_ fileURL: URL) -> AnyObject? {
        
        if let data = try? Data(contentsOf: fileURL) {
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                return jsonObject as AnyObject?
            } catch let error as NSError {
                print("Error parsing JSON object: \(error.localizedDescription)")
            }
        } else {
            print("Error reading JSON data from file at URL: \(fileURL.path)")
        }
        return nil
    }
    
    fileprivate func writeJSONObjectToFile(_ object: AnyObject, fileURL: URL) -> Bool {
        
        do {
            let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
            if (try? data.write(to: fileURL, options: [])) != nil {
                return true
            } else {
                print("Error writing JSON data to file at URL: \(fileURL.path)")
            }
        } catch let error as NSError {
            print("Error parsing JSON object: \(error.localizedDescription)")
        }
        return false
    }
    
    fileprivate func getSearchTokenForId(_ id: String) -> SearchToken?{
        
        var token: SearchToken? = nil
        
        managedObjectContext.performAndWait {
        
        //    let fetchRequest = NSFetchRequest(entityName: "SearchToken")
            let fetchRequest: NSFetchRequest<SearchToken>
            if #available(iOS 10.0, OSX 10.12, *) {
                fetchRequest = SearchToken.fetchRequest() as! NSFetchRequest<SearchToken>
            } else {
                fetchRequest = NSFetchRequest(entityName: "SearchToken")
            }
            let predicate = NSPredicate(format: "id == %@", id)
            fetchRequest.predicate = predicate
            
            do {
                let results = try self.managedObjectContext.fetch(fetchRequest)
                let searchTokens = results 
                token = searchTokens.first
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
            
        }
        
        return token
    }
    
    // MARK: - Core Data stack
    
    fileprivate lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Investment_Planner", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    fileprivate lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.documentsDirectory.appendingPathComponent("ReportIndex.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = "There was an error creating or loading the application's saved data." as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: DataErrorDomain, code: CoreDataInitializationErrorCode, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    fileprivate lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        managedObjectContext.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.mergeByPropertyObjectTrumpMergePolicyType)
        return managedObjectContext
    }()
}

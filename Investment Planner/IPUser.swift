//
//  IPUser.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/19/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPUser: IPModelObject {
    
    var userName: String = ""
    var name: String = ""
    var email: String = ""
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "userName",
            "name",
            "email"])
        return keys
    }
}

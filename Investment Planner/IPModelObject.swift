//
//  IPModelObject.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/18/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPModelObject: NSObject {
    
    fileprivate(set) var shouldUseJSONPropertyMappingForKVOOperations = false
     
    static var standardUTCDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formatter
    }()
    
    func JSONPropertyKeys() -> [String] {
        
        return [String]()
    }
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            return
        }
        super.setValue(value, forUndefinedKey: key)
    }
    
    func updateWithJSONObject(_ JSONObject: [String: AnyObject]) {
        
        self.shouldUseJSONPropertyMappingForKVOOperations = true
        self.setValuesForKeys(JSONObject)
        self.shouldUseJSONPropertyMappingForKVOOperations = false
    }
    
    func JSONObject() -> [String: AnyObject] {
        
        self.shouldUseJSONPropertyMappingForKVOOperations = true
        let JSON = self.dictionaryWithValues(forKeys: self.JSONPropertyKeys())
        self.shouldUseJSONPropertyMappingForKVOOperations = false
        return JSON as [String : AnyObject]
    }
}

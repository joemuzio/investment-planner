//
//  WelcomeViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/22/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

protocol WelcomeViewControllerDelegate {
    
    func welcomeViewControllerDidPrepareApplication(_ viewController: WelcomeViewController)
}

class WelcomeViewController: UIViewController {
    
    var reportListViewController = ReportListViewController()
    
    var backgroundImageView: UIImageView!
    var selectReportButton: UIButton!
    var logoutButton: UIButton!
    var infoButton: UIButton!
    var infoLabel: UILabel!
    
    var delegate: WelcomeViewControllerDelegate?

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "Default-Portrait")
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(backgroundImageView)
        self.view.addConstraint(NSLayoutConstraint(item: backgroundImageView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: backgroundImageView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: backgroundImageView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: backgroundImageView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        
        selectReportButton = UIButton()
        selectReportButton.setImage(UIImage(named: "button-select-report"), for: UIControlState())
        selectReportButton.addTarget(self, action: #selector(WelcomeViewController.selectReportButtonPressed), for: .touchUpInside)
        selectReportButton.translatesAutoresizingMaskIntoConstraints = false
        selectReportButton.alpha = 0
        self.view.addSubview(selectReportButton)
        self.view.addConstraint(NSLayoutConstraint(item: selectReportButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: selectReportButton, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.44, constant: 0))
        
        logoutButton = UIButton()
        logoutButton.setTitle("LOG OUT", for: UIControlState())
        logoutButton.titleLabel?.font = UIFont.catalinaBoldFontOfSize(14)
        logoutButton.sizeToFit()
        logoutButton.alpha = 0
        logoutButton.addTarget(self, action: #selector(WelcomeViewController.logoutButtonPressed), for: .touchUpInside)
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(logoutButton)
        self.view.addConstraint(NSLayoutConstraint(item: logoutButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: logoutButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -10))
        
        infoButton = UIButton(type: .infoDark)
        infoButton.tintColor = UIColor.catalinaWhiteColor()
        infoButton.addTarget(self, action: #selector(WelcomeViewController.infoButtonPressed), for: .touchUpInside)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(infoButton)
        self.view.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 20))
        self.view.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -20))
        
        infoLabel = UILabel()
        infoLabel.textColor = UIColor.catalinaWhiteColor()
        infoLabel.font = UIFont.catalinaMediumFontOfSize(18)
        infoLabel.text = ""
        infoLabel.sizeToFit()
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(infoLabel)
        self.view.addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -20))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        selectReportButton.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if WebservicesController.sharedWebservicesController.needsLogout {
            logoutButtonPressed()
        } else {
            if applicationIsReady() {
                if let delegate = self.delegate {
                    delegate.welcomeViewControllerDidPrepareApplication(self)
                }
            } else {
                self.prepareApplication()
            }
        }
    }
    
    func selectReportButtonPressed() {
        
        self.present(reportListViewController, animated: true, completion: nil)
    }
    
    func logoutButtonPressed() {
        
        WebservicesController.sharedWebservicesController.logout()
        SettingsController.sharedSettingsController.selectedReport = nil
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
            self.logoutButton.alpha = 0
            self.selectReportButton.alpha = 0
        })
        self.prepareApplication()
    }
    
    func infoButtonPressed() {
        
        let bundle = Bundle.main
        let plist = bundle.infoDictionary!
        let version = plist["CFBundleShortVersionString"]!
        let serviceInfo = WebservicesController.sharedWebservicesController.getServiceInfo()
        let appInfo = "\nApp Version: \(version)\n\n\(serviceInfo)"
        let alert = UIAlertController(title: "Catalina Investment Planner", message: appInfo, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func applicationIsReady() -> Bool {
        
        //make sure user is logged in
        if WebservicesController.sharedWebservicesController.userName == nil {
            return false
        }
        // if username is known send to analytics
        if let userName = WebservicesController.sharedWebservicesController.userName{
            AnalyticsController.sharedAnalyticsController.sendUserIdToAnalytics(userName)
        }
        //make sure user has a selected report
        if SettingsController.sharedSettingsController.selectedReport == nil {
            return false
        }
        
        return true
    }
    
    fileprivate func prepareApplication() {
        
        //we've never shown the onboarding screen, need to do that now
        if !SettingsController.sharedSettingsController.onboardingScreenShown {
            
            let onboarding = OnboardingViewController()
            onboarding.modalTransitionStyle = .crossDissolve
            self.present(onboarding, animated: true, completion: nil)
            SettingsController.sharedSettingsController.onboardingScreenShown = true
            
        } else {

            //first make sure we're logged in
            self.verifyUserIsLoggedIn({ () -> Void in
                
                //when we're sure the user is logged in we can check for new reports and start downloading them
                if !WebservicesController.sharedWebservicesController.isCheckingForNewReports {
                    WebservicesController.sharedWebservicesController.checkForNewReports({ (error) in
                        if let error = error {
                            let alert = UIAlertController(title: "Reports Service Error", message: "An error occurred while attempting to check for new Report data from the Reports Service.\n\nError Details:\n\(error.localizedDescription)", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                                if WebservicesController.sharedWebservicesController.needsLogout {
                                    self.logoutButtonPressed()
                                }
                                })
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
                
                //make sure we have a selected report by allowing the user to use the select report button
                UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                    self.selectReportButton.alpha = 1
                })
                
            })
            
        }
    }
    
    fileprivate func verifyUserIsLoggedIn(_ completion: @escaping (() -> Void)) {
        
        if WebservicesController.sharedWebservicesController.userName == nil {
            //need to show login view
            let login = LoginViewController()
            login.successBlock = { login in
                self.dismiss(animated: true, completion: { () -> Void in
                    completion()
                    UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                        self.logoutButton.alpha = 1
                    })
                })
            }
            login.modalPresentationStyle = .formSheet
            self.present(login, animated: true, completion: nil)
        } else {
            completion() //user is logged in, we're good
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                self.logoutButton.alpha = 1
            })
        }
    }
}

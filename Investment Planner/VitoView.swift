//
//  VitoView.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 7/25/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class VitoView: UIView {
    var topView:UIView!
    var topLabel:UILabel!
    var bottomLabel:UILabel!
    var kpiButton:UIButton!
    var efficiencyButton:UIButton!

    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        topView = UIView()
        topView.backgroundColor = UIColor.red
        topView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(topView)
        
        topLabel = UILabel()
        topLabel.text = "djfhlwsjfhs hjsdfh jkshf kjshd fdlsjh"
        topLabel.sizeToFit()
        topLabel.translatesAutoresizingMaskIntoConstraints = true
        topView.addSubview(topLabel)
        
        topView.addConstraint(NSLayoutConstraint(item: topLabel, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .top, multiplier: 1, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: topLabel, attribute: .left, relatedBy: .equal, toItem: topView, attribute: .left, multiplier: 1, constant: 0))
        
        kpiButton = UIButton(type: .custom)
        kpiButton.setTitle("Title", for: UIControlState())
        kpiButton.sizeToFit()
        kpiButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(kpiButton)
        
        topView.addConstraint(NSLayoutConstraint(item: kpiButton, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .top, multiplier: 1, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: kpiButton, attribute: .right, relatedBy: .equal, toItem: topLabel, attribute: .left, multiplier: 1, constant: 0))
        
    }
    
    func setupVitoStatement(_ kpiAmount:String, kpiType:String, efficiencyAmount:String, efficiencyType:String){
        kpiButton.setTitle(kpiType, for: UIControlState())
        efficiencyButton.setTitle(efficiencyType, for: UIControlState())
            }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

//
//  MainViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/17/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, WelcomeViewControllerDelegate {
    
    var insights: InsightsViewController!
    var potential: BrandPotentialViewController!
    var library: PortfolioViewController!
    var libraryNav: UINavigationController!
    var welcome: WelcomeViewController!
    
    var homeTabButton: UIButton!
    var insightsTabButton: UIButton!
    var potentialTabButton: UIButton!
    var libraryTabButton: UIButton!
    var settingsTabButton: UIButton!
    
    var containerView: UIView!
    var tabBarView: UIView!
    
    var selectedReportChangedObserver: NSObjectProtocol?
    
    var activeViewController: UIViewController? {
        willSet {
            if let viewController = activeViewController {
                viewController.willMove(toParentViewController: nil)
                viewController.view.removeFromSuperview()
                viewController.removeFromParentViewController()
            }
        }
        didSet {
            if let viewController = activeViewController {
                self.addChildViewController(viewController)
                viewController.view.frame = containerView.bounds
                containerView.addSubview(viewController.view)
                insightsTabButton.isSelected = false
                potentialTabButton.isSelected = false
                libraryTabButton.isSelected = false
                if activeViewController == insights {
                    insightsTabButton.isSelected = true
                } else if activeViewController == potential {
                    potentialTabButton.isSelected = true
                } else if activeViewController == libraryNav {
                    libraryTabButton.isSelected = true
                }
            }
        }
    }
    
    fileprivate let tabBarHeight:CGFloat = 50
    fileprivate let tabButtonWidth:CGFloat = 102

    // MARK: - UIViewController Overrides
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // MARK: Tab Bar
        
        tabBarView = UIView()
        tabBarView.frame = CGRect(x: 0, y: self.view.bounds.height - tabBarHeight, width: self.view.bounds.width, height: tabBarHeight)
        tabBarView.backgroundColor = UIColor.catalinaDarkBlueColor()
        tabBarView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tabBarView)
        self.view.addConstraint(NSLayoutConstraint(item: tabBarView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: tabBarView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: tabBarView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -tabBarHeight))
        self.view.addConstraint(NSLayoutConstraint(item: tabBarView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        
        homeTabButton = UIButton()
        homeTabButton.setImage(UIImage(named: "tab-bar-home-off"), for: UIControlState())
        homeTabButton.setImage(UIImage(named: "tab-bar-home-on"), for: .highlighted)
        homeTabButton.setImage(UIImage(named: "tab-bar-home-on"), for: .selected)
        homeTabButton.addTarget(self, action: #selector(MainViewController.tabButtonPressed(_:)), for: .touchUpInside)
        homeTabButton.sizeToFit()
        homeTabButton.translatesAutoresizingMaskIntoConstraints = false
        tabBarView.addSubview(homeTabButton)
        tabBarView.addConstraint(NSLayoutConstraint(item: homeTabButton, attribute: .centerX, relatedBy: .equal, toItem: tabBarView, attribute: .centerX, multiplier: 1, constant: -tabButtonWidth * 2))
        tabBarView.addConstraint(NSLayoutConstraint(item: homeTabButton, attribute: .centerY, relatedBy: .equal, toItem: tabBarView, attribute: .centerY, multiplier: 1, constant: 0))
        
        insightsTabButton = UIButton()
        insightsTabButton.setImage(UIImage(named: "tab-bar-insights-off"), for: UIControlState())
        insightsTabButton.setImage(UIImage(named: "tab-bar-insights-on"), for: .highlighted)
        insightsTabButton.setImage(UIImage(named: "tab-bar-insights-on"), for: .selected)
        insightsTabButton.addTarget(self, action: #selector(MainViewController.tabButtonPressed(_:)), for: .touchUpInside)
        insightsTabButton.sizeToFit()
        insightsTabButton.translatesAutoresizingMaskIntoConstraints = false
        tabBarView.addSubview(insightsTabButton)
        tabBarView.addConstraint(NSLayoutConstraint(item: insightsTabButton, attribute: .centerX, relatedBy: .equal, toItem: tabBarView, attribute: .centerX, multiplier: 1, constant: -tabButtonWidth))
        tabBarView.addConstraint(NSLayoutConstraint(item: insightsTabButton, attribute: .centerY, relatedBy: .equal, toItem: tabBarView, attribute: .centerY, multiplier: 1, constant: 0))
        
        potentialTabButton = UIButton()
        potentialTabButton.setImage(UIImage(named: "tab-bar-fullpotential-off"), for: UIControlState())
        potentialTabButton.setImage(UIImage(named: "tab-bar-fullpotential-on"), for: .highlighted)
        potentialTabButton.setImage(UIImage(named: "tab-bar-fullpotential-on"), for: .selected)
        potentialTabButton.sizeToFit()
        potentialTabButton.addTarget(self, action: #selector(MainViewController.tabButtonPressed(_:)), for: .touchUpInside)
        potentialTabButton.translatesAutoresizingMaskIntoConstraints = false
        tabBarView.addSubview(potentialTabButton)
        tabBarView.addConstraint(NSLayoutConstraint(item: potentialTabButton, attribute: .centerX, relatedBy: .equal, toItem: tabBarView, attribute: .centerX, multiplier: 1, constant: 0))
        tabBarView.addConstraint(NSLayoutConstraint(item: potentialTabButton, attribute: .centerY, relatedBy: .equal, toItem: tabBarView, attribute: .centerY, multiplier: 1, constant: 0))
        
        libraryTabButton = UIButton()
        libraryTabButton.setImage(UIImage(named: "tab-bar-growthplan-off"), for: UIControlState())
        libraryTabButton.setImage(UIImage(named: "tab-bar-growthplan-on"), for: .highlighted)
        libraryTabButton.setImage(UIImage(named: "tab-bar-growthplan-on"), for: .selected)
        libraryTabButton.sizeToFit()
        libraryTabButton.addTarget(self, action: #selector(MainViewController.tabButtonPressed(_:)), for: .touchUpInside)
        libraryTabButton.translatesAutoresizingMaskIntoConstraints = false
        tabBarView.addSubview(libraryTabButton)
        tabBarView.addConstraint(NSLayoutConstraint(item: libraryTabButton, attribute: .centerX, relatedBy: .equal, toItem: tabBarView, attribute: .centerX, multiplier: 1, constant: tabButtonWidth))
        tabBarView.addConstraint(NSLayoutConstraint(item: libraryTabButton, attribute: .centerY, relatedBy: .equal, toItem: tabBarView, attribute: .centerY, multiplier: 1, constant: 0))
        
        settingsTabButton = UIButton()
        settingsTabButton.setImage(UIImage(named: "tab-bar-settings-off"), for: UIControlState())
        settingsTabButton.setImage(UIImage(named: "tab-bar-settings-on"), for: .highlighted)
        settingsTabButton.setImage(UIImage(named: "tab-bar-settings-on"), for: .selected)
        settingsTabButton.addTarget(self, action: #selector(MainViewController.tabButtonPressed(_:)), for: .touchUpInside)
        settingsTabButton.sizeToFit()
        settingsTabButton.translatesAutoresizingMaskIntoConstraints = false
        tabBarView.addSubview(settingsTabButton)
        tabBarView.addConstraint(NSLayoutConstraint(item: settingsTabButton, attribute: .centerX, relatedBy: .equal, toItem: tabBarView, attribute: .centerX, multiplier: 1, constant: tabButtonWidth * 2))
        tabBarView.addConstraint(NSLayoutConstraint(item: settingsTabButton, attribute: .centerY, relatedBy: .equal, toItem: tabBarView, attribute: .centerY, multiplier: 1, constant: 0))
        
        // MARK: Container View
        
        containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - tabBarHeight)
        containerView.backgroundColor = UIColor.darkGray
        containerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(containerView)
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: containerView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -tabBarHeight))
        
        insights = InsightsViewController()
        potential = BrandPotentialViewController()
        library = PortfolioViewController()
        libraryNav = UINavigationController(rootViewController: library)
        libraryNav.setNavigationBarHidden(true, animated: false)
        welcome = WelcomeViewController()
        welcome.modalTransitionStyle = .crossDissolve
        welcome.delegate = self
        AnalyticsController.sharedAnalyticsController.setupDefaults()
        if welcome.applicationIsReady() {
            self.activeViewController = insights
        } else {
            self.showWelcomeView(false)
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        selectedReportChangedObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: SettingsController.selectedReportChangedNotification), object: nil, queue: OperationQueue.main, using: { (notification) -> Void in
            self.libraryNav.popToRootViewController(animated: false)
        })
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        if let observer = selectedReportChangedObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    //MARK: - Actions
    
    func tabButtonPressed(_ sender: UIButton) {
        
        if sender == insightsTabButton && activeViewController != insights {
            AnalyticsController.sharedAnalyticsController.sendScreenDataToAnalytics("Insights")
            self.activeViewController = insights
        } else if sender == potentialTabButton && activeViewController != potential {
            AnalyticsController.sharedAnalyticsController.sendScreenDataToAnalytics("Brand Potential")
            self.activeViewController = potential
        } else if sender == libraryTabButton && activeViewController != libraryNav {
            AnalyticsController.sharedAnalyticsController.sendScreenDataToAnalytics("Library")
            library.currentFolder = "/"
            self.activeViewController = libraryNav
//            if libraryNav.topViewController == library {
//                if let program = SettingsController.sharedSettingsController.selectedReport?.program {
//                    if program.scenarios.count == 1 {
//                        let deepDive = ScenarioBuilderDeepDiveViewController()
//                        deepDive.displayedScenario = program.scenarios.first!
//                        libraryNav.pushViewController(deepDive, animated: false)
//                    }
//                }
//            }
//            self.activeViewController = libraryNav
//        } else if sender == libraryTabButton && activeViewController == libraryNav {
//            if libraryNav.topViewController != library {
//                libraryNav.popToRootViewControllerAnimated(true)
//            }
        } else if sender == homeTabButton {
            AnalyticsController.sharedAnalyticsController.sendScreenDataToAnalytics("Home")
            SettingsController.sharedSettingsController.selectedReport = nil
            self.showWelcomeView(true)
        } else if sender == settingsTabButton {
            AnalyticsController.sharedAnalyticsController.sendScreenDataToAnalytics("Settings")
            let controlPanel = ControlPanelViewController()
            self.present(controlPanel, animated: true, completion: nil)
        }
    }
    
    //MARK: - WelcomeViewControllerDelegate Implementation
    
    func welcomeViewControllerDidPrepareApplication(_ viewController: WelcomeViewController) {
        
        self.activeViewController = insights
        self.hideWelcomeView(true)
    }
    
    //MARK: Helper Methods
    
    fileprivate func showWelcomeView(_ animated: Bool) {
        
        welcome.view.alpha = 0
        self.addChildViewController(welcome)
        welcome.view.frame = self.view.bounds
        self.view.addSubview(welcome.view)
        if animated {
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                self.welcome.view.alpha = 1
            })
        } else {
            welcome.view.alpha = 1
        }
    }
    
    fileprivate func hideWelcomeView(_ animated: Bool) {
        
        if animated {
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                self.welcome.view.alpha = 0
                }, completion: { (finished) -> Void in
                    self.welcome.willMove(toParentViewController: nil)
                    self.welcome.view.removeFromSuperview()
                    self.welcome.removeFromParentViewController()
            })
        } else {
            welcome.view.alpha = 0
            welcome.willMove(toParentViewController: nil)
            welcome.view.removeFromSuperview()
            welcome.removeFromParentViewController()
        }
    }
}

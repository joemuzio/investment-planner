//
//  ComparisonHeaderView.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/23/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ComparisonHeaderView: UICollectionReusableView {
    
    static let titleHeight: CGFloat = 60
    
    var backButton: UIButton!
    var infoButton: UIButton!
    var columnHeadersContentView: UIView!
    
    var columnTitlesContentView: UIView!
    var metricsLabel: UILabel!
    var headerSeparator1: UIView!
    var headerSeparator2: UIView!
    var headerSeparator3: UIView!
    var growthPlanNameLabel1: UILabel!
    var growthPlanNameLabel2: UILabel!
    var growthPlanNameLabel3: UILabel!
    var accessoryButton: UIButton!
    
    var strategicWeightingContentView: UIView!
    var strategicWeightingLabel: UILabel!
    var strategicWeightingSeparator1: UIView!
    var strategicWeightingSeparator2: UIView!
    var strategicWeightingSeparator3: UIView!
    var strategicWeightingValueLabel1: UILabel!
    var strategicWeightingValueLabel2: UILabel!
    var strategicWeightingValueLabel3: UILabel!
    
    var shouldShowThirdColumn = false {
        didSet {
            if shouldShowThirdColumn != oldValue {
                self.updatePositionConstraints(shouldShowThirdColumn)
            }
        }
    }
    
    var separatorWidth: CGFloat = 1 {
        didSet {
            if separatorWidth != oldValue {
                self.updateSeparatorWidthConstraints(separatorWidth)
            }
        }
    }
    
    var accessoryButtonActionHandler: ((ComparisonHeaderView) -> Void)?
    var backButtonActionHandler: ((ComparisonHeaderView) -> Void)?
    var infoButtonActionHandler: ((ComparisonHeaderView) -> Void)?
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.catalinaWhiteColor()
        
        backButton = UIButton()
        backButton.setTitle(" Library", for: UIControlState())
        backButton.titleLabel?.font = UIFont.catalinaMediumFontOfSize(20)
        backButton.setImage(UIImage(named: "icon-back-light-bg"), for: UIControlState())
        backButton.setTitleColor(UIColor.catalinaDarkBlueColor(), for: UIControlState())
        backButton.sizeToFit()
        backButton.addTarget(self, action: #selector(ComparisonHeaderView.backButtonPressed), for: .touchUpInside)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(backButton)
        self.addConstraint(NSLayoutConstraint(item: backButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: backButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "icon-page-definitions-light-bg"), for: UIControlState())
        infoButton.sizeToFit()
        infoButton.addTarget(self, action: #selector(ComparisonHeaderView.infoButtonPressed), for: .touchUpInside)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(infoButton)
        self.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 15))
        self.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
        
        columnHeadersContentView = UIView()
        columnHeadersContentView.backgroundColor = UIColor.catalinaDarkBlueColor()
        columnHeadersContentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(columnHeadersContentView)
        self.addConstraint(NSLayoutConstraint(item: columnHeadersContentView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: ComparisonHeaderView.titleHeight))
        self.addConstraint(NSLayoutConstraint(item: columnHeadersContentView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: columnHeadersContentView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: columnHeadersContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: (2 * ComparisonCollectionViewCell.rowHeight) + 1))
        
        //Titles
        
        columnTitlesContentView = UIView()
        columnTitlesContentView.backgroundColor = UIColor.catalinaDarkBlueColor()
        columnTitlesContentView.translatesAutoresizingMaskIntoConstraints = false
        columnHeadersContentView.addSubview(columnTitlesContentView)
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: columnTitlesContentView, attribute: .top, relatedBy: .equal, toItem: columnHeadersContentView, attribute: .top, multiplier: 1, constant: 0))
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: columnTitlesContentView, attribute: .left, relatedBy: .equal, toItem: columnHeadersContentView, attribute: .left, multiplier: 1, constant: 0))
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: columnTitlesContentView, attribute: .right, relatedBy: .equal, toItem: columnHeadersContentView, attribute: .right, multiplier: 1, constant: 0))
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: columnTitlesContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: ComparisonCollectionViewCell.rowHeight))
        
        metricsLabel = UILabel()
        metricsLabel.text = "Metrics"
        metricsLabel.font = ComparisonCollectionViewCell.textFont
        metricsLabel.backgroundColor = UIColor.clear
        metricsLabel.textColor = UIColor.catalinaWhiteColor()
        metricsLabel.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(metricsLabel)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: metricsLabel, attribute: .left, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .left, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: metricsLabel, attribute: .centerY, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        headerSeparator1 = UIView()
        headerSeparator1.backgroundColor = UIColor.catalinaWhiteColor()
        headerSeparator1.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(headerSeparator1)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: headerSeparator1, attribute: .top, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .top, multiplier: 1, constant: 0))
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: headerSeparator1, attribute: .bottom, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        headerSeparator2 = UIView()
        headerSeparator2.backgroundColor = UIColor.catalinaWhiteColor()
        headerSeparator2.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(headerSeparator2)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: headerSeparator2, attribute: .top, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .top, multiplier: 1, constant: 0))
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: headerSeparator2, attribute: .bottom, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        headerSeparator3 = UIView()
        headerSeparator3.backgroundColor = UIColor.catalinaWhiteColor()
        headerSeparator3.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(headerSeparator3)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: headerSeparator3, attribute: .top, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .top, multiplier: 1, constant: 0))
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: headerSeparator3, attribute: .bottom, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        growthPlanNameLabel1 = UILabel()
        growthPlanNameLabel1.font = ComparisonCollectionViewCell.textFont
        growthPlanNameLabel1.backgroundColor = UIColor.clear
        growthPlanNameLabel1.textColor = UIColor.catalinaWhiteColor()
        growthPlanNameLabel1.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(growthPlanNameLabel1)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: growthPlanNameLabel1, attribute: .centerY, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        growthPlanNameLabel2 = UILabel()
        growthPlanNameLabel2.font = ComparisonCollectionViewCell.textFont
        growthPlanNameLabel2.backgroundColor = UIColor.clear
        growthPlanNameLabel2.textColor = UIColor.catalinaWhiteColor()
        growthPlanNameLabel2.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(growthPlanNameLabel2)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: growthPlanNameLabel2, attribute: .centerY, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        growthPlanNameLabel3 = UILabel()
        growthPlanNameLabel3.font = ComparisonCollectionViewCell.textFont
        growthPlanNameLabel3.backgroundColor = UIColor.clear
        growthPlanNameLabel3.textColor = UIColor.catalinaWhiteColor()
        growthPlanNameLabel3.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(growthPlanNameLabel3)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: growthPlanNameLabel3, attribute: .centerY, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        accessoryButton = UIButton()
        accessoryButton.setImage(UIImage(named: "icon-more-dark-bg"), for: UIControlState())
        accessoryButton.addTarget(self, action: #selector(ComparisonHeaderView.accessoryButtonPressed), for: .touchUpInside)
        accessoryButton.sizeToFit()
        accessoryButton.translatesAutoresizingMaskIntoConstraints = false
        columnTitlesContentView.addSubview(accessoryButton)
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: accessoryButton, attribute: .right, relatedBy: .equal, toItem: headerSeparator1, attribute: .right, multiplier: 1, constant: -ComparisonCollectionViewCell.cellPadding))
        columnTitlesContentView.addConstraint(NSLayoutConstraint(item: accessoryButton, attribute: .centerY, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        //Strategic Weights
        
        strategicWeightingContentView = UIView()
        strategicWeightingContentView.backgroundColor = UIColor.catalinaWhiteColor()
        strategicWeightingContentView.translatesAutoresizingMaskIntoConstraints = false
        columnHeadersContentView.addSubview(strategicWeightingContentView)
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingContentView, attribute: .top, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .bottom, multiplier: 1, constant: 0))
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingContentView, attribute: .left, relatedBy: .equal, toItem: columnHeadersContentView, attribute: .left, multiplier: 1, constant: 0))
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingContentView, attribute: .right, relatedBy: .equal, toItem: columnHeadersContentView, attribute: .right, multiplier: 1, constant: 0))
        columnHeadersContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: ComparisonCollectionViewCell.rowHeight))
        
        strategicWeightingLabel = UILabel()
        strategicWeightingLabel.text = "Strategic Weighting"
        strategicWeightingLabel.font = ComparisonCollectionViewCell.textFont
        strategicWeightingLabel.backgroundColor = UIColor.clear
        strategicWeightingLabel.textColor = UIColor.catalinaDarkBlueColor()
        strategicWeightingLabel.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingLabel)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingLabel, attribute: .left, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .left, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingLabel, attribute: .centerY, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        strategicWeightingSeparator1 = UIView()
        strategicWeightingSeparator1.backgroundColor = UIColor.catalinaDarkBlueColor()
        strategicWeightingSeparator1.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingSeparator1)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator1, attribute: .top, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .top, multiplier: 1, constant: 0))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator1, attribute: .bottom, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        strategicWeightingSeparator2 = UIView()
        strategicWeightingSeparator2.backgroundColor = UIColor.catalinaDarkBlueColor()
        strategicWeightingSeparator2.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingSeparator2)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator2, attribute: .top, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .top, multiplier: 1, constant: 0))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator2, attribute: .bottom, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        strategicWeightingSeparator3 = UIView()
        strategicWeightingSeparator3.backgroundColor = UIColor.catalinaDarkBlueColor()
        strategicWeightingSeparator3.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingSeparator3)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator3, attribute: .top, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .top, multiplier: 1, constant: 0))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator3, attribute: .bottom, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        strategicWeightingValueLabel1 = UILabel()
        strategicWeightingValueLabel1.textAlignment = .center
        strategicWeightingValueLabel1.font = UIFont.catalinaBoldFontOfSize(16)
        strategicWeightingValueLabel1.backgroundColor = UIColor.clear
        strategicWeightingValueLabel1.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingValueLabel1)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingValueLabel1, attribute: .left, relatedBy: .equal, toItem: strategicWeightingSeparator1, attribute: .right, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingValueLabel1, attribute: .centerY, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        strategicWeightingValueLabel2 = UILabel()
        strategicWeightingValueLabel2.textAlignment = .center
        strategicWeightingValueLabel2.font = UIFont.catalinaBoldFontOfSize(16)
        strategicWeightingValueLabel2.backgroundColor = UIColor.clear
        strategicWeightingValueLabel2.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingValueLabel2)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingValueLabel2, attribute: .left, relatedBy: .equal, toItem: strategicWeightingSeparator2, attribute: .right, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingValueLabel2, attribute: .centerY, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        strategicWeightingValueLabel3 = UILabel()
        strategicWeightingValueLabel3.textAlignment = .center
        strategicWeightingValueLabel3.font = UIFont.catalinaBoldFontOfSize(16)
        strategicWeightingValueLabel3.backgroundColor = UIColor.clear
        strategicWeightingValueLabel3.translatesAutoresizingMaskIntoConstraints = false
        strategicWeightingContentView.addSubview(strategicWeightingValueLabel3)
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingValueLabel3, attribute: .left, relatedBy: .equal, toItem: strategicWeightingSeparator3, attribute: .right, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        strategicWeightingContentView.addConstraint(NSLayoutConstraint(item: strategicWeightingValueLabel3, attribute: .centerY, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        self.updatePositionConstraints(shouldShowThirdColumn)
        self.updateSeparatorWidthConstraints(separatorWidth)
    }
    
    fileprivate func updatePositionConstraints(_ showingThreeScenarios: Bool) {
    
        //this method is broken into multiple parts for clarity. the code could definitely be more consise.
        
        //--------------- separator positioning -----------------
        
        //remove current constraints
        var identifier1 = "separator1PositionConstraint"
        var identifier2 = "separator2PositionConstraint"
        var identifier3 = "separator3PositionConstraint"
        
        //remove header separator constraints
        for constraint in columnTitlesContentView.constraints {
            if constraint.identifier == identifier1 || constraint.identifier == identifier2 || constraint.identifier == identifier3 {
                columnTitlesContentView.removeConstraint(constraint)
            }
        }
        
        //remove strategic weigthing separator constraints
        for constraint in strategicWeightingContentView.constraints {
            if constraint.identifier == identifier1 || constraint.identifier == identifier2 || constraint.identifier == identifier3 {
                strategicWeightingContentView.removeConstraint(constraint)
            }
        }
        
        // add appropriate constraints
        var position1 = showingThreeScenarios ? ComparisonCollectionViewCell.threeScenarioPositionRatios[0] : ComparisonCollectionViewCell.twoScenarioPositionRatios[0]
        var position2 = showingThreeScenarios ? ComparisonCollectionViewCell.threeScenarioPositionRatios[1] : ComparisonCollectionViewCell.twoScenarioPositionRatios[1]
        var position3 = showingThreeScenarios ? ComparisonCollectionViewCell.threeScenarioPositionRatios[2] : ComparisonCollectionViewCell.twoScenarioPositionRatios[2]
        
        //add header separator constraints
        let headerConstraint1 = NSLayoutConstraint(item: headerSeparator1, attribute: .centerX, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .right, multiplier: position1, constant: 0)
        let headerConstraint2 = NSLayoutConstraint(item: headerSeparator2, attribute: .centerX, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .right, multiplier: position2, constant: 0)
        let headerConstraint3 = NSLayoutConstraint(item: headerSeparator3, attribute: .centerX, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .right, multiplier: position3, constant: 0)
        
        headerConstraint1.identifier = identifier1
        headerConstraint2.identifier = identifier2
        headerConstraint3.identifier = identifier3
        
        columnTitlesContentView.addConstraint(headerConstraint1)
        columnTitlesContentView.addConstraint(headerConstraint2)
        columnTitlesContentView.addConstraint(headerConstraint3)
        
        //add strategic weighting separator constraints
        let strategicWeightingConstraint1 = NSLayoutConstraint(item: strategicWeightingSeparator1, attribute: .centerX, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .right, multiplier: position1, constant: 0)
        let strategicWeightingConstraint2 = NSLayoutConstraint(item: strategicWeightingSeparator2, attribute: .centerX, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .right, multiplier: position2, constant: 0)
        let strategicWeightingConstraint3 = NSLayoutConstraint(item: strategicWeightingSeparator3, attribute: .centerX, relatedBy: .equal, toItem: strategicWeightingContentView, attribute: .right, multiplier: position3, constant: 0)
        
        strategicWeightingConstraint1.identifier = identifier1
        strategicWeightingConstraint2.identifier = identifier2
        strategicWeightingConstraint3.identifier = identifier3
        
        strategicWeightingContentView.addConstraint(strategicWeightingConstraint1)
        strategicWeightingContentView.addConstraint(strategicWeightingConstraint2)
        strategicWeightingContentView.addConstraint(strategicWeightingConstraint3)
        
        //--------------- title positioning -----------------
        
        //these position constraints are used to center the header titles
        //TODO: need to change this at some point to just set left, right, top, and bottom constraints on this label and let the label center the text
        
        identifier1 = "title1PositionConstraint"
        identifier2 = "title2PositionConstraint"
        identifier3 = "title3PositionConstraint"
        
        //remove existing title constraints
        for constraint in columnTitlesContentView.constraints {
            if constraint.identifier == identifier1 || constraint.identifier == identifier2 || constraint.identifier == identifier3 {
                columnTitlesContentView.removeConstraint(constraint)
            }
        }
        
        //add new title constraints
        position1 = (position1 + position2) / 2
        position2 = (position2 + position3) / 2
        position3 = (position3 + 1.0) / 2
        
        let titleConstraint1 = NSLayoutConstraint(item: growthPlanNameLabel1, attribute: .centerX, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .right, multiplier: position1, constant: 0)
        let titleConstraint2 = NSLayoutConstraint(item: growthPlanNameLabel2, attribute: .centerX, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .right, multiplier: position2, constant: 0)
        let titleConstraint3 = NSLayoutConstraint(item: growthPlanNameLabel3, attribute: .centerX, relatedBy: .equal, toItem: columnTitlesContentView, attribute: .right, multiplier: position3, constant: 0)
        
        titleConstraint1.identifier = identifier1
        titleConstraint2.identifier = identifier2
        titleConstraint3.identifier = identifier3
        
        columnTitlesContentView.addConstraint(titleConstraint1)
        columnTitlesContentView.addConstraint(titleConstraint2)
        columnTitlesContentView.addConstraint(titleConstraint3)
    }
    
    fileprivate func updateSeparatorWidthConstraints(_ width: CGFloat) {
        
        headerSeparator1.removeConstraints(headerSeparator1.constraints)
        headerSeparator2.removeConstraints(headerSeparator2.constraints)
        headerSeparator3.removeConstraints(headerSeparator3.constraints)
        headerSeparator1.addConstraint(NSLayoutConstraint(item: headerSeparator1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        headerSeparator2.addConstraint(NSLayoutConstraint(item: headerSeparator2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        headerSeparator3.addConstraint(NSLayoutConstraint(item: headerSeparator3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        
        strategicWeightingSeparator1.removeConstraints(strategicWeightingSeparator1.constraints)
        strategicWeightingSeparator2.removeConstraints(strategicWeightingSeparator2.constraints)
        strategicWeightingSeparator3.removeConstraints(strategicWeightingSeparator3.constraints)
        strategicWeightingSeparator1.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        strategicWeightingSeparator2.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        strategicWeightingSeparator3.addConstraint(NSLayoutConstraint(item: strategicWeightingSeparator3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
    }
    
    func backButtonPressed() {
        
        if let handler = backButtonActionHandler {
            handler(self)
        }
    }
    
    func infoButtonPressed() {
        
        if let handler = infoButtonActionHandler {
            handler(self)
        }
    }
    
    func accessoryButtonPressed() {
        
        if let handler = accessoryButtonActionHandler {
            handler(self)
        }
    }
    
    class func requiredHeight() -> CGFloat {
        
        return ComparisonHeaderView.titleHeight + (2 * ComparisonCollectionViewCell.rowHeight) + 1
    }
}

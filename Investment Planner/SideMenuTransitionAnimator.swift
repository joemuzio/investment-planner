//
//  SideMenuTransitionAnimator.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/3/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

enum SideMenuTransitionAnimatorStyle {
    case reveal
    case overlay
}

class SideMenuTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    var presentDuration = 0.5
    var dismissDuration = 0.25
    var style = SideMenuTransitionAnimatorStyle.reveal
    var rightSide = false
    var shouldUpdateOnDismiss = false
    fileprivate var isPresenting = false
    fileprivate var snapshotContainer = UIView()
    fileprivate var presentingViewController: UIViewController?
    
    // MARK: - UIViewControllerAnimatedTransitioning Implementation
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        
        if self.isPresenting {
            return presentDuration
        } else {
            return dismissDuration
        }
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let container = transitionContext.containerView
        
        if isPresenting {
            
            //update style related appearance properties
            if style == .reveal {
                snapshotContainer.layer.masksToBounds = false
                snapshotContainer.layer.shadowOffset = rightSide ? CGSize(width: 6, height: 0) : CGSize(width: -6, height: 0)
                snapshotContainer.layer.shadowRadius = 5
                snapshotContainer.layer.shadowOpacity = 0.5
            } else {
                toViewController.view.layer.masksToBounds = false
                toViewController.view.layer.shadowOffset = rightSide ? CGSize(width: -6, height: 0) : CGSize(width: 6, height: 0)
                toViewController.view.layer.shadowRadius = 5
                toViewController.view.layer.shadowOpacity = 0.5
            }
            
            //capture the presenting view controller for later use
            self.presentingViewController = fromViewController
            
            //take a snapshot of the presenting view controller and add it to the view heirarchy above the presented view controller
            snapshotContainer.frame = container.bounds
            self.updateSnapshotWithView(fromViewController.view)
            
            //if the toViewController view is being presented on the right side and it is not as wide as the fromViewController view, we need to go ahead position it where it will end up when the animation is complete
            if rightSide {
                toViewController.view.frame = CGRect(x: container.bounds.width - toViewController.view.bounds.width, y: toViewController.view.frame.origin.y, width: toViewController.view.frame.size.width, height: toViewController.view.frame.size.height)
            }
            
            //add views to the container
            if style == .reveal {
                container.addSubview(toViewController.view)
                container.addSubview(snapshotContainer)
            } else {
                //shift the toViewController offscreen so if can transition onscreen during our animation
                let offset = self.rightSide ? toViewController.view.bounds.width : -toViewController.view.bounds.width
                toViewController.view.transform = toViewController.view.transform.translatedBy(x: offset, y: 0)
                container.addSubview(snapshotContainer)
                container.addSubview(toViewController.view)
            }
            
            //add a gesture recognizer to the snapshot so we can dismiss on a tap
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SideMenuTransitionAnimator.handleTap(_:)))
            snapshotContainer.addGestureRecognizer(tapGesture)
            
        } else {
            
            if shouldUpdateOnDismiss {
                //update the snapshot in case our view changes while it was partially off screen (i.e. we switched to another view controller)
                self.updateSnapshotWithView(toViewController.view)
            }
        }
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: .curveLinear, animations: { () -> Void in
            
            if self.isPresenting {
                if self.style == .reveal {
                    if self.rightSide {
                        self.snapshotContainer.transform = CGAffineTransform(translationX: -toViewController.view.bounds.width, y: 0)
                    } else {
                        self.snapshotContainer.transform = CGAffineTransform(translationX: toViewController.view.bounds.width, y: 0)
                    }
                } else {
                    toViewController.view.transform = CGAffineTransform.identity
                }
            } else {
                if self.style == .reveal {
                    self.snapshotContainer.transform = CGAffineTransform.identity
                } else {
                    let offset = self.rightSide ? fromViewController.view.bounds.width : -fromViewController.view.bounds.width
                    fromViewController.view.transform = fromViewController.view.transform.translatedBy(x: offset, y: 0)
                }
            }
            
            }) { (finished) -> Void in
                
                transitionContext.completeTransition(true)
                if !self.isPresenting {
                    self.snapshotContainer.removeFromSuperview()
                    if self.style == .overlay {
                        fromViewController.view.transform = CGAffineTransform.identity
                    }
                }
        }
    }
    
    // MARK: - UIViewControllerTransitioningDelegate Implementation
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        isPresenting = false
        return self
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        isPresenting = true
        return self
    }
    
    // MARK: - Actions
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        
        if let vc = self.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }

    // MARK: - Helper Methods
    
    fileprivate func updateSnapshotWithView(_ view: UIView) {
        
        let existingSnapshot = snapshotContainer.subviews.first
        let updatedSnapshot = view.snapshotView(afterScreenUpdates: true)
        snapshotContainer.addSubview(updatedSnapshot!)
        updatedSnapshot?.isUserInteractionEnabled = true
        existingSnapshot?.removeFromSuperview()
    }
    
}

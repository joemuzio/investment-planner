//
//  ComparisonCollectionViewCell.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/17/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ComparisonCollectionViewCell: UICollectionViewCell {
    
    static let rowHeight: CGFloat = 40
    static let cellPadding: CGFloat = 20
    static let twoScenarioPositionRatios: [CGFloat] = [0.4, 0.7, 1.0]
    static let threeScenarioPositionRatios: [CGFloat] = [0.34, 0.56, 0.78]
    static let textFont = UIFont.catalinaBoldFontOfSize(20)
    
    var propertyNameLabel: UILabel!
    var separator1: UIView!
    var separator2: UIView!
    var separator3: UIView!
    var propertyValueLabel1: UILabel!
    var propertyValueLabel2: UILabel!
    var propertyValueLabel3: UILabel!
    var accessoryButton: UIButton!
    
    var longPressGesture: UILongPressGestureRecognizer!
    var longPressGestureHandler: ((UIGestureRecognizer) -> Void)? {
        didSet {
            if longPressGestureHandler == nil {
                self.removeGestureRecognizer(longPressGesture)
            } else {
                self.addGestureRecognizer(longPressGesture)
            }
        }
    }
    
    var shouldShowThirdColumn = false {
        didSet {
            if shouldShowThirdColumn != oldValue {
                self.updatePositionConstraints(shouldShowThirdColumn)
            }
        }
    }
    
    var separatorWidth: CGFloat = 1 {
        didSet {
            if separatorWidth != oldValue {
                self.updateSeparatorWidthConstraints(separatorWidth)
            }
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        propertyNameLabel = UILabel()
        propertyNameLabel.font = ComparisonCollectionViewCell.textFont
        propertyNameLabel.backgroundColor = UIColor.clear
        propertyNameLabel.textColor = UIColor.catalinaDarkBlueColor()
        propertyNameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyNameLabel)
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        separator1 = UIView()
        separator1.backgroundColor = UIColor.catalinaDarkBlueColor()
        separator1.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(separator1)
        self.contentView.addConstraint(NSLayoutConstraint(item: separator1, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: separator1, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        separator2 = UIView()
        separator2.backgroundColor = UIColor.catalinaDarkBlueColor()
        separator2.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(separator2)
        self.contentView.addConstraint(NSLayoutConstraint(item: separator2, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: separator2, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        separator3 = UIView()
        separator3.backgroundColor = UIColor.catalinaDarkBlueColor()
        separator3.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(separator3)
        self.contentView.addConstraint(NSLayoutConstraint(item: separator3, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: separator3, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0))
        
        propertyValueLabel1 = UILabel()
        propertyValueLabel1.font = ComparisonCollectionViewCell.textFont
        propertyValueLabel1.backgroundColor = UIColor.clear
        propertyValueLabel1.textColor = UIColor.catalinaDarkBlueColor()
        propertyValueLabel1.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel1)
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .left, relatedBy: .equal, toItem: separator1, attribute: .right, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        propertyValueLabel2 = UILabel()
        propertyValueLabel2.font = ComparisonCollectionViewCell.textFont
        propertyValueLabel2.backgroundColor = UIColor.clear
        propertyValueLabel2.textColor = UIColor.catalinaDarkBlueColor()
        propertyValueLabel2.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel2)
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .left, relatedBy: .equal, toItem: separator2, attribute: .right, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        propertyValueLabel3 = UILabel()
        propertyValueLabel3.font = ComparisonCollectionViewCell.textFont
        propertyValueLabel3.backgroundColor = UIColor.clear
        propertyValueLabel3.textColor = UIColor.catalinaDarkBlueColor()
        propertyValueLabel3.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel3)
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .left, relatedBy: .equal, toItem: separator3, attribute: .right, multiplier: 1, constant: ComparisonCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        accessoryButton = UIButton()
        accessoryButton.setImage(UIImage(named: "icon-menu-light-bg"), for: UIControlState())
        accessoryButton.sizeToFit()
        accessoryButton.isHidden = true
        accessoryButton.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(accessoryButton)
        self.contentView.addConstraint(NSLayoutConstraint(item: accessoryButton, attribute: .right, relatedBy: .equal, toItem: separator1, attribute: .right, multiplier: 1, constant: -ComparisonCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: accessoryButton, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(ComparisonCollectionViewCell.handleLongPress(_:)))
        
        self.updatePositionConstraints(shouldShowThirdColumn)
        self.updateSeparatorWidthConstraints(separatorWidth)
    }
    
    fileprivate func updatePositionConstraints(_ showingThreeScenarios: Bool) {
        
        //remove current constraints
        let identifier1 = "separator1PositionConstraint"
        let identifier2 = "separator2PositionConstraint"
        let identifier3 = "separator3PositionConstraint"
        
        for constraint in self.contentView.constraints {
            if constraint.identifier == identifier1 || constraint.identifier == identifier2 || constraint.identifier == identifier3 {
                self.contentView.removeConstraint(constraint)
            }
        }
        
        // add appropriate constraints
        let position1 = showingThreeScenarios ? ComparisonCollectionViewCell.threeScenarioPositionRatios[0] : ComparisonCollectionViewCell.twoScenarioPositionRatios[0]
        let position2 = showingThreeScenarios ? ComparisonCollectionViewCell.threeScenarioPositionRatios[1] : ComparisonCollectionViewCell.twoScenarioPositionRatios[1]
        let position3 = showingThreeScenarios ? ComparisonCollectionViewCell.threeScenarioPositionRatios[2] : ComparisonCollectionViewCell.twoScenarioPositionRatios[2]
        
        //add title constraints
        let constraint1 = NSLayoutConstraint(item: separator1, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: position1, constant: 0)
        let constraint2 = NSLayoutConstraint(item: separator2, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: position2, constant: 0)
        let constraint3 = NSLayoutConstraint(item: separator3, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: position3, constant: 0)
        
        constraint1.identifier = identifier1
        constraint2.identifier = identifier2
        constraint3.identifier = identifier3
        
        self.contentView.addConstraint(constraint1)
        self.contentView.addConstraint(constraint2)
        self.contentView.addConstraint(constraint3)
    }
    
    fileprivate func updateSeparatorWidthConstraints(_ width: CGFloat) {
        
        separator1.removeConstraints(separator1.constraints)
        separator2.removeConstraints(separator2.constraints)
        separator3.removeConstraints(separator3.constraints)
        separator1.addConstraint(NSLayoutConstraint(item: separator1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        separator2.addConstraint(NSLayoutConstraint(item: separator2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
        separator3.addConstraint(NSLayoutConstraint(item: separator3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width))
    }
    
    func handleLongPress(_ gesture: UIGestureRecognizer) {
        
        if let handler = self.longPressGestureHandler {
            handler(gesture)
        }
    }
}

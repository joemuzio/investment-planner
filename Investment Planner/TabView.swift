//
//  TabView.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/3/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class TabView: UIControl {
    
    var tabSpacing: CGFloat = 20 {
        didSet {
            self.updateTabs()
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
            self.updateTabs()
        }
    }
    
    var tabFont: UIFont = UIFont.catalinaMediumFontOfSize(20) {
        didSet {
            self.updateTabs()
        }
    }
    
    var tabs = [String]() {
        didSet {
            self.updateTabs()
            self.selectedTab = tabs.first
        }
    }
    
    var selectedTab: String? {
        didSet {
            if let selectedTab = selectedTab {
                if tabs.contains(selectedTab) {
                    if let button = self.getButtonForTab(selectedTab) {
                        if let indicator = selectionIndicator {
                            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                                indicator.frame = CGRect(x: button.frame.origin.x, y: button.frame.height, width: button.frame.width, height: 2)
                            })
                        }
                    }
                }
            }
        }
    }
    
    fileprivate var tabButtons = [UIButton]()
    fileprivate var selectionIndicator: UIView?
    
    func updateTabs() {
        
        for button in tabButtons {
            button.removeFromSuperview()
        }
        selectionIndicator?.removeFromSuperview()
        tabButtons.removeAll()
        var x:CGFloat = 0
        for tab in tabs {
            let button = UIButton()
            button.backgroundColor = UIColor.clear
            button.setTitle(tab, for: UIControlState())
            button.titleLabel!.font = self.tabFont
            button.setTitleColor(self.tintColor, for: UIControlState())
            button.addTarget(self, action: #selector(TabView.buttonPressed(_:)), for: .touchUpInside)
            button.sizeToFit()
            button.frame = CGRect(x: x, y: 0, width: button.frame.width, height: button.frame.height)
            x += button.frame.width + tabSpacing
            tabButtons.append(button)
            self.addSubview(button)
            if selectionIndicator == nil {
                let indicator = UIView()
                indicator.frame = CGRect(x: 0, y: button.frame.height, width: button.frame.width, height: 2)
                indicator.backgroundColor = self.tintColor
                self.addSubview(indicator)
                selectionIndicator = indicator
            }
        }
    }
    
    func buttonPressed(_ sender: UIButton) {
        
        self.selectedTab = sender.title(for: UIControlState())!
        self.sendActions(for: .valueChanged)
    }
    
    fileprivate func getButtonForTab(_ tab: String) -> UIButton? {
        
        for button in tabButtons {
            if button.title(for: UIControlState()) == tab {
                return button
            }
        }
        return nil
    }
    
    override var intrinsicContentSize : CGSize {
        
        return self.sizeThatFits(CGSize.zero)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
    
        var width:CGFloat = 0
        var height:CGFloat = 0
        for button in tabButtons {
            width += button.frame.width
        }
        if tabButtons.count > 1 {
            width += tabSpacing * CGFloat(tabButtons.count - 1)
        }
        if let indicator = selectionIndicator {
            height = indicator.frame.maxY
        }
        return CGSize(width: width, height: height)
    }
}

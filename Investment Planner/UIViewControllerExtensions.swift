//
//  UIViewControllerExtensions.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/17/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showPageDefinitionsWithSections(_ sections: [DefinitionsSection]?, animated: Bool) {
        
        let definitionsViewController = DefinitionsViewController()
        let menuAnimator = definitionsViewController.menuAnimator
        if let sections = sections {
            definitionsViewController.sections = sections
        } else {
            var defs = [String: String]()
            for _ in 1...10 {
                let key = Lorem.title
                let value = Lorem.paragraphs(1)
                defs[key] = value
            }
            definitionsViewController.sections = [DefinitionsSection(name: "", terms: defs)]
        }
        definitionsViewController.modalPresentationStyle = .custom
        menuAnimator.rightSide = true
        menuAnimator.style = .overlay
        definitionsViewController.transitioningDelegate = menuAnimator
        self.present(definitionsViewController, animated: animated, completion: nil)
    }
    
    func showPageDefinitions(_ definitions: [String:String]?, animated: Bool) {
        
        if let definitions = definitions {
            self.showPageDefinitionsWithSections([DefinitionsSection(name: "", terms: definitions)], animated: animated)
        } else {
            self.showPageDefinitionsWithSections(nil, animated: animated)
        }
    }
}

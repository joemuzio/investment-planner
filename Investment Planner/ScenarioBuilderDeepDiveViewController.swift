//
//  ScenarioBuilderDeepDiveViewController.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 1/31/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit
import MessageUI
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum SelectedEfficiency : String {
    case profitROI = "Profit ROI", retailROI = "Retail ROI", cpum = "CPUM", cpium = "CPIUM"
    static let allEfficiencies = [profitROI.rawValue, retailROI.rawValue, cpum.rawValue, cpium.rawValue]
}

enum SelectedKPI : String {
    case totalDollars = "Total Dollars", totalUnits = "Total Units", incrementalDollars = "Incremental Dollars", incrementalUnits = "Incremental Units", acquiredBuyers = "Acquired Buyers"
    static let allKPIs = [totalDollars.rawValue, totalUnits.rawValue, incrementalDollars.rawValue, incrementalUnits.rawValue, acquiredBuyers.rawValue]
}

class ScenarioBuilderDeepDiveViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate{

    enum MovingSliderTag: Int {
        case efficiencySliderTag
        case trialSliderTag
        case loyatySliderTag
        case volumeSliderTag
        case budgetSliderTag
    }
    
    var movingSliderTag:MovingSliderTag?
    var selectedKPI:SelectedKPI = .totalDollars
    var selectedEfficiency:SelectedEfficiency = .profitROI
    var displayedScenario:IPScenario!
    var isOptimizing = false
    var lastSolvedEfficiencyAmount = 0.0
    var lastSolvedTrialAmount = 0.0
    var lastSolvedLoyaltyAmount = 0.0
    var lastSolvedVolumeAmount = 0.0
    var lastSolvedBudgetAmount = 0.0
    var hideBottomLabels = false
    var viewDidLoadInitialAnalytics = true
    
    var vitoStatementSelectedMetrics = ["kpi": "Total Dollars", "efficiency": "Profit ROI"]
    
    let pageDefinitions = ["Distribution Investment": "Client's budget specific to distribution cost only.  Estimated Distribution x Cost per Impression", "Efficiency - Cost per Acquisition": "Total budget / acquired buyers.  N/A if trial is not part of program", "Efficiency - CPIUM": "Cost per incremental unit moved.  Total Budget / Incremental Units Moved", "Efficiency - CPUM": "Cost per unit moved\nCPUM with Coupon: Total Budget / Units Moved on Redemption\nCPUM with CCM: Total Budget / Units Moved on Reward", "Efficiency - Profit ROI": "Total incremental profit driven per dollar invested.  Retail ROI x Profit Margin", "Efficiency - Retail ROI": "Total incremental retail sales driven per dollar invested.  Incremental Retail Sales / Total Budget", "KPI Metric - Acquired Buyers": "Incremental new buyers driven by the trial programs.  Applies only to ID-based trial and lapsed offers.", "KPI Metric - Incremental Dollars": "Retail sales directly attributable to this program.  Incremental units x average retail price of product", "KPI Metric - Incremental Units": "Number of units moved that are directly attributable to this program.  Total units moved x incremental ratio midpoint", "KPI Metric - Total Dollars": "Retail dollars moved for all shoppers who redeemed a Catalina print or earned a Catalina reward.  Total units x average retail price.", "KPI Metric - Total Units": "All units moved for all shoppers who redeemed a Catalina print or earned a Catalina reward.", "Loyalty": "Own User Trade Up Programs", "Profit Margin": "The percentage of retail sales that is turned into profit.", "Redemption Investment": "# Redeemed x (Offer + Est. Clearinghouse Handling Fee)", "Total Investment": "Client total budget including distribution cost and redemption cost.", "Trial": "Projected from the Setup Report, Total Brand buyers - Total Recency Shoppers X .05%", "Volume": "Defined as short term Volume, this currently only comprises CCM programs"]
    
    var popoverTable = DeepDivePopoverTable()

    var topView:GradientView!
    var topViewGradientLayer:CAGradientLayer!
    var vitoStatementView:VitoStatementView!
    var slidersView:UIView!
    var barChartView:UIView!
    var trialBar:BarChartBarLayer!
    var loyaltyBar:BarChartBarLayer!
    var volumeBar:BarChartBarLayer!
    
    var scenarioNameTextField:UITextField!
    var programDataButton:UIButton!
    var shareButton:UIButton!
    var infoButton:UIButton!
    var exportButton:UIButton!
    var calendarButton:UIButton!
    
    var efficiencySliderView:SliderView!
    var trialSliderView:SliderView!
    var loyaltySliderView:SliderView!
    var volumeSliderView:SliderView!
    var budgetSliderView:SliderView!
    var hideTLVSlidersButton:UIButton!
    
    var barChartLabel:UILabel!
    var barChart:BarChart!
    
    var dynamicTopViewHeightConstraint:NSLayoutConstraint!
    var dynamicBarChartViewLeftConstraint:NSLayoutConstraint!
    var dynamicBarChartViewRightConstraint:NSLayoutConstraint!
    var dynamicBarChartViewTopConstraint:NSLayoutConstraint!
    var dynamicBarChartViewBottomConstraint:NSLayoutConstraint!
    var dynamicBarChartBottomConstraint:NSLayoutConstraint!
    var dynamicBarChartTopConstraint:NSLayoutConstraint!
    
    var dynamicEfficiencySliderTopConstraint:NSLayoutConstraint!
    var dynamicBudgetSliderTopConstraint:NSLayoutConstraint!
    var dynamicEfficiencySliderBottomConstraint:NSLayoutConstraint!
    var dynamicBudgetSliderBottomConstraint:NSLayoutConstraint!
    
    var portVC:PortfolioViewController!
    var kpiLabel:UILabel!
    var investmentLabel:UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(ScenarioBuilderDeepDiveViewController.popoverDismissed), name:NSNotification.Name(rawValue: "dismissPopoverTable"), object: nil)
        
        let selectedMetrics = SettingsController.sharedSettingsController.vitoStatementSelectedMetrics
        if(selectedMetrics?.count > 0){
            for(key, value) in selectedMetrics!{
                vitoStatementSelectedMetrics[key] = value as? String
            }
            setSelectedKPI(vitoStatementSelectedMetrics["kpi"]!)
            setSelectedEfficiency(vitoStatementSelectedMetrics["efficiency"]!)
        }
        topView = GradientView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.colors = [UIColor.catalinaLightBlueColor().cgColor, UIColor.catalinaDarkBlueColor().cgColor]
        topView.locations = [0.0, 1.0]
        self.view.addSubview(topView)
        
        calendarButton = UIButton()
        calendarButton.addTarget(self, action: #selector(calendarButtonPressed), for: .touchUpInside);
        calendarButton.setImage(UIImage(named: "calendar-icon"), for: UIControlState())
        calendarButton.sizeToFit()
        calendarButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(calendarButton)
        
        scenarioNameTextField = UITextField()
        scenarioNameTextField.delegate = self
        scenarioNameTextField.backgroundColor = UIColor.clear
        scenarioNameTextField.textColor = UIColor.white
        scenarioNameTextField.font = UIFont.catalinaBoldFontOfSize(14)
        scenarioNameTextField.textAlignment = .center
        scenarioNameTextField.text = displayedScenario.name
        scenarioNameTextField.translatesAutoresizingMaskIntoConstraints = false
        scenarioNameTextField.returnKeyType = .done
        topView.addSubview(scenarioNameTextField)
        
        exportButton = UIButton()
        exportButton.addTarget(self, action: #selector(exportButtonPressed), for: .touchUpInside);
        exportButton.setImage(UIImage(named: "email-icon"), for: UIControlState())
        exportButton.sizeToFit()
        exportButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(exportButton)
        
        programDataButton = UIButton()
        programDataButton.setImage(UIImage(named: "icon-view-program-data-dark-bg.png"), for: UIControlState())
        programDataButton.addTarget(self, action: #selector(programDataButtonPressed), for: .touchUpInside)
        programDataButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(programDataButton)
        
        shareButton = UIButton()
        shareButton.setImage(UIImage(named: "icon-share-growth-plan"), for: UIControlState())
        shareButton.addTarget(self, action: #selector(shareButtonPressed), for: .touchUpInside)
        shareButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(shareButton)
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "icon-page-definitions-dark-bg"), for: UIControlState())
        infoButton.sizeToFit()
        infoButton.addTarget(self, action: #selector(infoButtonPressed), for: .touchUpInside)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(infoButton)
        
        let vitoHeight = (self.view.frame.size.height * 0.2)
        let vitoY:CGFloat = 47.0
        vitoStatementView = VitoStatementView(frame: CGRect(x: 0, y: vitoY, width: 930, height: vitoHeight))
        vitoStatementView.kpiButton.addTarget(self, action: #selector(ScenarioBuilderDeepDiveViewController.kpiButtonPressed(_:)), for: .touchUpInside)
        vitoStatementView.efficiencyButton.addTarget(self, action: #selector(ScenarioBuilderDeepDiveViewController.efficiencyButtonPressed(_:)), for: .touchUpInside)
        topView.addSubview(vitoStatementView)
        
        slidersView = UIView()
        slidersView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(slidersView)
        
        efficiencySliderView = SliderView()
        efficiencySliderView.setupSlidersForNormalView()
        efficiencySliderView.setDescriptionLabelText(selectedEfficiency.rawValue)
        efficiencySliderView.slider.tag = MovingSliderTag.efficiencySliderTag.rawValue
        efficiencySliderView.slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        efficiencySliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpInside)
        efficiencySliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpOutside)
        efficiencySliderView.lockButton.addTarget(self, action: #selector(lockButtonPressed(_:)), for: .touchUpInside)
        efficiencySliderView.lockButton.tag = MovingSliderTag.efficiencySliderTag.rawValue
        slidersView.addSubview(efficiencySliderView)
        
        hideTLVSlidersButton = UIButton(type: .custom)
        hideTLVSlidersButton.setImage(UIImage(named: "icon-up-arrow"), for: UIControlState())
        hideTLVSlidersButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        hideTLVSlidersButton.sizeToFit()
        hideTLVSlidersButton.translatesAutoresizingMaskIntoConstraints = false
        hideTLVSlidersButton.addTarget(self, action: #selector(hideTLVSlidersButtonPressed), for: .touchUpInside)
        slidersView.addSubview(hideTLVSlidersButton)
        
        trialSliderView = SliderView()
        trialSliderView.setupSlidersForNormalView()
        trialSliderView.setDescriptionLabelText("trial")
        trialSliderView.slider.minimumValue = 0
        trialSliderView.slider.maximumValue = 100
        trialSliderView.slider.tag = MovingSliderTag.trialSliderTag.rawValue
        trialSliderView.slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        trialSliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpInside)
        trialSliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpOutside)
        trialSliderView.lockButton.addTarget(self, action: #selector(lockButtonPressed(_:)), for: .touchUpInside)
        trialSliderView.lockButton.tag = MovingSliderTag.trialSliderTag.rawValue
        slidersView.addSubview(trialSliderView)
        
        loyaltySliderView = SliderView()
        loyaltySliderView.setupSlidersForNormalView()
        loyaltySliderView.setDescriptionLabelText("loyalty")
        loyaltySliderView.slider.minimumValue = 0
        loyaltySliderView.slider.maximumValue = 100
        loyaltySliderView.slider.tag = MovingSliderTag.loyatySliderTag.rawValue
        loyaltySliderView.slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        loyaltySliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpInside)
        loyaltySliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpOutside)
        loyaltySliderView.lockButton.addTarget(self, action: #selector(lockButtonPressed(_:)), for: .touchUpInside)
        loyaltySliderView.lockButton.tag = MovingSliderTag.loyatySliderTag.rawValue
        slidersView.addSubview(loyaltySliderView)
        
        volumeSliderView = SliderView()
        volumeSliderView.setupSlidersForNormalView()
        volumeSliderView.setDescriptionLabelText("volume")
        volumeSliderView.slider.minimumValue = 0
        volumeSliderView.slider.maximumValue = 100
        volumeSliderView.slider.tag = MovingSliderTag.volumeSliderTag.rawValue
        volumeSliderView.slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        volumeSliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpInside)
        volumeSliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpOutside)
        volumeSliderView.lockButton.addTarget(self, action: #selector(lockButtonPressed(_:)), for: .touchUpInside)
        volumeSliderView.lockButton.tag = MovingSliderTag.volumeSliderTag.rawValue
        slidersView.addSubview(volumeSliderView)
        
        budgetSliderView = SliderView()
        budgetSliderView.setupSlidersForNormalView()
        budgetSliderView.setDescriptionLabelText("total investment")
        let maxScenario = displayedScenario.program.createNewScenario()
        SolverController.sharedSolverController.optimalInvestmentForConstraints(maxScenario)
        budgetSliderView.slider.minimumValue = Float(getMinimumBudget(maxScenario))
        let maxBudgetValue = Float(maxScenario.totalInvestment)
        budgetSliderView.slider.maximumValue = maxBudgetValue
        if(Float(displayedScenario.totalBudget) > maxBudgetValue){
            budgetSliderView.slider.value = maxBudgetValue
            
        }
        budgetSliderView.slider.tag = MovingSliderTag.budgetSliderTag.rawValue
        budgetSliderView.slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        budgetSliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpInside)
        budgetSliderView.slider.addTarget(self, action: #selector(saveSliderValue), for: .touchUpOutside)
        budgetSliderView.lockButton.addTarget(self, action: #selector(lockButtonPressed(_:)), for: .touchUpInside)
        budgetSliderView.lockButton.tag = MovingSliderTag.budgetSliderTag.rawValue
        slidersView.addSubview(budgetSliderView)
        
        barChartView = UIView()
        barChartView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(barChartView)
        
        barChartLabel = UILabel()
        barChartLabel.text = "Total Dollars"
        barChartLabel.font = UIFont.catalinaMediumFontOfSize(18)
        barChartLabel.textColor = UIColor.catalinaDarkBlueColor()
        barChartLabel.sizeToFit()
        barChartLabel.translatesAutoresizingMaskIntoConstraints = false
        barChartView.addSubview(barChartLabel)
        
        barChart = BarChart()
        barChart.titlePadding = 10
        barChart.titleColor = UIColor.catalinaDarkBlueColor()
        barChart.titleFont = UIFont.catalinaMediumFontOfSize(14)
        barChart.textPadding = 2
        barChart.textColor = UIColor.catalinaDarkBlueColor()
        barChart.textFont = UIFont.catalinaHeavyFontOfSize(14)
        barChart.barSpacing = 40
        barChart.baselineWidth = 2.0
        barChart.baselineColor = UIColor.catalinaDarkBlueColor()
        barChart.shouldAnimateUpdates = true
        barChart.shouldUseHighestBarValueForMax = false
        barChart.translatesAutoresizingMaskIntoConstraints = false
        barChartView.addSubview(barChart)
        
        trialBar = barChart.addBarWithValue(0, color: UIColor.catalinaDarkBlueColor(), title: "TRIAL", text: nil)
        loyaltyBar = barChart.addBarWithValue(0, color: UIColor.catalinaLightBlueColor(), title: "LOYALTY", text: nil)
        volumeBar = barChart.addBarWithValue(0, color: UIColor.catalinaLightGreenColor(), title: "VOLUME", text: nil)
        
        
        
        kpiLabel = UILabel()
        kpiLabel.textColor = UIColor.catalinaDarkBlueColor()
        kpiLabel.textAlignment = .left
        kpiLabel.text = "\(selectedEfficiency.rawValue): \(efficiencySliderView.amountLabel.text!)"
        kpiLabel.sizeToFit()
        kpiLabel.font = UIFont.catalinaBoldFontOfSize(14)
        kpiLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(kpiLabel)
        
        investmentLabel = UILabel()
        investmentLabel.textColor = UIColor.catalinaDarkBlueColor()
        investmentLabel.textAlignment = .left
        investmentLabel.text = "Total Investment: \(displayedScenario.totalInvestment.abbreviatedCurrencyString())"
        investmentLabel.sizeToFit()
        investmentLabel.font = UIFont.catalinaBoldFontOfSize(14)
        investmentLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(investmentLabel)
        
        // Top View Constraints
        
        dynamicTopViewHeightConstraint = NSLayoutConstraint(item: topView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: self.view.frame.size.height * 0.2)
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0))
        self.view.addConstraint(dynamicTopViewHeightConstraint)
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0))
        
        topView.addConstraint(NSLayoutConstraint(item: calendarButton, attribute: .left, relatedBy: .equal, toItem: topView, attribute: .left, multiplier: 1.0, constant: 20))
        topView.addConstraint(NSLayoutConstraint(item: calendarButton, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .top, multiplier: 1.0, constant: 15))
        calendarButton.addConstraint(NSLayoutConstraint(item: calendarButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 34))
        calendarButton.addConstraint(NSLayoutConstraint(item: calendarButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 34))
        
        topView.addConstraint(NSLayoutConstraint(item: scenarioNameTextField, attribute: .centerX, relatedBy: .equal, toItem: topView, attribute: .centerX, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: scenarioNameTextField, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .top, multiplier: 1.0, constant: 15))
        topView.addConstraint(NSLayoutConstraint(item: scenarioNameTextField, attribute: .width, relatedBy: .equal, toItem: topView, attribute: .width, multiplier: 0.5, constant: 0))
        
        
        infoButton.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 34))
        infoButton.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 34))
        topView.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .right, relatedBy: .equal, toItem: topView, attribute: .right, multiplier: 1.0, constant: -20))
        topView.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .top, multiplier: 1.0, constant: 15))
        
        topView.addConstraint(NSLayoutConstraint(item: programDataButton, attribute: .width, relatedBy: .equal, toItem: infoButton, attribute: .width, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: programDataButton, attribute: .height, relatedBy: .equal, toItem: infoButton, attribute: .height, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: programDataButton, attribute: .right, relatedBy: .equal, toItem: infoButton, attribute: .left, multiplier: 1.0, constant:(-30)))
        topView.addConstraint(NSLayoutConstraint(item: programDataButton, attribute: .top, relatedBy: .equal, toItem: infoButton, attribute: .top, multiplier: 1.0, constant: 0))
        
        topView.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .width, relatedBy: .equal, toItem: infoButton, attribute: .width, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .height, relatedBy: .equal, toItem: infoButton, attribute: .height, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .right, relatedBy: .equal, toItem: programDataButton, attribute: .left, multiplier: 1.0, constant: -30))
        topView.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .top, relatedBy: .equal, toItem: programDataButton, attribute: .top, multiplier: 1.0, constant: 0))
        
        topView.addConstraint(NSLayoutConstraint(item: exportButton, attribute: .width, relatedBy: .equal, toItem: infoButton, attribute: .width, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: exportButton, attribute: .height, relatedBy: .equal, toItem: infoButton, attribute: .height, multiplier: 1.0, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: exportButton, attribute: .right, relatedBy: .equal, toItem: shareButton, attribute: .left, multiplier: 1.0, constant: -30))
        topView.addConstraint(NSLayoutConstraint(item: exportButton, attribute: .top, relatedBy: .equal, toItem: shareButton, attribute: .top, multiplier: 1.0, constant: 0))
        
        
        // Slider View Constraints
        
        
        let sliderHeightFactor:CGFloat = 0.18
        let sliderWidthPixels:CGFloat = 10.0
        self.view.addConstraint(NSLayoutConstraint(item: slidersView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: slidersView, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .bottom, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: slidersView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: slidersView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0.55, constant: 0))
        
        slidersView.addConstraint(NSLayoutConstraint(item: efficiencySliderView, attribute: .right, relatedBy: .equal, toItem: slidersView, attribute: .right, multiplier: 1.0, constant: -sliderWidthPixels))
        slidersView.addConstraint(NSLayoutConstraint(item: efficiencySliderView, attribute: .width, relatedBy: .equal, toItem: slidersView, attribute: .width, multiplier: 0.9, constant: 0))
        dynamicEfficiencySliderTopConstraint = NSLayoutConstraint(item: efficiencySliderView, attribute: .top, relatedBy: .equal, toItem: slidersView, attribute: .top, multiplier: 1.0, constant: 30)
        slidersView.addConstraint(dynamicEfficiencySliderTopConstraint)
        slidersView.addConstraint(NSLayoutConstraint(item: efficiencySliderView, attribute: .height, relatedBy: .equal, toItem: slidersView, attribute: .height, multiplier: sliderHeightFactor, constant: 0))
        
        slidersView.addConstraint(NSLayoutConstraint(item: trialSliderView, attribute: .right, relatedBy: .equal, toItem: slidersView, attribute: .right, multiplier: 1.0, constant: -sliderWidthPixels))
        slidersView.addConstraint(NSLayoutConstraint(item: trialSliderView, attribute: .width, relatedBy: .equal, toItem: slidersView, attribute: .width, multiplier: 0.7, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: trialSliderView, attribute: .top, relatedBy: .equal, toItem: efficiencySliderView, attribute: .bottom, multiplier: 1.0, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: trialSliderView, attribute: .height, relatedBy: .equal, toItem: efficiencySliderView, attribute: .height, multiplier: 1.0, constant: 0))
        
        slidersView.addConstraint(NSLayoutConstraint(item: loyaltySliderView, attribute: .right, relatedBy: .equal, toItem: slidersView, attribute: .right, multiplier: 1.0, constant: -sliderWidthPixels))
        slidersView.addConstraint(NSLayoutConstraint(item: loyaltySliderView, attribute: .width, relatedBy: .equal, toItem: slidersView, attribute: .width, multiplier: 0.7, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: loyaltySliderView, attribute: .top, relatedBy: .equal, toItem: trialSliderView, attribute: .bottom, multiplier: 1.0, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: loyaltySliderView, attribute: .height, relatedBy: .equal, toItem: efficiencySliderView, attribute: .height, multiplier: 1.0, constant: 0))
        
        slidersView.addConstraint(NSLayoutConstraint(item: hideTLVSlidersButton, attribute: .left, relatedBy: .equal, toItem: efficiencySliderView, attribute: .left, multiplier: 1.0, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: hideTLVSlidersButton, attribute: .centerY, relatedBy: .equal, toItem: slidersView, attribute: .centerY, multiplier: 1.0, constant: -20))
        
        slidersView.addConstraint(NSLayoutConstraint(item: volumeSliderView, attribute: .right, relatedBy: .equal, toItem: slidersView, attribute: .right, multiplier: 1.0, constant: -sliderWidthPixels))
        slidersView.addConstraint(NSLayoutConstraint(item: volumeSliderView, attribute: .width, relatedBy: .equal, toItem: slidersView, attribute: .width, multiplier: 0.7, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: volumeSliderView, attribute: .top, relatedBy: .equal, toItem: loyaltySliderView, attribute: .bottom, multiplier: 1.0, constant: 0))
        slidersView.addConstraint(NSLayoutConstraint(item: volumeSliderView, attribute: .height, relatedBy: .equal, toItem: efficiencySliderView, attribute: .height, multiplier: 1.0, constant: 0))
        
        slidersView.addConstraint(NSLayoutConstraint(item: budgetSliderView, attribute: .right, relatedBy: .equal, toItem: slidersView, attribute: .right, multiplier: 1.0, constant: -sliderWidthPixels))
        slidersView.addConstraint(NSLayoutConstraint(item: budgetSliderView, attribute: .width, relatedBy: .equal, toItem: slidersView, attribute: .width, multiplier: 0.9, constant: 0))
        dynamicBudgetSliderBottomConstraint = NSLayoutConstraint(item: budgetSliderView, attribute: .bottom, relatedBy: .equal, toItem: slidersView, attribute: .bottom, multiplier: 1.0, constant: -30)
        slidersView.addConstraint(dynamicBudgetSliderBottomConstraint)
        slidersView.addConstraint(NSLayoutConstraint(item: budgetSliderView, attribute: .height, relatedBy: .equal, toItem: efficiencySliderView, attribute: .height, multiplier: 1.0, constant: 0))
        
        dynamicEfficiencySliderBottomConstraint = NSLayoutConstraint(item: efficiencySliderView, attribute: .bottom, relatedBy: .equal, toItem: trialSliderView, attribute: .top, multiplier: 1.0, constant: 0)
        slidersView.addConstraint(dynamicEfficiencySliderBottomConstraint)
        
        // Bar Chart View Constraints
        
        dynamicBarChartViewTopConstraint = NSLayoutConstraint(item: barChartView, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .bottom, multiplier: 1.0, constant: 0)
        dynamicBarChartViewBottomConstraint = NSLayoutConstraint(item: barChartView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: -35)
        dynamicBarChartViewLeftConstraint = NSLayoutConstraint(item: barChartView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 40)
        dynamicBarChartViewRightConstraint = NSLayoutConstraint(item: barChartView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: -40)

        self.view.addConstraint(dynamicBarChartViewRightConstraint)
        self.view.addConstraint(dynamicBarChartViewLeftConstraint)
        self.view.addConstraint(dynamicBarChartViewBottomConstraint)
        self.view.addConstraint(dynamicBarChartViewTopConstraint)
        
        barChartView.addConstraint(NSLayoutConstraint(item: barChartLabel, attribute: .centerX, relatedBy: .equal, toItem: barChartView, attribute: .centerX, multiplier: 1.0, constant: 0))
        barChartView.addConstraint(NSLayoutConstraint(item: barChartLabel, attribute: .top, relatedBy: .equal, toItem: barChartView, attribute: .top, multiplier: 1.0, constant: 10))
        
        dynamicBarChartTopConstraint = NSLayoutConstraint(item: barChart, attribute: .top, relatedBy: .equal, toItem: barChartLabel, attribute: .bottom, multiplier: 1.0, constant: 0)
        dynamicBarChartBottomConstraint = NSLayoutConstraint(item: barChart, attribute: .bottom, relatedBy: .equal, toItem: barChartView, attribute: .bottom, multiplier: 1.0, constant: -20)
        barChartView.addConstraint(dynamicBarChartTopConstraint)
        barChartView.addConstraint(dynamicBarChartBottomConstraint)
        barChartView.addConstraint(NSLayoutConstraint(item: barChart, attribute: .width, relatedBy: .equal, toItem: barChartView, attribute: .width, multiplier: 1.0, constant: 0))
        barChartView.addConstraint(NSLayoutConstraint(item: barChart, attribute: .left, relatedBy: .equal, toItem: barChartView, attribute: .left, multiplier: 1.0, constant: 0))
        
        
        self.view.addConstraint(NSLayoutConstraint(item: investmentLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: investmentLabel, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addConstraint(NSLayoutConstraint(item: kpiLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: kpiLabel, attribute: .bottom, relatedBy: .equal, toItem: investmentLabel, attribute: .top, multiplier: 1.0, constant: 0))
        
        
        self.vitoStatementView.alpha = 0
        self.programDataButton.alpha = 0
        self.shareButton.alpha = 0
        self.infoButton.alpha = 0
        self.slidersView.alpha = 0
        self.barChartLabel.alpha = 0
        self.trialBar.text = ""
        self.loyaltyBar.text = ""
        self.volumeBar.text = ""
        self.trialBar.title = ""
        self.loyaltyBar.title = ""
        self.volumeBar.title = ""
        self.barChart.titlePadding = 5
        self.barChart.textPadding = 2
        self.barChart.barSpacing = 10
        
        updateSliders()
        updateVitoStatement()
        updateBarGraph()
        displayedScenario.tlvIsHidden = !displayedScenario.tlvIsHidden
        hideTLVSlidersButtonPressed()

        
        kpiLabel.text = "\(selectedEfficiency.rawValue): \(efficiencySliderView.amountLabel.text!)"
        viewDidLoadInitialAnalytics = false
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if(hideBottomLabels){
            kpiLabel.alpha = 0
            investmentLabel.alpha = 0
        }
        if(displayedScenario.isLocked){ lockAllSliders() }
    }
    func setupConstraintsNormalMode(_ showAnimation: Bool){
        dynamicTopViewHeightConstraint.constant = self.view.frame.size.height * 0.3
        dynamicBarChartViewBottomConstraint.constant =  -35
        dynamicBarChartViewLeftConstraint.constant = (self.view.frame.size.width * 0.55) + 20
        dynamicBarChartViewRightConstraint.constant = -20
        dynamicBarChartBottomConstraint.constant = 0
        dynamicBarChartTopConstraint.constant = 10
        hideBottomLabels = true
        if(showAnimation){
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
                self.vitoStatementView.alpha = 1
                self.programDataButton.alpha = 1
                self.calendarButton.isHidden = false
                self.exportButton.alpha = 1
                self.shareButton.alpha = 1
                self.infoButton.alpha = 1
                self.slidersView.alpha = 1
                self.barChartLabel.alpha = 1
                self.trialBar.title = "TRIAL"
                self.loyaltyBar.title = "LOYALTY"
                self.volumeBar.title = "VOLUME"
                self.barChart.titlePadding = 10
                self.barChart.textPadding = 2
                self.barChart.barSpacing = 40
                self.scenarioNameTextField.font = UIFont.catalinaBoldFontOfSize(20)
                self.updateBarGraph()
            })
        }
        else{
            self.view.layoutIfNeeded()
            self.vitoStatementView.alpha = 1
            self.programDataButton.alpha = 1
            self.calendarButton.isHidden = false
            self.exportButton.alpha = 1
            self.shareButton.alpha = 1
            self.infoButton.alpha = 1
            self.slidersView.alpha = 1
            self.barChartLabel.alpha = 1
            self.trialBar.title = "TRIAL"
            self.loyaltyBar.title = "LOYALTY"
            self.volumeBar.title = "VOLUME"
            self.barChart.titlePadding = 10
            self.barChart.textPadding = 2
            self.barChart.barSpacing = 40
            
            self.updateBarGraph()
        }
    }
    func setupConstraintsPortfolioMode(_ showAnimation: Bool){
        dynamicTopViewHeightConstraint.constant = self.view.frame.size.height * 0.2
        dynamicBarChartViewTopConstraint.constant =  0
        dynamicBarChartViewLeftConstraint.constant = 40
        dynamicBarChartViewRightConstraint.constant = -40
        dynamicBarChartBottomConstraint.constant = -20
        dynamicBarChartTopConstraint.constant = 0
        hideBottomLabels = false
        if(showAnimation){
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.kpiLabel.alpha = 1
                self.investmentLabel.alpha = 1
                self.vitoStatementView.alpha = 0
                self.programDataButton.alpha = 0
                self.calendarButton.isHidden = true
                self.exportButton.alpha = 0
                self.shareButton.alpha = 0
                self.infoButton.alpha = 0
                self.slidersView.alpha = 0
                self.barChartLabel.alpha = 0
                self.trialBar.text = ""
                self.loyaltyBar.text = ""
                self.volumeBar.text = ""
                self.trialBar.title = ""
                self.loyaltyBar.title = ""
                self.volumeBar.title = ""
                self.barChart.titlePadding = 5
                self.barChart.textPadding = 2
                self.barChart.barSpacing = 10
                self.scenarioNameTextField.font = UIFont.catalinaBoldFontOfSize(14)
                self.view.layoutIfNeeded()
                self.barChart.update()
            })
        }
        else{
            self.kpiLabel.alpha = 1
            self.investmentLabel.alpha = 1
            self.vitoStatementView.alpha = 0
            self.programDataButton.alpha = 0
            self.calendarButton.isHidden = true
            self.exportButton.alpha = 0
            self.shareButton.alpha = 0
            self.infoButton.alpha = 0
            self.slidersView.alpha = 0
            self.barChartLabel.alpha = 0
            self.trialBar.text = ""
            self.loyaltyBar.text = ""
            self.volumeBar.text = ""
            self.trialBar.title = ""
            self.loyaltyBar.title = ""
            self.volumeBar.title = ""
            self.barChart.titlePadding = 5
            self.barChart.textPadding = 2
            self.barChart.barSpacing = 10
            self.view.layoutIfNeeded()
            self.barChart.update()
        }
    }
    
    func hideViewsForPortfolioView(){
        self.vitoStatementView.alpha = 0
        self.programDataButton.alpha = 0
        self.calendarButton.isHidden = true
        self.shareButton.alpha = 0
        self.infoButton.alpha = 0
        self.slidersView.alpha = 0
        self.barChartLabel.alpha = 0
        self.kpiLabel.alpha = 0
        self.investmentLabel.alpha = 0
        self.exportButton.alpha = 0
    }
    func calendarButtonPressed(){
        let vc = CalendarViewController()
        vc.scenario = displayedScenario
        portVC.present(vc, animated: true, completion: nil)
        AnalyticsController.sharedAnalyticsController.sendScreenDataToAnalytics("Calendar")
    }
    func exportButtonPressed(){
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            let csvObj = CSVCreator()
            let csvString = csvObj.createCSVForScenario(displayedScenario)
            let data = csvString.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)
            UIGraphicsBeginImageContextWithOptions(view.frame.size, true, 0.0);
            view.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            //   UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            let imgData = UIImageJPEGRepresentation(image!, 1.0)
            UIGraphicsEndImageContext()
            
            let calendarViewController = CalendarViewController()
            calendarViewController.scenario = displayedScenario
            UIGraphicsBeginImageContextWithOptions(calendarViewController.view.frame.size, true, 0.0);
            calendarViewController.view.layer.render(in: UIGraphicsGetCurrentContext()!)
            let calImage = UIGraphicsGetImageFromCurrentImageContext()
            //   UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            let calendarImageData = UIImageJPEGRepresentation(calImage!, 1.0)
            UIGraphicsEndImageContext()
            
            
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            //Set the subject and message of the email
            mailComposer.setSubject("Investment Planner - \(displayedScenario.name)")
          //  mailComposer.setMessageBody("", isHTML: false)

            if let csvData = data{
                mailComposer.addAttachmentData(csvData, mimeType: "text/csv", fileName: "\(displayedScenario.name).csv")
            }
            if let imageData = imgData{
                mailComposer.addAttachmentData(imageData, mimeType: "image/jpeg", fileName: "\(displayedScenario.name).jpeg")
                
            }
            if let calImageData = calendarImageData{
                mailComposer.addAttachmentData(calImageData, mimeType: "image/jpeg", fileName: "\(displayedScenario.name)Calendar.jpeg")
//                let fileManager = NSFileManager.defaultManager()
//                fileManager.createFileAtPath("/Volumes/Catalina/OptimizedROI/\(displayedScenario.name)Calendar.jpeg", contents: calImageData, attributes: nil)
            }
            
            self.present(mailComposer, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Can not export", message: "Please open a remedy force ticket regarding your mail application.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default) {(action) in
                alert.dismiss(animated: true, completion: nil)
                })
            self.present(alert, animated: true, completion: nil)
        }
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Mail Button Pressed", action: "Growth Plan Email Started", label: "", value: 0)
        
    }
    func menuButtonPressed(){
         _ = self.navigationController?.popViewController(animated: true)
    }
    func programDataButtonPressed(){
        let vc = DesignInputsViewController(scenario: displayedScenario)
        portVC.present(vc, animated: true, completion: nil)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Program Data Button Pressed", action: "Growth Plan Program Data Shown", label: "", value: 0)

    }
    func shareButtonPressed(){
        let share = ShareViewController(scenarioToShare: displayedScenario)
        share.modalPresentationStyle = .formSheet
        self.present(share, animated: true, completion: nil)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Share Button Pressed", action: "Growth Plan Share Started", label: "", value: 0)

    }
    func infoButtonPressed(){
        portVC.showPageDefinitions(pageDefinitions, animated: true)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Info Button Pressed", action: "Growth Plan Definitions Shown", label: "", value: 0)

    }
    func hideTLVSlidersButtonPressed(){
        if displayedScenario.tlvIsHidden {
            displayedScenario.tlvIsHidden = false
            self.dynamicEfficiencySliderTopConstraint.constant =  30
            self.dynamicBudgetSliderBottomConstraint.constant = -30
            UIView.animate(withDuration: 0.25, animations: {
                self.slidersView.layoutIfNeeded()
                self.hideTLVSlidersButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                
                }, completion: { finished in
                    
                    if finished {
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            self.trialSliderView.alpha = 1
                            self.loyaltySliderView.alpha = 1
                            self.volumeSliderView.alpha = 1
                        })
                    }
            })
        }
        else{
            displayedScenario.tlvIsHidden = true
            UIView.animate(withDuration: 0.25, animations: {
                self.trialSliderView.alpha = 0
                self.loyaltySliderView.alpha = 0
                self.volumeSliderView.alpha = 0
                self.hideTLVSlidersButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                }, completion: { finished in
                    
                    if finished {
                        self.dynamicEfficiencySliderTopConstraint.constant = self.efficiencySliderView.frame.size.height + 30
                        self.dynamicBudgetSliderBottomConstraint.constant = -self.efficiencySliderView.frame.size.height - 30
                        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                            self.slidersView.layoutIfNeeded()
                        })
                    }
            })
        }
        if self.viewDidLoadInitialAnalytics == false{
            if(displayedScenario.tlvIsHidden){
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Hide/Unhide TLV Sliders Button Pressed", action: "TLV Sliders Hidden", label: "", value: 0)
            }
            else{
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Hide/Unhide TLV Sliders Button Pressed", action: "TLV Sliders Unhidden", label: "", value: 0)
            }
        }
    }
    func sliderValueChanged(_ sender: UISlider){
        if(isOptimizing == false){
            if(displayedScenario.isLocked){ displayedScenario.isLocked = false }
            var efficiencyAmount: Double?
            var trialPercent: Double?
            var loyaltyPercent: Double?
            var volumePercent: Double?
            var budgetAmount: Double?
            if(sender.tag == MovingSliderTag.budgetSliderTag.rawValue){
                movingSliderTag = .budgetSliderTag
                if efficiencySliderView.isLocked == true{
                    efficiencyAmount = Double(round(efficiencySliderView.slider.value))
                }
                if trialSliderView.isLocked == true{
                    trialPercent = Double(round(trialSliderView.slider.value))
                }
                if loyaltySliderView.isLocked == true {
                    loyaltyPercent = Double(round(loyaltySliderView.slider.value))
                }
                if volumeSliderView.isLocked == true {
                    volumePercent = Double(round(volumeSliderView.slider.value))
                }
                    budgetAmount = Double(round(budgetSliderView.slider.value / 100000) * 100000)
            }
            else if(sender.tag == MovingSliderTag.efficiencySliderTag.rawValue){
                movingSliderTag = .efficiencySliderTag
                efficiencyAmount = Double(round(efficiencySliderView.slider.value))
    
                if trialSliderView.isLocked == true{
                    trialPercent = Double(round(trialSliderView.slider.value))
                }
                if loyaltySliderView.isLocked == true {
                    loyaltyPercent = Double(round(loyaltySliderView.slider.value))
                }
                if volumeSliderView.isLocked == true {
                    volumePercent = Double(round(volumeSliderView.slider.value))
                }
                if budgetSliderView.isLocked == true{
                    budgetAmount = Double(round(budgetSliderView.slider.value / 100000) * 100000)
                }
            }
            else if(sender.tag == MovingSliderTag.trialSliderTag.rawValue){
                movingSliderTag = .trialSliderTag
                if efficiencySliderView.isLocked == true{
                    efficiencyAmount = Double(round(efficiencySliderView.slider.value))
                }
                
                trialPercent = Double(round(trialSliderView.slider.value))
                
                if loyaltySliderView.isLocked == true {
                    loyaltyPercent = Double(round(loyaltySliderView.slider.value))
                }
                if volumeSliderView.isLocked == true {
                    volumePercent = Double(round(volumeSliderView.slider.value))
                }
                if budgetSliderView.isLocked == true{
                    budgetAmount = Double(round(budgetSliderView.slider.value / 100000) * 100000)
                }
            }
            else if(sender.tag == MovingSliderTag.loyatySliderTag.rawValue){
                movingSliderTag = .loyatySliderTag
                if efficiencySliderView.isLocked == true{
                    efficiencyAmount = Double(round(efficiencySliderView.slider.value))
                }
                if trialSliderView.isLocked == true{
                    trialPercent = Double(round(trialSliderView.slider.value))
                }
                loyaltyPercent = Double(round(loyaltySliderView.slider.value))
                
                if volumeSliderView.isLocked == true {
                    volumePercent = Double(round(volumeSliderView.slider.value))
                }
                if budgetSliderView.isLocked == true{
                    budgetAmount = Double(round(budgetSliderView.slider.value / 100000) * 100000)
                }
            }
            else if(sender.tag == MovingSliderTag.volumeSliderTag.rawValue){
                movingSliderTag = .volumeSliderTag
                if efficiencySliderView.isLocked == true{
                    efficiencyAmount = Double(round(efficiencySliderView.slider.value))
                }
                if trialSliderView.isLocked == true{
                    trialPercent = Double(round(trialSliderView.slider.value))
                }
                if loyaltySliderView.isLocked == true {
                    loyaltyPercent = Double(round(loyaltySliderView.slider.value))
                }
                
                volumePercent = Double(round(volumeSliderView.slider.value))
                
                if budgetSliderView.isLocked == true{
                    budgetAmount = Double(round(budgetSliderView.slider.value / 100000) * 100000)
                }
            }
            optimzeForSelectedEfficiency(efficiencyAmount, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, budgetAmount: budgetAmount)
            
            updateScreen()
        }
    }
    func saveSliderValue(){
                if(SolverController.sharedSolverController.checkLastSolveWasSuccessful() == false) {
                    if(movingSliderTag == MovingSliderTag.efficiencySliderTag){
                        if(selectedEfficiency == .cpium){
                            efficiencySliderView.setAmountLabelText(displayedScenario.cpium.currencyString(2))
                            efficiencySliderView.slider.setValue(Float(lastSolvedEfficiencyAmount), animated: true)
                        }
                        else if(selectedEfficiency == .cpum){
                            efficiencySliderView.setAmountLabelText(displayedScenario.cpum.currencyString(2))
                            efficiencySliderView.slider.setValue(Float(lastSolvedEfficiencyAmount), animated: true)
                        }
                        else if(selectedEfficiency == .profitROI){
                            efficiencySliderView.setAmountLabelText(displayedScenario.profitRoi.currencyString(2))
                            efficiencySliderView.slider.setValue(Float(lastSolvedEfficiencyAmount), animated: true)
                        }
                        else if(selectedEfficiency == .retailROI){
                            efficiencySliderView.setAmountLabelText(displayedScenario.retailRoi.currencyString(2))
                            efficiencySliderView.slider.setValue(Float(lastSolvedEfficiencyAmount), animated: true)
                        }
                    }
                    else if(movingSliderTag == MovingSliderTag.trialSliderTag){
                        trialSliderView.setAmountLabelText(displayedScenario.trialPercent.percentString())
                        trialSliderView.slider.setValue(Float(lastSolvedTrialAmount), animated: true)
                    }
                    else if(movingSliderTag == MovingSliderTag.loyatySliderTag){
                        loyaltySliderView.setAmountLabelText(displayedScenario.loyaltyPercent.percentString())
                        loyaltySliderView.slider.setValue(Float(lastSolvedLoyaltyAmount), animated: true)
                    }
                    else if(movingSliderTag == MovingSliderTag.volumeSliderTag){
                        volumeSliderView.setAmountLabelText(displayedScenario.volumePercent.percentString())
                        volumeSliderView.slider.setValue(Float(lastSolvedVolumeAmount), animated: true)
                    }
                    else if(movingSliderTag == MovingSliderTag.budgetSliderTag){
                        budgetSliderView.setAmountLabelText(displayedScenario.totalBudget.abbreviatedCurrencyString())
                        budgetSliderView.slider.setValue(Float(lastSolvedBudgetAmount), animated: true)
                    }
                    efficiencySliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
                    trialSliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
                    loyaltySliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
                    volumeSliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
                    budgetSliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
                }
                movingSliderTag = nil
                _ = displayedScenario.program.report.save()
        investmentLabel.text = "Total Investment: \(displayedScenario.totalInvestment.abbreviatedCurrencyString())"
        kpiLabel.text = "\(selectedEfficiency.rawValue): \(efficiencySliderView.amountLabel.text!)"
    }
    
    func optimzeForSelectedEfficiency(_ efficiencyAmount: Double?, trialPercent: Double?, loyaltyPercent: Double?, volumePercent: Double?, budgetAmount: Double?){
        
        var efficiencyAmountConverted:Double?
        if(efficiencyAmount != nil){
            efficiencyAmountConverted = efficiencyAmount! / 100.0
        }
        if selectedEfficiency == .retailROI {
            SolverController.sharedSolverController.optimalRetailRoiForConstraints(displayedScenario, retailRoi: efficiencyAmountConverted, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: budgetAmount)
        }
        else if selectedEfficiency == .profitROI {
            SolverController.sharedSolverController.optimalProfitRoiForConstraints(displayedScenario, profitRoi: efficiencyAmountConverted, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: budgetAmount)
        }
        else if selectedEfficiency == .cpum {
            SolverController.sharedSolverController.optimalCpumForConstraints(displayedScenario, cpum: efficiencyAmountConverted, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: budgetAmount)
        }
        else if selectedEfficiency == .cpium {
            SolverController.sharedSolverController.optimalCpiumForConstraints(displayedScenario, cpium: efficiencyAmountConverted, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: budgetAmount)
        }
    }
    func updateVitoStatement(){
        var kpiAmountString = ""
        var efficiencyAmountString = ""
        if(selectedKPI == .acquiredBuyers){
            kpiAmountString = (displayedScenario.totalAcquiredBuyers).abbreviatedNumberString()
        }
        else if(selectedKPI == .incrementalDollars){
            kpiAmountString = (displayedScenario.totalIncrementalDollars).abbreviatedCurrencyString()
        }
        else if(selectedKPI == .incrementalUnits){
            kpiAmountString = (displayedScenario.totalIncrementalUnits).abbreviatedNumberString()
        }
        else if(selectedKPI == .totalUnits){
            kpiAmountString = (displayedScenario.totalUnits).abbreviatedNumberStringRounded()
        }
        else if(selectedKPI == .totalDollars){
            kpiAmountString = (displayedScenario.totalDollars).abbreviatedCurrencyString()
        }
        if(selectedEfficiency == .profitROI){
            efficiencyAmountString = displayedScenario.profitRoi.currencyString(2)
        }
        else if(selectedEfficiency == .retailROI){
            efficiencyAmountString = displayedScenario.retailRoi.currencyString(2)
        }
        else if(selectedEfficiency == .cpum){
            efficiencyAmountString = displayedScenario.cpum.currencyString(2)
        }
        else if(selectedEfficiency == .cpium){
            efficiencyAmountString = displayedScenario.cpium.currencyString(2)
        }
        vitoStatementView.setupVitoStatement(kpiAmountString, kpiType: selectedKPI.rawValue, efficiencyAmount: efficiencyAmountString, efficiencyType: selectedEfficiency.rawValue)
    }
    func updateBarGraph(){
        
        var trialValue: Double = 0
        var loyaltyValue: Double = 0
        var volumeValue: Double = 0
        var trialPercent: Double = 0
        var loyaltyPercent: Double = 0
        var volumePercent: Double = 0
        var isKPIDollarBased = false
        if(selectedKPI == .incrementalUnits){
            trialValue = displayedScenario.getTotalIncrementalUnitsForType(.Trial)
            loyaltyValue = displayedScenario.getTotalIncrementalUnitsForType(.Loyalty)
            volumeValue = displayedScenario.getTotalIncrementalUnitsForType(.Volume)
        }
        else if(selectedKPI == .incrementalDollars){
            trialValue = displayedScenario.getTotalIncrementalDollarsForType(.Trial)
            loyaltyValue = displayedScenario.getTotalIncrementalDollarsForType(.Loyalty)
            volumeValue = displayedScenario.getTotalIncrementalDollarsForType(.Volume)
            isKPIDollarBased = true
        }
        else if(selectedKPI == .acquiredBuyers){
            trialValue = displayedScenario.getTotalAcquiredBuyersForType(.Trial)
            loyaltyValue = displayedScenario.getTotalAcquiredBuyersForType(.Loyalty)
            volumeValue = displayedScenario.getTotalAcquiredBuyersForType(.Volume)
        }
        else if(selectedKPI == .totalUnits){
            trialValue = displayedScenario.getTotalUnitsForType(.Trial)
            loyaltyValue = displayedScenario.getTotalUnitsForType(.Loyalty)
            volumeValue = displayedScenario.getTotalUnitsForType(.Volume)
        }
        else if(selectedKPI == .totalDollars){
            trialValue = displayedScenario.getTotalDollarsForType(.Trial)
            loyaltyValue = displayedScenario.getTotalDollarsForType(.Loyalty)
            volumeValue = displayedScenario.getTotalDollarsForType(.Volume)
            isKPIDollarBased = true
        }
        var highValue: Double = trialValue
        if(loyaltyValue > highValue){
            highValue = loyaltyValue
        }
        if(volumeValue > highValue){
            highValue = volumeValue
        }
        if(trialValue > 0){
            trialPercent = (trialValue / highValue) * 100.0
        }
        if(loyaltyValue > 0){
            loyaltyPercent = (loyaltyValue / highValue) * 100.0
        }
        if(volumeValue > 0){
            volumePercent = (volumeValue / highValue) * 100.0
        }
        trialBar.value = trialPercent
        loyaltyBar.value = loyaltyPercent
        volumeBar.value = volumePercent
        barChart.maxBarValue = 100
        if(isKPIDollarBased){
            trialBar.text = (trialValue.abbreviatedCurrencyString())
            loyaltyBar.text = (loyaltyValue.abbreviatedCurrencyString())
            volumeBar.text = (volumeValue.abbreviatedCurrencyString())
        }
        else{
            trialBar.text = (trialValue.abbreviatedNumberString())
            loyaltyBar.text = (loyaltyValue.abbreviatedNumberString())
            volumeBar.text = (volumeValue.abbreviatedNumberString())
        }
        barChart.update()
    }
    func updateSliders(){
        if(selectedEfficiency == .profitROI){
            efficiencySliderView.slider.maximumValue = Float(displayedScenario.maxProfitRoi) * 100
            efficiencySliderView.slider.minimumValue = Float(displayedScenario.minProfitRoi) * 100
            efficiencySliderView.slider.setValue(Float(round(displayedScenario.profitRoi * 100)), animated: false)
            efficiencySliderView.setAmountLabelText(displayedScenario.profitRoi.currencyString(2))
        }
        else if(selectedEfficiency == .retailROI){
            efficiencySliderView.slider.maximumValue = Float(displayedScenario.maxRetailRoi) * 100
            efficiencySliderView.slider.minimumValue = Float(displayedScenario.minRetailRoi) * 100
            efficiencySliderView.slider.setValue(Float(round(displayedScenario.retailRoi * 100)), animated: false)
            efficiencySliderView.setAmountLabelText(displayedScenario.retailRoi.currencyString(2))
        }
        else if(selectedEfficiency == .cpium){
            efficiencySliderView.slider.maximumValue = Float(displayedScenario.maxCpium) * 100
            efficiencySliderView.slider.minimumValue = Float(displayedScenario.minCpium) * 100
            efficiencySliderView.slider.setValue(Float(round(displayedScenario.cpium * 100)), animated: false)
            efficiencySliderView.setAmountLabelText(displayedScenario.cpium.currencyString(2))
        }
        else if(selectedEfficiency == .cpum){
            efficiencySliderView.slider.maximumValue = Float(displayedScenario.maxCpum) * 100
            efficiencySliderView.slider.minimumValue = Float(displayedScenario.minCpum) * 100
            efficiencySliderView.slider.setValue(Float(round(displayedScenario.cpum * 100)), animated: false)
            efficiencySliderView.setAmountLabelText(displayedScenario.cpum.currencyString(2))
        }
        efficiencySliderView.setDescriptionLabelText(selectedEfficiency.rawValue)
        trialSliderView.slider.setValue(Float(round(displayedScenario.trialPercent * 100)), animated: true)
        trialSliderView.setAmountLabelText(displayedScenario.trialPercent.percentString())
        loyaltySliderView.slider.setValue(Float(round(displayedScenario.loyaltyPercent * 100)), animated: true)
        loyaltySliderView.setAmountLabelText(displayedScenario.loyaltyPercent.percentString())
        volumeSliderView.slider.setValue(Float(round(displayedScenario.volumePercent * 100)), animated: true)
        volumeSliderView.setAmountLabelText(displayedScenario.volumePercent.percentString())
        budgetSliderView.slider.setValue(Float(round(displayedScenario.totalBudget)), animated: true)
        budgetSliderView.setAmountLabelText(displayedScenario.totalBudget.abbreviatedCurrencyString())
        
    }
    func updateScreen() {
        if(SolverController.sharedSolverController.checkLastSolveWasSuccessful()) {
            isOptimizing = true
            efficiencySliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
            trialSliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
            loyaltySliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
            volumeSliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
            budgetSliderView.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
            //Check if locked to not change value
            if(efficiencySliderView.isLocked == false){
                if(movingSliderTag != MovingSliderTag.efficiencySliderTag){
                    if(selectedEfficiency == .cpium){
                        efficiencySliderView.slider.setValue(Float(round(displayedScenario.cpium * 100)), animated: false)
                        efficiencySliderView.setAmountLabelText(displayedScenario.cpium.currencyString(2))
                    }
                    else if(selectedEfficiency == .cpum){
                        efficiencySliderView.slider.setValue(Float(round(displayedScenario.cpum * 100)), animated: false)
                        efficiencySliderView.setAmountLabelText(displayedScenario.cpum.currencyString(2))
                    }
                    else if(selectedEfficiency == .retailROI){
                        efficiencySliderView.slider.setValue(Float(round(displayedScenario.retailRoi * 100)), animated: false)
                        efficiencySliderView.setAmountLabelText(displayedScenario.retailRoi.currencyString(2))
                    }
                    else if(selectedEfficiency == .profitROI){
                        efficiencySliderView.slider.setValue(Float(round(displayedScenario.profitRoi * 100)), animated: false)
                        efficiencySliderView.setAmountLabelText(displayedScenario.profitRoi.currencyString(2))
                    }
                }
                else{
                    if(selectedEfficiency == .cpium){
                        efficiencySliderView.setAmountLabelText(displayedScenario.cpium.currencyString(2))
                        lastSolvedEfficiencyAmount = round(Double(efficiencySliderView.slider.value))
                    }
                    else if(selectedEfficiency == .cpum){
                        efficiencySliderView.setAmountLabelText(displayedScenario.cpum.currencyString(2))
                        lastSolvedEfficiencyAmount = round(Double(efficiencySliderView.slider.value))
                    }
                    else if(selectedEfficiency == .retailROI){
                        efficiencySliderView.setAmountLabelText(displayedScenario.retailRoi.currencyString(2))
                        lastSolvedEfficiencyAmount = round(Double(efficiencySliderView.slider.value))
                    }
                    else if(selectedEfficiency == .profitROI){
                        efficiencySliderView.setAmountLabelText(displayedScenario.profitRoi.currencyString(2))
                        lastSolvedEfficiencyAmount = round(Double(efficiencySliderView.slider.value))
                    }
                }
            }
            if(trialSliderView.isLocked == false){
                if(movingSliderTag != MovingSliderTag.trialSliderTag){
                    trialSliderView.slider.setValue(Float(round(displayedScenario.trialPercent * 100)), animated: true)
                    trialSliderView.setAmountLabelText(displayedScenario.trialPercent.percentString())
                }
                else{
                    trialSliderView.setAmountLabelText(displayedScenario.trialPercent.percentString())
                    lastSolvedTrialAmount = round(Double(trialSliderView.slider.value))
                }
            }
            if(loyaltySliderView.isLocked == false){
                if(movingSliderTag != MovingSliderTag.loyatySliderTag){
                    loyaltySliderView.slider.setValue(Float(round(displayedScenario.loyaltyPercent * 100)), animated: true)
                    loyaltySliderView.setAmountLabelText(displayedScenario.loyaltyPercent.percentString())
                }
                else{
                    loyaltySliderView.setAmountLabelText(displayedScenario.loyaltyPercent.percentString())
                    lastSolvedLoyaltyAmount = Double(loyaltySliderView.slider.value)
                }
            }
            if(volumeSliderView.isLocked == false){
                if(movingSliderTag != MovingSliderTag.volumeSliderTag){
                    volumeSliderView.slider.setValue(Float(round(displayedScenario.volumePercent * 100)), animated: true)
                    volumeSliderView.setAmountLabelText(displayedScenario.volumePercent.percentString())
                }
                else{
                    volumeSliderView.setAmountLabelText(displayedScenario.volumePercent.percentString())
                    lastSolvedVolumeAmount = round(Double(volumeSliderView.slider.value))
                }
            }
            if(budgetSliderView.isLocked == false){
                if(movingSliderTag != MovingSliderTag.budgetSliderTag){
                    budgetSliderView.slider.setValue(Float(round(displayedScenario.totalBudget)), animated: true)
                    budgetSliderView.setAmountLabelText(displayedScenario.totalBudget.abbreviatedCurrencyString())
                }
                else{
                    budgetSliderView.setAmountLabelText(displayedScenario.totalBudget.abbreviatedCurrencyString())
                    lastSolvedBudgetAmount = round(Double(budgetSliderView.slider.value))
                }
            }
            updateVitoStatement()
            updateBarGraph()
            isOptimizing = false
        } else {
            efficiencySliderView.amountLabel.textColor = UIColor.catalinaLightBlueColor()
            trialSliderView.amountLabel.textColor = UIColor.catalinaLightBlueColor()
            loyaltySliderView.amountLabel.textColor = UIColor.catalinaLightBlueColor()
            volumeSliderView.amountLabel.textColor = UIColor.catalinaLightBlueColor()
            budgetSliderView.amountLabel.textColor = UIColor.catalinaLightBlueColor()
            if(movingSliderTag == MovingSliderTag.efficiencySliderTag){
                efficiencySliderView.setAmountLabelText(Double(efficiencySliderView.slider.value / 100).currencyString(2))
            }
            else if(movingSliderTag == MovingSliderTag.trialSliderTag){
                trialSliderView.setAmountLabelText(Double(trialSliderView.slider.value / 100).percentString())
            }
            else if(movingSliderTag == MovingSliderTag.loyatySliderTag){
                loyaltySliderView.setAmountLabelText(Double(loyaltySliderView.slider.value / 100).percentString())
            }
            else if(movingSliderTag == MovingSliderTag.volumeSliderTag){
                volumeSliderView.setAmountLabelText(Double(volumeSliderView.slider.value / 100).percentString())
            }
            else if(movingSliderTag == MovingSliderTag.budgetSliderTag){
                budgetSliderView.setAmountLabelText(Double(budgetSliderView.slider.value).abbreviatedCurrencyString())
            }
        }
    }
        func setSelectedKPI(_ type: String){
            if(type == SelectedKPI.acquiredBuyers.rawValue){
                selectedKPI = .acquiredBuyers
            }
            else if(type == SelectedKPI.incrementalDollars.rawValue){
                selectedKPI = .incrementalDollars
            }
            else if(type == SelectedKPI.incrementalUnits.rawValue){
                selectedKPI = .incrementalUnits
            }
            else if(type == SelectedKPI.totalDollars.rawValue){
                selectedKPI = .totalDollars
            }
            else if(type == SelectedKPI.totalUnits.rawValue){
                selectedKPI = .totalUnits
            }
        }
        func setSelectedEfficiency(_ type: String){
            if(type == SelectedEfficiency.cpium.rawValue){
                selectedEfficiency = .cpium
            }
            else if(type == SelectedEfficiency.cpum.rawValue){
                selectedEfficiency = .cpum
            }
            else if(type == SelectedEfficiency.profitROI.rawValue){
                selectedEfficiency = .profitROI
            }
            else if(type == SelectedEfficiency.retailROI.rawValue){
                selectedEfficiency = .retailROI
            }
        }
        func getMinimumBudget(_ scenario: IPScenario)->Double {
            var minBudget = 999999999.0
            var minOutuOCM = 100.0
            for offer in scenario.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.type == .Trial {
                            minBudget = min(minBudget, MINIMUM_TRIAL_EACH * offer.getOfferCostMultiplierForCycle(cycleName))
                        } else if( offer.type == .Loyalty) {
                            minOutuOCM = min( minOutuOCM, offer.getOfferCostMultiplierForCycle(cycleName))
                        } else if( offer.type == .Volume) {
                            minBudget = min(minBudget, MINIMUM_VOLUME_EACH)
                        } else {
                            print("Unknown offer type: \(offer.type)")
                        }
                    }
                }
            }
            return min(minBudget, MINIMUM_OUTU_TOTAL * minOutuOCM)
        }
        func kpiButtonPressed(_ sender:UIButton) {
            popoverTable.modalPresentationStyle = .popover
            popoverTable.setupTable(SelectedKPI.allKPIs)
            if let popover = popoverTable.popoverPresentationController {
                let buttonPoint = sender.superview?.convert(sender.center, to: self.view)
                popover.delegate = self
                popover.sourceRect = CGRect(x: buttonPoint!.x, y: buttonPoint!.y, width: 0, height: 0)
                popover.sourceView = self.view
                popover.permittedArrowDirections = .up
                self.present(popoverTable, animated:true, completion: nil)
            }
        }
    
        func efficiencyButtonPressed(_ sender:UIButton) {
            popoverTable.modalPresentationStyle = .popover
            popoverTable.setupTable(SelectedEfficiency.allEfficiencies)
    
            if let popover = popoverTable.popoverPresentationController {
                let buttonPoint = sender.superview?.convert(sender.center, to: self.view)
                popover.delegate = self
                popover.sourceRect = CGRect(x: buttonPoint!.x, y: buttonPoint!.y, width: 0, height: 0)
                popover.sourceView = self.view
                popover.permittedArrowDirections = .up
                self.present(popoverTable, animated:true, completion: nil)
            }
        }
    func lockButtonPressed(_ sender: UIButton){
        var sliderView:SliderView!
        if(sender.tag == MovingSliderTag.efficiencySliderTag.rawValue){
            sliderView = efficiencySliderView
        }
        else if(sender.tag == MovingSliderTag.trialSliderTag.rawValue){
            sliderView = trialSliderView
        }
        else if(sender.tag == MovingSliderTag.loyatySliderTag.rawValue){
            sliderView = loyaltySliderView
        }
        else if(sender.tag == MovingSliderTag.volumeSliderTag.rawValue){
            sliderView = volumeSliderView
        }
        else if(sender.tag == MovingSliderTag.budgetSliderTag.rawValue){
            sliderView = budgetSliderView
        }
        
        if(sliderView.isLocked){
            if(sender.tag == MovingSliderTag.trialSliderTag.rawValue || sender.tag == MovingSliderTag.loyatySliderTag.rawValue || sender.tag == MovingSliderTag.volumeSliderTag.rawValue){
                if(trialSliderView.isLocked == true && loyaltySliderView.isLocked == true && volumeSliderView.isLocked == true){
                    unlockSliderView(trialSliderView)
                    unlockSliderView(loyaltySliderView)
                    unlockSliderView(volumeSliderView)
                }
                else{
                    unlockSliderView(sliderView)
                }
            }
            else{
                unlockSliderView(sliderView)
            }
        }
        else{
            if(sender.tag == MovingSliderTag.trialSliderTag.rawValue || sender.tag == MovingSliderTag.loyatySliderTag.rawValue || sender.tag == MovingSliderTag.volumeSliderTag.rawValue){
                let slidersArray = [trialSliderView, loyaltySliderView, volumeSliderView]
                var numberOfLocked = 1
                for j in 0...2 {
                    let sView = slidersArray[j]! as SliderView
                    if(sView.isLocked == true){
                        numberOfLocked += 1
                    }
                }
                if(numberOfLocked == 2){
                    for j in 0...2 {
                        let sView = slidersArray[j]! as SliderView
                        lockSliderView(sView)
                    }
                }
                else{
                    lockSliderView(sliderView)
                }
            }
            else{
                lockSliderView(sliderView)
            }
        }
    }
    func unlockSliderView(_ sender: SliderView){
        
        sender.isLocked = false
        sender.slider.isUserInteractionEnabled = true
        sender.slider.setThumbImage(UIImage(named: "activated-slider-top.png"), for: UIControlState())
        sender.slider.setMinimumTrackImage(UIImage(named: "activated-slider-bottom.png"), for: UIControlState())
        sender.slider.setMaximumTrackImage(UIImage(named: "activated-slider-bottom.png"), for: UIControlState())
        sender.lockButton.setImage(UIImage(named: "icon-unlock.png"), for: UIControlState())
        sender.amountLabel.textColor = UIColor.catalinaDarkBlueColor()
    }
    func lockSliderView(_ sender: SliderView){
        sender.isLocked = true
        sender.slider.isUserInteractionEnabled = false
        sender.slider.setThumbImage(UIImage(named: "deactivated-slider-top.png"), for: UIControlState())
        sender.slider.setMinimumTrackImage(UIImage(named: "deactivated-slider-bottom.png"), for: UIControlState())
        sender.slider.setMaximumTrackImage(UIImage(named: "deactivated-slider-bottom.png"), for: UIControlState())
        sender.lockButton.setImage(UIImage(named: "icon-lock.png"), for: UIControlState())
        sender.amountLabel.textColor = UIColor.lightGray
    }
    func lockAllSliders(){
        lockSliderView(efficiencySliderView)
        lockSliderView(trialSliderView)
        lockSliderView(loyaltySliderView)
        lockSliderView(volumeSliderView)
        lockSliderView(budgetSliderView)
        lockSliderView(efficiencySliderView)
    }
    func popoverDismissed(){
        let unit = popoverTable.selectedKPIString
        var isEfficiency = false
        var isKPI = false
        for string in SelectedEfficiency.allEfficiencies{
            if (string == unit){
                isEfficiency = true
            }
        }
        if(isEfficiency){
            vitoStatementSelectedMetrics["efficiency"] = unit
            setSelectedEfficiency(unit)
            popoverTable.dismiss(animated: true, completion: nil)
        }
        else{
            for string in SelectedKPI.allKPIs{
                if(string == unit){
                    isKPI = true
                }
            }
            if(isKPI){
                setSelectedKPI(unit)
                vitoStatementSelectedMetrics["kpi"] = unit
                barChartLabel.text = unit.uppercased()
                popoverTable.dismiss(animated: true, completion: nil)
            }
        }
        SettingsController.sharedSettingsController.vitoStatementSelectedMetrics = vitoStatementSelectedMetrics as [String : AnyObject]?
        updateVitoStatement()
        updateSliders()
        updateBarGraph()
        investmentLabel.text = "Total Investment: \(displayedScenario.totalInvestment.abbreviatedCurrencyString())"
        kpiLabel.text = "\(selectedEfficiency.rawValue): \(efficiencySliderView.amountLabel.text!)"
        if unit != ""{
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Efficiency/KPI Button Pressed", action: "New Value: \(unit)", label: "", value: 0)
        }
        else{
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Efficiency/KPI Button Pressed", action: "No Efficiency/KPI Selected", label: "", value: 0)
        }

    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        if let text = textField.text{
            displayedScenario.name = text.capitalized
            _ = displayedScenario.program.report.save()
            scenarioNameTextField.text = displayedScenario.name
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
class SliderView: UIView {
    var slider: SmoothSlider!
    var lockButton: UIButton!
    var descriptionLabel:UILabel!
    var amountLabel:UILabel!
    var isLocked = false
    var lockButtonWidth:CGFloat = 0.0
    init() {
        super.init(frame: CGRect.zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        lockButton = UIButton()
        lockButton.setImage(UIImage(named: "icon-unlock.png"), for: UIControlState())
        lockButton.sizeToFit()
        lockButtonWidth = lockButton.frame.size.width
        lockButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lockButton)
        
        descriptionLabel = UILabel()
        descriptionLabel.font = UIFont.catalinaMediumFontOfSize(18)
        descriptionLabel.text = "Profit ROI"
        descriptionLabel.textColor = UIColor.catalinaDarkBlueColor()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(descriptionLabel)
        
        slider = SmoothSlider()
        slider.setThumbImage(UIImage(named: "activated-slider-top.png"), for: UIControlState())
        slider.setMinimumTrackImage(UIImage(named: "activated-slider-bottom.png"), for: UIControlState())
        slider.setMaximumTrackImage(UIImage(named: "activated-slider-bottom.png"), for: UIControlState())
        slider.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(slider)
        
        amountLabel = UILabel()
        amountLabel.font = UIFont.catalinaHeavyFontOfSize(14)
        amountLabel.textColor = UIColor.catalinaDarkBlueColor()
        amountLabel.text = "$0.14"
        amountLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(amountLabel)
    }
    func setupSlidersForNormalView(){
        self.addConstraint(NSLayoutConstraint(item: lockButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: lockButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 5))
        self.addConstraint(NSLayoutConstraint(item: lockButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: lockButtonWidth))
        
        self.addConstraint(NSLayoutConstraint(item: descriptionLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 5))
        self.addConstraint(NSLayoutConstraint(item: descriptionLabel, attribute: .left, relatedBy: .equal, toItem: lockButton, attribute: .right, multiplier: 1.0, constant: 10))
        
        self.addConstraint(NSLayoutConstraint(item: slider, attribute: .top, relatedBy: .equal, toItem: descriptionLabel, attribute: .bottom, multiplier: 1.0, constant: 5))
        self.addConstraint(NSLayoutConstraint(item: slider, attribute: .left, relatedBy: .equal, toItem: descriptionLabel, attribute: .left, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: amountLabel, attribute: .centerY, relatedBy: .equal, toItem: slider, attribute: .centerY, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: amountLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: -5))
        self.addConstraint(NSLayoutConstraint(item: amountLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 70))
        
        self.addConstraint(NSLayoutConstraint(item: slider, attribute: .right, relatedBy: .equal, toItem: amountLabel, attribute: .left, multiplier: 1.0, constant: -5))
        
        self.addConstraint(NSLayoutConstraint(item: descriptionLabel, attribute: .width, relatedBy: .equal, toItem: slider, attribute: .width, multiplier: 1.0, constant: 0))
    }
    func setAmountLabelText(_ text: String){
        amountLabel.text = text
    }
    func setDescriptionLabelText(_ text: String){
        descriptionLabel.text = text.uppercased()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class DeepDivePopoverTable: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var cellsArray:[String]?
    var tableView = UITableView()
    var selectedKPIString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear

    }
    func setupTable(_ array:[String]){
        cellsArray = array
        let tableHeight = ((self.cellsArray!.count) * 44 <= 600) ? (self.cellsArray!.count) * 44 : 600
        let viewRect = CGRect(x: 0, y: 0, width: 300, height: CGFloat(tableHeight))
        self.preferredContentSize = CGSize(width: viewRect.size.width, height: viewRect.size.height)
        tableView.frame = viewRect
        tableView.delegate = self
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
        tableView.reloadData()
    }
    // use tag for scenario number or program number

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.textLabel?.text = self.cellsArray![(indexPath as NSIndexPath).row]

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let cellText = cell!.textLabel!.text
        selectedKPIString = cellText!
        NotificationCenter.default.post(name: Notification.Name(rawValue: "dismissPopoverTable"), object: nil)
    }
    func numberOfSections(in tableView:UITableView)->Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsArray!.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //Removes bottom space on tableview
        return CGFloat.leastNormalMagnitude
    }
}

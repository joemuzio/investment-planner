//
//  InsightsViewController.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 1/31/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

let availableStrategicRecommendations = [
    "Drive Trial among most valuable buyers",
    "Grow share of spend among brand buyers",
    "Drive efficient sales volume",
    "Re-engage lapsed shoppers",
  //  "Increase brand consumption",
    "Drive trial with heavy category shoppers not buying your brand",
    "Maintain loyal shopper engagement",
//    "Increase brand share",
//    "Drive repeat purchases of new product(s)",
//    "Insulate your buyers from competitive threats",
    "Increase purchase frequency",
//    "Compress purchase cycle"
]
let pageDefinitions = ["Category Buyers Not Buying Brand": "Recency Shoppers across 52 weeks, shows the revenue potential simply by looking at never buyers in the category",
    "Some Category Shoppers Are More Valuable": "Recency Shoppers across 52 weeks, illustrates the importance of targeting heavy category shoppers to drive trial",
    "Cost of Losing Brand Shoppers": "Consistent shoppers across two 52 weeks periods, identifies shoppers of the brand who left the brand in the second period although remain active in the category",
    "Brand Buyers Share of Category Spend": "Recency Shoppers across 52 weeks, illustrates how much  existing brand buyers spend on the remaining category",
    "Maintaining Brand Loyalty": "Consistent shoppers across two 52 week periods, this is a new visualization of the Loyalty Defection Report.  Illustrating the dollar volume loss due to shoppers reducing or leaving the brand entirely",
    "A Small Number of Shoppers Drive Brand Sales": "Recency Shoppers across 52 weeks, this is the Pivotal Shopper/Consumer truths report",
    "Purchase Frequency is the Key Driver of Brand Volume": "Recency Shoppers across 52 weeks, speaks to importance of frequency being the leading strategy to drive brand volume",
    "Catalina Recommendations": "Select from up to 3 statements that support your overall goals/assessment of the brands opportunities"]

var selectedStrategicRecommendations = [String]()

private let leftPositionPercentage: CGFloat = 0.2
private let centerPositionPercentage: CGFloat = 0.5
private let rightPositionPercentage: CGFloat = 0.8
private let insightWidthPercentage: CGFloat = 0.28

private let trialPrimaryColor = UIColor(hexColor: 0x274A80)
private let trialSecondaryColor = UIColor(hexColor: 0x4379CB)
private let trialTertiaryColor = UIColor(hexColor: 0x5799F4)
private let loyaltyPrimaryColor = UIColor(hexColor: 0x00ADEF)
private let loyaltySecondaryColor = UIColor(hexColor: 0x86DCFA)
private let loyaltyTertiaryColor = UIColor(hexColor: 0xBAE6F3)
private let volumePrimaryColor = UIColor(hexColor: 0xA5DA00)
private let volumeSecondaryColor = UIColor(hexColor: 0xD5F472)

class InsightsViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var infoButton: UIButton!
    var trialImageButton: UIButton!
    var loyaltyImageButton: UIButton!
    var volumeImageButton: UIButton!
    
    var trialBarChartInsightView: BarChartInsightView!
    var trialPieChartInsightView: PieChartInsightView!
    var trialRingChartInsightView: RingChartInsightsView!
    var loyaltyPieChartInsightView: PieChartInsightView!
    var loyaltyRingChartInsightView: RingChartInsightsView!
    var volumeBarChartInsightView: BarChartInsightView!
    var volumePieChartInsightView: PieChartInsightView!
    
    var recommendationsContainer: GradientView!
    var recommendationsTitleLable: UILabel!
   // var recommendationsButton: UIButton!
    var recommendationsLabel1: UILabel!
    var recommendationsLabel2: UILabel!
    var recommendationsLabel3: UILabel!
    var recommendationsArrow1: UIImageView!
    var recommendationsArrow2: UIImageView!
    var recommendationsArrow3: UIImageView!
    
    var trialInsightView: InsightView!
    var loyaltyInsightView: InsightView!
    var volumeInsightView: InsightView!
    
    var trialView: UIView!
    var loyaltyView: UIView!
    var volumeView: UIView!
    var dynamicTrialViewHeightConstraint:NSLayoutConstraint!
    var dynamicLoyaltyViewHeightConstraint:NSLayoutConstraint!
    var dynamicVolumeViewHeightConstraint:NSLayoutConstraint!
    
    
    var popoverTable = InsightsPopoverTable()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        //top view
        
        let viewHeight = self.view.frame.size.height - 200
        
        trialView = UIView()
        trialView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(trialView)
        
        let trialSwipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(trialViewSwipe(_:)))
        trialSwipeDownRecognizer.direction = UISwipeGestureRecognizerDirection.down
        trialView.addGestureRecognizer(trialSwipeDownRecognizer)
        
        let trialSwipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(trialViewSwipe(_:)))
        trialSwipeUpRecognizer.direction = UISwipeGestureRecognizerDirection.up
        trialView.addGestureRecognizer(trialSwipeUpRecognizer)
        
        
        dynamicTrialViewHeightConstraint  = NSLayoutConstraint(item: trialView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0)
        self.view.addConstraint(dynamicTrialViewHeightConstraint)
        self.view.addConstraint(NSLayoutConstraint(item: trialView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: viewHeight))
        self.view.addConstraint(NSLayoutConstraint(item: trialView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: trialView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 1 / 3, constant: 0))
        
        loyaltyView = UIView()
        loyaltyView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loyaltyView)
        
        let loyaltySwipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(loyaltyViewSwipe(_:)))
        loyaltySwipeDownRecognizer.direction = UISwipeGestureRecognizerDirection.down
        loyaltyView.addGestureRecognizer(loyaltySwipeDownRecognizer)
        
        let loyaltySwipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(loyaltyViewSwipe(_:)))
        loyaltySwipeUpRecognizer.direction = UISwipeGestureRecognizerDirection.up
        loyaltyView.addGestureRecognizer(loyaltySwipeUpRecognizer)
        
        dynamicLoyaltyViewHeightConstraint  = NSLayoutConstraint(item: loyaltyView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0)
        self.view.addConstraint(dynamicLoyaltyViewHeightConstraint)
        self.view.addConstraint(NSLayoutConstraint(item: loyaltyView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: viewHeight))
        self.view.addConstraint(NSLayoutConstraint(item: loyaltyView, attribute: .left, relatedBy: .equal, toItem: trialView, attribute: .right, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: loyaltyView, attribute: .width, relatedBy: .equal, toItem: trialView, attribute: .width, multiplier: 1.0, constant: 0))
        
        volumeView = UIView()
        volumeView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(volumeView)
        
        let volumeSwipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(volumeViewSwipe(_:)))
        volumeSwipeDownRecognizer.direction = UISwipeGestureRecognizerDirection.down
        volumeView.addGestureRecognizer(volumeSwipeDownRecognizer)
        
        let volumeSwipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(volumeViewSwipe(_:)))
        volumeSwipeUpRecognizer.direction = UISwipeGestureRecognizerDirection.up
        volumeView.addGestureRecognizer(volumeSwipeUpRecognizer)
        
        dynamicVolumeViewHeightConstraint  = NSLayoutConstraint(item: volumeView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0)
        self.view.addConstraint(dynamicVolumeViewHeightConstraint)
        self.view.addConstraint(NSLayoutConstraint(item: volumeView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: viewHeight))
        self.view.addConstraint(NSLayoutConstraint(item: volumeView, attribute: .left, relatedBy: .equal, toItem: loyaltyView, attribute: .right, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: volumeView, attribute: .width, relatedBy: .equal, toItem: trialView, attribute: .width, multiplier: 1.0, constant: 0))
        
        
        let trialImage = UIImage(named: "insights-trial-graphic")!
        trialImageButton = UIButton()
        trialImageButton.setImage(trialImage, for: UIControlState())
        trialImageButton.addTarget(self, action: #selector(InsightsViewController.trialImageButtonPressed), for: .touchUpInside)
        trialImageButton.translatesAutoresizingMaskIntoConstraints = false
        trialView.addSubview(trialImageButton)
        trialView.addConstraint(NSLayoutConstraint(item: trialImageButton, attribute: .top, relatedBy: .equal, toItem: trialView, attribute: .top, multiplier: 1.0, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialImageButton, attribute: .right, relatedBy: .equal, toItem: trialView, attribute: .right, multiplier: 1.0, constant: -10))
        trialView.addConstraint(NSLayoutConstraint(item: trialImageButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: trialImage.size.width))
        trialView.addConstraint(NSLayoutConstraint(item: trialImageButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: trialImage.size.height))
        
        let loyaltyImage = UIImage(named: "insights-loyalty-graphic")!
        loyaltyImageButton = UIButton()
        loyaltyImageButton.setImage(loyaltyImage, for: UIControlState())
        loyaltyImageButton.addTarget(self, action: #selector(InsightsViewController.loyaltyImageButtonPressed), for: .touchUpInside)
        loyaltyImageButton.translatesAutoresizingMaskIntoConstraints = false
        loyaltyView.addSubview(loyaltyImageButton)
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyImageButton, attribute: .top, relatedBy: .equal, toItem: loyaltyView, attribute: .top, multiplier: 1.0, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyImageButton, attribute: .centerX, relatedBy: .equal, toItem: loyaltyView, attribute: .centerX, multiplier: 1.0, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyImageButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: loyaltyImage.size.width))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyImageButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: loyaltyImage.size.height))
        
        let volumeImage = UIImage(named: "insights-volume-graphic")!
        volumeImageButton = UIButton()
        volumeImageButton.setImage(volumeImage, for: UIControlState())
        volumeImageButton.addTarget(self, action: #selector(InsightsViewController.volumeImageButtonPressed), for: .touchUpInside)
        volumeImageButton.translatesAutoresizingMaskIntoConstraints = false
        volumeView.addSubview(volumeImageButton)
        volumeView.addConstraint(NSLayoutConstraint(item: volumeImageButton, attribute: .top, relatedBy: .equal, toItem: volumeView, attribute: .top, multiplier: 1.0, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumeImageButton, attribute: .left, relatedBy: .equal, toItem: volumeView, attribute: .left, multiplier: 1.0, constant: 10))
        volumeView.addConstraint(NSLayoutConstraint(item: volumeImageButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: volumeImage.size.width))
        volumeView.addConstraint(NSLayoutConstraint(item: volumeImageButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: volumeImage.size.height))
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "icon-page-definitions-light-bg"), for: UIControlState())
        infoButton.addTarget(self, action: #selector(InsightsViewController.infoButtonPressed), for: .touchUpInside)
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(infoButton)
        infoButton.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 34))
        infoButton.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 34))
        self.view.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: -20))
        self.view.addConstraint(NSLayoutConstraint(item: infoButton, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 15))
        
        
        
        //insights views
        
        trialBarChartInsightView = BarChartInsightView()
        _ = trialBarChartInsightView.chart.addBarWithValue(1, color: trialPrimaryColor, title: "Heavy Category\nShopper", text: nil)
        _ = trialBarChartInsightView.chart.addBarWithValue(1, color: trialSecondaryColor, title: "Remaining Category\nShopper", text: nil)
        trialBarChartInsightView.baseColor = trialPrimaryColor
        trialBarChartInsightView.titleLabel.text = "Some Category Shoppers Are More Valuable"
        trialBarChartInsightView.isHidden = true
        trialBarChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        trialView.addSubview(trialBarChartInsightView)
        trialView.addConstraint(NSLayoutConstraint(item: trialBarChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: trialImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialBarChartInsightView, attribute: .width, relatedBy: .equal, toItem: trialImageButton, attribute: .width, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialBarChartInsightView, attribute: .top, relatedBy: .equal, toItem: trialImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialBarChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: trialView, attribute: .bottom, multiplier: 1, constant: 0))
        
        trialPieChartInsightView = PieChartInsightView()
        _ = trialPieChartInsightView.chart.addSectionWithValue(1, color: trialPrimaryColor, title: "Never Buyers", text: nil, scaleAmount: nil, translateAmount: nil)
        _ = trialPieChartInsightView.chart.addSectionWithValue(1, color: trialSecondaryColor, title: "Brand Buyers", text: nil, scaleAmount: nil, translateAmount: 5)
        trialPieChartInsightView.baseColor = trialPrimaryColor
        trialPieChartInsightView.titleLabel.text = "Category Buyers Not Buying Brand"
        trialPieChartInsightView.isHidden = true
        trialPieChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        trialView.addSubview(trialPieChartInsightView)
        trialView.addConstraint(NSLayoutConstraint(item: trialPieChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: trialImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialPieChartInsightView, attribute: .width, relatedBy: .equal, toItem: trialImageButton, attribute: .width, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialPieChartInsightView, attribute: .top, relatedBy: .equal, toItem: trialImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialPieChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: trialView, attribute: .bottom, multiplier: 1, constant: 0))
        
        trialRingChartInsightView = RingChartInsightsView()
        _ = trialRingChartInsightView.chart.addSectionWithValue(1, color: trialPrimaryColor, title: "Buyers Lapsed", text: nil, scaleAmount: 5, translateAmount: nil)
       _ = trialRingChartInsightView.chart.addSectionWithValue(1, color: trialSecondaryColor, title: "Retained", text: nil, scaleAmount: nil, translateAmount: nil)
        _ = trialRingChartInsightView.chart.addSectionWithValue(1, color: trialTertiaryColor, title: "Lapsed Category", text: nil, scaleAmount: nil, translateAmount: nil)
        trialRingChartInsightView.baseColor = trialPrimaryColor
        trialRingChartInsightView.titleLabel.text = "The Cost of Losing Brand Shoppers"
        trialRingChartInsightView.isHidden = true
        trialRingChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        trialView.addSubview(trialRingChartInsightView)
        trialView.addConstraint(NSLayoutConstraint(item: trialRingChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: trialImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialRingChartInsightView, attribute: .width, relatedBy: .equal, toItem: trialImageButton, attribute: .width, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialRingChartInsightView, attribute: .top, relatedBy: .equal, toItem: trialImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        trialView.addConstraint(NSLayoutConstraint(item: trialRingChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: trialView, attribute: .bottom, multiplier: 1, constant: 0))
        
        loyaltyPieChartInsightView = PieChartInsightView()
        _ = loyaltyPieChartInsightView.chart.addSectionWithValue(1, color: loyaltyPrimaryColor, title: "Remaining Category Sales", text: nil, scaleAmount: nil, translateAmount: nil)
        _ = loyaltyPieChartInsightView.chart.addSectionWithValue(1, color: loyaltySecondaryColor, title: "Brand Sales", text: nil, scaleAmount: nil, translateAmount: 5)
        loyaltyPieChartInsightView.baseColor = loyaltyPrimaryColor
        loyaltyPieChartInsightView.titleLabel.text = "Brand Buyers Share of Category Spend"
        loyaltyPieChartInsightView.isHidden = true
        loyaltyPieChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        loyaltyView.addSubview(loyaltyPieChartInsightView)
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyPieChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: loyaltyImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyPieChartInsightView, attribute: .width, relatedBy: .equal, toItem: loyaltyImageButton, attribute: .width, multiplier: 1, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyPieChartInsightView, attribute: .top, relatedBy: .equal, toItem: loyaltyImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyPieChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: loyaltyView, attribute: .bottom, multiplier: 1, constant: 0))
        
        loyaltyRingChartInsightView = RingChartInsightsView()
        _ = loyaltyRingChartInsightView.chart.addSectionWithValue(1, color: loyaltyPrimaryColor, title: "Defected", text: nil, scaleAmount: 5, translateAmount: nil)
        _ = loyaltyRingChartInsightView.chart.addSectionWithValue(1, color: loyaltySecondaryColor, title: "Reduced Loyalty", text: nil, scaleAmount: nil, translateAmount: nil)
        _ = loyaltyRingChartInsightView.chart.addSectionWithValue(1, color: loyaltyTertiaryColor, title: "Remain Loyal", text: nil, scaleAmount: nil, translateAmount: nil)
        loyaltyRingChartInsightView.baseColor = loyaltyPrimaryColor
        loyaltyRingChartInsightView.titleLabel.text = "Maintaining Brand Loyalty"
        loyaltyRingChartInsightView.isHidden = true
        loyaltyRingChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        loyaltyView.addSubview(loyaltyRingChartInsightView)
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyRingChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: loyaltyImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyRingChartInsightView, attribute: .width, relatedBy: .equal, toItem: loyaltyImageButton, attribute: .width, multiplier: 1, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyRingChartInsightView, attribute: .top, relatedBy: .equal, toItem: loyaltyImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        loyaltyView.addConstraint(NSLayoutConstraint(item: loyaltyRingChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: loyaltyView, attribute: .bottom, multiplier: 1, constant: 0))
        
        volumeBarChartInsightView = BarChartInsightView()
        _ = volumeBarChartInsightView.chart.addBarWithValue(1, color: volumePrimaryColor, title: "Heavy Buyer", text: nil)
        _ = volumeBarChartInsightView.chart.addBarWithValue(1, color: volumeSecondaryColor, title: "Remaining Buyer", text: nil)
        volumeBarChartInsightView.baseColor = volumePrimaryColor
        volumeBarChartInsightView.titleLabel.text = "Purchase Frequency Is the Key Driver of Brand Volume"
        volumeBarChartInsightView.isHidden = true
        volumeBarChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        volumeView.addSubview(volumeBarChartInsightView)
        volumeView.addConstraint(NSLayoutConstraint(item: volumeBarChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: volumeImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumeBarChartInsightView, attribute: .width, relatedBy: .equal, toItem: volumeImageButton, attribute: .width, multiplier: 1, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumeBarChartInsightView, attribute: .top, relatedBy: .equal, toItem: volumeImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumeBarChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: volumeView, attribute: .bottom, multiplier: 1, constant: 0))
        
        volumePieChartInsightView = PieChartInsightView()
        _ = volumePieChartInsightView.chart.addSectionWithValue(1, color: volumePrimaryColor, title: "Top Buyers", text: nil, scaleAmount: nil, translateAmount: nil)
        _ = volumePieChartInsightView.chart.addSectionWithValue(1, color: volumeSecondaryColor, title: "Remaining Buyers", text: nil, scaleAmount: nil, translateAmount: 5)
        volumePieChartInsightView.baseColor = volumePrimaryColor
        volumePieChartInsightView.titleLabel.text = "A Small Number of Shoppers Drive Brand Sales"
        volumePieChartInsightView.isHidden = true
        volumePieChartInsightView.translatesAutoresizingMaskIntoConstraints = false
        volumeView.addSubview(volumePieChartInsightView)
        volumeView.addConstraint(NSLayoutConstraint(item: volumePieChartInsightView, attribute: .centerX, relatedBy: .equal, toItem: volumeImageButton, attribute: .centerX, multiplier: 1, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumePieChartInsightView, attribute: .width, relatedBy: .equal, toItem: volumeImageButton, attribute: .width, multiplier: 1, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumePieChartInsightView, attribute: .top, relatedBy: .equal, toItem: volumeImageButton, attribute: .bottom, multiplier: 1, constant: 0))
        volumeView.addConstraint(NSLayoutConstraint(item: volumePieChartInsightView, attribute: .bottom, relatedBy: .equal, toItem: volumeView, attribute: .bottom, multiplier: 1, constant: 0))
        
        
        //recommendations view
        
        recommendationsContainer = GradientView()
        recommendationsContainer.colors = [UIColor(hexColor: 0x284C7E).cgColor, UIColor(hexColor: 0x1F78AF).cgColor]
        recommendationsContainer.locations = [0,1]
        recommendationsContainer.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(recommendationsContainer)
        self.view.addConstraint(NSLayoutConstraint(item: recommendationsContainer, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: recommendationsContainer, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: recommendationsContainer, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: recommendationsContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 150))
        
        recommendationsTitleLable = UILabel()
        recommendationsTitleLable.textColor = UIColor.white
        recommendationsTitleLable.text = "CATALINA RECOMMENDATIONS"
        recommendationsTitleLable.font = UIFont.catalinaMediumFontOfSize(24)
        recommendationsTitleLable.sizeToFit()
        recommendationsTitleLable.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsTitleLable)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsTitleLable, attribute: .centerX, relatedBy: .equal, toItem: recommendationsContainer, attribute: .centerX, multiplier: 1.0, constant: 0))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsTitleLable, attribute: .top, relatedBy: .equal, toItem: recommendationsContainer, attribute: .top, multiplier: 1.0, constant: 20))
        
        //        recommendationsButton = UIButton()
        //        recommendationsButton.addTarget(self, action: #selector(InsightsViewController.recommendationsButtonPressed(_:)), forControlEvents: .TouchUpInside)
        //        recommendationsButton.setImage(UIImage(named: "icon-more-dark-bg"), forState: .Normal)
        //        recommendationsButton.sizeToFit()
        //        recommendationsButton.translatesAutoresizingMaskIntoConstraints = false
        //        recommendationsContainer.addSubview(recommendationsButton)
        //        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsButton, attribute: .CenterY, relatedBy: .Equal, toItem: recommendationsTitleLable, attribute: .CenterY, multiplier: 1.0, constant: 0))
        //        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsButton, attribute: .Right, relatedBy: .Equal, toItem: recommendationsContainer, attribute: .Right, multiplier: 1.0, constant: -20))
        
        recommendationsLabel1 = UILabel()
        recommendationsLabel1.text = availableStrategicRecommendations[0]
        recommendationsLabel1.textColor = UIColor.white
        recommendationsLabel1.font = UIFont.catalinaMediumFontOfSize(18)
        recommendationsLabel1.lineBreakMode = .byWordWrapping
        recommendationsLabel1.numberOfLines = 0
        recommendationsLabel1.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsLabel1)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel1, attribute: .centerX, relatedBy: .equal, toItem: recommendationsContainer, attribute: .right, multiplier: leftPositionPercentage, constant: 0))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel1, attribute: .top, relatedBy: .equal, toItem: recommendationsTitleLable, attribute: .bottom, multiplier: 1.0, constant: 30))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel1, attribute: .width, relatedBy: .equal, toItem: recommendationsContainer, attribute: .width, multiplier: insightWidthPercentage, constant: 0))
        
        recommendationsArrow1 = UIImageView(image: UIImage(named: "insights-recommendation-arrow"))
        recommendationsArrow1.sizeToFit()
        recommendationsArrow1.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsArrow1)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsArrow1, attribute: .top, relatedBy: .equal, toItem: recommendationsLabel1, attribute: .top, multiplier: 1.0, constant: 2))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsArrow1, attribute: .right, relatedBy: .equal, toItem: recommendationsLabel1, attribute: .left, multiplier: 1.0, constant: -5))
        
        recommendationsLabel2 = UILabel()
        recommendationsLabel2.text = availableStrategicRecommendations[1]
        recommendationsLabel2.textColor = UIColor.white
        recommendationsLabel2.font = UIFont.catalinaMediumFontOfSize(18)
        recommendationsLabel2.lineBreakMode = .byWordWrapping
        recommendationsLabel2.numberOfLines = 0
        recommendationsLabel2.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsLabel2)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel2, attribute: .centerX, relatedBy: .equal, toItem: recommendationsContainer, attribute: .right, multiplier: centerPositionPercentage, constant: 0))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel2, attribute: .top, relatedBy: .equal, toItem: recommendationsTitleLable, attribute: .bottom, multiplier: 1.0, constant: 30))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel2, attribute: .width, relatedBy: .equal, toItem: recommendationsContainer, attribute: .width, multiplier: insightWidthPercentage, constant: 0))
        
        recommendationsArrow2 = UIImageView(image: UIImage(named: "insights-recommendation-arrow"))
        recommendationsArrow2.sizeToFit()
        recommendationsArrow2.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsArrow2)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsArrow2, attribute: .top, relatedBy: .equal, toItem: recommendationsLabel2, attribute: .top, multiplier: 1.0, constant: 2))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsArrow2, attribute: .right, relatedBy: .equal, toItem: recommendationsLabel2, attribute: .left, multiplier: 1.0, constant: -5))
        
        recommendationsLabel3 = UILabel()
        recommendationsLabel3.text = availableStrategicRecommendations[2]
        recommendationsLabel3.textColor = UIColor.white
        recommendationsLabel3.font = UIFont.catalinaMediumFontOfSize(18)
        recommendationsLabel3.lineBreakMode = .byWordWrapping
        recommendationsLabel3.numberOfLines = 0
        recommendationsLabel3.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsLabel3)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel3, attribute: .centerX, relatedBy: .equal, toItem: recommendationsContainer, attribute: .right, multiplier: rightPositionPercentage, constant: 0))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel3, attribute: .top, relatedBy: .equal, toItem: recommendationsTitleLable, attribute: .bottom, multiplier: 1.0, constant: 30))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsLabel3, attribute: .width, relatedBy: .equal, toItem: recommendationsContainer, attribute: .width, multiplier: insightWidthPercentage, constant: 0))
        
        recommendationsArrow3 = UIImageView(image: UIImage(named: "insights-recommendation-arrow"))
        recommendationsArrow3.sizeToFit()
        recommendationsArrow3.translatesAutoresizingMaskIntoConstraints = false
        recommendationsContainer.addSubview(recommendationsArrow3)
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsArrow3, attribute: .top, relatedBy: .equal, toItem: recommendationsLabel3, attribute: .top, multiplier: 1.0, constant: 2))
        recommendationsContainer.addConstraint(NSLayoutConstraint(item: recommendationsArrow3, attribute: .right, relatedBy: .equal, toItem: recommendationsLabel3, attribute: .left, multiplier: 1.0, constant: -5))
        
        selectedStrategicRecommendations.append(availableStrategicRecommendations[0])
        selectedStrategicRecommendations.append(availableStrategicRecommendations[1])
        selectedStrategicRecommendations.append(availableStrategicRecommendations[2])
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let program = SettingsController.sharedSettingsController.selectedReport?.program {
            
            //trialBarChartInsightView
            if let ratio = program.report.data.setup["Heavy to Remaining Shoppers - Brand Spend Ratio (Insights - Volume)"] as? Double {
                if let amount = program.report.data.setup["Category Dollars - Heavy Shoppers Never Buy Brand (Insights - Trial)"] as? Double {
                    trialBarChartInsightView.statementLabel.text = "Heavy category shoppers are \(ratio.numberString())x as valuable as remaining category shoppers. They spend \(amount.abbreviatedCurrencyString()) on the category and don't purchase your brand"
                }
            }
            if let dollars = program.report.data.setup["Annual Brand Dollars - Heavy (Insights - Volume and Trial)"] as? Double {
                if let bar = trialBarChartInsightView.chart.barWithTitle("Heavy Category\nShopper") {
                    bar.value = dollars
                    bar.text = dollars.currencyString(0)
                }
            }
            if let dollars = program.report.data.setup["Annual Brand Dollars - Remaining (Insights - Volume)"] as? Double {
                if let bar = trialBarChartInsightView.chart.barWithTitle("Remaining Category\nShopper") {
                    bar.value = dollars
                    bar.text = dollars.currencyString(0)
                }
            }
            
            //trialRingChartInsightView
            if let percent = program.report.data.lapsedBuyerBgt["Lapsed Brand Shoppers %"] as? Double {
                if let dollars = program.report.data.lapsedBuyerBgt["Sales Dollars Lost"] as? Double {
                    trialRingChartInsightView.statementLabel.text = "\(percent.percentString()) of shoppers lapsed from \(program.focusBrand), but still purchase the category resulting in a loss of \(dollars.abbreviatedCurrencyString())"
                }
            }
            if let percentRetained = program.report.data.lapsedBuyerBgt["Retained Shoppers %"] as? Double {
                if let section = trialRingChartInsightView.chart.sectionWithTitle("Retained") {
                    section.value = percentRetained
                    section.text = percentRetained.percentString()
                }
                if let percentLapsed = program.report.data.lapsedBuyerBgt["Lapsed Brand Shoppers %"] as? Double {
                    if let section = trialRingChartInsightView.chart.sectionWithTitle("Buyers Lapsed") {
                        section.value = percentLapsed
                        section.text = percentLapsed.percentString()
                    }
                    let categoryLapsed: Double = 1 - (round(percentRetained * 100) / 100) - (round(percentLapsed * 100) / 100)
                    if let section = trialRingChartInsightView.chart.sectionWithTitle("Lapsed Category") {
                        section.value = categoryLapsed
                        section.text = categoryLapsed.percentString()
                    }
                }
            }
            
            //trialPieChartInsightView
            if let neverBuyPercent = program.report.data.setup["Shoppers Never Buy to Total Category (Insights - Trial)"] as? Double {
                if let categoryDollars = program.report.data.setup["Category Dollars - Category Shoppers Never Buy Brand (Insights - Trial)"] as? Double {
                    trialPieChartInsightView.statementLabel.text = "\(neverBuyPercent.percentString()) of all category shoppers, who spend \(categoryDollars.abbreviatedCurrencyString()) in the category, don't purchase \(program.focusBrand)"
                }
                if let section = trialPieChartInsightView.chart.sectionWithTitle("Never Buyers") {
                    section.value = neverBuyPercent
                    section.text = neverBuyPercent.percentString()
                }
                if let section = trialPieChartInsightView.chart.sectionWithTitle("Brand Buyers") {
                    section.value = 1 - neverBuyPercent
                    section.text = (1 - neverBuyPercent).percentString()
                }
            }
            
            //loyaltyPieChartInsightView
            if let brandDollars = program.report.data.setup["Annual Brand Dollars - Total Category (Insights - Trial)"] as? Double {
                if let section = loyaltyPieChartInsightView.chart.sectionWithTitle("Brand Sales") {
                    section.value = brandDollars
                    section.text = brandDollars.currencyString(0)
                }
                if let categoryDollars = program.report.data.setup["Annual Category Dollars - Total Category with Never Buyers"] as? Double {
                    if let section = loyaltyPieChartInsightView.chart.sectionWithTitle("Remaining Category Sales") {
                        section.value = categoryDollars
                        section.text = categoryDollars.currencyString(0)
                    }
                        let calculatedPercent = brandDollars / (brandDollars + categoryDollars)
                        loyaltyPieChartInsightView.statementLabel.text = "\(calculatedPercent.percentString()) of \(program.focusBrand) Shoppers category spend is going to \(program.focusBrand)"
                }
            }
            
            //loyaltyRingChartInsightView
            if let defectedPercent = program.report.data.loyaltyDefection["Fully Defected"] as? Double {
                if let section = loyaltyRingChartInsightView.chart.sectionWithTitle("Defected") {
                    section.value = defectedPercent
                    section.text = defectedPercent.percentString()
                }
                var dollarsLost = 0.0
                var reducedPercent = 0.0
                if let loyalPercent = program.report.data.loyaltyDefection["Remained Loyal"] as? Double {
                    if let dLost = program.report.data.loyaltyDefection["Annual Cost of Defection"] as? Double {
                        dollarsLost = dLost
                        if let section = loyaltyRingChartInsightView.chart.sectionWithTitle("Remain Loyal") {
                            section.value = loyalPercent
                            section.text = loyalPercent.percentString()
                        }
                        if let section = loyaltyRingChartInsightView.chart.sectionWithTitle("Reduced Loyalty") {
                            section.value = 1 - loyalPercent - defectedPercent
                            section.text = (1 - loyalPercent - defectedPercent).percentString()
                            reducedPercent = section.value
                        }
                    }
                    loyaltyRingChartInsightView.statementLabel.text = "\((defectedPercent + reducedPercent).percentString()) of \(program.focusBrand) loyals defected or reduced their loyalty, resulting in an annual loss of \(dollarsLost.abbreviatedCurrencyString())"
                }
            }
            
            //volumeBarChartInsightView
            if let heavyToRemainingBuyerRatio = program.report.data.setup["Heavy to Remaining Shoppers - Brand Trips Ratio (Insights - Volume)"] as? Double {
                if let dollarsPercent = program.report.data.setup["Brand Dollars Ratio - Heavy to Total (Insights - Volume)"] as? Double {
                    volumeBarChartInsightView.statementLabel.text = "Heavy \(program.focusBrand) shoppers, who purchase \(heavyToRemainingBuyerRatio.numberString(1))x as often, drive \(dollarsPercent.percentString()) of brand sales"
                }
            }
            if let heavyBuyerTrips = program.report.data.setup["Brand Trips per Year - Heavy (Insights - Volume)"] as? Double {
                if let bar = volumeBarChartInsightView.chart.barWithTitle("Heavy Buyer") {
                    bar.value = heavyBuyerTrips
                    bar.text = heavyBuyerTrips.numberString(1)
                }
            }
            if let brandTrips = program.report.data.setup["Brand Trips per Year - Remaining (Insights - Volume)"] as? Double {
                if let bar = volumeBarChartInsightView.chart.barWithTitle("Remaining Buyer") {
                    bar.value = brandTrips
                    bar.text = brandTrips.numberString(1)
                }
            }
            
            //volumePieChartInsightView
            if let percent = program.report.data.consumerTruth["% of Static Qual ID's"] as? Double {
                if let dollars = program.report.data.consumerTruth["Brand Dollars per ID"] as? Double {
                    volumePieChartInsightView.statementLabel.text = "\(percent.percentString(1)) of shoppers account for 80% of \(program.focusBrand) volume and spend \(dollars.currencyString(0)) a year on your brand"
                }
                if let section = volumePieChartInsightView.chart.sectionWithTitle("Top Buyers") {
                    section.value = percent
                    section.text = percent.percentString(1)
                }
                if let section = volumePieChartInsightView.chart.sectionWithTitle("Remaining Buyers") {
                    section.value = 1 - percent
                    section.text = nil
                }
            }
        }
        
        if trialPieChartInsightView.isHidden && trialBarChartInsightView.isHidden && trialRingChartInsightView.isHidden {
            trialPieChartInsightView.isHidden = false
        }
        if loyaltyPieChartInsightView.isHidden && loyaltyRingChartInsightView.isHidden {
            loyaltyPieChartInsightView.isHidden = false
        }
        if volumePieChartInsightView.isHidden && volumeBarChartInsightView.isHidden {
            volumePieChartInsightView.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        trialBarChartInsightView.chart.update()
        trialPieChartInsightView.chart.update()
        trialRingChartInsightView.chart.update()
        loyaltyPieChartInsightView.chart.update()
        loyaltyRingChartInsightView.chart.update()
        volumeBarChartInsightView.chart.update()
        volumePieChartInsightView.chart.update()
        
    }
    
    //MARK: - Actions
    
    func infoButtonPressed() {
        let defSection = DefinitionsSection(name: "Definitions", terms: pageDefinitions)
        self.showPageDefinitionsWithSections([defSection], animated: true)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Info Button Pressed", action: "Insights Definitions Shown", label: "", value: 0)
    }
    
    func trialImageButtonPressed() {

        if trialPieChartInsightView.isHidden == false {
            trialPieChartInsightView.isHidden = true
            trialBarChartInsightView.isHidden = false
            selectedStrategicRecommendations[0] = availableStrategicRecommendations[4]
        } else if trialBarChartInsightView.isHidden == false {
            trialBarChartInsightView.isHidden = true
            trialRingChartInsightView.isHidden = false
            selectedStrategicRecommendations[0] = availableStrategicRecommendations[3]
        } else if trialRingChartInsightView.isHidden == false {
            trialRingChartInsightView.isHidden = true
            trialPieChartInsightView.isHidden = false
            selectedStrategicRecommendations[0] = availableStrategicRecommendations[0]
        } else {
            trialPieChartInsightView.isHidden = false
        }
        recommendationsLabel1.text = selectedStrategicRecommendations[0]
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Button Pressed", action: "Trial Insight Changed", label: "", value: 0)
    }
    
    func loyaltyImageButtonPressed() {
        
        if loyaltyPieChartInsightView.isHidden == false {
            loyaltyPieChartInsightView.isHidden = true
            loyaltyRingChartInsightView.isHidden = false
            selectedStrategicRecommendations[1] = availableStrategicRecommendations[5]
        } else if loyaltyRingChartInsightView.isHidden == false {
            loyaltyRingChartInsightView.isHidden = true
            loyaltyPieChartInsightView.isHidden = false
            selectedStrategicRecommendations[1] = availableStrategicRecommendations[1]
        } else {
            loyaltyPieChartInsightView.isHidden = false
        }
        recommendationsLabel2.text = selectedStrategicRecommendations[1]
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Button Pressed", action: "Loyalty Insight Changed", label: "", value: 0)
    }
    
    func volumeImageButtonPressed() {
        
        if volumePieChartInsightView.isHidden == false {
            volumePieChartInsightView.isHidden = true
            volumeBarChartInsightView.isHidden = false
            selectedStrategicRecommendations[2] = availableStrategicRecommendations[6]
        } else if volumeBarChartInsightView.isHidden == false {
            volumeBarChartInsightView.isHidden = true
            volumePieChartInsightView.isHidden = false
            selectedStrategicRecommendations[2] = availableStrategicRecommendations[2]
        } else {
            volumePieChartInsightView.isHidden = false
        }
        recommendationsLabel3.text = selectedStrategicRecommendations[2]
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Button Pressed", action: "Volume Insight Changed", label: "", value: 0)
    }
    
//    func recommendationsButtonPressed(sender: UIButton) {
//        
//        popoverTable.modalPresentationStyle = .Popover
//        popoverTable.setupTable(availableStrategicRecommendations)
//        if let popover = popoverTable.popoverPresentationController {
//            let buttonPoint = sender.superview?.convertPoint(sender.center, toView: self.view)
//            popover.delegate = self
//            popover.sourceRect = CGRectMake(buttonPoint!.x, buttonPoint!.y, 0, 0)
//            popover.sourceView = self.view
//            popover.permittedArrowDirections = .Right
//            self.presentViewController(popoverTable, animated:true, completion: nil)
//        }
//    }
    
    func trialViewSwipe(_ gesture: UIGestureRecognizer){
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                UIView.animate(withDuration: 0.25, animations: {
                    self.trialRingChartInsightView.alpha = 0
                    self.trialBarChartInsightView.alpha = 0
                    self.trialPieChartInsightView.alpha = 0
                    }, completion: { finished in
                        if finished {
                            self.dynamicTrialViewHeightConstraint.constant = self.trialView.frame.size.height - 50
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                self.view.layoutIfNeeded()
                                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Hide / Unhide", action: "Trial Insight Hidden", label: "", value: 0)
                            })
                        }
                })
            case UISwipeGestureRecognizerDirection.up:
                dynamicTrialViewHeightConstraint.constant = 0
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                    }, completion: { finished in
                        if finished {
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                self.trialRingChartInsightView.alpha = 1
                                self.trialBarChartInsightView.alpha = 1
                                self.trialPieChartInsightView.alpha = 1
                                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Hide / Unhide", action: "Trial Insight Unhidden", label: "", value: 0)
                            })
                        }
                })
            default:
                break
            }
        }
    }
    
    
    func loyaltyViewSwipe(_ gesture: UIGestureRecognizer){
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                UIView.animate(withDuration: 0.25, animations: {
                    self.loyaltyPieChartInsightView.alpha = 0
                    self.loyaltyRingChartInsightView.alpha = 0
                    }, completion: { finished in
                        if finished {
                            self.dynamicLoyaltyViewHeightConstraint.constant =  self.loyaltyView.frame.size.height - 50
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                self.loyaltyView.layoutIfNeeded()
                                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Hide / Unhide", action: "Loyalty Insight Hidden", label: "", value: 0)
                            })
                        }
                })
            case UISwipeGestureRecognizerDirection.up:
                dynamicLoyaltyViewHeightConstraint.constant = 0
                UIView.animate(withDuration: 0.25, animations: {
                    self.loyaltyView.layoutIfNeeded()
                    }, completion: { finished in
                        if finished {
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                self.loyaltyRingChartInsightView.alpha = 1
                                self.loyaltyPieChartInsightView.alpha = 1
                                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Hide / Unhide", action: "Loyalty Insight Unhidden", label: "", value: 0)
                            })
                        }
                })
                
                default:
                break
            }
        }
    }
    
    
    func volumeViewSwipe(_ gesture: UIGestureRecognizer){
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                UIView.animate(withDuration: 0.25, animations: {
                    self.volumePieChartInsightView.alpha = 0
                    self.volumeBarChartInsightView.alpha = 0
                    }, completion: { finished in
                        if finished {
                            self.dynamicVolumeViewHeightConstraint.constant =  self.volumeView.frame.size.height - 50
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                self.volumeView.layoutIfNeeded()
                                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Hide / Unhide", action: "Volume Insight Hidden", label: "", value: 0)
                            })
                        }
                })
            case UISwipeGestureRecognizerDirection.up:
                dynamicVolumeViewHeightConstraint.constant = 0
                UIView.animate(withDuration: 0.25, animations: {
                    self.volumeView.layoutIfNeeded()
                    }, completion: { finished in
                        if finished {
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                self.volumePieChartInsightView.alpha = 1
                                self.volumeBarChartInsightView.alpha = 1
                                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Insight Hide / Unhide", action: "Volume Insight Unhidden", label: "", value: 0)
                            })
                        }
                })
            default:
                break
            }
        }
    }
    
//    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
//        
//        for i in 0...selectedStrategicRecommendations.count - 1{
//            if(i == 0){
//                recommendationsLabel1.text = selectedStrategicRecommendations[i]
//                recommendationsArrow1.hidden = false
//            }
//            else if(i == 1){
//                recommendationsLabel2.text = selectedStrategicRecommendations[i]
//                recommendationsArrow2.hidden = false
//            }
//            else{
//                recommendationsLabel3.text = selectedStrategicRecommendations[i]
//                recommendationsArrow3.hidden = false
//            }
//        }
//        if(selectedStrategicRecommendations.count == 0){
//            recommendationsLabel1.text = ""
//            recommendationsLabel2.text = ""
//            recommendationsLabel3.text = ""
//            recommendationsArrow1.hidden = true
//            recommendationsArrow2.hidden = true
//            recommendationsArrow3.hidden = true
//        }
//        else if(selectedStrategicRecommendations.count == 1){
//            recommendationsLabel2.text = ""
//            recommendationsLabel3.text = ""
//            recommendationsArrow2.hidden = true
//            recommendationsArrow3.hidden = true
//        }
//        else if(selectedStrategicRecommendations.count == 2){
//            recommendationsLabel3.text = ""
//            recommendationsArrow3.hidden = true
//        }
//    }
}

class InsightView: UIView {
    
    var titleLabel: UILabel!
    var statementLabel: UILabel!
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.clear
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.catalinaBoldObliqueFontOfSize(14)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 4))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .width, relatedBy: .lessThanOrEqual, toItem: self, attribute: .width, multiplier: 1, constant: 0))
        
        statementLabel = UILabel()
        statementLabel.font = UIFont.catalinaBoldFontOfSize(14)
        statementLabel.numberOfLines = 0
        statementLabel.textAlignment = .center
        statementLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(statementLabel)
        self.addConstraint(NSLayoutConstraint(item: statementLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: statementLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -90))
        self.addConstraint(NSLayoutConstraint(item: statementLabel, attribute: .width, relatedBy: .lessThanOrEqual, toItem: self, attribute: .width, multiplier: 1, constant: 0))
    }
}

class BarChartInsightView: InsightView {
    
    var chart: BarChart!
    
    var baseColor: UIColor? {
        didSet {
            if let color = baseColor {
                self.titleLabel.textColor = color
                self.statementLabel.textColor = color
                chart.baselineColor = color
                chart.textColor = color
                chart.titleColor = color
            }
        }
    }
    
    override var isHidden: Bool {
        didSet {
            chart.update()
        }
    }
    
    fileprivate override func commonInit() {
        
        super.commonInit()
        
        chart = BarChart()
        chart.shouldAnimateUpdates = false
        chart.baselineWidth = 2
        chart.barSpacing = 32
        chart.textFont = UIFont.catalinaBoldFontOfSize(12)
        chart.titleFont = UIFont.catalinaMediumFontOfSize(10)
        chart.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(chart)
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -25))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 220))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 220))
    }
}

class PieChartInsightView: InsightView {
    
    var chart: PieChart!
    
    var baseColor: UIColor? {
        didSet {
            if let color = baseColor {
                self.titleLabel.textColor = color
                self.statementLabel.textColor = color
                chart.textColor = color
                chart.titleColor = color
            }
        }
    }
    
    override var isHidden: Bool {
        didSet {
            chart.update()
        }
    }
    
    fileprivate override func commonInit() {
        
        super.commonInit()
        
        chart = PieChart()
        chart.textFont = UIFont.catalinaBoldFontOfSize(12)
        chart.titleFont = UIFont.catalinaMediumFontOfSize(10)
        chart.radius = 76
        chart.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(chart)
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -25))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 280))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 280))
    }
}

class RingChartInsightsView: PieChartInsightView {
    
    
    fileprivate override func commonInit() {
        
        super.commonInit()
        self.chart.innerRadius = 44
    }
}

class InsightsPopoverTable: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var cellsArray:[String]?
    var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
    }
    func setupTable(_ array:[String]){
        cellsArray = array
        let tableHeight = ((self.cellsArray!.count) * 44 <= 600) ? (self.cellsArray!.count) * 44 : 600
        let viewRect = CGRect(x: 0, y: 0, width: 500, height: CGFloat(tableHeight))
        self.preferredContentSize = CGSize(width: viewRect.size.width, height: viewRect.size.height)
        tableView.frame = viewRect
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let recommendation = availableStrategicRecommendations[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = recommendation
        if selectedStrategicRecommendations.contains(recommendation) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recommendation =  availableStrategicRecommendations[(indexPath as NSIndexPath).row]
        
        if (selectedStrategicRecommendations.contains(recommendation) && selectedStrategicRecommendations.count > 2) {
            selectedStrategicRecommendations.remove(at: selectedStrategicRecommendations.index(of: recommendation)!)
        } else {
            if(selectedStrategicRecommendations.count < 3){
            //adds the recommendation while maintaining the original order
            var tempArray = selectedStrategicRecommendations
            tempArray.append(recommendation)
            selectedStrategicRecommendations.removeAll()
            for recommendation in availableStrategicRecommendations {
                if tempArray.contains(recommendation) {
                    selectedStrategicRecommendations.append(recommendation)
                    }
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
    }
    func numberOfSections(in tableView:UITableView)->Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsArray!.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //Removes bottom space on tableview
        return CGFloat.leastNormalMagnitude
    }
}

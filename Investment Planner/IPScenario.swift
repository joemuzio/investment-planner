//
//  IPScenario.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/18/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPScenario: IPModelObject {
    
    //transient properties added for use with the library
    var selectedForComparison: Bool = false
    var eligibleForComparison: Bool = true
    
    var showValueStatement: Bool = true
    
    var name: String = ""
    var folder: String = ""
    var isLocked: Bool = false
    var offers: [IPOffer] = [IPOffer]()
    var tlvIsHidden: Bool = true
    
    var program: IPProgram!
    
    init(program: IPProgram) {
        super.init()
        self.program = program
    }
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "name",
            "folder",
            "isLocked",
            "offers",
            "showValueStatement"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "offers" {
                var offers = [IPOffer]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let offer = IPOffer(scenario: self)
                        offer.updateWithJSONObject(item)
                        offers.append(offer)
                    }
                }
                self.offers = offers
                return
            }
        }
        super.setValue(value, forKey: key)
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "offers" {
                var JSONArray = [[String : AnyObject]]()
                for offer in self.offers {
                    JSONArray.append(offer.JSONObject())
                }
                return JSONArray
            }
        }
        return super.value(forKey: key)
    }
    
    func getOrderedCoupons() -> [IPCoupon] {
        
        var coupons = [IPCoupon]()
        for offer in offers {
            coupons.append(contentsOf: offer.coupons)
        }
        coupons.sort { (coupon1, coupon2) -> Bool in
            if coupon1.offer.type == coupon2.offer.type {
                return coupon1.triggerQuantity < coupon2.triggerQuantity
            } else {
                if coupon1.offer.type == .Trial {
                    return true
                } else if coupon1.offer.type == .Loyalty {
                    return coupon2.offer.type == .Volume
                } else {
                    return false
                }
            }
        }
        return coupons
    }
    
    func duplicate() -> IPScenario {
        
        let JSON = self.JSONObject()
        let newScenario = IPScenario(program: program)
        newScenario.updateWithJSONObject(JSON)
        return newScenario
    }
}

//extension for calculated variables
extension IPScenario {
        
    var maxBudget: Double {
        get {
            var volumeCycles = Set<String>()
            for offer in program.templateScenario!.offers {
                if offer.type == .Volume {
                    if let firstCoupon = offer.coupons.first {
                        for cycleName in firstCoupon.distributionCycles.keys {
                            volumeCycles.insert(cycleName)
                        }
                    }
                }
            }
            
            var result: Double = 0
            for coupon in program.templateScenario!.getOrderedCoupons() {
                for (cycleName, distribution) in coupon.distributionCycles {
                    if coupon.offer.type == .Loyalty {
                        if !volumeCycles.contains(cycleName) {
                            result += coupon.printCostMultiplier * distribution
                        }
                    }
                    else {
                        result += coupon.printCostMultiplier * distribution
                    }
                    
                }
                
            }
            return result
        }
    }
    
    var maxTotalUnits: Double {
        get {
            var volumeCycles = Set<String>()
            for offer in program.templateScenario!.offers {
                if offer.type == .Volume {
                    if let firstCoupon = offer.coupons.first {
                        for cycleName in firstCoupon.distributionCycles.keys {
                            volumeCycles.insert(cycleName)
                        }
                    }
                }
            }
            
            var result: Double = 0
            for offer in program.templateScenario!.offers {
                for coupon in offer.coupons {
                    for (cycleName, distribution) in coupon.distributionCycles {
                        if coupon.offer.type == .Loyalty {
                            if !volumeCycles.contains(cycleName) {
                                result += coupon.purchaseRequirement * distribution * coupon.redemptionRate
                            }
                        }
                        else if coupon.offer.type == .Volume {
                            result += coupon.purchaseRequirement * distribution
                        }
                        else {
                            if program.shortTermROI {
                                result += coupon.purchaseRequirement * distribution * coupon.redemptionRate
                            }
                            else {
                                result += coupon.purchaseRequirement * coupon.offer.repeatFactor * distribution * coupon.redemptionRate
                            }
                        }
                        
                    }
                }
                
            }
            return result
        }
    }
    
    var maxTotalDollars: Double {
        get {
            return maxTotalUnits * program.retailPricePerUnit
        }
    }
    
    var maxTotalIncrementalUnits: Double {
        get {
            var volumeCycles = Set<String>()
            for offer in program.templateScenario!.offers {
                if offer.type == .Volume {
                    if let firstCoupon = offer.coupons.first {
                        for cycleName in firstCoupon.distributionCycles.keys {
                            volumeCycles.insert(cycleName)
                        }
                    }
                }
            }
            
            var result: Double = 0
            for offer in program.templateScenario!.offers {
                for coupon in offer.coupons {
                    for (cycleName, distribution) in coupon.distributionCycles {
                        if coupon.offer.type == .Loyalty {
                            if !volumeCycles.contains(cycleName) {
                                result += coupon.purchaseRequirement * coupon.offer.incrementalRatio * distribution * coupon.redemptionRate
                            }
                        }
                        else if coupon.offer.type == .Volume {
                                result += coupon.purchaseRequirement * coupon.offer.incrementalRatio * distribution
                        }
                        else {
                            if program.shortTermROI {
                                result += coupon.purchaseRequirement * coupon.offer.incrementalRatio * distribution * coupon.redemptionRate
                            }
                            else {
                                result += coupon.purchaseRequirement * coupon.offer.incrementalRatio * coupon.offer.repeatFactor * distribution * coupon.redemptionRate
                            }
                        }
                        
                    }
                }
                
            }
            return result
        }
    }
    
    var maxTotalIncrementalDollars: Double {
        get {
            return maxTotalIncrementalUnits * program.retailPricePerUnit
        }
    }
    
    var minProfitRoi: Double {
        get {
            var lowestEfficiency: Double = Double(Int.max)
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getProfitRoiForCycle(cycleName) < lowestEfficiency {
                            lowestEfficiency = offer.getProfitRoiForCycle(cycleName)
                        }
                    }
                }
            }
            return floor(lowestEfficiency * 100) / 100
        }
    }
    
    var maxProfitRoi: Double {
        get {
            var highestEfficiency: Double = 0
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getProfitRoiForCycle(cycleName) > highestEfficiency {
                            highestEfficiency = offer.getProfitRoiForCycle(cycleName)
                        }
                    }
                }
            }
            return ceil(highestEfficiency * 100) / 100
        }
    }
    
    var minRetailRoi: Double {
        get {
            var lowestEfficiency: Double = Double(Int.max)
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getRetailRoiForCycle(cycleName) < lowestEfficiency {
                            lowestEfficiency = offer.getRetailRoiForCycle(cycleName)
                        }
                    }
                }
            }
            return ceil(lowestEfficiency * 100) / 100
        }
    }
    
    var maxRetailRoi: Double {
        get {
            var highestEfficiency: Double = 0
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getRetailRoiForCycle(cycleName) > highestEfficiency {
                            highestEfficiency = offer.getRetailRoiForCycle(cycleName)
                        }
                    }
                }
            }
            return floor(highestEfficiency * 100) / 100
        }
    }
    
    var minCpum: Double {
        get {
            var lowestEfficiency: Double = Double(Int.max)
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getCpumForCycle(cycleName) < lowestEfficiency {
                            lowestEfficiency = offer.getCpumForCycle(cycleName)
                        }
                    }
                }
            }
            return ceil(lowestEfficiency * 100) / 100
        }
    }
    
    var maxCpum: Double {
        get {
            var highestEfficiency: Double = 0
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getCpumForCycle(cycleName) > highestEfficiency {
                            highestEfficiency = offer.getCpumForCycle(cycleName)
                        }
                    }
                }
            }
            return floor(highestEfficiency * 100) / 100
        }
    }
    
    var minCpium: Double {
        get {
            var lowestEfficiency: Double = Double(Int.max)
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getCpiumForCycle(cycleName) < lowestEfficiency {
                            lowestEfficiency = offer.getCpiumForCycle(cycleName)
                        }
                    }
                }
            }
            return ceil(lowestEfficiency * 100) / 100
        }
    }
    
    var maxCpium: Double {
        get {
            var highestEfficiency: Double = 0
            for offer in program.templateScenario!.offers {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.getCpiumForCycle(cycleName) > highestEfficiency {
                            highestEfficiency = offer.getCpiumForCycle(cycleName)
                        }
                    }
                }
            }
            return floor(highestEfficiency * 100) / 100
        }
    }
    
    var totalInvestment: Double {
        get {
            var total: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    total += coupon.printCostMultiplier * coupon.totalDistribution
                }
            }
            return total
        }
    }
    
    var totalUnits: Double {
        get {
            var total: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    total += coupon.totalUnits
                }
            }
            return total
        }
    }
    
    var totalDollars: Double {
        get {
            let result = totalUnits * program.retailPricePerUnit
            return result
        }
    }
    
    var totalAcquiredBuyers: Double {
        get {
            var total: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    total += coupon.totalAcquiredBuyers
                }
            }
            return total
        }
    }
    
    var totalIncrementalUnits: Double {
        get {
            var total: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    total += coupon.totalIncrementalUnits
                }
            }
            return total
        }
    }
    
    var totalIncrementalDollars: Double {
        get {
            var total: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    total += coupon.totalIncrementalDollars
                }
            }
            return total
        }
    }
    
    var retailRoi: Double {
        get {
            return totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0
        }
    }
    
    var profitRoi: Double {
        get {
            return retailRoi * program.profitMargin
        }
    }
    
    var cpum: Double {
        get {
            var totalInvestment: Double = 0
            var totalUnits: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    totalInvestment += coupon.totalInvestment
                    totalUnits += coupon.totalUnits
                }
            }
            
            return totalUnits > 0 ? totalInvestment / totalUnits : 0
        }
    }
    
    var cpium: Double {
        get {
            var totalInvestment: Double = 0
            var totalUnits: Double = 0
            for offer in offers {
                for coupon in offer.coupons {
                    totalInvestment += coupon.totalInvestment
                    totalUnits += coupon.totalIncrementalUnits
                }
            }
            
            return totalUnits > 0 ? totalInvestment / totalUnits : 0
        }
    }

    var trialPercent: Double {
        get {
            let percent = totalBudget > 0 ? trialBudget / totalBudget : 0
            return percent
        }
    }
    
    var loyaltyPercent: Double {
        get {
            let percent = totalBudget > 0 ? loyaltyBudget / totalBudget : 0
            return percent
        }
    }
    
    var volumePercent: Double {
        get {
            let percent = totalBudget > 0 ? volumeBudget / totalBudget : 0
            return percent
        }
    }
    
    var trialBudget: Double {
        get {
            var budget: Double = 0
            for offer in offers {
                if offer.type == .Trial {
                    budget += offer.totalInvestment
                }
            }
            return budget
        }
    }
    
    var loyaltyBudget: Double {
        get {
            var budget: Double = 0
            for offer in offers {
                if offer.type == .Loyalty {
                    budget += offer.totalInvestment
                }
            }
            return budget
        }
    }
    
    var volumeBudget: Double {
        get {
            var budget: Double = 0
            for offer in offers {
                if offer.type == .Volume {
                    budget += offer.totalInvestment
                }
            }
            return budget
        }
    }

    var totalBudget: Double {
        get {
            return trialBudget + loyaltyBudget + volumeBudget
        }
    }
    var estimatedDistribution:Int{
        get{
            var estimatedDistribution = 0
            for offer in offers{
                for coupon in offer.coupons{
                    estimatedDistribution += Int(coupon.totalDistribution)
                    if(coupon.offer.type == .Volume){
                        estimatedDistribution += Int(coupon.totalDistribution) * 4
                    }
                }
            }
            return estimatedDistribution
        }
    }
    var estimatedRedemption:Int{
        get{
            var estimatedRedemption = 0
            for offer in offers{
                for coupon in offer.coupons{
                    estimatedRedemption += Int(coupon.totalRedemption)
                }
            }
            return estimatedRedemption
        }
    }
    var distributionInvestment:Double{
        get{
            var distributionInvestment = 0.0
            for offer in offers{
                distributionInvestment += offer.totalDistribution * offer.printCost
            }
            return distributionInvestment
        }
    }
    var redemptionInvestment:Double{
        get{
            var redemptionInvestment = 0.0
            for coupon in getOrderedCoupons(){
                redemptionInvestment += coupon.totalRedemption * (program.handlingFee + coupon.promotionValue)
            }
            return redemptionInvestment
        }
    }
    
    var costPerAcquisition:Double{
        get{
            var totalInv: Double = 0
            var totalAcquired: Double = 0
            for coupon in getOrderedCoupons()  {
                if(coupon.offer.type == .Trial){
                    totalInv += coupon.totalInvestment
                    totalAcquired += coupon.totalAcquiredBuyers
                }
            }
            if(totalAcquired != 0){
                return totalInv / totalAcquired
            }
            else{
                return 0.0
            }
        }
    }
    func getTotalUnitsForType(_ type: IPOfferType) -> Double {
        
        var budget: Double = 0
        for offer in offers {
            if offer.type == type {
                budget += offer.totalUnits
            }
        }
        return budget
        
    }
    
    func getTotalDollarsForType(_ type: IPOfferType) -> Double {
        
        var budget: Double = 0
        for offer in offers {
            if offer.type == type {
                budget += offer.totalDollars
            }
        }
        return budget
        
    }
    
    func getTotalIncrementalUnitsForType(_ type: IPOfferType) -> Double {
        
        var budget: Double = 0
        for coupon in getOrderedCoupons()  {
            if coupon.offer.type == type{
                budget += coupon.totalIncrementalUnits
            }
        }
        return budget
        
    }
    
    func getTotalIncrementalDollarsForType(_ type: IPOfferType) -> Double {
        
        var budget: Double = 0
        for coupon in getOrderedCoupons()  {
            if coupon.offer.type == type{
                budget += coupon.totalIncrementalDollars
            }
        }
        return budget
        
    }
    
    func getTotalAcquiredBuyersForType(_ type: IPOfferType) -> Double {
        
        var budget: Double = 0
        for offer in offers {
            if offer.type == type {
                budget += offer.totalAcquiredBuyers
            }
        }
        return budget
        
    }
    
    func getTotalDistributionForType(_ type: IPOfferType) -> Double {
        
        var budget: Double = 0
        for offer in offers {
            if offer.type == type {
                budget += offer.totalDistribution
            }
        }
        return budget
        
    }
    
    func getAdjustedRetailers() -> [IPRetailer] {
        let originalRetailers = self.program.report.data.retailers;
        var adjustedRetailers = [IPRetailer]()
        
        // Get the total percent
        var total = 0.0
        for retailer in originalRetailers {
            total += retailer.percentTrips
        }
        
        // Build the adjusted retailers
        for retailer in originalRetailers {
            let newRetailer = IPRetailer()
            newRetailer.retailer = retailer.retailer
            if(total > 0) {
            newRetailer.percentTrips = retailer.percentTrips / total
            } else {
                newRetailer.percentTrips = 0
            }
            adjustedRetailers.append(newRetailer)
        }
        return adjustedRetailers
    }
    override func setNilValueForKey(_ key: String) {
        if key == "isLocked" {
            self.isLocked = false
        }
    }
}

//
//  CSVCreator.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 8/10/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class CSVCreator: NSObject {

    var columnGroups = [DesignInputsColumnGroup]()
    var rows = [DesignInputsRow]()
    
    func getColumnAtIndex(_ index: Int) -> DesignInputsColumn? {
        
        var count = 0
        for group in columnGroups {
            for column in group.columns {
                if index == count {
                    return column
                }
                count += 1
            }
        }
        return nil
    }
    func getTotalColumnCount() -> Int {
        
        var count = 0
        for group in columnGroups {
            count += group.columns.count
        }
        return count
    }
    var cellValueFormatter: ((_ column: DesignInputsColumn) -> String)?
    func cellValueForColumn(_ column: DesignInputsColumn) -> String {
        
        if let formatter = cellValueFormatter {
            return formatter(column)
        }
        return "--"
    }
    
    func createCSVForScenario(_ scenario: IPScenario)-> NSMutableString{
        let csvString = NSMutableString()
        csvString.append(",")
        initializeRowsAndColumns(scenario)
        for section in 0...columnGroups.count - 1{
            let columnHeaders = columnGroups[section]
            for headers in 0...columnHeaders.columns.count - 1{
                let string = columnHeaders.columns[headers]
                csvString.append("\(string.headerText),")
            }
        }
        csvString.append("\n")
        
        for r in 0...rows.count - 1{
            let row = rows[r]
            for section in 0...columnGroups.count - 1{
                if(section == 0){
                    csvString.append("\(row.headerText),")
                }
                let columnHeaders = columnGroups[section]
                for headers in 0...columnHeaders.columns.count - 1{
                    let column = columnHeaders.columns[headers]
                    let string = row.cellValueForColumn(column).replacingOccurrences(of: ",", with: "")
                    
                    csvString.append("\(string),")
                }
            }
            csvString.append("\n")
        }
        return csvString
    }
    

    func initializeRowsAndColumns(_ scenario: IPScenario) {
        
        //MARK: --------------------------------------------  COLUMNS  -------------------------------------------------------
        
        //trial offers
        let trialOfferGroup = DesignInputsColumnGroup(headerText: "TRIAL")
        var trialCoupons = [IPCoupon]()
        for offer in scenario.offers {
            if offer.type.rawValue.uppercased() == trialOfferGroup.headerText {
                for coupon in offer.coupons {
                    let column = DesignInputsColumn(headerText: offer.name.rawValue, referenceObject: coupon)
                    trialOfferGroup.columns.append(column)
                    trialCoupons.append(coupon)
                }
            }
        }
        let trialTotalsColumn = DesignInputsColumn(headerText: "Total Trial", referenceObject: trialCoupons as AnyObject?)
        trialOfferGroup.columns.append(trialTotalsColumn)
        self.columnGroups.append(trialOfferGroup)
        
        //loyalty offers
        let loyaltyOfferGroup = DesignInputsColumnGroup(headerText: "LOYALTY")
        var loyaltyCoupons = [IPCoupon]()
        for offer in scenario.offers {
            if offer.type.rawValue.uppercased() == loyaltyOfferGroup.headerText {
                let highestTrigger = offer.highestTriggerQuantity
                for coupon in offer.coupons {
                    var name = "\(offer.name.rawValue.capitalized) Buy \(coupon.triggerQuantity.numberString())"
                    if coupon.triggerQuantity == highestTrigger {
                        name += "+"
                    }
                    let column = DesignInputsColumn(headerText: name, referenceObject: coupon)
                    loyaltyOfferGroup.columns.append(column)
                    loyaltyCoupons.append(coupon)
                }
            }
        }
        let loyaltyTotalsColumn = DesignInputsColumn(headerText: "Total Loyalty", referenceObject: loyaltyCoupons as AnyObject?)
        loyaltyOfferGroup.columns.append(loyaltyTotalsColumn)
        self.columnGroups.append(loyaltyOfferGroup)
        
        //volume offers
        let volumeOfferGroup = DesignInputsColumnGroup(headerText: "VOLUME")
        let announcementColumn = DesignInputsColumn(headerText: "CCM Announcement", referenceObject: "Announcement" as AnyObject?)
        volumeOfferGroup.columns.append(announcementColumn)
        
        var volumeCoupons = [IPCoupon]()
        for offer in scenario.offers {
            if offer.type.rawValue.uppercased() == volumeOfferGroup.headerText {
                let highestTrigger = offer.highestTriggerQuantity
                for coupon in offer.coupons {
                    var name = "\(offer.name.rawValue.uppercased()) Buy \(coupon.triggerQuantity.numberString())"
                    if coupon.triggerQuantity == highestTrigger {
                        name += "+"
                    }
                    let column = DesignInputsColumn(headerText: name, referenceObject: coupon)
                    volumeOfferGroup.columns.append(column)
                    volumeCoupons.append(coupon)
                }
            }
        }
        let volumeTotalsColumn = DesignInputsColumn(headerText: "Total Volume", referenceObject: volumeCoupons as AnyObject?)
        volumeOfferGroup.columns.append(volumeTotalsColumn)
        self.columnGroups.append(volumeOfferGroup)
        
        //create column for overall totals and add it to its own group
        let totalsColumn = DesignInputsColumn(headerText: "Annual Plan Total", referenceObject: scenario.getOrderedCoupons() as AnyObject?)
        let totalsColumnGroup = DesignInputsColumnGroup(headerText: "")
        totalsColumnGroup.columns.append(totalsColumn)
        self.columnGroups.append(totalsColumnGroup)
        
        //MARK: ----------------------------------------------------------------------------------------------------------------
        
        //MARK: -----------------------------------------------  ROWS  ---------------------------------------------------------
        
        self.rows.append(DesignInputsRow(headerText: "#Cycles", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.numberOfCycles.numberString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var multipleTypesFound = false
                var offerType:IPOfferType?
                var loyaltyCycleTotal = 0
                var volumeCycleTotal = 0
                var trialCycleTotal = 0
                var biggestCycleTotal = 0
                for coupon in coupons{
                    let total = coupon.numberOfCycles
                    if(total > biggestCycleTotal){
                        biggestCycleTotal = total
                    }
                    if(offerType == nil){
                        offerType = coupon.offer.type
                    }
                    else{
                        if(offerType != coupon.offer.type){
                            multipleTypesFound = true
                        }
                    }
                    if(coupon.offer.type == .Trial){
                        if(total > trialCycleTotal){
                            trialCycleTotal = total
                        }
                    }
                    else if(coupon.offer.type == .Loyalty){
                        if(total > loyaltyCycleTotal){
                            loyaltyCycleTotal = total
                        }
                    }
                    else if(coupon.offer.type == .Volume){
                        if(total > volumeCycleTotal){
                            volumeCycleTotal = total
                        }
                    }
                }
                if(multipleTypesFound == false){
                    return biggestCycleTotal.numberString()
                }
                else{
                    var annualCycleTotal = 0
                    let volumePlusCCMCycleTotal = loyaltyCycleTotal + volumeCycleTotal
                    if(volumePlusCCMCycleTotal > trialCycleTotal){
                        if(volumePlusCCMCycleTotal > 13){
                            annualCycleTotal = 13
                        }
                        else{
                            annualCycleTotal = volumePlusCCMCycleTotal
                        }
                    }
                    else{
                        annualCycleTotal = trialCycleTotal
                    }
                    return annualCycleTotal.numberString()
                }
                
            } else {
                return "--"
            }
            
        }))
        self.rows.append(DesignInputsRow(headerText: "Print Cost ID", cellValueFormatter: { (column) -> String in
            return scenario.program.printCostId.currencyString()
        }))
        self.rows.append(DesignInputsRow(headerText: "Print Cost Transactional", cellValueFormatter: { (column) -> String in
            return scenario.program.printCostTransactional.currencyString()
        }))
        self.rows.append(DesignInputsRow(headerText: "CCM Announcement Cost", cellValueFormatter: { (column) -> String in
            return scenario.program.ccmAnnouncementCost.currencyString()
        }))
        self.rows.append(DesignInputsRow(headerText: "Handling Fee", cellValueFormatter: { (column) -> String in
            return scenario.program.handlingFee.currencyString()
        }))
        self.rows.append(DesignInputsRow(headerText: "Estimated Distribution", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalDistribution.numberString()
            } else if let _ = column.referenceObject as? String {
                let total: Double = scenario.getTotalDistributionForType(IPOfferType.Volume) * 4
                return total.numberString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                for coupon in coupons {
                    total += coupon.totalDistribution
                    if coupon.offer.type == .Volume {
                        total += coupon.totalDistribution * 4
                    }
                }
                return total.numberString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Estimated Redemption", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalRedemption.numberString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalRedemption
                }
                return total.numberString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Total Units", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalUnits.numberString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                for coupon in coupons {
                    total += coupon.totalUnits
                }
                return total.numberString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Total Dollars", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalDollars.currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalDollars
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Incremental Units", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    return coupon.totalIncrementalUnits.numberString()
                }
                else {
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                for coupon in coupons {
                    total += coupon.totalIncrementalUnits
                }
                return total.numberString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Incremental Dollars", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    //       print(coupon.totalIncrementalDollars.currencyString(0))
                    return coupon.totalIncrementalDollars.currencyString(0)
                }
                else {
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalIncrementalDollars
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Profit Margin", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if(coupon.totalDistribution == 0){
                    return 0.percentString()
                }
                else{
                    return coupon.offer.scenario.program.profitMargin.percentString()
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                for coupon in coupons {
                    if(coupon.totalDistribution != 0){
                        return coupon.offer.scenario.program.profitMargin.percentString()
                    }
                }
                return 0.percentString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Retail ROI", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else {
                        return coupon.retailRoi.currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalIncrementalDollars += coupon.totalIncrementalDollars
                }
                
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Profit ROI", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else {
                        return coupon.profitRoi.currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalIncrementalDollars += coupon.totalIncrementalDollars * coupon.offer.scenario.program.profitMargin
                }
                
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Acquired Buyers", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    return coupon.totalAcquiredBuyers.numberString()
                }
                else {
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                var type: IPOfferType = .Volume
                for coupon in coupons {
                    total += coupon.totalAcquiredBuyers
                    type = coupon.offer.type
                }
                if(total > 0 || type == .Trial){
                    return total.numberString()
                }
                else{
                    return "--"
                }
                //                if(type == .Trial){
                //                    return total.numberString()
                //                }
                //                else{
                //                    return "--"
                //                }
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Cost per Acquisition", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if(coupon.offer.type == .Trial){
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else{
                        return (coupon.totalInvestment / coupon.totalAcquiredBuyers).currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var trialTypeFound = false
                var totalAcquired: Double = 0
                for coupon in coupons {
                    if(coupon.offer.type == .Trial){
                        totalInvestment += coupon.totalInvestment
                        totalAcquired += coupon.totalAcquiredBuyers
                        trialTypeFound = true
                    }
                }
                
                
                if trialTypeFound == true {
                    return (totalAcquired > 0 ? totalInvestment / totalAcquired : 0).currencyString()
                }
                else {
                    return "--"
                }
                
                
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "CPUM", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if(coupon.totalDistribution == 0){
                    return 0.currencyString()
                }
                else {
                    if(coupon.offer.type == .Volume || coupon.offer.type == .Loyalty){
                        return "--"
                    }
                    return coupon.cpum.currencyString()
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalUnits += coupon.totalUnits
                }
                
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "CPIUM", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else {
                        return coupon.cpium.currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalUnits += coupon.totalIncrementalUnits
                }
                
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Total Investment", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Volume {
                    let totalInvestment = coupon.totalDistribution * (scenario.program.printCostTransactional + (coupon.redemptionRate * (coupon.promotionValue + scenario.program.handlingFee)))
                    return totalInvestment.currencyString(0)
                }
                else {
                    return coupon.totalInvestment.currencyString(0)
                }
            } else if let _ = column.referenceObject as? String {
                let total: Double = (scenario.getTotalDistributionForType(IPOfferType.Volume) * 4) * scenario.program.ccmAnnouncementCost
                return total.currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalInvestment
                }
                
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Distribution Investment", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Volume {
                    return (coupon.totalDistribution * scenario.program.printCostTransactional).currencyString(0)
                }
                else {
                    return (coupon.totalDistribution * coupon.offer.printCost).currencyString(0)
                }
            } else if let _ = column.referenceObject as? String {
                let total: Double = (scenario.getTotalDistributionForType(IPOfferType.Volume) * 4) * scenario.program.ccmAnnouncementCost
                return total.currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += (coupon.totalDistribution * coupon.offer.printCost)
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Redemption Investment", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue)).currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Strategic Weighting", cellValueFormatter: { (column) -> String in
            if let _ = column.referenceObject as? IPCoupon {
                return "--"
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var foundOfferType:IPOfferType?
                var multipleTypes = false
                var total: Double = 0
                for coupon in coupons {
                    if(foundOfferType == nil){
                        foundOfferType = coupon.offer.type
                    }
                    else{
                        if(foundOfferType != coupon.offer.type){
                            multipleTypes = true
                        }
                    }
                    total += coupon.totalInvestment / coupon.offer.scenario.totalBudget
                }
                if(multipleTypes == true){
                    return "--"
                }
                else{
                    return total.percentString()
                }
            } else {
                return "--"
            }
        }))
        
        //MARK: ----------------------------------------------------------------------------------------------------------------
    }
}

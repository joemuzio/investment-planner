//
//  LoginViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/8/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var titleLabel: UILabel!
    var userNameTextField: UITextField!
    var passwordTextField: UITextField!
    var loginButton: UIButton!
    var successBlock: ((LoginViewController) -> Void)?
    var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        self.preferredContentSize = CGSize(width: 400, height: 300)
        
        userNameTextField = UITextField()
        userNameTextField.placeholder = "user name"
        userNameTextField.clearButtonMode = .whileEditing
        userNameTextField.borderStyle = .roundedRect
        userNameTextField.enablesReturnKeyAutomatically = true
        userNameTextField.keyboardType = .default
        userNameTextField.autocapitalizationType = .none
        userNameTextField.autocorrectionType = .no
        userNameTextField.returnKeyType = .next
        userNameTextField.delegate = self
        userNameTextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(userNameTextField)
        self.view.addConstraint(NSLayoutConstraint(item: userNameTextField, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: userNameTextField, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: -20))
        self.view.addConstraint(NSLayoutConstraint(item: userNameTextField, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.catalinaMediumFontOfSize(24)
        titleLabel.text = "USER LOGIN"
        titleLabel.textColor = UIColor.catalinaDarkBlueColor()
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(titleLabel)
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .bottom, relatedBy: .equal, toItem: userNameTextField, attribute: .top, multiplier: 1, constant: -40))
        
        passwordTextField = UITextField()
        passwordTextField.placeholder = "password"
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isSecureTextEntry = true
        passwordTextField.enablesReturnKeyAutomatically = true
        passwordTextField.keyboardType = .default
        passwordTextField.autocapitalizationType = .none
        passwordTextField.autocorrectionType = .no
        passwordTextField.returnKeyType = .default
        passwordTextField.delegate = self
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(passwordTextField)
        self.view.addConstraint(NSLayoutConstraint(item: passwordTextField, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: passwordTextField, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 20))
        self.view.addConstraint(NSLayoutConstraint(item: passwordTextField, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        
        loginButton = UIButton()
        loginButton.setTitle("Login", for: UIControlState())
        loginButton.layer.cornerRadius = 6
        loginButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
        loginButton.setTitleColor(UIColor.catalinaWhiteColor(), for: UIControlState())
        loginButton.addTarget(self, action: #selector(LoginViewController.loginButtonPressed), for: .touchUpInside)
        loginButton.isEnabled = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loginButton)
        self.view.addConstraint(NSLayoutConstraint(item: loginButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: loginButton, attribute: .top, relatedBy: .equal, toItem: passwordTextField, attribute: .bottom, multiplier: 1, constant: 40))
        self.view.addConstraint(NSLayoutConstraint(item: loginButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        self.view.addConstraint(NSLayoutConstraint(item: loginButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 40))
        
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.catalinaDarkBlueColor()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(activityIndicator)
        self.view.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == userNameTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var enableLogin = false
        if var text = textField.text {
            text = (text as NSString).replacingCharacters(in: range, with: string)
            if text.characters.count > 0 {
                if textField == userNameTextField {
                    if let password = passwordTextField.text {
                        if password.characters.count > 0 {
                            enableLogin = true
                        }
                    }
                } else {
                    if let userName = userNameTextField.text {
                        if userName.characters.count > 0 {
                            enableLogin = true
                        }
                    }
                }
            }
        }
        if enableLogin {
            loginButton.isEnabled = true
            loginButton.backgroundColor = UIColor.catalinaDarkBlueColor()
        } else {
            loginButton.isEnabled = false
            loginButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        loginButton.isEnabled = false
        loginButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
        return true
    }
    
    func loginButtonPressed() {
     
        if let userName = userNameTextField.text?.lowercased(), let password = passwordTextField.text {
            if (userName.rangeOfCharacter(from: CharacterSet(charactersIn: "@")) == nil) {
                activityIndicator.startAnimating()
                loginButton.isEnabled = false
                loginButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
                WebservicesController.sharedWebservicesController.login(userName, password: password) { (error) -> Void in
                    self.activityIndicator.stopAnimating()
                    self.loginButton.isEnabled = true
                    self.loginButton.backgroundColor = UIColor.catalinaDarkBlueColor()
                    if let error = error {
                        self.tryAgain("\(error.localizedDescription) Please try again.")
                    } else {
                        if let block = self.successBlock {
                            block(self)
                        }
                    }
                }
            } else {
                self.tryAgain("Please login using your Catalina Network ID and Password.")
            }
        }
    }
    
    func tryAgain(_ message: String) {
        
        let alert = UIAlertController(title: "Login Failed", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
            self.passwordTextField.becomeFirstResponder()
            })
        self.present(alert, animated: true, completion: nil)
    }
}

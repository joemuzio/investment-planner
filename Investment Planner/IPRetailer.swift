//
//  IPRetailer.swift
//  Investment Planner
//
//  Created by Douglas Wall on 4/24/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import Foundation

class IPRetailer: IPModelObject {
    
    var retailer: Int = 0
    var percentTrips: Double = 0
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "retailer",
            "percentTrips"])
        return keys
    }
}

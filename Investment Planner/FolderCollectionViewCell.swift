//
//  FolderCollectionViewCell.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 7/5/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class FolderCollectionViewCell: UICollectionViewCell {
    fileprivate(set) var deleteButton: UIButton!
    fileprivate(set) var nameLabel: UILabel!
    var folder = ""
    
    var longPressGesture: UILongPressGestureRecognizer!
    var longPressGestureHandler: ((UILongPressGestureRecognizer) -> Void)? {
        didSet {
            if longPressGestureHandler == nil {
                self.removeGestureRecognizer(longPressGesture)
            } else {
                self.addGestureRecognizer(longPressGesture)
            }
        }
    }
    
    var borderColor = UIColor.catalinaLightBlueColor() {
        didSet {
            self.contentView.layer.borderColor = borderColor.cgColor
            self.nameLabel.backgroundColor = borderColor
        }
    }
    
    var deleteActionHandler: ((FolderCollectionViewCell) -> Void)? {
        didSet {
            deleteButton.alpha = (deleteActionHandler == nil) ? 0 : 1
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        let imageView = UIImageView()
        imageView.image = UIImage.init(named: "folder.png")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0))
        
        let deleteButtonImage = UIImage(named: "icon-delete-card")
        deleteButton = UIButton()
        deleteButton.setImage(deleteButtonImage, for: UIControlState())
        deleteButton.sizeToFit()
        deleteButton.alpha = 0
        deleteButton.addTarget(self, action: #selector(PortfolioItemCollectionViewCell.deleteButtonPressed), for: .touchUpInside)
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(deleteButton)
        self.addConstraint(NSLayoutConstraint(item: deleteButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: -12))
        self.addConstraint(NSLayoutConstraint(item: deleteButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: -12))
        
        nameLabel = UILabel()
        nameLabel.textColor = UIColor.catalinaWhiteColor()
        nameLabel.textAlignment = .center
        nameLabel.text = "Title"
        nameLabel.font = UIFont.catalinaBoldFontOfSize(14)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(nameLabel)
        self.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 50))
        self.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(PortfolioItemCollectionViewCell.handleLongPress(_:)))
    }
    
    func updateForPortfolioMode(){
        nameLabel.font = UIFont.catalinaBoldFontOfSize(14)
    }
    func updateForNormalMode(){
        nameLabel.font = UIFont.catalinaBoldFontOfSize(20)
    }
    
    // MARK: - Actions
    
    internal func deleteButtonPressed() {
        
        if let delete = deleteActionHandler {
            delete(self)
        }
        
    }
    
    func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        
        if let handler = self.longPressGestureHandler {
            handler(gesture)
        }
    }
    
    // MARK: - Solid Line
    
    fileprivate var solidOutlineLayer: CAShapeLayer?
    
    var shouldShowSolidOutline = false {
        
        didSet {
            if shouldShowSolidOutline != oldValue { //make sure this has actually changed so we don't unintentionally duplicate the outline layer
                if shouldShowSolidOutline {
                    let shapeLayer = CAShapeLayer()
                    shapeLayer.lineWidth = 2
                    //         shapeLayer.lineDashPattern = [10,5]
                    shapeLayer.fillColor = UIColor.clear.cgColor
                    shapeLayer.strokeColor = UIColor.catalinaDarkBlueColor().cgColor
                    shapeLayer.frame = self.bounds.insetBy(dx: -6, dy: -6)
                    shapeLayer.path = UIBezierPath(roundedRect: shapeLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 6, height: 6)).cgPath
                    self.layer .addSublayer(shapeLayer)
                    solidOutlineLayer = shapeLayer
                } else {
                    if let shapeLayer = solidOutlineLayer {
                        shapeLayer.removeFromSuperlayer()
                        solidOutlineLayer = nil
                    }
                }
            }
        }
    }
    
    // MARK: - Active (i.e. Color vs. Grayscale) Appearance
    
    var active = true {
        
        didSet {
            if active {
                self.borderColor = UIColor.catalinaLightBlueColor()
                self.alpha = 1
            } else {
                self.borderColor = UIColor.lightGray
                self.alpha = 0.75
            }
        }
    }
}

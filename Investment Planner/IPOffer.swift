//
//  IPOffer.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/18/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

enum IPOfferType: String {
    case Trial = "trial"
    case Loyalty = "loyalty"
    case Volume = "volume"
}

enum IPOfferName: String {
    case LapsedBuyer = "Lapsed Buyer"
    case DataMining = "Data Mining"
    case HeavyNeverBuy = "Heavy Category Never Buy"
    case OUTU = "Expand Consumption"
    case CCM = "CCM"
}

class IPOffer: IPModelObject {
    
    var name: IPOfferName = .LapsedBuyer
    var type: IPOfferType = .Trial
    var coupons: [IPCoupon] = [IPCoupon]()
    var incrementalRatio: Double = 0
    var enabled: Bool = true
    
    var scenario: IPScenario!
    
    init(scenario: IPScenario) {
        super.init()
        self.scenario = scenario
    }
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "name",
            "type",
            "coupons",
            "incrementalRatio",
            "enabled"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "type" {
                if let stringValue = value as? String {
                    if let type = IPOfferType(rawValue: stringValue) {
                        self.type = type
                        return
                    }
                }
            }
            if key == "name" {
                if let stringValue = value as? String {
                    if let name = IPOfferName(rawValue: stringValue) {
                        self.name = name
                        return
                    }
                }
            }
            if key == "coupons" {
                var coupons = [IPCoupon]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let coupon = IPCoupon(offer: self)
                        coupon.updateWithJSONObject(item)
                        coupons.append(coupon)
                    }
                }
                self.coupons = coupons
                return
            }
            if key == "enabled" {
                if let boolValue = value as? Bool {
                    self.enabled = boolValue
                }
                else {
                    self.enabled = true
                }
            }
        }
        super.setValue(value, forKey: key)
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "type" {
                return self.type.rawValue
            }
            if key == "name" {
                return self.name.rawValue
            }
            if key == "coupons" {
                var array = [[String : AnyObject]]()
                for coupon in self.coupons {
                    array.append(coupon.JSONObject())
                }
                return array
            }
            if key == "enabled" {
                return self.enabled
            }
        }
        return super.value(forKey: key)
    }
    
    func activeCycles() -> [MPICycle] {
        if let firstCoupon = coupons.first {
            var activeCycles = [MPICycle]()
            let orderedCycles = scenario.program.report.data.getOrderedCycles()
            
            for distributionCycle in firstCoupon.distributionCycles {
                if getTotalDistributionForCycle(distributionCycle.0) > 0 {
                    let cycle = distributionCycle.0
                    if cycle.characters.count > 10 {
                        let yearString = cycle.substring(with: (cycle.startIndex ..< cycle.characters.index(cycle.startIndex, offsetBy: 4)))
                        let cycleString = cycle.substring(with: (cycle.characters.index(cycle.startIndex, offsetBy: 8) ..< cycle.characters.index(cycle.startIndex, offsetBy: 10)))
                        
                        let yearInt = Int(yearString) ?? 0
                        let cycleInt = Int(cycleString) ?? 0
                        
                        activeCycles.append(MPICycle(cycleInt, yearInt))
                    }
                    else if cycle == "first" {
                        activeCycles.append(orderedCycles[0])
                    }
                    else if cycle == "second" {
                        activeCycles.append(orderedCycles[7])
                    }
                }
            }
            
            return activeCycles
        }
        return [MPICycle]()
    }
}

//extension for calculated variables
extension IPOffer {
    
    var totalInvestment: Double {
        get {
            var result: Double = 0
            for coupon in coupons {
                result += coupon.totalInvestment
            }
            return result
        }
    }
    
    var totalDistribution: Double {
        get {
            var result: Double = 0
            for coupon in coupons {
                result += coupon.totalDistribution
            }
            if enabled {
            return result
            }
            else {
                return 0.0
            }
        }
    }
    
    var totalRedemption: Double {
        get {
            var result: Double = 0
            for coupon in coupons {
                result += coupon.totalRedemption
            }
            return result
        }
    }
    
    var totalUnits: Double {
        get {
            var result: Double = 0
            for coupon in coupons {
                result += coupon.totalUnits
            }
            return result
        }
    }
    
    var totalDollars: Double {
        get {
            return totalUnits * scenario.program.retailPricePerUnit
        }
    }
    
    var totalIncrementalUnits: Double {
        get {
            return totalUnits * incrementalRatio
        }
    }
    
    var totalIncrementalDollars: Double {
        get {
            return totalIncrementalUnits * scenario.program.retailPricePerUnit
        }
    }
    
    var totalAcquiredBuyers: Double {
        get {
            var result: Double = 0
            for coupon in coupons {
                result += coupon.totalAcquiredBuyers
            }
            return result
        }
    }
    //
    //    var retailRoi: Double {
    //        get {
    //            return totalIncrementalDollars / totalInvestment
    //        }
    //    }
    //
    //    var profitRoi: Double {
    //        get {
    //            return retailRoi * scenario.program.profitMargin
    //        }
    //    }
    //
    //    var cpum: Double {
    //        get {
    //            return totalInvestment / totalUnits
    //        }
    //    }
    //
    //    var cpium: Double {
    //        get {
    //            return totalInvestment / totalIncrementalUnits
    //        }
    //    }
    
    var printCost: Double {
        get {
            if type == .Trial {
                return scenario.program.printCostId
            }
            else if type == .Loyalty {
                return scenario.program.printCostTransactional
            }
            else if type == .Volume {
                return scenario.program.printCostTransactional + (4 * scenario.program.ccmAnnouncementCost)
            }
            else {
                return scenario.program.printCostTransactional
            }
        }
    }
    
    var highestTriggerQuantity: Int {
        get {
            var highest = 0
            for coupon in coupons {
                if coupon.triggerQuantity > highest {
                    highest = coupon.triggerQuantity
                }
            }
            return highest
        }
    }
    
    var repeatFactor: Double {
        get {
            if type == .Trial {
                var rptIntrcpt: Double = 0
                var rptXdscntPct: Double = 0
                if (name == .LapsedBuyer) {
                    for result in scenario.program.report.data.results {
                        if result.target == "Trial - Lapsed" {
                            rptIntrcpt = result.repeatIntercept
                            rptXdscntPct = result.repeatXdiscountPercent
                            break
                        }
                    }
                }
                else if (name == .DataMining) {
                    for result in scenario.program.report.data.results {
                        if result.target == "Trial - DM" {
                            rptIntrcpt = result.repeatIntercept
                            rptXdscntPct = result.repeatXdiscountPercent
                            break
                        }
                    }
                }
                else if (name == .HeavyNeverBuy) {
                    for result in scenario.program.report.data.results {
                        if result.target == "Trial - Hvy NB" {
                            rptIntrcpt = result.repeatIntercept
                            rptXdscntPct = result.repeatXdiscountPercent
                            break
                        }
                    }
                }
                var discountPercent: Double = 0
                if let firstCoupon = coupons.first {
                    discountPercent = firstCoupon.discountPercent
                }
                return MathController.sharedMathController.getRepeatFactor(discountPercent, rptIntrcpt: rptIntrcpt, rptXdscntPct: rptXdscntPct)
            }
            else {
                return 1
            }
        }
    }
    
    func getTemplateOffer() -> IPOffer {
        if let templateScenario = scenario.program.templateScenario {
            for templateOffer in templateScenario.offers {
                if templateOffer.name == self.name {
                    return templateOffer
                }
            }
        }
        return IPOffer(scenario: scenario)
    }
    
    func getMaxDistributionForCycle(_ cycleName: String) -> Double {
        var result: Double = 0
        let templateOffer = getTemplateOffer()
        for coupon in templateOffer.coupons {
            result += coupon.distributionCycles[cycleName] ?? 0
        }
        return result
    }
    
    func getTotalDistributionForCycle(_ cycleName: String) -> Double {
        var result: Double = 0
        for coupon in coupons {
            result += coupon.distributionCycles[cycleName] ?? 0
        }
        return result
    }
    
    func getTotalDistributionForAllCycles() -> Double {
        var result: Double = 0
        for coupon in coupons {
            result += coupon.totalDistribution
        }
        return result
    }
    
    func getCpumForCycle(_ cycleName: String) -> Double {
        
        var totalCycleUnits: Double = 0
        var totalDistributionInvestment: Double = 0
        var totalRedemptionInvestment: Double = 0
        
        var tieBreaker: Double = 1
        if name == .CCM {
            tieBreaker = 1.04
        }
        else if name == .OUTU {
            tieBreaker = 1.03
        }
        else if name == .LapsedBuyer {
            tieBreaker = 1.02
        }
        else if name == .DataMining {
            tieBreaker = 1.01
        }
        else if name == .HeavyNeverBuy {
            tieBreaker = 1.00
        }
        
        for coupon in coupons {
            let templateCoupon = coupon.getTemplateCoupon()
            if type == .Trial || type == .Loyalty {
                totalCycleUnits += (templateCoupon.distributionCycles[cycleName]! * templateCoupon.redemptionRate) * templateCoupon.purchaseRequirement
            }
            else {
                totalCycleUnits += templateCoupon.distributionCycles[cycleName]! * templateCoupon.purchaseRequirement
            }
            totalDistributionInvestment += templateCoupon.distributionCycles[cycleName]! * printCost
            totalRedemptionInvestment += templateCoupon.distributionCycles[cycleName]! * templateCoupon.redemptionRate * (scenario.program.handlingFee + templateCoupon.promotionValue)
            
        }
        let totalCycleInvestment = totalDistributionInvestment + totalRedemptionInvestment
        
//        print(name.rawValue, cycleName, totalDistributionInvestment, totalRedemptionInvestment)
//        print(name.rawValue, cycleName, totalCycleInvestment, totalCycleUnits, (totalCycleInvestment / totalCycleUnits) / tieBreaker)
        return totalCycleUnits > 0 ?  (totalCycleInvestment / totalCycleUnits) / tieBreaker : 0
        
    }
    
    func getCpiumForCycle(_ cycleName: String) -> Double {
        
        var totalCycleIncrementalUnits: Double = 0
        var totalCycleInvestment: Double = 0
        
        var tieBreaker: Double = 1
        if name == .CCM {
            tieBreaker = 1.04
        }
        else if name == .OUTU {
            tieBreaker = 1.03
        }
        else if name == .LapsedBuyer {
            tieBreaker = 1.02
        }
        else if name == .DataMining {
            tieBreaker = 1.01
        }
        else if name == .HeavyNeverBuy {
            tieBreaker = 1.00
        }
        
        for coupon in coupons {
            if type == .Trial {
                if(scenario.program.shortTermROI){
                    totalCycleIncrementalUnits += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().purchaseRequirement * incrementalRatio * coupon.getTemplateCoupon().redemptionRate
                }
                else{
                    totalCycleIncrementalUnits += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().purchaseRequirement * incrementalRatio * coupon.getTemplateCoupon().redemptionRate * repeatFactor
                }
            }
            else if type == .Loyalty {
                totalCycleIncrementalUnits += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().purchaseRequirement * incrementalRatio * coupon.getTemplateCoupon().redemptionRate
            }
            else {
                totalCycleIncrementalUnits += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().purchaseRequirement * incrementalRatio
            }
            totalCycleInvestment += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().printCostMultiplier
            
        }
        
        return totalCycleIncrementalUnits > 0 ? (totalCycleInvestment / totalCycleIncrementalUnits) / tieBreaker : 0
        
    }
    
    func getRetailRoiForCycle(_ cycleName: String) -> Double {
        
        var totalCycleIncrementalDollars: Double = 0
        var totalCycleInvestment: Double = 0
        
        var tieBreaker: Double = 1
        if name == .CCM {
            tieBreaker = 1.04
        }
        else if name == .OUTU {
            tieBreaker = 1.03
        }
        else if name == .LapsedBuyer {
            tieBreaker = 1.02
        }
        else if name == .DataMining {
            tieBreaker = 1.01
        }
        else if name == .HeavyNeverBuy {
            tieBreaker = 1.00
        }
        
        for templateCoupon in getTemplateOffer().coupons {
            if type == .Trial {
                if scenario.program.shortTermROI {
                    totalCycleIncrementalDollars += templateCoupon.distributionCycles[cycleName]! * templateCoupon.purchaseRequirement * scenario.program.retailPricePerUnit * incrementalRatio * templateCoupon.redemptionRate
                }
                else {
                    totalCycleIncrementalDollars += templateCoupon.distributionCycles[cycleName]! * templateCoupon.purchaseRequirement * scenario.program.retailPricePerUnit * incrementalRatio * templateCoupon.redemptionRate * repeatFactor
                }
            }
            else if type == .Loyalty {
                totalCycleIncrementalDollars += templateCoupon.distributionCycles[cycleName]! * templateCoupon.purchaseRequirement * scenario.program.retailPricePerUnit * incrementalRatio * templateCoupon.redemptionRate
            }
            else {
                totalCycleIncrementalDollars += templateCoupon.distributionCycles[cycleName]! * templateCoupon.purchaseRequirement * scenario.program.retailPricePerUnit * incrementalRatio
            }
            totalCycleInvestment += templateCoupon.distributionCycles[cycleName]! * templateCoupon.printCostMultiplier
            
        }
        
        return totalCycleInvestment > 0 ? (totalCycleIncrementalDollars / totalCycleInvestment) * tieBreaker : 0
    }
    
    func getProfitRoiForCycle(_ cycleName: String) -> Double {
        return getRetailRoiForCycle(cycleName) * scenario.program.profitMargin
    }
    
    func getUnitsPerOfferForCycle(_ cycleName: String) -> Double {
        var totalCycleUnits: Double = 0
        var totalCycleDistribution: Double = 0
        
        for coupon in coupons {
            totalCycleUnits += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().purchaseRequirement
            totalCycleDistribution += coupon.getTemplateCoupon().distributionCycles[cycleName]!
        }
        
        return totalCycleDistribution > 0 ? totalCycleUnits / totalCycleDistribution : 0
    }
    
    func getOfferValueForCycle(_ cycleName: String) -> Double {
        var totalCycleDiscount: Double = 0
        var totalCycleDistribution: Double = 0
        
        for coupon in coupons {
            totalCycleDiscount += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().promotionValue
            totalCycleDistribution += coupon.getTemplateCoupon().distributionCycles[cycleName]!
        }
        
        return totalCycleDiscount / totalCycleDistribution
    }
    
    func getOfferCostMultiplierForCycle(_ cycleName: String) -> Double {
        let offerValue = getOfferValueForCycle(cycleName)
        let redemptionCostMultiplier = MathController.sharedMathController.getRedemptionCostMultiplier(scenario.program.handlingFee, promoValue: offerValue, redemptionRate: getRedemptionRateForCycle(cycleName))
        return MathController.sharedMathController.getOfferCostMultiplier(printCost, redemptionCostMulti: redemptionCostMultiplier)
    }
    
    func getDiscountPercentForCycle(_ cycleName: String) -> Double {
        var totalCycleDiscount: Double = 0
        var totalCycleDollars: Double = 0
        
        for coupon in coupons {
            totalCycleDiscount += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().promotionValue
            totalCycleDollars += coupon.getTemplateCoupon().distributionCycles[cycleName]! * scenario.program.retailPricePerUnit * coupon.getTemplateCoupon().purchaseRequirement
        }
        
        return totalCycleDollars > 0 ? totalCycleDiscount / totalCycleDollars : 0
        
    }
    
    func getRedemptionRateForCycle(_ cycleName: String) -> Double {
        var totalCycleRedemption: Double = 0
        var totalCycleDistribution: Double = 0
        
        for coupon in coupons {
            totalCycleRedemption += coupon.getTemplateCoupon().distributionCycles[cycleName]! * coupon.getTemplateCoupon().redemptionRate
            totalCycleDistribution += coupon.getTemplateCoupon().distributionCycles[cycleName]!
        }
        
        return totalCycleDistribution > 0 ? round((totalCycleRedemption / totalCycleDistribution) * 10000) / 10000 : 0
    }
    
    override func setNilValueForKey(_ key: String) {
        if key == "enabled" {
            self.enabled = true
        }
    }
    
}

//
//  OnboardingViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 5/9/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController, UIScrollViewDelegate {
    
    var doneButton: UIButton!
    var pageIndicator: UIPageControl!
    var scrollView: UIScrollView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.catalinaLightBlueColor()
        scrollView.frame = self.view.bounds
        scrollView.isPagingEnabled = true
        scrollView.alwaysBounceHorizontal = true
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width: self.view.bounds.width * 4, height: self.view.bounds.height)
        self.view.addSubview(scrollView)
        
        var position: CGFloat = 0
        for item in 1...4 {
            
            let image = UIImage(named: "onboarding-screen0\(item)")
            let imageView = UIImageView(frame: CGRect(x: position, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
            imageView.image = image
            scrollView.addSubview(imageView)
            position += self.view.bounds.width
        }
        
        let doneButtonImage = UIImage(named: "icon-arrow-right")?.withRenderingMode(.alwaysTemplate)

        doneButton = UIButton()
        doneButton.setImage(doneButtonImage, for: UIControlState())
        doneButton.setTitleColor(UIColor.white, for: UIControlState())
        doneButton.setTitleColor(UIColor.darkGray, for: .highlighted)
        doneButton.tintColor = UIColor.white
        doneButton.titleLabel?.font = UIFont.catalinaMediumFontOfSize(16)
        //trick to reverse the image and text position on the button
        doneButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        doneButton.titleLabel!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        doneButton.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        doneButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 4, 0)
        doneButton.sizeToFit()
        doneButton.addTarget(self, action: #selector(OnboardingViewController.doneButtonPressed), for: .touchUpInside)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(doneButton)
        self.view.addConstraint(NSLayoutConstraint(item: doneButton, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: -20))
        self.view.addConstraint(NSLayoutConstraint(item: doneButton, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 15))
        
        pageIndicator = UIPageControl()
        pageIndicator.numberOfPages = 4
        pageIndicator.currentPage = 0
        pageIndicator.isUserInteractionEnabled = false
        pageIndicator.backgroundColor = UIColor.clear
        pageIndicator.pageIndicatorTintColor = UIColor(white: 0.85, alpha: 1)// .lightGrayColor()
        pageIndicator.currentPageIndicatorTintColor = UIColor.white
        pageIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(pageIndicator)
        self.view.addConstraint(NSLayoutConstraint(item: pageIndicator, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -100))
        self.view.addConstraint(NSLayoutConstraint(item: pageIndicator, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        
        self.updatePageIndicator()
    }
    
    func doneButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        self.updatePageIndicator()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate {
            self.updatePageIndicator()
        }
    }
    
    func updatePageIndicator() {
        
        pageIndicator.currentPage = Int(scrollView.contentOffset.x / self.view.bounds.width)
        if pageIndicator.currentPage == pageIndicator.numberOfPages - 1 {
            doneButton.setTitle("DONE  ", for: UIControlState())
        } else {
            doneButton.setTitle("SKIP  ", for: UIControlState())
        }
    }
}

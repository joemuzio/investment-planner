//
//  IPProgram.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/18/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPProgram: IPModelObject {

    var clientName: String = ""
    var focusBrand: String = ""
    
    var printCostTransactional: Double = 0
    var printCostId: Double = 0
    var handlingFee: Double = 0
    var profitMargin: Double = 0
    var defaultInvestment: Double = 0
    var ccmAnnouncementCost: Double = 0
    var assumedUnitsPerTrialTrip: Double = 0
    var lapsedBuyerFactor: Double = 0
    //var trialDeduplicationAmount: Double = 0
    
    var retailPricePerUnit: Double = 0
    var shortTermROI: Bool = false
    
    var beginDate: Date = Date()
    var endDate: Date = Date()
    
    var templateScenario: IPScenario?
    var scenarios: [IPScenario] = [IPScenario]()
    
    var report: IPReport!
    
    init(report: IPReport) {
        super.init()
        self.report = report
    }
    
    func createTemplateScenario() -> IPScenario {
        
        let templateScenario = IPScenario(program: self)
        
        templateScenario.name = "Template"
        
        let lapsedBuyerOffer = createTemplateLapsedBuyerOffer(templateScenario)
        templateScenario.offers.append(lapsedBuyerOffer)
        let dataMiningOffer = createTemplateDataMiningOffer(templateScenario)
        templateScenario.offers.append(dataMiningOffer)
        let heavyCatNBOffer = createTemplateHeavyCatNeverBuyOffer(templateScenario)
        templateScenario.offers.append(heavyCatNBOffer)
        let outuOffer = createTemplateOutuOffer(templateScenario)
        templateScenario.offers.append(outuOffer)
        let ccmOffer = createTemplateCcmOffer(templateScenario)
        templateScenario.offers.append(ccmOffer)
        
        return templateScenario
    }
    
    func createNewScenario() -> IPScenario {
        
        let newScenario = templateScenario!.duplicate()
        newScenario.name = focusBrand
        newScenario.folder = "/"
        newScenario.isLocked = false
        if defaultInvestment > newScenario.maxBudget{
           SolverController.sharedSolverController.optimalProfitRoiForConstraints(newScenario, profitRoi: nil, trialPercent: nil, loyaltyPercent: nil, volumePercent: nil, investment: nil)
        }
        else{
            SolverController.sharedSolverController.optimalProfitRoiForConstraints(newScenario, profitRoi: nil, trialPercent: nil, loyaltyPercent: nil, volumePercent: nil, investment: defaultInvestment)
        }
        
        
        return newScenario
    }
    
    func createTemplateLapsedBuyerOffer(_ scenario: IPScenario) -> IPOffer {
        
        let promoValue = ceil(retailPricePerUnit * 0.4 / 0.25) * 0.25
        let discountPercent = promoValue / retailPricePerUnit
        
        var incShrtInt: Double = 0
        var incShrtXDscPct: Double = 0
        var rdmpIntrcpt: Double = 0
        var rdmpXdscntPct: Double = 0
        var rdmpXdscntAmt: Double = 0
        var rdmpXbuylvl: Double = 0
        
        for result in report.data.results {
            if result.target == "Trial - Lapsed" {
                incShrtInt = result.incrementalityShorttermIntercept
                incShrtXDscPct = result.incrementalityShorttermXdiscountPercent
                rdmpIntrcpt = result.redemptionIntercept
                rdmpXdscntPct = result.redemptionXdiscountPercent
                rdmpXdscntAmt = result.redemptionXdiscountAmount
                rdmpXbuylvl = result.redemptionXbuylevel
                break
            }
        }
        
        let templateOffer = IPOffer(scenario: scenario)
        templateOffer.name = .LapsedBuyer
        templateOffer.type = .Trial
        templateOffer.incrementalRatio = MathController.sharedMathController.getIncrementalRatio(discountPercent, incrShttrmIntrcpt: incShrtInt, incrShttrmXdscntPct: incShrtXDscPct, tlvType: templateOffer.type)
        
        let buy1Coupon = IPCoupon(offer: templateOffer)
        buy1Coupon.triggerQuantity = 1
        buy1Coupon.purchaseRequirement = 1
        buy1Coupon.promotionValue = promoValue
        buy1Coupon.redemptionRate = MathController.sharedMathController.getRedemptionRate(discountPercent, promoValue: promoValue, unitsMovedMulti: buy1Coupon.purchaseRequirement, rdmpIntrcpt: rdmpIntrcpt, rdmpXdscntPct: rdmpXdscntPct, rdmpXdscntAmt: rdmpXdscntAmt, rdmpXbuylvl: rdmpXbuylvl)
        
        templateOffer.coupons.append(buy1Coupon)
        
        if let totalShoppers = report.data.setup["Estimated Total Lapsed Buyers (Total Category and High, Medium, and Low Loyals)"] as? Double {
            buy1Coupon.distributionCycles["first"] = round(totalShoppers * 0.65)
            buy1Coupon.distributionCycles["second"] = round(totalShoppers * 0.65)
        }
        
        return templateOffer
    }
    
    func createTemplateDataMiningOffer(_ scenario: IPScenario) -> IPOffer {
        
        let promoValue = ceil(retailPricePerUnit * 0.4 / 0.25) * 0.25
        let discountPercent = promoValue / retailPricePerUnit
        
        var incShrtInt: Double = 0
        var incShrtXDscPct: Double = 0
        var rdmpIntrcpt: Double = 0
        var rdmpXdscntPct: Double = 0
        var rdmpXdscntAmt: Double = 0
        var rdmpXbuylvl: Double = 0
        
        for result in report.data.results {
            if result.target == "Trial - DM" {
                incShrtInt = result.incrementalityShorttermIntercept
                incShrtXDscPct = result.incrementalityShorttermXdiscountPercent
                rdmpIntrcpt = result.redemptionIntercept
                rdmpXdscntPct = result.redemptionXdiscountPercent
                rdmpXdscntAmt = result.redemptionXdiscountAmount
                rdmpXbuylvl = result.redemptionXbuylevel
                break
            }
        }
        
        let templateOffer = IPOffer(scenario: scenario)
        templateOffer.name = .DataMining
        templateOffer.type = .Trial
        templateOffer.incrementalRatio = MathController.sharedMathController.getIncrementalRatio(discountPercent, incrShttrmIntrcpt: incShrtInt, incrShttrmXdscntPct: incShrtXDscPct, tlvType: templateOffer.type)
        
        let buy1Coupon = IPCoupon(offer: templateOffer)
        buy1Coupon.triggerQuantity = 1
        buy1Coupon.purchaseRequirement = 1
        buy1Coupon.promotionValue = promoValue
        buy1Coupon.redemptionRate = MathController.sharedMathController.getRedemptionRate(discountPercent, promoValue: promoValue, unitsMovedMulti: buy1Coupon.purchaseRequirement, rdmpIntrcpt: rdmpIntrcpt, rdmpXdscntPct: rdmpXdscntPct, rdmpXdscntAmt: rdmpXdscntAmt, rdmpXbuylvl: rdmpXbuylvl)
        
        templateOffer.coupons.append(buy1Coupon)
        
        if let totalShoppers = report.data.setup["DM Target"] as? Double{
            buy1Coupon.distributionCycles["first"] = round(totalShoppers * 0.65)
            buy1Coupon.distributionCycles["second"] = round(totalShoppers * 0.65)
        }
        
        return templateOffer
    }
    
    func createTemplateHeavyCatNeverBuyOffer(_ scenario: IPScenario) -> IPOffer {
        
        var incShrtInt: Double = 0
        var incShrtXDscPct: Double = 0
        var rdmpIntrcpt: Double = 0
        var rdmpXdscntPct: Double = 0
        var rdmpXdscntAmt: Double = 0
        var rdmpXbuylvl: Double = 0
        
        for result in report.data.results {
            if result.target == "Trial - Hvy NB" {
                incShrtInt = result.incrementalityShorttermIntercept
                incShrtXDscPct = result.incrementalityShorttermXdiscountPercent
                rdmpIntrcpt = result.redemptionIntercept
                rdmpXdscntPct = result.redemptionXdiscountPercent
                rdmpXdscntAmt = result.redemptionXdiscountAmount
                rdmpXbuylvl = result.redemptionXbuylevel
                break
            }
        }
        
        let templateOffer = IPOffer(scenario: scenario)
        templateOffer.name = .HeavyNeverBuy
        templateOffer.type = .Trial
        
        let promoValue = ceil(retailPricePerUnit * 0.4 / 0.25) * 0.25
        let discountPercent = promoValue / retailPricePerUnit

        let buy1Coupon = IPCoupon(offer: templateOffer)
        buy1Coupon.triggerQuantity = 1
        buy1Coupon.purchaseRequirement = 1
        buy1Coupon.promotionValue = promoValue
        buy1Coupon.redemptionRate = MathController.sharedMathController.getRedemptionRate(discountPercent, promoValue: promoValue, unitsMovedMulti: buy1Coupon.purchaseRequirement, rdmpIntrcpt: rdmpIntrcpt, rdmpXdscntPct: rdmpXdscntPct, rdmpXdscntAmt: rdmpXdscntAmt, rdmpXbuylvl: rdmpXbuylvl)
        templateOffer.coupons.append(buy1Coupon)
        
        templateOffer.incrementalRatio = MathController.sharedMathController.getIncrementalRatio(discountPercent, incrShttrmIntrcpt: incShrtInt, incrShttrmXdscntPct: incShrtXDscPct, tlvType: templateOffer.type)

        if let totalShoppers = report.data.setup["Shoppers (Heavy Category and Never Buy)"] as? Double {
            buy1Coupon.distributionCycles["first"] = round((totalShoppers) * 0.65)
            buy1Coupon.distributionCycles["second"] = round((totalShoppers) * 0.65)
        }
        
        
        
        return templateOffer
    }
    
    func createTemplateOutuOffer(_ scenario: IPScenario) -> IPOffer {
        
        var incShrtInt: Double = 0
        var incShrtXDscPct: Double = 0
        var rdmpIntrcpt: Double = 0
        var rdmpXdscntPct: Double = 0
        var rdmpXdscntAmt: Double = 0
        var rdmpXbuylvl: Double = 0
        
        for result in report.data.results {
            if result.target == "OUTU" {
                incShrtInt = result.incrementalityShorttermIntercept
                incShrtXDscPct = result.incrementalityShorttermXdiscountPercent
                rdmpIntrcpt = result.redemptionIntercept
                rdmpXdscntPct = result.redemptionXdiscountPercent
                rdmpXdscntAmt = result.redemptionXdiscountAmount
                rdmpXbuylvl = result.redemptionXbuylevel
                break
            }
        }
        
        let templateOffer = IPOffer(scenario: scenario)
        templateOffer.name = .OUTU
        templateOffer.type = .Loyalty
        
        let triggerQuantities = getTriggerQuantitiesForOUTU()
        
        var templateCoupons: [Int: IPCoupon] = [Int : IPCoupon]()
        var highestTrigger: Int = 0
        
        for triggerQty in triggerQuantities {
            let newCoupon = IPCoupon(offer: templateOffer)
            newCoupon.triggerQuantity = triggerQty
            newCoupon.purchaseRequirement = Double(triggerQty + 1)
            
            let costPerRedemption =  ceil(MathController.sharedMathController.averageOf(ceil(retailPricePerUnit * 0.15 * Double(newCoupon.purchaseRequirement) / 0.25) * 0.25, floor(retailPricePerUnit * 0.20 * Double(newCoupon.purchaseRequirement) / 0.25) * 0.25) / 0.25) * 0.25
            newCoupon.promotionValue = Double(costPerRedemption)
            
            let discountPercent = newCoupon.promotionValue / (retailPricePerUnit * newCoupon.purchaseRequirement)
            newCoupon.redemptionRate = MathController.sharedMathController.getRedemptionRate(discountPercent, promoValue: newCoupon.promotionValue, unitsMovedMulti: newCoupon.purchaseRequirement, rdmpIntrcpt: rdmpIntrcpt, rdmpXdscntPct: rdmpXdscntPct, rdmpXdscntAmt: rdmpXdscntAmt, rdmpXbuylvl: rdmpXbuylvl)
            
            templateCoupons[triggerQty] = newCoupon
            templateOffer.coupons.append(newCoupon)
            
            if triggerQty > highestTrigger {
                highestTrigger = triggerQty
            }
        }
        
        var totalOfferDistribution: Double = 0
        
        for mpi in report.data.mpi {
            if let coupon = templateCoupons[mpi.buyQty] {
                if let value = coupon.distributionCycles[mpi.cycle] {
                    coupon.distributionCycles[mpi.cycle] = value + Double(mpi.trips) * 0.9
                }
                else {
                    coupon.distributionCycles[mpi.cycle] = Double(mpi.trips) * 0.9
                }
                totalOfferDistribution += Double(mpi.trips) * 0.9
            }
            else if mpi.buyQty > highestTrigger {
                if let coupon = templateCoupons[highestTrigger] {
                    if let value = coupon.distributionCycles[mpi.cycle] {
                        coupon.distributionCycles[mpi.cycle] = value + Double(mpi.trips) * 0.9
                    }
                    else{
                        coupon.distributionCycles[mpi.cycle] = Double(mpi.trips) * 0.9
                    }
                    totalOfferDistribution += Double(mpi.trips) * 0.9
                }
            }
        }
        
        var summedPromotionValue: Double = 0
        var summedDollars: Double = 0
        for coupon in templateOffer.coupons {
            for (_, distribution) in coupon.distributionCycles {
                summedPromotionValue += coupon.promotionValue * distribution * coupon.redemptionRate
                summedDollars += retailPricePerUnit * coupon.purchaseRequirement * distribution * coupon.redemptionRate
            }
        }
        
        let discountPercent = summedDollars > 0 ? summedPromotionValue / summedDollars : 0
        templateOffer.incrementalRatio = MathController.sharedMathController.getIncrementalRatio(discountPercent, incrShttrmIntrcpt: incShrtInt, incrShttrmXdscntPct: incShrtXDscPct, tlvType: templateOffer.type)
        
        return templateOffer
    }
    
    func createTemplateCcmOffer(_ scenario: IPScenario) -> IPOffer {
        
        var incShrtInt: Double = 0
        var incShrtXDscPct: Double = 0
        var rdmpIntrcpt: Double = 0
        var rdmpXdscntPct: Double = 0
        var rdmpXdscntAmt: Double = 0
        var rdmpXbuylvl: Double = 0
        
        for result in report.data.results {
            if result.target == "CCM" {
                incShrtInt = result.incrementalityShorttermIntercept
                incShrtXDscPct = result.incrementalityShorttermXdiscountPercent
                rdmpIntrcpt = result.redemptionIntercept
                rdmpXdscntPct = result.redemptionXdiscountPercent
                rdmpXdscntAmt = result.redemptionXdiscountAmount
                rdmpXbuylvl = result.redemptionXbuylevel
                break
            }
        }
        
        let templateOffer = IPOffer(scenario: scenario)
        templateOffer.name = .CCM
        templateOffer.type = .Volume
        var templateCoupons = [Int : IPCoupon]()
        var highestTrigger: Int = 0
        
        let triggerQuantities = getTriggerQuantitiesForCCM()
        
        var redemptionValues = [Double] ()
        for triggerQty in triggerQuantities {
            let highDiscount = (Double(triggerQty) * retailPricePerUnit) * 0.2
            let highest25Increment = floor(highDiscount / 0.25) * 0.25
            
            redemptionValues.append(highest25Increment)
        }
        
        for i in 0 ..< redemptionValues.count {
            var redemptionValue = redemptionValues[i]
            if i == 0 {
                if redemptionValue < 0.50 {
                    redemptionValue = 0.50
                }
            }
            else {
                let previousValue = redemptionValues[i-1]
                if previousValue >= redemptionValue {
                    redemptionValues[i] = previousValue + 0.25
                }
            }
        }
        for i in 0 ..< triggerQuantities.count {
            let triggerQty = triggerQuantities[i]
            let newCoupon = IPCoupon(offer: templateOffer)
            newCoupon.triggerQuantity = triggerQty
            newCoupon.purchaseRequirement = Double(newCoupon.triggerQuantity)
            
            let costPerRedemption = redemptionValues[i]
            newCoupon.promotionValue = Double(costPerRedemption)
            
            let discountPercent = newCoupon.promotionValue / (retailPricePerUnit * newCoupon.purchaseRequirement)
            newCoupon.redemptionRate = MathController.sharedMathController.getRedemptionRate(discountPercent, promoValue: newCoupon.promotionValue, unitsMovedMulti: newCoupon.purchaseRequirement, rdmpIntrcpt: rdmpIntrcpt, rdmpXdscntPct: rdmpXdscntPct, rdmpXdscntAmt: rdmpXdscntAmt, rdmpXbuylvl: rdmpXbuylvl)
            
            templateCoupons[triggerQty] = newCoupon
            templateOffer.coupons.append(newCoupon)
            
            if triggerQty > highestTrigger {
                highestTrigger = triggerQty
            }
        }
        
        var totalOfferDistribution: Double = 0

        for mpi in report.data.mpiCcm {
            if let coupon = templateCoupons[mpi.buyQty] {
                if let value = coupon.distributionCycles[mpi.cycle]{
                    coupon.distributionCycles[mpi.cycle] = value + Double(mpi.trips) * 0.9
                }
                else{
                    coupon.distributionCycles[mpi.cycle] = Double(mpi.trips) * 0.9
                }
                totalOfferDistribution += Double(mpi.trips) * 0.9
            }
            else if mpi.buyQty > highestTrigger {
                if let coupon = templateCoupons[highestTrigger] {
                    if let value = coupon.distributionCycles[mpi.cycle]{
                        coupon.distributionCycles[mpi.cycle] = value + Double(mpi.trips) * 0.9
                    }
                    else{
                        coupon.distributionCycles[mpi.cycle] = Double(mpi.trips) * 0.9
                    }
                    totalOfferDistribution += Double(mpi.trips) * 0.9
                }
            }
        }
        
        if let lastCoupon = templateCoupons[highestTrigger] {
            lastCoupon.purchaseRequirement = 0
            var totalUnits: Double = 0
            var totalTrips: Double = 0
            for i in lastCoupon.triggerQuantity ... 10 {
                for mpi in report.data.mpiCcm {
                    if mpi.buyQty == i {
                        totalUnits += Double(mpi.units)
                        totalTrips += Double(mpi.trips)
                    }
                }
            }
            lastCoupon.purchaseRequirement = totalUnits / totalTrips
        }
        
        var summedDiscountPercent: Double = 0
        for coupon in templateOffer.coupons {
            for (_, distribution) in coupon.distributionCycles {
                let discountPercent = coupon.promotionValue / (retailPricePerUnit * coupon.purchaseRequirement)
                summedDiscountPercent += discountPercent * distribution
            }
        }

        let weightedDiscountPercent = totalOfferDistribution > 0 ? summedDiscountPercent / totalOfferDistribution : 0
        templateOffer.incrementalRatio = MathController.sharedMathController.getIncrementalRatio(weightedDiscountPercent, incrShttrmIntrcpt: incShrtInt, incrShttrmXdscntPct: incShrtXDscPct, tlvType: templateOffer.type)
        
        return templateOffer
    }
    
    func getTriggerQuantitiesForOUTU()->[Int] {
        var buyQtysArray = [Int]()
        var cumTrips: Double = 0
        let mpiTotalTrips = Double(report.data.mpiTotal.trips)
        for i in 1 ... 10 {
            let totalAmount = getTotalTripsForBuyQty(report.data.mpi, buyQty: i)
            cumTrips += totalAmount
            let percent = cumTrips / mpiTotalTrips
            if(i == 1){
                if(percent > 0.9){
                    buyQtysArray.append(i)
                    print("<<< Should be Own User Repeat")
                    break
                }
                else{
                    buyQtysArray.append(i)
                }
            }
            else {
                if(percent < 0.95){
                    buyQtysArray.append(i)
                }else if(percent < 0.98){
                    buyQtysArray.append(i)
                    break
                }
                else{
                    break
                }
            }
        }
        return buyQtysArray
    }
    
    func getTriggerQuantitiesForCCM()->[Int]{
        var buyLevelOver50: Int = 1
        var totalCumUnitPercent: Double = 0.0
        
        for i in 1 ... 10 {
            var numberOfMpi: Double = 0.0
            
            for ccmMpi in report.data.mpiCcm {
                if ccmMpi.buyQty == i {
                    totalCumUnitPercent += ccmMpi.cumulativeUnitsPercent
                    numberOfMpi += 1.0
                }
            }
            print(numberOfMpi)
            print(totalCumUnitPercent)
            let avgCumUnitPercent = numberOfMpi != 0 ? totalCumUnitPercent / numberOfMpi : 0.0
            
            if avgCumUnitPercent > 0.5 {
                buyLevelOver50 = i
                if i == 1{
                    buyLevelOver50 = 2
                }
                break
            }
        }
        
        if buyLevelOver50 > 8 {
            buyLevelOver50 = 8
        }
        
        return [buyLevelOver50, buyLevelOver50+1, buyLevelOver50+2]
    }
    
    func getTotalTripsForBuyQty(_ mpiArray: [IPMPI], buyQty: Int) -> Double {
        var totalAmount: Double = 0
        for mpiReport in mpiArray {
            if(buyQty == mpiReport.buyQty){
                totalAmount += Double(mpiReport.trips)
            }
        }
        return totalAmount
    }
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "clientName",
            "focusBrand",
            "printCostTransactional",
            "printCostId",
            "handlingFee",
            "profitMargin",
            "defaultInvestment",
            "ccmAnnouncementCost",
            "assumedUnitsPerTrialTrip",
            "lapsedBuyerFactor",
            //"trialDeduplicationAmount",
            "retailPricePerUnit",
            "shortTermROI",
            "beginDate",
            "endDate",
            "templateScenario",
            "scenarios"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "beginDate" {
                if let stringValue = value as? String {
                    if let date = IPModelObject.standardUTCDateFormatter.date(from: stringValue) {
                        self.beginDate = date
                    }
                }
                return
            }
            if key == "endDate" {
                if let stringValue = value as? String {
                    if let date = IPModelObject.standardUTCDateFormatter.date(from: stringValue) {
                        self.endDate = date
                    }
                }
                return
            }
            if key == "templateScenario" {
                if let JSONDictionary = value as? [String: AnyObject] {
                    let scenario = IPScenario(program: self)
                    scenario.updateWithJSONObject(JSONDictionary)
                    self.templateScenario = scenario
                }
                return
            }
            if key == "scenarios" {
                var scenarios = [IPScenario]()
                if let JSONArray = value as? [[String: AnyObject]] {
                    for item in JSONArray {
                        let scenario = IPScenario(program: self)
                        scenario.updateWithJSONObject(item)
                        scenarios.append(scenario)
                    }
                }
                self.scenarios = scenarios
                return
            }
        }
        super.setValue(value, forKey: key)
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "beginDate" {
                return IPModelObject.standardUTCDateFormatter.string(from: self.beginDate)
            }
            if key == "endDate" {
                return IPModelObject.standardUTCDateFormatter.string(from: self.endDate)
            }
            if key == "templateScenario" {
                if let scenario = self.templateScenario {
                    return scenario.JSONObject()
                }
                return nil
            }
            if key == "scenarios" {
                var JSONArray = [[String : AnyObject]]()
                for scenario in self.scenarios {
                    JSONArray.append(scenario.JSONObject())
                }
                return JSONArray
            }
        }
        return super.value(forKey: key)
    }
    
}

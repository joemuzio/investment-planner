//
//  ComparisonStagingView.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/12/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ComparisonStagingView: UIView {
    
    fileprivate(set) var compareViewCenter: ComparisonPlaceholderView!
    fileprivate(set) var compareViewLeft: ComparisonPlaceholderView!
    fileprivate(set) var compareViewRight: ComparisonPlaceholderView!
    fileprivate(set) var compareNameCenter: UILabel!
    fileprivate(set) var compareNameLeft: UILabel!
    fileprivate(set) var compareNameRight: UILabel!
    fileprivate(set) var compareButton: UIButton!
    
    fileprivate(set) var hasTwoStagedObjects = false {
        didSet {
            compareButton.isEnabled = hasTwoStagedObjects
        }
    }
    fileprivate(set) var hasThreeStagedObjects = false
    
    var compareActionHandler: ((ComparisonStagingView) -> Void)?
    
    var isActive: Bool = false {
        
        didSet {
            if isActive {
                self.backgroundColor = UIColor.catalinaLightBlueColor()
            } else {
                self.backgroundColor = UIColor.catalinaLightGreenColor()
            }
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.catalinaLightGreenColor()
        
        compareViewCenter = ComparisonPlaceholderView()
        compareViewCenter.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareViewCenter)
        compareViewCenter.addConstraint(NSLayoutConstraint(item: compareViewCenter, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80))
        compareViewCenter.addConstraint(NSLayoutConstraint(item: compareViewCenter, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 80))
        self.addConstraint(NSLayoutConstraint(item: compareViewCenter, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: compareViewCenter, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        compareViewLeft = ComparisonPlaceholderView()
        compareViewLeft.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareViewLeft)
        compareViewLeft.addConstraint(NSLayoutConstraint(item: compareViewLeft, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80))
        compareViewLeft.addConstraint(NSLayoutConstraint(item: compareViewLeft, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 80))
        self.addConstraint(NSLayoutConstraint(item: compareViewLeft, attribute: .right, relatedBy: .equal, toItem: compareViewCenter, attribute: .left, multiplier: 1, constant: -60))
        self.addConstraint(NSLayoutConstraint(item: compareViewLeft, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        compareViewRight = ComparisonPlaceholderView()
        compareViewRight.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareViewRight)
        compareViewRight.addConstraint(NSLayoutConstraint(item: compareViewRight, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80))
        compareViewRight.addConstraint(NSLayoutConstraint(item: compareViewRight, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 80))
        self.addConstraint(NSLayoutConstraint(item: compareViewRight, attribute: .left, relatedBy: .equal, toItem: compareViewCenter, attribute: .right, multiplier: 1, constant: 60))
        self.addConstraint(NSLayoutConstraint(item: compareViewRight, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        compareNameCenter = UILabel()
        compareNameCenter.textAlignment = .center
        compareNameCenter.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareNameCenter)
        compareNameCenter.addConstraint(NSLayoutConstraint(item: compareNameCenter, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80))
        compareNameCenter.addConstraint(NSLayoutConstraint(item: compareNameCenter, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: compareNameCenter, attribute: .centerX, relatedBy: .equal, toItem: compareViewCenter, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: compareNameCenter, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -50))
        
        compareNameLeft = UILabel()
        compareNameLeft.textAlignment = .center
        compareNameLeft.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareNameLeft)
        compareNameLeft.addConstraint(NSLayoutConstraint(item: compareNameLeft, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80))
        compareNameLeft.addConstraint(NSLayoutConstraint(item: compareNameLeft, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: compareNameLeft, attribute: .right, relatedBy: .equal, toItem: compareNameCenter, attribute: .left, multiplier: 1, constant: -60))
        self.addConstraint(NSLayoutConstraint(item: compareNameLeft, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -50))
        
        compareNameRight = UILabel()
        compareNameRight.textAlignment = .center
        compareNameRight.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareNameRight)
        compareNameRight.addConstraint(NSLayoutConstraint(item: compareNameRight, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80))
        compareNameRight.addConstraint(NSLayoutConstraint(item: compareNameRight, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: compareNameRight, attribute: .left, relatedBy: .equal, toItem: compareNameCenter, attribute: .right, multiplier: 1, constant: 60))
        self.addConstraint(NSLayoutConstraint(item: compareNameRight, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -50))
        
        compareButton = UIButton()
        compareButton.setImage(UIImage(named: "button-compare-growth-plan"), for: UIControlState())
        compareButton.addTarget(self, action: #selector(ComparisonStagingView.compareButtonPressed), for: .touchUpInside)
        compareButton.isEnabled = false
        compareButton.sizeToFit()
        compareButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(compareButton)
        self.addConstraint(NSLayoutConstraint(item: compareButton, attribute: .left, relatedBy: .equal, toItem: compareViewRight, attribute: .right, multiplier: 1, constant: 60))
        self.addConstraint(NSLayoutConstraint(item: compareButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    internal func compareButtonPressed() {
        if let compare = compareActionHandler {
            compare(self)
        }
    }
    
    func dropComparisonObjectRepresentationView(_ view: UIView, _ scenarioName: String) {
        
        if self.compareViewLeft.comparisonObjectRepresentationView == nil {
            self.compareViewLeft.comparisonObjectRepresentationView = view
            self.compareNameLeft.text = scenarioName
            hasTwoStagedObjects = false
            hasThreeStagedObjects = false
        } else if self.compareViewCenter.comparisonObjectRepresentationView == nil {
            self.compareViewCenter.comparisonObjectRepresentationView = view
            self.compareNameCenter.text = scenarioName
            hasTwoStagedObjects = true
            hasThreeStagedObjects = false
        } else if compareViewRight.comparisonObjectRepresentationView == nil {
            self.compareViewRight.comparisonObjectRepresentationView = view
            self.compareNameRight.text = scenarioName
            hasTwoStagedObjects = true
            hasThreeStagedObjects = true
        }
    }

    func clearComparison() {
        
        compareViewLeft.comparisonObjectRepresentationView = nil
        compareViewCenter.comparisonObjectRepresentationView = nil
        compareViewRight.comparisonObjectRepresentationView = nil
        compareNameLeft.text = ""
        compareNameCenter.text = ""
        compareNameRight.text = ""
        hasThreeStagedObjects = false
        hasTwoStagedObjects = false
    }
    
}

class ComparisonPlaceholderView: UIView {
    
    var shapeLayer = CAShapeLayer()
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.lightGray
        self.borderColor = UIColor.catalinaWhiteColor()
        
        shapeLayer.lineWidth = 2
        shapeLayer.lineDashPattern = [10,5]
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.catalinaWhiteColor().cgColor
        shapeLayer.frame = self.bounds.insetBy(dx: 1, dy: 1)
        shapeLayer.path = UIBezierPath(rect: shapeLayer.bounds).cgPath
        shapeLayer.zPosition = 1
        
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
        self.layer.addSublayer(shapeLayer)
        
    }
    
    override func layoutSublayers(of Layer: CALayer) {
        
        if layer == self.layer {
            shapeLayer.frame = self.bounds.insetBy(dx: 1, dy: 1)
            shapeLayer.path = UIBezierPath(roundedRect: shapeLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 6, height: 6)).cgPath
        }
    }
    
    var borderColor: UIColor! {
        
        didSet {
            shapeLayer.strokeColor = borderColor.cgColor
        }
    }
    
    var borderIsMoving: Bool {
        
        get {
            return self.layer.animation(forKey: "pythagoras.animation.ants-marching") != nil
        }
        set {
            if newValue {
                let animation = CABasicAnimation(keyPath: "lineDashPhase")
                animation.fromValue = 0
                animation.toValue = 15
                animation.duration = 0.3
                animation.repeatCount = .infinity
                shapeLayer.add(animation, forKey: "pythagoras.animation.ants-marching")
            } else {
                shapeLayer.removeAnimation(forKey: "pythagoras.animation.ants-marching")
            }
        }
    }
    
    var comparisonObjectRepresentationView: UIView? = nil {
        
        didSet {
            if let view = comparisonObjectRepresentationView {
                self.addSubview(view)
                self.borderColor = UIColor.catalinaDarkBlueColor()
                self.backgroundColor = UIColor.catalinaWhiteColor()
            } else {
                for view in self.subviews {
                    view.removeFromSuperview()
                }
                self.borderColor = UIColor.catalinaWhiteColor()
                self.backgroundColor = UIColor.lightGray
            }
        }
    }
    
}

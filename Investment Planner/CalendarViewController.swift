//
//  CalendarViewController.swift
//  Investment Planner
//
//  Created by Douglas Wall on 8/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController{
    
    var scenario: IPScenario!
    var backButton: UIButton!
    var financialDataButton: UIButton!
    
    var header: FinancialHeaderView!
    var statementLabel: UILabel!
    
    var topView: UIView!
    var leftView: UIView!
    var rightView: UIView!
    var bottomView: UIView!
    
    var cycleCalendarView:CycleCalendarView!
    var pieChartView: FinancialChartView!
    
    override func loadView() {
        
        super.loadView()
        
        topView = UIView()
        topView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(topView)
        
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 64))
        
        leftView = UIView()
        leftView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(leftView)
        
        self.view.addConstraint(NSLayoutConstraint(item: leftView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: leftView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0.6, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: leftView, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: leftView, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.5, constant: 0))
        
        rightView = UIView()
        rightView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(rightView)
        
        self.view.addConstraint(NSLayoutConstraint(item: rightView, attribute: .left, relatedBy: .equal, toItem: leftView, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: rightView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: rightView, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: rightView, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.5, constant: 0))
        
        bottomView = UIView()
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(bottomView)
        
        self.view.addConstraint(NSLayoutConstraint(item: bottomView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: bottomView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: bottomView, attribute: .top, relatedBy: .equal, toItem: leftView, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: bottomView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        backButton = UIButton()
        backButton.setImage(UIImage(named: "icon-back-dark-bg"), for: UIControlState())
        backButton.sizeToFit()
        backButton.addTarget(self, action: #selector(self.backButtonPressed), for: .touchUpInside)
        
        financialDataButton = UIButton()
        financialDataButton.setImage(UIImage(named: "roi-chart-white-icon"), for: UIControlState())
        financialDataButton.sizeToFit()
        financialDataButton.addTarget(self, action: #selector(self.financialDataButtonPressed), for: .touchUpInside)
        
        header = FinancialHeaderView()
        header.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 0)
        header.titleLabel.text = scenario.program.report.brand + " Calendar" //late decision to remove titles
        header.leftButtons = [backButton]
        header.rightButtons = [financialDataButton]
        header.sizeToFit()
        topView.addSubview(header)
        topView.addConstraint(NSLayoutConstraint(item: header, attribute: .top, relatedBy: .equal, toItem: topView, attribute: .top, multiplier: 1, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: header, attribute: .height, relatedBy: .equal, toItem: topView, attribute: .height, multiplier: 1, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: header, attribute: .left, relatedBy: .equal, toItem: topView, attribute: .left, multiplier: 1, constant: 0))
        topView.addConstraint(NSLayoutConstraint(item: header, attribute: .width, relatedBy: .equal, toItem: topView, attribute: .width, multiplier: 1, constant: 0))
        
        
        
        statementLabel = UILabel()
        statementLabel.translatesAutoresizingMaskIntoConstraints = false
        statementLabel.numberOfLines = 0
        statementLabel.lineBreakMode = .byWordWrapping
        statementLabel.font = UIFont.catalinaBookFontOfSize(36)
        statementLabel.textAlignment = .center
        statementLabel.backgroundColor = UIColor.clear
        updateStatementLabelText(scenario: scenario)
        leftView.addSubview(statementLabel)
        
        leftView.addConstraint(NSLayoutConstraint(item: statementLabel, attribute: .centerX, relatedBy: .equal, toItem: leftView, attribute: .centerX, multiplier: 1, constant: 0))
        leftView.addConstraint(NSLayoutConstraint(item: statementLabel, attribute: .centerY, relatedBy: .equal, toItem: leftView, attribute: .centerY, multiplier: 1, constant: 0))
        
        var distributionBudget = 0.0
        for offer in scenario.offers {
            for coupon in offer.coupons {
                distributionBudget += (coupon.totalDistribution * coupon.offer.printCost)
            }
        }
        
        var redemptionBudget = 0.0
        for offer in scenario.offers {
            for coupon in offer.coupons {
                redemptionBudget += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
            }
        }
        
        pieChartView = FinancialChartView()
        pieChartView.translatesAutoresizingMaskIntoConstraints = false
        _ = pieChartView.chart.addSectionWithValue(scenario.trialPercent, color: UIColor.catalinaDarkBlueColor(), title: "Trial", text: scenario.trialPercent.percentString(0), scaleAmount: nil, translateAmount: nil)
        _ = pieChartView.chart.addSectionWithValue(scenario.loyaltyPercent, color: UIColor.catalinaLightBlueColor(), title: "Loyalty", text: scenario.loyaltyPercent.percentString(0), scaleAmount: nil, translateAmount: nil)
        _ = pieChartView.chart.addSectionWithValue(scenario.volumePercent, color: UIColor.catalinaLightGreenColor(), title: "Volume", text: scenario.volumePercent.percentString(0), scaleAmount: nil, translateAmount: nil)
        rightView.addSubview(pieChartView)
        
        rightView.addConstraint(NSLayoutConstraint(item: pieChartView, attribute: .left, relatedBy: .equal, toItem: rightView, attribute: .left, multiplier: 1, constant: 0))
        rightView.addConstraint(NSLayoutConstraint(item: pieChartView, attribute: .right, relatedBy: .equal, toItem: rightView, attribute: .right, multiplier: 1, constant: 0))
        rightView.addConstraint(NSLayoutConstraint(item: pieChartView, attribute: .top, relatedBy: .equal, toItem: rightView, attribute: .top, multiplier: 1, constant: 0))
        rightView.addConstraint(NSLayoutConstraint(item: pieChartView, attribute: .bottom, relatedBy: .equal, toItem: rightView, attribute: .bottom, multiplier: 1, constant: 0))
        
        cycleCalendarView = CycleCalendarView(scenario: scenario)
        cycleCalendarView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(cycleCalendarView)
        
        bottomView.addConstraint(NSLayoutConstraint(item: cycleCalendarView, attribute: .left, relatedBy: .equal, toItem: bottomView, attribute: .left, multiplier: 1, constant: 0))
        bottomView.addConstraint(NSLayoutConstraint(item: cycleCalendarView, attribute: .right, relatedBy: .equal, toItem: bottomView, attribute: .right, multiplier: 1, constant: 0))
        bottomView.addConstraint(NSLayoutConstraint(item: cycleCalendarView, attribute: .top, relatedBy: .equal, toItem: bottomView, attribute: .top, multiplier: 1, constant: 0))
        bottomView.addConstraint(NSLayoutConstraint(item: cycleCalendarView, attribute: .bottom, relatedBy: .equal, toItem: bottomView, attribute: .bottom, multiplier: 1, constant: 0))
        
        cycleCalendarView.lapsedBuyerRow.addButtonTarget(target: self, selector: #selector(lapsedBuyerButtonPressed(sender:)))
        cycleCalendarView.dataMiningRow.addButtonTarget(target: self, selector: #selector(dataMiningButtonPressed(sender:)))
        cycleCalendarView.heavyCategoryNeverBuyRow.addButtonTarget(target: self, selector: #selector(heavyCategoryNeverBuyButtonPressed(sender:)))
        cycleCalendarView.expandConsumptionRow.addButtonTarget(target: self, selector: #selector(expandConsumptionButtonPressed(sender:)))
        cycleCalendarView.ccmRow.addButtonTarget(target: self, selector: #selector(ccmButtonPressed(sender:)))
        
        updatePieChart(scenario: scenario)
    }
    
    // MARK: - Actions
    
    func backButtonPressed() {
        _ = scenario.program.report.save()
        presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    
    func financialDataButtonPressed() {
        let financialSummary = FinancialSummaryViewController()
        financialSummary.scenario = scenario
        self.present(financialSummary, animated: true, completion: nil)
    }
    func lapsedBuyerButtonPressed(sender: CalendarButton){
        if sender.isOn{
            sender.backgroundColor = UIColor.lightGray
            sender.isOn = false
        }
        else{
            sender.backgroundColor = UIColor.catalinaDarkBlueColor()
            sender.isOn = true
        }
        cycleCalendarView.lapsedBuyerRow.enableDisableButtonsOnCount()
        updateCycle(scenario: scenario, offerName: IPOfferName.LapsedBuyer, cycleString: sender.cycleString, turnOn: sender.isOn)
        scenario.isLocked = true
    }
    func dataMiningButtonPressed(sender: CalendarButton){
        if sender.isOn{
            sender.backgroundColor = UIColor.lightGray
            sender.isOn = false
        }
        else{
            sender.backgroundColor = UIColor.catalinaDarkBlueColor()
            sender.isOn = true
        }
        cycleCalendarView.dataMiningRow.enableDisableButtonsOnCount()
        updateCycle(scenario: scenario, offerName: IPOfferName.DataMining, cycleString: sender.cycleString, turnOn: sender.isOn)
        scenario.isLocked = true
        
    }
    func heavyCategoryNeverBuyButtonPressed(sender: CalendarButton){
        if sender.isOn{
            sender.backgroundColor = UIColor.lightGray
            sender.isOn = false
        }
        else{
            sender.backgroundColor = UIColor.catalinaDarkBlueColor()
            sender.isOn = true
        }
        cycleCalendarView.heavyCategoryNeverBuyRow.enableDisableButtonsOnCount()
        updateCycle(scenario: scenario, offerName: IPOfferName.HeavyNeverBuy, cycleString: sender.cycleString, turnOn: sender.isOn)
        scenario.isLocked = true
    }
    func expandConsumptionButtonPressed(sender: CalendarButton){
        if sender.isOn{
            sender.backgroundColor = UIColor.lightGray
            sender.isOn = false
        }
        else{
            sender.backgroundColor = UIColor.catalinaLightBlueColor()
            sender.isOn = true
        }
        updateCycle(scenario: scenario, offerName: IPOfferName.OUTU, cycleString: sender.cycleString, turnOn: sender.isOn)
        scenario.isLocked = true
    }
    func ccmButtonPressed(sender: CalendarButton){
        if sender.isOn{
            sender.backgroundColor = UIColor.lightGray
            sender.isOn = false
        }
        else{
            sender.backgroundColor = UIColor.catalinaLightGreenColor()
            sender.isOn = true
        }
        updateCycle(scenario: scenario, offerName: IPOfferName.CCM, cycleString: sender.cycleString, turnOn: sender.isOn)
        scenario.isLocked = true
    }
    
    fileprivate func updateStatementLabelText(scenario: IPScenario){
        var kpiValueString = ""
        var kpiString = ""
        var efficiencyValueString = ""
        var efficiencyString = ""
        
        if let selectedMetrics = SettingsController.sharedSettingsController.vitoStatementSelectedMetrics as? [String: String] {
            let selectedKpi: String = (selectedMetrics["kpi"] ?? "Total Dollars")
            kpiString = selectedKpi
            if kpiString == SelectedKPI.incrementalDollars.rawValue {
                kpiValueString = scenario.totalIncrementalDollars.abbreviatedCurrencyString()
            } else if kpiString == SelectedKPI.incrementalUnits.rawValue {
                kpiValueString = scenario.totalIncrementalUnits.abbreviatedNumberString()
            } else if kpiString == SelectedKPI.totalDollars.rawValue {
                kpiValueString = scenario.totalDollars.abbreviatedCurrencyString()
            } else if kpiString == SelectedKPI.totalUnits.rawValue {
                kpiValueString = scenario.totalUnits.abbreviatedNumberString()
            } else if kpiString == SelectedKPI.acquiredBuyers.rawValue {
                kpiValueString = scenario.totalAcquiredBuyers.abbreviatedNumberString()
            }
            
            let selectedEfficiency: String = (selectedMetrics["efficiency"] ?? "Profit ROI")
            efficiencyString = selectedEfficiency
            if efficiencyString == SelectedEfficiency.cpium.rawValue {
                efficiencyValueString = scenario.cpium.currencyString(2)
            } else if efficiencyString == SelectedEfficiency.cpum.rawValue {
                efficiencyValueString = scenario.cpum.currencyString(2)
            } else if efficiencyString == SelectedEfficiency.profitROI.rawValue {
                efficiencyValueString = scenario.profitRoi.currencyString(2)
            } else if efficiencyString == SelectedEfficiency.retailROI.rawValue {
                efficiencyValueString = scenario.retailRoi.currencyString(2)
            }
        }
        else {
            kpiString = "Total Dollars"
            kpiValueString = scenario.totalDollars.abbreviatedCurrencyString()
            
            efficiencyString = "Profit ROI"
            efficiencyValueString = scenario.profitRoi.currencyString(2)
        }
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "An investment of ", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaBlackColor()]))
        attributedString.append(NSAttributedString(string: scenario.totalInvestment.abbreviatedCurrencyString() + "\n", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaLightBlueColor()]))
        attributedString.append(NSAttributedString(string: "will drive ", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaBlackColor()]))
        attributedString.append(NSAttributedString(string: kpiValueString + "\n", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaLightBlueColor()]))
        attributedString.append(NSAttributedString(string: kpiString, attributes: [ NSForegroundColorAttributeName: UIColor.catalinaLightBlueColor()]))
        attributedString.append(NSAttributedString(string: " to\n", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaBlackColor()]))
        attributedString.append(NSAttributedString(string: "your brand at a ", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaBlackColor()]))
        attributedString.append(NSAttributedString(string: efficiencyValueString + "\n", attributes: [ NSForegroundColorAttributeName: UIColor.catalinaLightBlueColor()]))
        attributedString.append(NSAttributedString(string: efficiencyString, attributes: [ NSForegroundColorAttributeName: UIColor.catalinaLightBlueColor()]))
        statementLabel.attributedText = attributedString
        statementLabel.sizeToFit()
    }
    fileprivate func updatePieChart(scenario: IPScenario){
        if let trialSection = pieChartView.chart.sectionWithTitle("Trial"){
            if scenario.trialPercent > 0{
                trialSection.value = scenario.trialPercent
                trialSection.text = scenario.trialPercent.percentString(0)
                trialSection.color = UIColor.catalinaDarkBlueColor()
            }
            else{
                trialSection.value = 0
                trialSection.text = ""
                trialSection.color = .clear
                
            }
        }
        if let loyaltySection = pieChartView.chart.sectionWithTitle("Loyalty"){
            if scenario.loyaltyPercent > 0{
                loyaltySection.value = scenario.loyaltyPercent
                loyaltySection.text = scenario.loyaltyPercent.percentString(0)
                loyaltySection.color = UIColor.catalinaLightBlueColor()
            }
            else{
                loyaltySection.value = 0
                loyaltySection.text = ""
                loyaltySection.color = .clear
            }
        }
        if let volumeSection = pieChartView.chart.sectionWithTitle("Volume"){
            if scenario.volumePercent > 0{
                volumeSection.value = scenario.volumePercent
                volumeSection.text = scenario.volumePercent.percentString(0)
                volumeSection.color = UIColor.catalinaLightGreenColor()
            }
            else{
                volumeSection.value = 0
                volumeSection.text = ""
                volumeSection.color = .clear
            }
        }
        pieChartView.chart.update()
    }
    fileprivate func updateCycle(scenario: IPScenario, offerName: IPOfferName, cycleString: String, turnOn: Bool) {
        for offer in scenario.offers {
            if offer.name == offerName{
                for coupon in offer.coupons {
                    let templateCoupon = coupon.getTemplateCoupon()
                    for (cycleName, distribution) in templateCoupon.distributionCycles {
                        if cycleName.contains(cycleString){
                            if turnOn{
                                coupon.distributionCycles[cycleName] = distribution
                            }
                            else{
                                coupon.distributionCycles[cycleName] = 0
                            }
                        }
                        else{
                            // is trial
                            if offer.type == .Trial{
                                var onCount = 0
                                switch offer.name {
                                case .LapsedBuyer:
                                    onCount = cycleCalendarView.lapsedBuyerRow.buttonOnCount
                                case .DataMining:
                                    onCount = cycleCalendarView.dataMiningRow.buttonOnCount
                                case.HeavyNeverBuy:
                                    onCount = cycleCalendarView.heavyCategoryNeverBuyRow.buttonOnCount
                                default:
                                    ()
                                }
                                if turnOn{
                                    switch onCount {
                                    case 1:
                                        coupon.distributionCycles["second"] = distribution
                                    case 2:
                                        coupon.distributionCycles["first"] = distribution
                                    default:
                                        ()
                                    }
                                }
                                else{
                                    switch onCount {
                                    case 0:
                                        coupon.distributionCycles["second"] = 0
                                    case 1:
                                        coupon.distributionCycles["first"] = 0
                                    default:
                                        ()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        updateStatementLabelText(scenario: scenario)
        updatePieChart(scenario: scenario)
        
    }
}

class FinancialChartView: UIView {
    
    var titleLabel: UILabel!
    var chart: PieChart!
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.clear
        
        titleLabel = UILabel()
        titleLabel.text = "Investment Allocation"
        titleLabel.font = UIFont.catalinaBookFontOfSize(28)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .width, relatedBy: .lessThanOrEqual, toItem: self, attribute: .width, multiplier: 1, constant: 0))
        
        chart = PieChart()
        chart.textFont = UIFont.catalinaBoldFontOfSize(12)
        chart.titleFont = UIFont.catalinaMediumFontOfSize(10)
        chart.radius = UIScreen.main.bounds.size.width * 0.4 * 0.225
        
        chart.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(chart)
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 25))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: chart, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0))
    }
}

//
//  MathController.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 12/21/15.
//  Copyright © 2015 Pythagoras.io. All rights reserved.
//

import Foundation
import UIKit
import Accelerate

private let SharedMathControllerInstance = MathController()

class MathController:NSObject {
    
    class var sharedMathController: MathController {
        
        return SharedMathControllerInstance
    }
    
    func averageOf(_ numbers: Double...) -> Float {
        let numberTotal = numbers.count
        if numberTotal == 0 {
            return 0
        }
        var sum = 0.0
        
        for number in numbers {
            sum += number
        }
        return Float(sum)/Float(numberTotal)
    }
    
    func getRedemptionCostMultiplier(_ handlingFee: Double, promoValue: Double, redemptionRate: Double) -> Double {
        return (handlingFee + promoValue) * redemptionRate
    }

    func getOfferCostMultiplier(_ printCost: Double, redemptionCostMulti: Double) -> Double {
        return redemptionCostMulti + printCost
    }
    
    func getCpum(_ offerCostMulti: Double, unitsMovedMulti: Double, redemptionRate: Double, repeatFactor: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Volume) {
            return unitsMovedMulti > 0 ? offerCostMulti / unitsMovedMulti : 0
        }
        else {
            if (shortTerm) {
                return (unitsMovedMulti * redemptionRate) > 0 ? offerCostMulti / (unitsMovedMulti * redemptionRate) : 0
            }
            else {
                return ((unitsMovedMulti * redemptionRate) * repeatFactor) > 0 ? offerCostMulti / ((unitsMovedMulti * redemptionRate) * repeatFactor) : 0
            }
        }
    }
    
    func getCpium(_ offerCostMulti: Double, unitsMovedMulti: Double, redemptionRate: Double, repeatFactor: Double, incrementalRatio: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Volume) {
            return (unitsMovedMulti * incrementalRatio) > 0 ? offerCostMulti / (unitsMovedMulti * incrementalRatio) : 0
        }
        else {
            if (shortTerm) {
                return (unitsMovedMulti * redemptionRate * incrementalRatio) > 0 ? offerCostMulti / (unitsMovedMulti * redemptionRate * incrementalRatio) : 0
            }
            else {
                return ((unitsMovedMulti * redemptionRate) * repeatFactor * incrementalRatio) > 0 ? offerCostMulti / ((unitsMovedMulti * redemptionRate) * repeatFactor * incrementalRatio) : 0
            }
        }
    }
    
    func getRetailRoi(_ offerCostMulti: Double, unitsMovedMulti: Double, averageRetailPrice: Double, redemptionRate: Double, repeatFactor: Double, incrementalRatio: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Volume) {
            return offerCostMulti > 0 ? (unitsMovedMulti * averageRetailPrice * incrementalRatio) / offerCostMulti : 0
        }
        else {
            if (shortTerm) {
                return offerCostMulti > 0 ? (unitsMovedMulti * averageRetailPrice * incrementalRatio * redemptionRate) / offerCostMulti : 0
            }
            else {
                return offerCostMulti > 0 ? (unitsMovedMulti * averageRetailPrice * incrementalRatio * redemptionRate * repeatFactor) / offerCostMulti : 0
            }
        }
    }
    
    
    func getProfitRoi(_ offerCostMulti: Double, unitsMovedMulti: Double, averageRetailPrice: Double, redemptionRate: Double, repeatFactor: Double, incrementalRatio: Double, profitMargin: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Volume) {
            return offerCostMulti > 0 ? (unitsMovedMulti * averageRetailPrice * incrementalRatio * profitMargin) / offerCostMulti : 0
        }
        else {
            if (shortTerm) {
                return offerCostMulti > 0 ? (unitsMovedMulti * averageRetailPrice * incrementalRatio * redemptionRate * profitMargin) / offerCostMulti : 0
            }
            else {
                return offerCostMulti > 0 ? (unitsMovedMulti * averageRetailPrice * incrementalRatio * redemptionRate * repeatFactor * profitMargin) / offerCostMulti : 0
            }
        }
    }
    
    func getCostPerAcquisition(_ offerCostMulti: Double, unitsMovedMulti: Double, redemptionRate: Double, repeatFactor: Double, incrementalRatio: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Trial) {
            if (shortTerm) {
                return (unitsMovedMulti * redemptionRate * incrementalRatio) > 0 ? (unitsMovedMulti * offerCostMulti) / (unitsMovedMulti * redemptionRate * incrementalRatio) : 0
            }
            else {
                return (unitsMovedMulti * redemptionRate * incrementalRatio * repeatFactor) > 0 ? (unitsMovedMulti * offerCostMulti) / (unitsMovedMulti * redemptionRate * incrementalRatio * repeatFactor) : 0
            }
        }
        else {
            return 0
        }
    }
    
    func getUnitsMovedMultiplier(_ triggerQuantity: Int, redemptionRate: Double, repeatFactor: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Volume) {
            return Double(triggerQuantity)
        }
        else {
            if (shortTerm) {
                return Double(triggerQuantity) * redemptionRate
            }
            else {
                return Double(triggerQuantity) * redemptionRate * repeatFactor
            }
        }
    }
    
    func getIncrementalUnitsMovedMultiplier(_ unitsMovedMulti: Double, redemptionRate: Double, repeatFactor: Double, incrementalRatio: Double, tlvType: IPOfferType, shortTerm: Bool) -> Double {
        if (tlvType == .Volume) {
            return unitsMovedMulti * incrementalRatio
        }
        else {
            if (shortTerm) {
                return unitsMovedMulti * redemptionRate * incrementalRatio
            }
            else {
                return unitsMovedMulti * redemptionRate * repeatFactor * incrementalRatio
            }
        }
    }
    
    func getDiscountPercent(_ promoValue: Double, averageRetailPrice: Double, unitsMovedMulti: Double) -> Double {
        return (averageRetailPrice * unitsMovedMulti) > 0 ? promoValue / (averageRetailPrice * unitsMovedMulti) : 0
    }
    
    func getIncrementalRatio(_ discountPercent: Double, incrShttrmIntrcpt:Double, incrShttrmXdscntPct:Double, tlvType: IPOfferType)->Double{
        var incrementalRatio =  incrShttrmIntrcpt + (incrShttrmXdscntPct * discountPercent)
        if(tlvType == .Volume){
            if(incrementalRatio > 1){
                incrementalRatio = 1
            }
        }
        else{
            if(incrementalRatio > 2){
                incrementalRatio = 2
            }
        }
        return incrementalRatio
    }
    
    func getRedemptionRate(_ discountPercent: Double, promoValue: Double, unitsMovedMulti: Double, rdmpIntrcpt:Double, rdmpXdscntPct:Double, rdmpXdscntAmt:Double, rdmpXbuylvl:Double)->Double{
        return rdmpIntrcpt + (rdmpXdscntPct * discountPercent) + (rdmpXdscntAmt * promoValue) + (rdmpXbuylvl * unitsMovedMulti)
    }
    
    func getRepeatFactor(_ discountPercent: Double, rptIntrcpt:Double, rptXdscntPct:Double)->Double{
        return rptIntrcpt + (rptXdscntPct * discountPercent)
    }
}

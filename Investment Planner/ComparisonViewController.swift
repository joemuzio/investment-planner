//
//  ComparisonViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/17/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ComparisonMetric {
    
    var displayName: String!
    var valueFormatter: ((_ scenario: IPScenario) -> String)?
    
    init(displayName: String, valueFormatter: @escaping ((_ scenario: IPScenario) -> String)) {
        self.displayName = displayName
        self.valueFormatter = valueFormatter
    }
    
    func metricValueForObject(_ scenario: IPScenario) -> String {
        
        if let formatter = valueFormatter {
            return formatter(scenario)
        }
        return "--"
    }
}

class ComparisonViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    var scenarios: [IPScenario]?
    var selectedMetrics: [ComparisonMetric]?
    var availableMetrics: [ComparisonMetric] = [
        ComparisonMetric(displayName: "Total Investment", valueFormatter: { (scenario) -> String in
            return scenario.totalInvestment.abbreviatedCurrencyString()
        }),
        ComparisonMetric(displayName: "    Distribution Investment", valueFormatter: { (scenario) -> String in
            return scenario.distributionInvestment.abbreviatedCurrencyString()
        }),
        ComparisonMetric(displayName: "    Redemption Investment", valueFormatter: { (scenario) -> String in
            return scenario.redemptionInvestment.abbreviatedCurrencyString()
        }),
        ComparisonMetric(displayName: "Estimated Distribution", valueFormatter: { (scenario) -> String in
            return scenario.estimatedDistribution.abbreviatedNumberString()
        }),
        ComparisonMetric(displayName: "Estimated Redemption", valueFormatter: { (scenario) -> String in
            return scenario.estimatedRedemption.abbreviatedNumberString()
        }),
        ComparisonMetric(displayName: "Retail ROI", valueFormatter: { (scenario) -> String in
            return scenario.retailRoi.currencyString()
        }),
        ComparisonMetric(displayName: "Profit ROI", valueFormatter: { (scenario) -> String in
            return scenario.profitRoi.currencyString()
        }),
        ComparisonMetric(displayName: "Cost Per Acquisition", valueFormatter: { (scenario) -> String in
            return scenario.costPerAcquisition.currencyString()
        }),
        ComparisonMetric(displayName: "CPUM", valueFormatter: { (scenario) -> String in
            return scenario.cpum.currencyString()
        }),
        ComparisonMetric(displayName: "CPIUM", valueFormatter: { (scenario) -> String in
            return scenario.cpium.currencyString()
        }),
        ComparisonMetric(displayName: "Total Dollars", valueFormatter: { (scenario) -> String in
            return scenario.totalDollars.abbreviatedCurrencyString()
        }),
        ComparisonMetric(displayName: "Total Units", valueFormatter: { (scenario) -> String in
            return scenario.totalUnits.abbreviatedNumberString()
        }),
        ComparisonMetric(displayName: "Profit Margin", valueFormatter: { (scenario) -> String in
            return scenario.program.profitMargin.percentString()
        }),
        ComparisonMetric(displayName: "Incremental Units", valueFormatter: { (scenario) -> String in
            return scenario.totalIncrementalUnits.abbreviatedNumberString()
        }),
        ComparisonMetric(displayName: "Incremental Dollars", valueFormatter: { (scenario) -> String in
            return scenario.totalIncrementalDollars.abbreviatedCurrencyString()
        }),
        ComparisonMetric(displayName: "Acquired Buyers", valueFormatter: { (scenario) -> String in
            return scenario.totalAcquiredBuyers.abbreviatedNumberString()
        }),
//        ComparisonMetric(displayName: "Geography", valueFormatter: { (scenario) -> String in
//            return "--"
//        }),
    ]

    var collectionViewContentBackground: UIView!
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    
    let primaryRowColor = UIColor.catalinaWhiteColor()
    let alternateRowColor = UIColor(hexColor: 0xDBF5FD)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        assert(scenarios != nil, "the comparison view controller must have a non-nil scenarios array before it is loaded")
        assert(scenarios?.count == 2 || scenarios?.count == 3, "the comparison view controller must have between 2 and 3 scenarios be in the scenarios array before it is loaded")
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        self.loadSelectedMetrics()
        
        collectionViewContentBackground = UIView()
        collectionViewContentBackground.backgroundColor = UIColor.catalinaDarkBlueColor()
        self.view.addSubview(collectionViewContentBackground)
        
        // Collection View
        
        layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.bounds.width, height: ComparisonCollectionViewCell.rowHeight)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0
        layout.sectionHeadersPinToVisibleBounds = true
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.headerReferenceSize = CGSize(width: 0, height:ComparisonHeaderView.requiredHeight())
        
        collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.register(ComparisonCollectionViewCell.self, forCellWithReuseIdentifier: "PropertyCell")
        collectionView.register(ComparisonHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderView")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.clear
        collectionView.bounces = true
        collectionView.alwaysBounceVertical = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(collectionView)
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        collectionViewContentBackground.frame = self.getCollectionViewContentBounds()
    }
    
    // MARK: - UICollectionViewDataSource Implementation
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let metrics = selectedMetrics {
            return metrics.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = selectedMetrics![(indexPath as NSIndexPath).item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertyCell", for: indexPath) as! ComparisonCollectionViewCell
        cell.propertyNameLabel.text = item.displayName
        cell.propertyValueLabel1.text = item.metricValueForObject(scenarios![0])
        cell.propertyValueLabel2.text = item.metricValueForObject(scenarios![1])
        if scenarios!.count > 2 {
            cell.shouldShowThirdColumn = true
            cell.propertyValueLabel3.text = item.metricValueForObject(scenarios![2])
        } else {
            cell.shouldShowThirdColumn = false
            cell.propertyValueLabel3.text = ""
        }
        cell.backgroundColor = UIColor.catalinaWhiteColor()
        cell.longPressGestureHandler = { gesture in self.handleCellDragGesture(gesture) }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderView", for: indexPath) as! ComparisonHeaderView
        header.growthPlanNameLabel1.text = scenarios![0].name
        header.strategicWeightingValueLabel1.attributedText = self.getStrategicWeightingStringForScenario(scenarios![0])
        header.growthPlanNameLabel2.text = scenarios![1].name
        header.strategicWeightingValueLabel2.attributedText = self.getStrategicWeightingStringForScenario(scenarios![1])
        if scenarios!.count > 2 {
            header.shouldShowThirdColumn = true
            header.growthPlanNameLabel3.text = scenarios![2].name
            header.strategicWeightingValueLabel3.attributedText = self.getStrategicWeightingStringForScenario(scenarios![2])
        } else {
            header.shouldShowThirdColumn = false
            header.growthPlanNameLabel3.text = ""
            header.strategicWeightingValueLabel3.attributedText = nil
        }
        header.accessoryButtonActionHandler = { header in self.moreButtonPressed(header.accessoryButton) }
        header.backButtonActionHandler = { header in self.navigationController?.popViewController(animated: true) }
        header.infoButtonActionHandler = { header in self.infoButtonPressed() }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let metric = selectedMetrics!.remove(at: (sourceIndexPath as NSIndexPath).item)
        selectedMetrics!.insert(metric, at: (destinationIndexPath as NSIndexPath).item)
        self.saveSelectedMetrics()
    }
    
    // MARK: - UICollectionViewDelegate Implementation
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (indexPath as NSIndexPath).item % 2 == 0 {
            cell.backgroundColor = alternateRowColor
        } else {
            cell.backgroundColor = primaryRowColor
        }
    }
    
    // MARK: - UIScrollViewDelegate Implementation
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.collectionViewContentBackground.frame = self.getCollectionViewContentBounds()
    }
    
    // MARK: - Long Press Gesture
    
    //variables used during a long press and drag gesture, placed here for clarity and convenience
    var movingCell: ComparisonCollectionViewCell?
    
    func handleCellDragGesture(_ gesture: UIGestureRecognizer) {
        
        let touchLocation = gesture.location(in: self.collectionView)
        let horizontallyCenteredLocation = CGPoint(x: collectionView.bounds.midX, y: touchLocation.y)
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.began: // long press began, grab the cell
            
            if let selectedIndexPath = collectionView.indexPathForItem(at: touchLocation) {
                
                //capture a reference to the cell that will be moving as a result of this gesture so we can use it later
                movingCell = collectionView.cellForItem(at: selectedIndexPath) as? ComparisonCollectionViewCell
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: .curveLinear, animations: { () -> Void in
                    //"grab" the cell and animate it jumping to the touch point with a subtle bounce
                    if let cell = self.movingCell {
                        self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
                        self.collectionView.updateInteractiveMovementTargetPosition(horizontallyCenteredLocation)
                        cell.isRaised = true
                    }
                    }, completion: nil)
            }
            
        case UIGestureRecognizerState.changed:
            
            // long press changed, tell the collection view to update the cells location
            collectionView.updateInteractiveMovementTargetPosition(horizontallyCenteredLocation)
            
        default:
            
            // long press ended, "drop" the cell end the inteactive movement within the collection view
            collectionView.endInteractiveMovement()
            if let cell = movingCell {
                cell.isRaised = false
                movingCell = nil
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
                self.collectionView.reloadData()
            }
            
        }
    }
    
    // MARK: - Actions
    
    func moreButtonPressed(_ sender: UIButton) {
        
        let picker = MetricPicker()
        picker.prnt = self
        picker.modalPresentationStyle = .popover
        if let popover = picker.popoverPresentationController {
            popover.sourceView = sender
            popover.sourceRect = sender.bounds
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    func infoButtonPressed() {
        
        self.showPageDefinitions(definitions, animated: true)
    }
    
    // MARK: - Helper Methods
    
    //MARK: - Internal Helpers
    
    fileprivate func loadSelectedMetrics() {
        
        if let metricNames = SettingsController.sharedSettingsController.compareScreenSelectedMetrics {
            var temp = [ComparisonMetric]()
            for metricName in metricNames {
                for metric in availableMetrics {
                    if metric.displayName == metricName {
                        temp.append(metric)
                    }
                }
            }
            selectedMetrics = temp
        } else {
            selectedMetrics = availableMetrics
        }
    }
    
    fileprivate func saveSelectedMetrics() {
        
        var temp = [String]()
        if let metrics = selectedMetrics {
            for metric in metrics {
                temp.append(metric.displayName)
            }
        }
        SettingsController.sharedSettingsController.compareScreenSelectedMetrics = temp
    }
    
    fileprivate func getCollectionViewContentBounds() -> CGRect {
        
        let x = collectionView.contentOffset.x
        let y = -collectionView.contentOffset.y
        let width = collectionView.contentSize.width
        let height = collectionView.contentSize.height + 1
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    fileprivate func getStrategicWeightingStringForScenario(_ scenario: IPScenario) -> NSAttributedString {
        
        let string = NSMutableAttributedString()
        string.append(NSAttributedString(string: "T = \((scenario.trialPercent).percentString())", attributes: [NSForegroundColorAttributeName : UIColor.catalinaDarkBlueColor()]))
        string.append(NSAttributedString(string: " L = \((scenario.loyaltyPercent).percentString())", attributes: [NSForegroundColorAttributeName : UIColor.catalinaLightBlueColor()]))
        string.append(NSAttributedString(string: " V = \((scenario.volumePercent).percentString())", attributes: [NSForegroundColorAttributeName : UIColor.catalinaLightGreenColor()]))
        return string
    }

    let definitions = [
        "Acquired Buyers" : "Incremental new buyers driven by the trial programs.  Applies only to ID-based trial and lapsed offers.",
        "Cost per Acquisition" : "Total budget / acquired buyers\nN/A if trial is not part of program",
        "CPIUM" : "Cost per incremental unit moved\nTotal Budget / Incremental Units Moved",
        "CPUM" : "Cost per unit moved\nCPUM with Coupon: Total Budget / Units Moved on Redemption\nCPUM with CCM: Total Budget / Units Moved on Reward ",
        "Distribution Investment" : "Client's budget specific to distribution cost only.  This is what the client will pay Catalina.\nEstimated Distribution x Cost per Impression",
        "Estimated Distribution" : "(Base ID/Transaction Count x Distribution Adjustment %) / Transaction Frequency",
        "Estimated Redemption" : "The estimated redemption percentage.",
        "Geography" : "Geography used to create the Investment Planner data in MicroStrategy",
        "Incremental Dollars" : "Retail sales directly attributable to this program.\nIncremental units x average retail price of product",
        "Incremental Units" : "Number of units moved that are directly attributable to this program\nTotal units moved x incremental ratio midpoint",
        "Loyalty" : "Own User Trade Up Programs",
        "Profit Margin" : "The percentage of retail sales that is turned into profit for the manufacturer.",
        "Profit ROI" : "Total incremental profit driven per dollar invested\nRetail ROI x Profit Margin",
        "Redemption Investment" : "Client's redemption budget, this is what the client will pay their clearinghouse.\n# Redeemed x (Offer + Est. Clearinghouse Handling Fee)",
        "Retail ROI" : "Total incremental retail sales driven per dollar invested\nIncremental Retail Sales / Total Budget",
        "Shoppers Targeted" : "The number of IDs that can be targeted for a historical offer",
        "Store Count" : "# of stores used for which the simulation is based",
        "Strategic Weighting" : "The percentage of the Growth Plan that will be applied to Trial, Loyalty, and Volume",
        "Total Dollars" : "All retail dollars moved for all households who redeemed a Catalina print or earned a Catalina reward.\nTotal units x average retail price",
        "Total Investment" : "Client total budget including distribution cost and redemption cost.\nDistribution Budget + Redemption Budget",
        "Total Units" : "All units moved for all households who redeemed a Catalina print or earned a Catalina reward.",
        "Trial" : "Projected from the Setup Report, Total Brand buyers - Total Recency Shoppers X 1%",
        "Volume" : "Defined as short term Volume, this currently only comprises CCM programs",
    ]
}

class MetricPicker: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var prnt: ComparisonViewController!
    var tableView = UITableView()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let frame = CGRect(x: 0, y: 0, width: 320, height: 600)
        self.preferredContentSize = CGSize(width: frame.size.width, height: frame.size.height)
        tableView.frame = frame
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return prnt.availableMetrics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let metric = prnt.availableMetrics[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = metric.displayName
        
        if prnt.selectedMetrics!.contains(where: { $0 === metric }){
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let metric =  prnt.availableMetrics[(indexPath as NSIndexPath).row]
        
        if prnt.selectedMetrics!.contains(where: { $0 === metric }){
            let index = prnt.selectedMetrics!.index(where: { $0 === metric })!
            prnt.selectedMetrics!.remove(at: index)
        } else {
            prnt.selectedMetrics!.append(metric)
        }
        prnt.saveSelectedMetrics()
        
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        
        prnt.collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.01 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            self.prnt.collectionViewContentBackground.frame = self.prnt.getCollectionViewContentBounds()
        }
    }
}

//
//  IPMPI.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/20/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

struct MPICycle: Hashable {
    var cycle: Int
    var year: Int
    
    init(_ cycle: Int, _ year: Int) {
        self.cycle = cycle
        self.year = year
    }
    
    var hashValue: Int {
        return cycle.hashValue ^ year.hashValue
    }
    
}

func ==(lhs: MPICycle, rhs: MPICycle) -> Bool {
    return lhs.cycle == rhs.cycle && lhs.year == rhs.year
}
class IPMPI: IPModelObject {

    var cycle: String = ""
    var buyQty: Int = 0
    var averagePrice: Double = 0
    var trips: Int = 0
    var tripsPerPeriod: Int = 0
    var units: Int = 0
    var dollars: Double = 0
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "cycle",
            "buyQty",
            "averagePrice",
            "trips",
            "tripsPerPeriod",
            "units",
            "dollars"])
        return keys
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "buyQty" {
                if let valueString = value as? String {
                    if valueString == "Total" {
                        self.buyQty = -1
                    } else if valueString == "10+" {
                        self.buyQty = 10
                    } else {
                        let formatter = NumberFormatter()
                        if let number = formatter.number(from: valueString) {
                            self.buyQty = number.intValue
                        }
                    }
                }
                return
            }
        }
        super.setValue(value, forKey: key)
    }
    
    override func value(forKey key: String) -> Any? {
        
        if self.shouldUseJSONPropertyMappingForKVOOperations {
            if key == "buyQty" {
                if self.buyQty == -1 {
                    return "Total"
                } else if self.buyQty == 10 {
                    return "10+"
                } else {
                    return "\(self.buyQty)"
                }
            }
        }
        return super.value(forKey: key)
    }
    
    func getCycleObject() -> MPICycle? {
        if cycle.characters.count > 10 {
            let yearString = cycle.substring(with: (cycle.startIndex ..< cycle.characters.index(cycle.startIndex, offsetBy: 4)))
            let cycleString = cycle.substring(with: (cycle.characters.index(cycle.startIndex, offsetBy: 8) ..< cycle.characters.index(cycle.startIndex, offsetBy: 10)))
            
            let yearInt = Int(yearString) ?? 0
            let cycleInt = Int(cycleString) ?? 0
            
            return MPICycle(cycleInt, yearInt)
        }
        else {
            return nil
        }
    }
}

class IPMPICCM: IPMPI {
    
    var tripsPercent: Double = 0
    var cumulativeUnitsPercent: Double = 0
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "tripsPercent",
            "cumulativeUnitsPercent"])
        return keys
    }
}

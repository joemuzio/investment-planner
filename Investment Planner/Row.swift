import UIKit

class Row: NSObject {
    var a = [String:Double]()
    
    func add(_ b:String, _ c:Double){
        let newC = FormulaSystem.roundPrecision(c.isNaN ? 0.0 : c)
        if let value = a[b]{
            a[b] = value + newC
        }
        else{
            a[b] = newC
        }
    }
    func subtract(_ b:String, _ c:Double){
        let newC = FormulaSystem.roundPrecision(c.isNaN ? 0.0 : c)
        if let value = a[b]{
            a[b] = value - newC
        }
        else{
            a[b] = -newC
        }
    }
}

//
//  PortfolioViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/4/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

enum PortfolioViewControllerMode {
    case normal
    case detail
    case edit
    case compare
}

class PortfolioViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate {
    
    fileprivate var addButton: UIButton!
    fileprivate var compareButton: UIButton!
    fileprivate var infoButton: UIButton!
    fileprivate var closeButton: UIButton!
    fileprivate var kpiButton: UIButton!
    var backButton: UIButton!
    fileprivate var comparisonStagingView: ComparisonStagingView!
    
    fileprivate var selectedKPI = "Profit ROI"
    fileprivate var kpiPicker = KPIPicker()
    var currentFolder = "/"
    
    fileprivate var controlPanelViewController: ControlPanelViewController!
    
    fileprivate var layout: UICollectionViewFlowLayout!
    fileprivate var detailLayout: PromotionsCollectionViewLayout!
    fileprivate var collectionView: UICollectionView!
    fileprivate var shouldCancelMovement = false
    
    var pageDefinitions = ["CPIUM": "Cost per incremental unit moved\nTotal Budget / Incremental Units Moved", "CPUM": "Cost per unit moved\nCPUM with Coupon: Total Budget / Units Moved on Redemption\nCPUM with CCM: Total Budget / Units Moved on Reward", "Profit ROI": "Total incremental profit driven per dollar invested\nRetail ROI x Profit Margin", "Retail ROI": "Total incremental retail sales driven per dollar invested\nIncremental Retail Sales / Total Budget", "Total Investment": "Client total budget including distribution cost and redemption cost\nDistribution Budget + Redemption Budget", "Assumed Units Per Trial Trip": "Average number of units purchased on the first promoted brand trip for a trial target (i.e., a Never Buyer).  Defaults from Analytics Results DB", "Average Retail Price": "The average price for a single unit.  Defaults based on report data from MicroStrategy.  Used in the calculations for all Growth Plans", "CCM Announcement": "An ID based print/digital ad/annoucement that targets customers who are very likely to participate in the program", "CCM Reward": "A transactional based print in the Catalina Coupon Management Program that rewards the customer for purchasing based on the program purchase requirements\nExample:  A coupon for $1 off their next shopping order because they just purchased 4 sports drinks", "Client Name": "User defined field.  Once defined used for searching reports", "Data Mining": "Estimated from the Setup Report: 1% of total recency shoppers excluding 1+ brand buyers", "Default Investment": "The Investment used to create the default Growth Plan.  Typically this is the Distribution Budget + Redemption Budget", "Estimated Redemption": "The estimated redemption percentage defaulted to data provided by Analytics Results DB", "Focus Brand": "The main brand for which the Investment Plan was created", "Handling Fee": "This is the cost per redeemed promotion that the client will pay to the clearinghouse.  Defaults to $.014", "Heavy Category Never Buyers": "From the Setup Report, top 25% of 2+ category buyers who did not buy the brand in the most recent 52 weeks", "Historical": "ID based targeting that occurs prior to the customer’s current shopping trip", "Impression Cost": "AKA Print rate, print cost, cost per coupon, Cost per Impression.  Catalina's cost for delivering one promotion to the consumer\nLoyalty, CCM Reward:  Defaults to $0.11\nTrial, CCM Announcement:  Defaults to $0.14", "Incremental Ratio": "Percent of units moved that can be directly attributed to Catalina's program. Defaults from Analytics Results DB", "Lapsed Buyers": "Estimated from the 2+ category buyers of the Setup Report based on average lapse rates across a variety of brands and categories", "Own User Transactional": "“Own User Trade Up” or “Own User Repeat”.  Multiples based programs used to drive brand loyalty using transactional based offers", "Print Factor": "A factor is applied to a distribution estimate to take into consideration prints being suppressed for business purposes such as Shopping Basket Exclusivity.  For historical offers, this also accounts for the targeted shoppers that may not be shopping, losing their shopper card, not using their shopper card.  A factor of 90% is applied to transactional offers and 65% is applied to historical offers.  Example:  Estimated Baskets for a transactional offer is 100,000; after applying the 90% factor, the Estimated Distribution becomes 90,000.", "Profit Margin": "The percentage of retail sales that is turned into profit for the manufacturer", "Promotion Value": "This is the face value of the coupon, incentive, impression or reward.  Defaults from Analytics Resuls DB", "Purchase Requirement": "The number of products required to be purchased to redeem the offer.  Defaults from Analytics Results DB", "Repeat Factor": "Factor applied to account for additional repeat purchases after initial trial purchase. Defaults from Analytics Results DB", "Short-Term vs Long-Term ROI": "Used in the Profit ROI calculations for all Growth Plans", "Transactional": "UPC based targeting that occurs at the time the customer is shopping"]
    
    var mode =  PortfolioViewControllerMode.normal {
        
        willSet {
            if newValue == .detail {
                collectionView.alwaysBounceHorizontal = true
//                collectionView.alwaysBounceVertical = true
                collectionView.setCollectionViewLayout(detailLayout, animated: true, completion: { (completion) -> Void in
                    for cell in self.collectionView.visibleCells{
                        if cell is PortfolioItemCollectionViewCell{
                            let portCell = cell as! PortfolioItemCollectionViewCell
                            portCell.setupNormalModeConstraintsWithAnimation(true)
                        }
                        else if cell is FolderCollectionViewCell{
                            let folderCell = cell as! FolderCollectionViewCell
                            folderCell.updateForNormalMode()
                        }
                    }
                })
            } else {
                if collectionView.collectionViewLayout != layout {
                    collectionView.alwaysBounceHorizontal = false
//                    collectionView.alwaysBounceVertical = false
                    collectionView.setCollectionViewLayout(layout, animated: true, completion: { (completion) -> Void in
                        for cell in self.collectionView.visibleCells{
                            if cell is PortfolioItemCollectionViewCell{
                                let portCell = cell as! PortfolioItemCollectionViewCell
                                portCell.setupPorfolioModeConstraintsWithAnimation(true)
                            }
                            else if cell is FolderCollectionViewCell{
                                let folderCell = cell as! FolderCollectionViewCell
                                folderCell.updateForPortfolioMode()
                            }
                        }
                        
                    })
                }
            }
        }
        
        didSet {
            
            collectionView.reloadData()
            if mode == .compare {
                self.layout.headerReferenceSize = CGSize(width: 0,height: 180)
            } else {
                self.comparisonStagingView.clearComparison()
                if let program = SettingsController.sharedSettingsController.selectedReport?.program {
                    for item in program.scenarios {
                        item.eligibleForComparison = true
                        item.selectedForComparison = false
                    }
                }
                self.layout.headerReferenceSize = CGSize(width: 0,height: 110)
            }
        }
    }
    
    // MARK: - UIViewController Overrides
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        kpiPicker.portfolioViewController = self
        // Header
        
        addButton = UIButton()
        addButton.setImage(UIImage(named: "icon-new-light-bg"), for: UIControlState())
        addButton.sizeToFit()
        addButton.addTarget(self, action: #selector(PortfolioViewController.addButtonPressed), for: .touchUpInside)
        
        compareButton = UIButton()
        compareButton.setImage(UIImage(named: "icon-compare"), for: UIControlState())
        compareButton.sizeToFit()
        compareButton.addTarget(self, action: #selector(PortfolioViewController.compareButtonPressed), for: .touchUpInside)
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "icon-page-definitions-light-bg"), for: UIControlState())
        infoButton.sizeToFit()
        infoButton.addTarget(self, action: #selector(PortfolioViewController.infoButtonPressed), for: .touchUpInside)
        
        closeButton = UIButton()
        closeButton.setImage(UIImage(named: "icon-close-light-bg"), for: UIControlState())
        closeButton.sizeToFit()
        closeButton.addTarget(self, action: #selector(PortfolioViewController.closeButtonPressed), for: .touchUpInside)
        
        backButton = UIButton()
        backButton.setTitle("Back", for: UIControlState())
        backButton.setTitleColor(UIColor.catalinaDarkBlueColor(), for: UIControlState())
        backButton.sizeToFit()
        backButton.addTarget(self, action: #selector(PortfolioViewController.backButtonPressed), for: .touchUpInside)
        backButton.isHidden = true
        
        kpiButton = UIButton()
        kpiButton.setTitle("\(selectedKPI) ", for: UIControlState())
        kpiButton.setImage(UIImage(named: "icon-arrow-down"), for: UIControlState())
        kpiButton.setTitleColor(UIColor.catalinaDarkBlueColor(), for: UIControlState())
        kpiButton.sizeToFit()
        kpiButton.addTarget(self, action: #selector(PortfolioViewController.kpiButtonPressed), for: .touchUpInside)
        //trick to reverse the image and text position on the button
        kpiButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        kpiButton.titleLabel!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        kpiButton.imageView!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        
        comparisonStagingView = ComparisonStagingView()
        comparisonStagingView.compareActionHandler = { view in self.showComparison() }
        comparisonStagingView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 130)
        
        // Collection View
        
        layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 205, height: 250)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 40
        layout.minimumInteritemSpacing = 40
        layout.sectionHeadersPinToVisibleBounds = true
        layout.sectionInset = UIEdgeInsets(top: 40, left: 40, bottom: 40, right: 40)
        
        detailLayout = PromotionsCollectionViewLayout()
        detailLayout.itemSize = CGSize(width: 930, height: 660)
        detailLayout.scrollDirection = .horizontal
        detailLayout.minimumLineSpacing = 20
        detailLayout.sectionInset = UIEdgeInsets(top: 0, left: 100, bottom: 0, right: 100)
        
        collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.register(PortfolioItemCollectionViewCell.self, forCellWithReuseIdentifier: "PortfolioItemCell")
        collectionView.register(PortfolioHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "PortfolioHeaderView")
        collectionView.register(FolderCollectionViewCell.self, forCellWithReuseIdentifier: "FolderItemCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.catalinaWhiteColor()
        collectionView.bounces = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.delaysContentTouches = false
        self.view.addSubview(collectionView)
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        
        // control panel
        controlPanelViewController = ControlPanelViewController()
        
        self.mode = .normal
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if let program = SettingsController.sharedSettingsController.selectedReport?.program{
            var needToSave = false
            let slash = "/"
            for scenario in program.scenarios{
                if(scenario.folder == ""){
                    scenario.folder = "\(slash)"
                    needToSave = true
                }
            }
            if(needToSave){
                _ = program.report.save()
            }
        }
        
        if(currentFolder == "/"){
            backButton.isHidden = true
        }
        self.collectionView.reloadData()

    }
    // MARK: - UICollectionViewDataSource Implementation
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let items = self.getItemsInFolder(currentFolder)
        return items.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let items = self.getItemsInFolder(currentFolder)
        if let item = items[(indexPath as NSIndexPath).item] as? IPScenario {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PortfolioItemCell", for: indexPath) as! PortfolioItemCollectionViewCell
            let vc = ScenarioBuilderDeepDiveViewController()
            vc.portVC = self
            vc.displayedScenario = item
            cell.setupViewController(vc)
            
            var value: Double = 0.0
            var string: String = ""
            if self.kpiPicker.selectedKPI == "Cost Per Incremental Unit Moved" {
                value = item.cpium
                string = "CPIUM"
            } else if self.kpiPicker.selectedKPI == "Cost Per Unit Moved" {
                value = item.cpum
                string = "CPUM"
            } else if self.kpiPicker.selectedKPI == "Profit ROI" {
                value = item.profitRoi
                string = "Profit ROI"
            } else if self.kpiPicker.selectedKPI == "Retail ROI" {
                value = item.retailRoi
                string = "Retail ROI"
            }
            cell.viewController.kpiLabel.text = string + ": " + value.currencyString(2)

            cell.longPressGestureHandler = { gesture in self.handleCellLongPressGesture(gesture) }
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FolderItemCell", for: indexPath) as! FolderCollectionViewCell
            cell.folder = items[(indexPath as NSIndexPath).item] as! String
            if(cell.folder == "/Previous"){
                cell.nameLabel.text = "Previous \u{25B2}"
            }
            else{
                cell.nameLabel.text = getFolderName(cell.folder)
            }
            cell.longPressGestureHandler = { gesture in self.handleCellLongPressGesture(gesture) }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "PortfolioHeaderView", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        if let program = SettingsController.sharedSettingsController.selectedReport?.program {
//            let item = program.scenarios.removeAtIndex(sourceIndexPath.item)
//            program.scenarios.insert(item, atIndex: destinationIndexPath.item)
//            program.report.save()
//        }
    }
    
    func swipeUp(_ gesture: UISwipeGestureRecognizer) {
        if self.mode == .detail {
            for cell in collectionView.visibleCells{
                if cell is PortfolioItemCollectionViewCell{
                    let portCell = cell as! PortfolioItemCollectionViewCell
                    portCell.viewController.hideViewsForPortfolioView()
                }
            }
            self.mode = .normal
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Scenario Swiped", action: "Scenario View Minimized", label: "", value: 0)
        }
    }
    
    // MARK: - UICollectionViewDelegate Implementation
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.cellForItem(at: indexPath) is PortfolioItemCollectionViewCell{
            if(mode == .normal){
                self.mode = .detail
                AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Scenario Pressed", action: "Scenario View Expanded", label: "", value: 0)
                for cell in collectionView.visibleCells{
                    if cell is PortfolioItemCollectionViewCell{
                        let portCell = cell as! PortfolioItemCollectionViewCell
                        portCell.viewController.kpiLabel.alpha = 0
                        portCell.viewController.investmentLabel.alpha = 0
                        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeUp(_:)))
                        swipeGesture.direction = UISwipeGestureRecognizerDirection.up
                        portCell.addGestureRecognizer(swipeGesture)
                    }
                }
            }
        }
        else if collectionView.cellForItem(at: indexPath) is FolderCollectionViewCell{
            if mode != .compare{
                let folderCell = collectionView.cellForItem(at: indexPath) as! FolderCollectionViewCell
                if(folderCell.folder != "/Previous"){
                    currentFolder = folderCell.folder
                    self.collectionView.reloadData()
                    if(backButton.isHidden == true){
                        backButton.isHidden = false
                    }
                }
                else{
                    self.backButtonPressed()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if(cell is PortfolioItemCollectionViewCell){
            if let cell = cell as? PortfolioItemCollectionViewCell {
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    
                    switch self.mode {
                        
                    case .normal:
                        cell.isBouncing = false
                        cell.active = true
                        cell.shouldShowDashedOutline = false
                        cell.deleteActionHandler = nil
                        cell.duplicateActionHandler = nil
                        cell.longPressGesture.minimumPressDuration = 0.5
                        cell.setupPorfolioModeConstraintsWithAnimation(false)
                        break
                    case .detail:
                        cell.isBouncing = false
                        cell.active = true
                        cell.shouldShowDashedOutline = false
                        cell.deleteActionHandler = nil
                        cell.duplicateActionHandler = nil
                        cell.longPressGesture.minimumPressDuration = 0.5
                        cell.setupNormalModeConstraintsWithAnimation(false)
                    case .edit:
                        cell.isBouncing = true
                        cell.active = true
                        cell.shouldShowDashedOutline = false
                        cell.deleteActionHandler = { cell in self.deletePortfolioItemRepresentedByCell(cell) }
                        cell.duplicateActionHandler = { cell in self.duplicatePortfolioItemRepresentedByCell(cell) }
                        cell.longPressGesture.minimumPressDuration = 0.2
                        cell.setupPorfolioModeConstraintsWithAnimation(false)
                        break
                    case .compare:
                    //    let program = SettingsController.sharedSettingsController.selectedReport!.program!
                        let item = cell.scenario
                        cell.isBouncing = (item?.eligibleForComparison)!
                        cell.active = (item?.eligibleForComparison)! || (item?.selectedForComparison)!
                        cell.shouldShowDashedOutline = (item?.selectedForComparison)!
                        cell.deleteActionHandler = nil
                        cell.duplicateActionHandler = nil
                        cell.longPressGesture.minimumPressDuration = 0.2
                        cell.setupPorfolioModeConstraintsWithAnimation(false)
                        break
                        
                    }
                })
            }
        }
        else{
            if let cell = cell as? FolderCollectionViewCell {
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    
                    switch self.mode {
                        
                    case .normal:
                        cell.isBouncing = false
                        cell.active = true
                        cell.deleteActionHandler = nil
                        cell.longPressGesture.minimumPressDuration = 0.5
                        break
                    case .detail:
                        cell.isBouncing = false
                        cell.active = true
                        cell.deleteActionHandler = nil
                        cell.longPressGesture.minimumPressDuration = 0.5
                        break
                    case .edit:
                        cell.isBouncing = true
                        cell.active = true
                        cell.deleteActionHandler = { cell in self.deleteFolderRepresentedByCell(cell) }
                        cell.longPressGesture.minimumPressDuration = 0.2
                        break
                    case .compare:
//                        let program = SettingsController.sharedSettingsController.selectedReport!.program!
//                        let item = program.scenarios[indexPath.item]
//                        cell.isBouncing = item.eligibleForComparison
//                        cell.active = item.eligibleForComparison || item.selectedForComparison
//                        cell.deleteActionHandler = nil
//                        cell.longPressGesture.minimumPressDuration = 0.2
                        break
                        
                    }
                })
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if let header = view as? PortfolioHeaderView {
            
            switch self.mode {
                
            case .normal:
                if(currentFolder == "/"){
                    header.titleLabel.text = "LIBRARY"
                }
                else{
                    header.titleLabel.text = getFolderName(currentFolder).uppercased()
                }
                header.leftButtons = [self.backButton]
                header.rightButtons = [self.infoButton, self.compareButton, self.addButton]
                header.supplementaryViews = [self.kpiButton]
                break
            case .detail:
                header.titleLabel.text = "LIBRARY"
                header.leftButtons = nil
                header.rightButtons = [self.infoButton, self.compareButton, self.addButton]
                header.supplementaryViews = [self.kpiButton]
            case .edit:
                if(currentFolder == "/"){
                    header.titleLabel.text = "LIBRARY"
                }
                else{
                    header.titleLabel.text = getFolderName(currentFolder).uppercased()
                }
                header.leftButtons = nil
                header.rightButtons = [self.closeButton]
                header.supplementaryViews = [self.kpiButton]
                break
            case .compare:
                header.titleLabel.text = "Select up to 3  Growth Plans for side by side comparison by dragging and dropping in the spaces below:"
                header.leftButtons = nil
                header.rightButtons = [self.closeButton]
                header.supplementaryViews = [self.comparisonStagingView]
                break
                
            }
        }
    }
    
    // MARK: - Actions
    
    func addButtonPressed() {
        let alert = UIAlertController(title: "Name Required", message: "Please name your new scenario", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        let saveAction = UIAlertAction(title: "Save", style: .default) { (action) in
            let nameTextField = alert.textFields![0] as UITextField
            if(nameTextField.text!.characters.count > 0){
                self.collectionView.performBatchUpdates({ () -> Void in
                    if let program = SettingsController.sharedSettingsController.selectedReport?.program {
                        let numberOfScenarios = self.getItemsInFolder(self.currentFolder).count
                        let indexPath = IndexPath(item: numberOfScenarios, section: 0)
                        self.collectionView.insertItems(at: [indexPath])
                        let newScenario = program.createNewScenario()
                        newScenario.name = nameTextField.text!.capitalized
                        newScenario.folder = self.currentFolder
                        program.scenarios.append(newScenario)
                        _ = program.report.save()
                        
                    }}, completion: {(Bool) in
                        self.collectionView.reloadData()
                })
            }
        }
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Scenario Name"
        })
        alert.addAction(saveAction)
        
        self.present(alert, animated: true, completion: nil)
        
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Add Scenario Button Pressed", action: "New Scenario Created", label: "", value: 0)

        
    }
    
    func compareButtonPressed() {
        
        self.mode = .compare
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Compare Button Pressed", action: "Compare Started", label: "", value: 0)

    }
    
    func infoButtonPressed() {
        let program = SettingsController.sharedSettingsController.selectedReport!.program!
        let promptDictionary = program.report.prettyPrompts
        let defSection = DefinitionsSection(name: "Definitions", terms: pageDefinitions)
        let promptSection = DefinitionsSection(name: "Prompt Answers", terms: promptDictionary)
        let sections = [defSection, promptSection]
        self.showPageDefinitionsWithSections(sections, animated: true)
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Info Button Pressed", action: "Library Definitions Shown", label: "", value: 0)

    }
    
    func closeButtonPressed() {
        
        self.mode = .normal
    }
    
    func backButtonPressed() {
        var splitFolderArray = currentFolder.components(separatedBy: "/")
        splitFolderArray.removeLast()
        let stringToRemove = "\(splitFolderArray.last!)/"
        let newCurrentFolder = currentFolder.replacingOccurrences(of: stringToRemove, with: "")
        currentFolder = newCurrentFolder
        collectionView.reloadData()
        if(currentFolder == "/"){
            backButton.isHidden = true
        }
    }
    
    func kpiButtonPressed() {
        
        kpiPicker.modalPresentationStyle = .popover
        kpiPicker.selectedKPI = selectedKPI
        kpiPicker.kpiSepectionUpdateBlock = {
            (picker: KPIPicker, selectedKPI: String) -> Void in
            self.selectedKPI = selectedKPI
            self.kpiButton.setTitle("\(selectedKPI.uppercased()) ", for: UIControlState())
            self.kpiButton.sizeToFit()
            self.sortBySelectedKPI()
            self.collectionView.reloadData()
            self.dismiss(animated: true, completion: nil)
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("KPI Button Pressed", action: "New KPI Type: \(self.selectedKPI)", label: "", value: 0)

        }
        
        if let popover = kpiPicker.popoverPresentationController {
            popover.sourceView = kpiButton
            popover.sourceRect = kpiButton.bounds
            self.present(kpiPicker, animated:true, completion: nil)
        }
    }
    
    func showComparison() {
        
        let vc = ComparisonViewController()
        let program = SettingsController.sharedSettingsController.selectedReport!.program!
        var scenariosToCompare = [IPScenario]()
        for scenario in program.scenarios {
            if scenario.selectedForComparison {
                scenariosToCompare.append(scenario)
            }
        }
        vc.scenarios = scenariosToCompare
        self.mode = .normal
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deletePortfolioItemRepresentedByCell(_ cell: PortfolioItemCollectionViewCell) {
        
        collectionView.performBatchUpdates({ () -> Void in
            if let indexPath = self.collectionView.indexPath(for: cell) {
                self.collectionView.deleteItems(at: [indexPath])
                let program = SettingsController.sharedSettingsController.selectedReport!.program!
                program.scenarios.remove(at: (indexPath as NSIndexPath).item)
                _ = program.report.save()
            }}, completion: nil)
    }
    func deleteFolderRepresentedByCell(_ cell: FolderCollectionViewCell) {
        let program = SettingsController.sharedSettingsController.selectedReport!.program!
        var indexsToRemove = [Int]()
        for i in 0...program.scenarios.count - 1{
            let scenario = program.scenarios[i]
            if scenario.folder.hasPrefix(cell.folder){
                indexsToRemove.append(i)
            }
        }
        program.scenarios = program.scenarios
            .enumerated()
            .filter { !indexsToRemove.contains($0.offset) }
            .map { $0.element }
        _ = program.report.save()
        cell.removeFromSuperview()
        if(getItemsInFolder(currentFolder).count == 0 && currentFolder != "/"){
            self.backButtonPressed()
        }
        else{
            collectionView.reloadData()
        }
    }
    
    func duplicatePortfolioItemRepresentedByCell(_ cell: PortfolioItemCollectionViewCell) {
        
        collectionView.performBatchUpdates({ () -> Void in
            if let indexPath = self.collectionView.indexPath(for: cell) {
                let newItemIndexPath = IndexPath(item: (indexPath as NSIndexPath).item + 1, section: (indexPath as NSIndexPath).section)
                self.collectionView.insertItems(at: [newItemIndexPath])
                let program = SettingsController.sharedSettingsController.selectedReport!.program!
                let item = cell.scenario
                let newPortfolioItem = item?.duplicate()
                newPortfolioItem?.name = "Duplicate of \(item?.name)"
                program.scenarios.insert(newPortfolioItem!, at: (newItemIndexPath as NSIndexPath).item)
                _ = program.report.save()
            }
            }, completion: nil)
    }
    
    //variables used during a long press and drag gesture, placed here for clarity and convenience
    var movingCell: UICollectionViewCell?
    var compareMoveBeginningLocation = CGPoint.zero

    func handleCellLongPressGesture(_ gesture: UILongPressGestureRecognizer) {
        
        switch self.mode {
        case .normal:
            self.handleNormalModeLongPressGesture(gesture)
            break
        case .detail:
            break
        case .edit:
            self.handleEditModeLongPressGesture(gesture)
            break
        case .compare:
            self.handleCompareModeLongPressGesture(gesture)
            break
        }
    }
    
    fileprivate func handleNormalModeLongPressGesture(_ gesture: UILongPressGestureRecognizer) {
        
        // a long press in Normal mode should just put us into Edit mode, but only if the long press is on one of the cells
        
        if gesture.state == .began {
            let touchLocation = gesture.location(in: self.collectionView)
            if collectionView.indexPathForItem(at: touchLocation) != nil {
                self.mode = .edit
            }
        }
    }
    
   // var touchDelayTimer:NSTimer?
    fileprivate func handleEditModeLongPressGesture(_ gesture: UILongPressGestureRecognizer) {
        
        // in Edit mode, a long press should grab the cell (make it stop wiggling and appear raised) and allow it to move around. movements should be tracked by the collection view, other cells should move to make a space for the moving cell to be dropped into, and when the cell is dropped it should be placed in the open space.
        
        let touchLocation = gesture.location(in: self.collectionView)
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.began: // long press began, grab the cell, make it stop bouncing and appear raised
            
            if let selectedIndexPath = collectionView.indexPathForItem(at: touchLocation) {
                
                //capture a reference to the cell that will be moving as a result of this gesture so we can use it later
              //  movingCell = collectionView.cellForItemAtIndexPath(selectedIndexPath) as? PortfolioItemCollectionViewCell
                if(self.collectionView.cellForItem(at: selectedIndexPath) is PortfolioItemCollectionViewCell){
                    self.movingCell = self.collectionView.cellForItem(at: selectedIndexPath) as? PortfolioItemCollectionViewCell
                }
                else{
                    self.movingCell = self.collectionView.cellForItem(at: selectedIndexPath) as? FolderCollectionViewCell
                }
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: .curveLinear, animations: { () -> Void in
                    //"grab" the cell and animate it jumping to the touch point with a subtle bounce
                    if let cell = self.movingCell {
                        self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
                        self.collectionView.updateInteractiveMovementTargetPosition(touchLocation)
                        cell.isBouncing = false
                        cell.isRaised = true
                    }
                    }, completion: nil)
            }
            
        case UIGestureRecognizerState.changed:
            
            // long press changed, tell the collection view to update the cells location
            collectionView.updateInteractiveMovementTargetPosition(touchLocation)
            if let cell = movingCell{
             //   cell.center = touchLocation
                highlightCellIfTouching(cell)
//                if let timer = touchDelayTimer{
//                    timer.invalidate()
//                    touchDelayTimer = nil
//                }
//                touchDelayTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: collectionView, selector: #selector(collectionView.updateInteractiveMovementTargetPosition(_:)), userInfo: NSValue(CGPoint: touchLocation), repeats: false)
            }
            
        default:
            
            // long press ended, "drop" the cell end the inteactive movement within the collection view
            if let cell = movingCell {
                if let folderingCells = getTouchingCells(){
                    if(folderingCells.count == 2){
                        handleCellDropForFoldering(folderingCells)
                    }
                }
                cell.isBouncing = true
                cell.isRaised = false
                movingCell?.removeFromSuperview()
                movingCell = nil
            }
            collectionView.endInteractiveMovement()
            
            let visibleCellsArray = collectionView.visibleCells
            for i in (0...visibleCellsArray.count - 1){
                let cell = visibleCellsArray[i]
                if(cell is PortfolioItemCollectionViewCell){
                    let portCell = visibleCellsArray[i] as! PortfolioItemCollectionViewCell
                    portCell.shouldShowSolidOutline = false
                }
                else{
                    let folderCell = visibleCellsArray[i] as! FolderCollectionViewCell
                    folderCell.shouldShowSolidOutline = false
                }
            }
        }
    }
    
    fileprivate func addPortfolioItemToFolder(_ portCell: PortfolioItemCollectionViewCell, folderCell: FolderCollectionViewCell){
        if let program = SettingsController.sharedSettingsController.selectedReport?.program{
            portCell.shouldShowSolidOutline = false
            folderCell.shouldShowSolidOutline = false
            if(folderCell.folder == "/Previous"){
                folderCell.folder = getPreviousFolder()
            }
            else{
                portCell.scenario.folder = folderCell.folder
            }
            portCell.scenario.folder = folderCell.folder
            _ = program.report.save()
            self.mode = .normal
        }
    }
    fileprivate func getPreviousFolder() -> String{
        var splitFolderArray = currentFolder.components(separatedBy: "/")
        splitFolderArray.removeLast()
        let stringToRemove = "\(splitFolderArray.last!)/"
        return currentFolder.replacingOccurrences(of: stringToRemove, with: "")
    }
    
    fileprivate func addFolderToFolder(_ folder1: FolderCollectionViewCell, folder2: FolderCollectionViewCell){
        let program = SettingsController.sharedSettingsController.selectedReport!.program!
        if(folder1.folder == "/Previous"){
            folder1.folder = getPreviousFolder()
        }
        else if(folder2.folder == "/Previous"){
            folder2.folder = getPreviousFolder()
        }
        if(folder1 == self.movingCell){
            if(folder1.folder != getPreviousFolder()){
            for scenario in program.scenarios{
                if(scenario.folder == folder1.folder){
                    scenario.folder = "\(folder2.folder)\(getFolderName(folder1.folder))/"
                }
            }
            }
        }
        else{
            if(folder2.folder != getPreviousFolder()){
            for scenario in program.scenarios{
                if(scenario.folder == folder2.folder){
                    scenario.folder = "\(folder1.folder)\(getFolderName(folder2.folder))/"
                }
            }
            }
        }
        _ = program.report.save()
        self.mode = .normal
    }
    fileprivate func handleCellDropForFoldering(_ folderingCells:[AnyObject]){
        let cell1 = folderingCells[0]
        let cell2 = folderingCells[1]
        if(cell1 is PortfolioItemCollectionViewCell){
            if(cell2 is  PortfolioItemCollectionViewCell){
                self.showAlertForNewFolder(folderingCells, message: "Please name your folder")
            }
            else{
                let portCell = cell1 as! PortfolioItemCollectionViewCell
                let folderCell = cell2 as! FolderCollectionViewCell
                addPortfolioItemToFolder(portCell, folderCell: folderCell)
            }
        }
        else{
            if(cell2 is PortfolioItemCollectionViewCell){
                let portCell = cell2 as! PortfolioItemCollectionViewCell
                let folderCell = cell1 as! FolderCollectionViewCell
                addPortfolioItemToFolder(portCell, folderCell: folderCell)
            }
            else{
                let folder1 = cell1 as! FolderCollectionViewCell
                let folder2 = cell2 as! FolderCollectionViewCell
                addFolderToFolder(folder1, folder2: folder2)
            }
        }
    }
    fileprivate func highlightCellIfTouching(_ movingCell: UICollectionViewCell){
        let movingCellRect = movingCell.frame
        let visibleCellsArray = collectionView.visibleCells
        for i in (0...visibleCellsArray.count - 1){
            let cell = visibleCellsArray[i]
            if(cell.frame.intersects(movingCellRect) && cell != movingCell){
                if(cell is PortfolioItemCollectionViewCell){
                    let portCell = visibleCellsArray[i] as! PortfolioItemCollectionViewCell
                    portCell.shouldShowSolidOutline = true
                }
                else{
                    let folderCell = visibleCellsArray[i] as! FolderCollectionViewCell
                    folderCell.shouldShowSolidOutline = true
                }
            }
            else{
                if(cell is PortfolioItemCollectionViewCell){
                    let portCell = visibleCellsArray[i] as! PortfolioItemCollectionViewCell
                    portCell.shouldShowSolidOutline = false
                }
                else{
                    let folderCell = visibleCellsArray[i] as! FolderCollectionViewCell
                    folderCell.shouldShowSolidOutline = false
                }
            }
        }
    }
    fileprivate func getTouchingCells() -> [AnyObject]?{
        var touchingCells = [AnyObject]()
        let movingCellRect = movingCell!.frame
        let visibleCellsArray = collectionView.visibleCells
        for cell in visibleCellsArray{
            if(cell.frame.intersects(movingCellRect) && cell != movingCell){
                touchingCells.append(cell)
            }
        }
        if(touchingCells.count > 0){
            touchingCells.append(movingCell!)
            return touchingCells
        }
        else{
            return nil
        }
    }
    
    fileprivate func handleCompareModeLongPressGesture(_ gesture: UILongPressGestureRecognizer) {
        
        // compare mode is similar to edit mode but we need to do extra work to manage the comparison staging view. also, not all movements should be tracked by the collection view. in compare mode we do not want the other cells to move around and make a space for the moving cell.
        
        let touchLocation = gesture.location(in: self.collectionView)
        
        switch(gesture.state) {
            
        case UIGestureRecognizerState.began:
            
            if let selectedIndexPath = self.collectionView.indexPathForItem(at: touchLocation) {
                
                //                let program = SettingsController.sharedSettingsController.selectedReport!.program!
                let cell = collectionView.cellForItem(at: selectedIndexPath)
                var item: IPScenario?
                if cell is PortfolioItemCollectionViewCell{
                    let portCell = cell as! PortfolioItemCollectionViewCell
                    item = portCell.scenario
                }
                if(item != nil){
                    if item!.eligibleForComparison {
                        
                        //capture the beginning location for the move so we can put the cell back when it is dropped
                        compareMoveBeginningLocation = touchLocation
                        
                        // self.movingCell = self.collectionView.cellForItemAtIndexPath(selectedIndexPath) as? PortfolioItemCollectionViewCell
                        
                        if(self.collectionView.cellForItem(at: selectedIndexPath) is PortfolioItemCollectionViewCell){
                            self.movingCell = self.collectionView.cellForItem(at: selectedIndexPath) as? PortfolioItemCollectionViewCell
                        }
//                        else{
//                            self.movingCell = self.collectionView.cellForItemAtIndexPath(selectedIndexPath) as? FolderCollectionViewCell
//                        }
                        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: .curveLinear, animations: { () -> Void in
                            self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
                            self.collectionView.updateInteractiveMovementTargetPosition(touchLocation)
                            if let cell = self.movingCell {
                                cell.isBouncing = false
                                cell.isRaised = true
                            }
                            }, completion: nil)
                    }
                }
            }
            
        case UIGestureRecognizerState.changed:
            
            // if we're dragging the cell over the header and our comparison staging view, update a visual cue to let the user know they can drop the cell here
            if let cell = movingCell {
                
                if touchLocation.y <= self.layout.headerReferenceSize.height + collectionView.contentOffset.y {
                    comparisonStagingView.isActive = true
                } else {
                    comparisonStagingView.isActive = false
                }
                cell.center = touchLocation
            }
            
        default:
            
            if movingCell == nil {
                break
            }
            
            if comparisonStagingView.isActive {
                let portfolioCell = movingCell as! PortfolioItemCollectionViewCell
                let graph = BarChart.tlvGraphWithTrial(0, loyalty: 0, volume: 0)
                graph.bars[0].title = nil
                graph.bars[1].title = nil
                graph.bars[2].title = nil
                graph.frame = CGRect(x: 0,y: 0,width: 80,height: 80)
                graph.bars[0].value = portfolioCell.viewController.trialBar.value
                graph.bars[1].value = portfolioCell.viewController.loyaltyBar.value
                graph.bars[2].value = portfolioCell.viewController.volumeBar.value
                graph.update()
                comparisonStagingView.dropComparisonObjectRepresentationView(graph, portfolioCell.scenario.name)
                portfolioCell.shouldShowDashedOutline = true
                let item = portfolioCell.scenario
                item?.selectedForComparison = true
                item?.eligibleForComparison = false
            }
            comparisonStagingView.isActive = false
            collectionView.updateInteractiveMovementTargetPosition(compareMoveBeginningLocation)
            collectionView.endInteractiveMovement()
            if let cell = movingCell {
                cell.isBouncing = false
                cell.isRaised = false
                movingCell = nil
            }
            if comparisonStagingView.hasThreeStagedObjects {
                let program = SettingsController.sharedSettingsController.selectedReport!.program!
                for item in program.scenarios {
                    item.eligibleForComparison = false
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
                self.collectionView.reloadData()
            }
            
        }
        
        //-------------------------------------------------------------------------------------------------------------------
    }
    
    fileprivate func sortBySelectedKPI() {
        
        if let program = SettingsController.sharedSettingsController.selectedReport?.program {
            
            program.scenarios.sort(by: { (scenario1, scenario2) -> Bool in
                switch selectedKPI {
                case "Cost Per Incremental Unit Moved" :
                    return scenario1.cpium > scenario2.cpium
                case "Cost Per Unit Moved" :
                    return scenario1.cpum > scenario2.cpum
                case "Profit ROI" :
                    return scenario1.profitRoi > scenario2.profitRoi
                case "Retail ROI" :
                    return scenario1.retailRoi > scenario2.retailRoi
                default :
                    return scenario1.profitRoi > scenario2.profitRoi
                }
            })
        }
    }
    
    
    fileprivate func getItemsInFolder(_ folder: String) -> [AnyObject] {
        
        var unsortedScenarios = [IPScenario]()
        var subFolders = [String]()
        
        if let program = SettingsController.sharedSettingsController.selectedReport?.program {

            for scenario in program.scenarios {
                if scenario.folder == folder {
                    unsortedScenarios.append(scenario)
                }
            }
            
            for scenario in program.scenarios {
                if scenario.folder != folder {
                    if scenario.folder.hasPrefix(folder) {
                        var folderMatchFound = false
                        for folder in subFolders {
                            if folder.hasPrefix(getNextFolderName(scenario.folder)) {
                                folderMatchFound = true
                                break
                            }
                        }
                        if !folderMatchFound {
                            subFolders.append(getNextFolderName(scenario.folder))
                        }
                    }
                    
                }
            }
        }
        
        if self.kpiPicker.selectedKPI == "Cost Per Incremental Unit Moved" {
            unsortedScenarios.sort(by: { $0.cpium < $1.cpium })
        } else if self.kpiPicker.selectedKPI == "Cost Per Unit Moved" {
            unsortedScenarios.sort(by: { $0.cpum < $1.cpum })
        } else if self.kpiPicker.selectedKPI == "Profit ROI" {
            unsortedScenarios.sort(by: { $0.profitRoi > $1.profitRoi })
        } else if self.kpiPicker.selectedKPI == "Retail ROI" {
            unsortedScenarios.sort(by: { $0.retailRoi > $1.retailRoi })
        }
        
        var items = [AnyObject]()
        if(currentFolder != "/"){
            items.append("/Previous" as AnyObject)
        }
        
        for string in subFolders {
            items.append(string as AnyObject)
        }
        
        for scenario in unsortedScenarios {
            items.append(scenario)
        }
        
        return items
    }
    
    fileprivate func getNextFolderName(_ folder: String) -> String{
        var removedCurrentStringFolder = ""
        if(currentFolder == "/"){
            removedCurrentStringFolder = String(folder.characters.dropFirst())
        }
        else{
            removedCurrentStringFolder = folder.replacingOccurrences(of: currentFolder, with: "")
        }
        var splitFolderArray = removedCurrentStringFolder.components(separatedBy: "/")
        if(splitFolderArray[0] == ""){
            splitFolderArray.removeFirst()
        }
        let stringToAdd = "\(splitFolderArray.first!)"
        
        return "\(currentFolder)\(stringToAdd)/"
    }
    
    fileprivate func showAlertForNewFolder(_ folderingCells:[AnyObject], message: String){
        let cell1 = folderingCells[0] as! PortfolioItemCollectionViewCell
        let cell2 = folderingCells[1] as! PortfolioItemCollectionViewCell
        let alert = UIAlertController(title: "Create a new folder", message: message, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Name"
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            if(textField.text == ""){
                self.showAlertForNewFolder(folderingCells, message: "Please name your folder")
            }
            else{
                let newString = textField.text!.replacingOccurrences(of: "/", with: "")
                let newFolder = "\(self.currentFolder)\(newString)/"
                var nameExists = false
                let program = SettingsController.sharedSettingsController.selectedReport?.program
                for scenario in (program?.scenarios)!{
                    if(newFolder == scenario.folder){
                        nameExists = true
                    }
                }
                if(nameExists == false){
                    cell1.scenario.folder = newFolder
                    cell2.scenario.folder = newFolder
                    _ = program?.report.save()
                    self.mode = .normal
                    self.collectionView.reloadData()
                }
                else{
                    self.showAlertForNewFolder(folderingCells, message: "A folder with that name already exists")
                }
            }
        }))
         alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
            
            }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getFolderName(_ folder: String) -> String{
        var splitFolderArray = folder.components(separatedBy: "/")
        splitFolderArray.removeLast()
        var folderNameString = "\(splitFolderArray.last!)"
        if folderNameString == ""{
            folderNameString = "/"
        }
        return folderNameString
    }
}

//MARK: - KPI Picker -


let availableKPIs = [
    "Cost Per Incremental Unit Moved",
    "Cost Per Unit Moved",
    "Profit ROI",
    "Retail ROI"
]

class KPIPicker: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tableView = UITableView()
    var selectedKPI = "Profit ROI"
    var kpiSepectionUpdateBlock: ((_ picker: KPIPicker, _ selectedKPI:String) -> Void)?
    var portfolioViewController: PortfolioViewController?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let frame = CGRect(x: 0, y: 0, width: 320, height: 180)
        self.preferredContentSize = CGSize(width: frame.size.width, height: frame.size.height)
        tableView.frame = frame
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return availableKPIs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let kpi = availableKPIs[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = kpi
        if selectedKPI == kpi {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let kpi =  availableKPIs[(indexPath as NSIndexPath).row]
        
        if selectedKPI != kpi {
            selectedKPI = kpi
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        
        if let block = kpiSepectionUpdateBlock {
            block(self, selectedKPI)
        }
        
        portfolioViewController!.collectionView.reloadData()
    }
}

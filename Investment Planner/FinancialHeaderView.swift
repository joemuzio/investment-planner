//
//  FinancialHeaderView.swift
//  Investment Planner
//
//  Created by Douglas Wall on 8/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class FinancialHeaderView: UICollectionReusableView {
    
    fileprivate(set) var titleLabel: UILabel!
    
    var leftButtons: [UIButton]? {
        
        willSet {
            
            //the buttons are about to change, remove any existing buttons
            if let buttons = leftButtons {
                for button in buttons {
                    button.removeFromSuperview()
                }
            }
        }
        didSet {
            
            //layout the new buttons from left to right in the top left corner of this view
            if let buttons = leftButtons {
                var previousButton: UIButton? = nil
                for button in buttons {
                    self.addSubview(button)
                    if previousButton != nil {
                        button.frame = CGRect(x: previousButton!.frame.origin.x + previousButton!.bounds.width + 30, y: 16, width: button.bounds.width, height: button.bounds.height)
                    } else {
                        button.frame = CGRect(x: 21, y: 16, width: button.bounds.width, height: button.bounds.height)
                    }
                    previousButton = button
                }
            }
        }
        
    }
    
    var rightButtons: [UIButton]? {
        
        willSet {
            
            //the buttons are about to change, remove any existing buttons
            if let buttons = rightButtons {
                for button in buttons {
                    button.removeFromSuperview()
                }
            }
        }
        didSet {
            
            //layout the new buttons from left to right in the top left corner of this view
            if let buttons = rightButtons {
                var previousButton: UIButton? = nil
                for button in buttons {
                    self.addSubview(button)
                    if previousButton != nil {
                        button.frame = CGRect(x: previousButton!.frame.origin.x - (button.bounds.width + 30), y: 16, width: button.bounds.width, height: button.bounds.height)
                    } else {
                        button.frame = CGRect(x: self.bounds.width - (button.bounds.width + 21), y: 16, width: button.bounds.width, height: button.bounds.height)
                    }
                    previousButton = button
                }
            }
        }
    }
    
    var supplementaryViews: [UIView]? {
        
        willSet {
            
            //the suplementary views are about to change, remove any existing views
            if let views = supplementaryViews {
                for view in views {
                    view.removeFromSuperview()
                }
            }
        }
        didSet {
            
            //stack the new views under the title label
            if let views = supplementaryViews {
                var previousView: UIView? = nil
                for view in views {
                    self.addSubview(view)
                    if previousView != nil {
                        view.frame = CGRect(x: (self.bounds.width/2) - (view.bounds.width/2), y: previousView!.frame.origin.y + previousView!.frame.size.height + 20, width: view.bounds.width, height: view.bounds.height)
                    } else {
                        view.frame = CGRect(x: (self.bounds.width/2) - (view.bounds.width/2), y: 70, width: view.bounds.width, height: view.bounds.height)
                    }
                    previousView = view
                }
            }
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.layer.masksToBounds = true
        
        
        

        let backgroundView = GradientView()
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.colors = [UIColor.catalinaLightBlueColor().cgColor, UIColor.catalinaDarkBlueColor().cgColor]
        backgroundView.locations = [0.0, 1.0]
        self.addSubview(backgroundView)
        self.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0))
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.catalinaBookFontOfSize(32)
        titleLabel.text = "Investment Summary"
        titleLabel.textColor = UIColor.catalinaWhiteColor()
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        
        var height: CGFloat = 0
        for view in self.subviews {
            let bottom = view.frame.maxY
            if bottom > height {
                height = bottom
            }
        }
        height += 16
        return CGSize(width: size.width, height: height)
    }
}

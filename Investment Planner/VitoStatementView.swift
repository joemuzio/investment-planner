//
//  VitoStatementView.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 1/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class VitoStatementView: UIView {
    var kpiButton = UIButton()
    var efficiencyButton = UIButton()
    var beginDateButton = UIButton()
    var endDateButton = UIButton()
    var topLabel = UILabel()
    var middleLabel1 = UILabel()
    var middleLabel2 = UILabel()
    var bottomLabel1 = UILabel()
    var bottomLabel2 = UILabel()
    let planDriveString = "This plan will drive "
    let inString = "in "
    let atAString = "at a "

  //  let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    override init (frame : CGRect) {
        super.init(frame : frame)
        kpiButton.setTitleColor(UIColor.catalinaLightGreenColor(), for: UIControlState())
        kpiButton.titleLabel!.font = UIFont.catalinaMediumFontOfSize(32)
        efficiencyButton.setTitleColor(UIColor.catalinaLightGreenColor(), for: UIControlState())
        efficiencyButton.titleLabel!.font = UIFont.catalinaMediumFontOfSize(32)
        beginDateButton.setTitleColor(UIColor.white, for: UIControlState())
        beginDateButton.titleLabel!.font = UIFont.catalinaLightFontOfSize(32)
        endDateButton.setTitleColor(UIColor.white, for: UIControlState())
        endDateButton.titleLabel!.font = UIFont.catalinaLightFontOfSize(32)
        
        beginDateButton.isUserInteractionEnabled = false
        endDateButton.isUserInteractionEnabled = false
        
        topLabel.font = UIFont.catalinaLightFontOfSize(36)
        topLabel.textColor = UIColor.white
        middleLabel1.font = UIFont.catalinaLightFontOfSize(36)
        middleLabel1.textColor = UIColor.white
        middleLabel2.font = UIFont.catalinaLightFontOfSize(36)
        middleLabel2.textColor = UIColor.white
        bottomLabel1.font = UIFont.catalinaLightFontOfSize(36)
        bottomLabel1.textColor = UIColor.white
        bottomLabel2.font = UIFont.catalinaLightFontOfSize(36)
        bottomLabel2.textColor = UIColor.white
        
        self.addSubview(kpiButton)
        self.addSubview(beginDateButton)
        self.addSubview(endDateButton)
        self.addSubview(topLabel)
        self.addSubview(middleLabel1)
        self.addSubview(efficiencyButton)
        self.addSubview(middleLabel2)
        self.addSubview(bottomLabel1)
        self.addSubview(bottomLabel2)
        
    }
    func setupVitoStatement(_ kpiAmount:String, kpiType:String, efficiencyAmount:String, efficiencyType:String){
        kpiButton.setTitle(kpiType, for: UIControlState())
        efficiencyButton.setTitle(efficiencyType, for: UIControlState())
        var topLabelString = ""
            topLabelString = "\(planDriveString)\(kpiAmount) "
        topLabel.text = topLabelString
        let topLabelWidth = getWidthForLabel(topLabel)
        let kpiButtonWidth = getWidthForButton(kpiButton)
        let totalTopLabelWidth = topLabelWidth + kpiButtonWidth
        let topLabelX = (self.frame.size.width - totalTopLabelWidth) / 2
        let kpiButtonX = topLabelX + topLabelWidth
        
        let middleLabel1String = "\(atAString)\(efficiencyAmount) "
        middleLabel1.text = middleLabel1String
        let middleLabelWidth = getWidthForLabel(middleLabel1)
        let middleLabel2Width = getWidthForLabel(middleLabel2)
        let efficiencyButtonWidth = getWidthForButton(efficiencyButton)
        let middleLabel1X = (self.frame.size.width - (middleLabelWidth + middleLabel2Width + efficiencyButtonWidth)) / 2
        let efficiencyButtonX = middleLabel1X + middleLabelWidth
        let middleLabel2X = efficiencyButtonX + efficiencyButtonWidth
        
        let topLabelHeight = self.frame.size.height * 0.28
        let middleLabelHeight = self.frame.size.height * 0.28
        
        let totalVitoHeight = topLabelHeight + middleLabelHeight
        let topLineY = (self.frame.size.height - totalVitoHeight) / 2
        let middleLineY = topLineY + topLabelHeight
        
        topLabel.frame = CGRect(x: topLabelX, y: topLineY, width: topLabelWidth, height: topLabelHeight)
        kpiButton.frame = CGRect(x: kpiButtonX, y: topLineY, width: kpiButtonWidth, height: topLabelHeight)
        middleLabel1.frame = CGRect(x: middleLabel1X, y: middleLineY, width: middleLabelWidth, height: middleLabelHeight)
        efficiencyButton.frame = CGRect(x: efficiencyButtonX, y: middleLineY, width: efficiencyButtonWidth, height: middleLabelHeight)
        middleLabel2.frame = CGRect(x: middleLabel2X, y: middleLineY, width: middleLabel2Width, height: middleLabelHeight)
        
    }
    func getKPI()->String{
        return kpiButton.titleLabel!.text!
    }
    
    func getHeightForLabel(_ label:UILabel) -> CGFloat{
        let newLabel:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: label.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        newLabel.numberOfLines = 0
        newLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        newLabel.font = label.font
        newLabel.text = label.text
        newLabel.sizeToFit()
        return newLabel.frame.height
    }
    func getWidthForLabel(_ label:UILabel) -> CGFloat{
        let newLabel:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: label.frame.size.height))
        newLabel.numberOfLines = 0
        newLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        newLabel.font = label.font
        newLabel.text = label.text
        newLabel.sizeToFit()
        return newLabel.frame.width
    }
    func getWidthForButton(_ button:UIButton) -> CGFloat{
        let newButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: button.frame.size.height))
        newButton.titleLabel!.numberOfLines = 0
        newButton.titleLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
        newButton.titleLabel!.font = button.titleLabel!.font
        newButton.titleLabel!.text = button.titleLabel!.text
        newButton.titleLabel!.sizeToFit()
        return newButton.titleLabel!.frame.width
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

//
//  Constants.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 1/5/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import Foundation

class Constants:NSObject{
    
    let kpiMetricAcquiredBuyersString = "Acquired Buyers"
    let kpiMetricIncrementalDollarsString = "Incremental Dollars"
    let kpiMetricIncrementalUnitsString = "Incremental Units"
    let kpiMetricTotalDollarsString = "Total Dollars"
    let kpiMetricTotalUnitsString = "Total Units"
    
    let efficiencyProfitROIString = "Profit ROI"
    let efficiencyCPUMString = "CPUM"
    let efficiencyCostPerAcquisitionString = "Cost Per Acquisition"
    let efficiencyCPIUMString = "CPIUM"
    let efficiencyIncrementalRetailROIString = "Retail ROI"
    
    let budgetTypeTotalBudgetString = "Total Investment"
    let budgetTypeRedemptionBudgetString = "Redemption Investment"
    let budgetTypeDistributionBudgetString = "Distribution Investment"
    
    let profitMarginString = "Profit Margin"
    let shoppersTargetedString = "Shoppers Targeted"
    let geographyString = "Geography"
    let totalStoreCountString = "Total Store Count"
    let offerDescriptionString = "Offer Description"
    let strategicWeightingString = "Strategic Weighting"
    
    var kpiMetricUnitsArray:[String]?
    var efficienciesArray:[String]?
    var budgetTypesArray:[String]?
    var comparisonViewOrderArray:[String]?
    
   override init(){
        super.init()
    kpiMetricUnitsArray = [kpiMetricAcquiredBuyersString, kpiMetricIncrementalDollarsString, kpiMetricIncrementalUnitsString, kpiMetricTotalDollarsString, kpiMetricTotalUnitsString]
    efficienciesArray = [efficiencyProfitROIString, efficiencyCPUMString, efficiencyCPIUMString, efficiencyIncrementalRetailROIString]
//    budgetTypesArray = [budgetTypeTotalBudgetString, budgetTypeRedemptionBudgetString, budgetTypeDistributionBudgetString]
    comparisonViewOrderArray = [budgetTypeTotalBudgetString, budgetTypeDistributionBudgetString, budgetTypeRedemptionBudgetString, kpiMetricTotalDollarsString, efficiencyIncrementalRetailROIString, efficiencyCPUMString, kpiMetricTotalUnitsString, kpiMetricIncrementalUnitsString, kpiMetricIncrementalDollarsString, efficiencyProfitROIString, efficiencyCPIUMString, kpiMetricAcquiredBuyersString, efficiencyCostPerAcquisitionString, profitMarginString, shoppersTargetedString, geographyString, totalStoreCountString, offerDescriptionString]
    
    }
}

//
//  SolverController.swift
//  Investment Planner
//
//  Created by Douglas Wall on 4/20/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import Foundation

private let SharedSolverControllerInstance = SolverController()

/* Constants */
let RANGE_PERCENTAGE = 0.01
let RANGE_EKPI = 0.01
let RANGE_INVESTMENT_M = 100000.0
let RANGE_INVESTMENT_500K = 50000.0
let RANGE_INVESTMENT_50K = 10000.0

let EKPI_TYPE_PROFIT_ROI = 1
let EKPI_TYPE_RETAIL_ROI = 2
let EKPI_TYPE_CPUM = 3
let EKPI_TYPE_CPIUM = 4
let MULT_TYPE_OCM = 5

let MINIMUM_VOLUME_EACH = 50000.0
let MINIMUM_OUTU_TOTAL = 250000.0
let MINIMUM_TRIAL_EACH = 50000.0

class SolverController: NSObject {
    
    fileprivate var lastSuccessfulSolveId = 0
    fileprivate var currentSolveId = 0
    fileprivate var lastSolution = [String: Double]()
    
    class var sharedSolverController: SolverController {
        
        return SharedSolverControllerInstance
        
    }
    
    func optimalRetailRoiForConstraints(_ scenario: IPScenario, retailRoi: Double?, trialPercent: Double?, loyaltyPercent: Double?, volumePercent: Double?, investment: Double?) {
        let lp = FormulaSystem()
        setupDistributionColumns(lp, scenario: scenario)
        if retailRoi != nil {
            addRetailRoiConstraint(lp, scenario: scenario, retailRoi: retailRoi!)
        }
        setRetailRoiObjective(lp, scenario: scenario)
        setupConstraints(lp, scenario: scenario, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: investment)    }
    
    func optimalProfitRoiForConstraints(_ scenario: IPScenario, profitRoi: Double?, trialPercent: Double?, loyaltyPercent: Double?, volumePercent: Double?, investment: Double?) {
        //        let program = SettingsController.sharedSettingsController.selectedProgram!
        let lp = FormulaSystem()
        setupDistributionColumns(lp, scenario: scenario)
        if profitRoi != nil {
            addProfitRoiConstraint(lp, scenario: scenario, profitRoi: profitRoi!)
        }
        setProfitRoiObjective(lp, scenario: scenario)
        setupConstraints(lp, scenario: scenario, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: investment)
    }
    
    func optimalCpumForConstraints(_ scenario: IPScenario, cpum: Double?, trialPercent: Double?, loyaltyPercent: Double?, volumePercent: Double?, investment: Double?) {
        let lp = FormulaSystem()
        setupDistributionColumns(lp, scenario: scenario)
        if cpum != nil {
            addCpumConstraint(lp, scenario: scenario, cpum: cpum!)
        }
        setCpumObjective(lp, scenario: scenario)
        setupConstraints(lp, scenario: scenario, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: investment)
    }
    
    func optimalCpiumForConstraints(_ scenario: IPScenario, cpium: Double?, trialPercent: Double?, loyaltyPercent: Double?, volumePercent: Double?, investment: Double?) {
        let lp = FormulaSystem()
        setupDistributionColumns(lp, scenario: scenario)
        if cpium != nil {
            addCpiumConstraint(lp, scenario: scenario, cpium: cpium!)
        }
        setCpiumObjective(lp, scenario: scenario)
        setupConstraints(lp, scenario: scenario, trialPercent: trialPercent, loyaltyPercent: loyaltyPercent, volumePercent: volumePercent, investment: investment)
    }
    
    func optimalInvestmentForConstraints(_ scenario: IPScenario) {
        let lp = FormulaSystem()
        setupDistributionColumns(lp, scenario: scenario)
        setInvestmentObjective(lp, scenario: scenario)
        setupConstraints(lp, scenario: scenario, trialPercent: nil, loyaltyPercent: nil, volumePercent: nil, investment: nil)
    }
    
    fileprivate func setupConstraints(_ lp:FormulaSystem, scenario: IPScenario, trialPercent: Double?, loyaltyPercent: Double?, volumePercent: Double?, investment: Double?) {
        if trialPercent != nil {
            addTrialPercentConstraint(lp, scenario: scenario, budgetPercent: trialPercent!)
        }
        if loyaltyPercent != nil {
            addLoyaltyPercentConstraint(lp, scenario: scenario, budgetPercent: loyaltyPercent!)
        }
        if volumePercent != nil {
            addVolumePercentConstraint(lp, scenario: scenario, budgetPercent: volumePercent!)
        }
        if investment != nil {
            addInvestmentConstraint(lp, scenario: scenario, investment: investment!)
        }
        addMinBudgetConstraint(lp, scenario: scenario)
        addMaxDistributionConstraints(lp, scenario: scenario)
        setTrialDeduplicationConstraint(lp, scenario: scenario)
        setupOnOffLinkage(lp, scenario: scenario)
        setupTrialMinimums(lp, scenario: scenario)
        setupOUTUMinimum(lp, scenario: scenario)
        setupCCMandOUTUdedup(lp, scenario: scenario)
        setupCCM(lp, scenario: scenario)
        
//        print("==============================================================")
        let (status, solution) = lp.solve()
        currentSolveId += 1
        //lp.printSystemInFriendlyForm()
        if(status == 1) {
            lastSuccessfulSolveId = currentSolveId
            lastSolution.removeAll()
            for column in lp.columns {
                lastSolution[column.name] = solution[column.name] ?? 0.0
            }
            mapSolutionToScenario(scenario)
        }
    }
    
    func setupDistributionColumns(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        _ = formulaSystem.addColumn("\(offer.name)?\(cycleName)", isBinary: false)
                        _ = formulaSystem.addColumn("\(offer.name)F\(cycleName)", isBinary: true)
                        
                    }
                }
            }
        }
    }
    
    /*!
     * @brief Function to load the cycles values for the selectd eKPI
     * @param scenario The scenario object to use.
     * @param type The EKPI_TYPE constant representing the eKPI type.
     */
    fileprivate func loadEKpiCycleArray(_ scenario: IPScenario, type: Int) -> [String:Double] {
        var returnArray = [String: Double]()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        let key = offer.name.rawValue + cycleName
                        switch type {
                        case EKPI_TYPE_PROFIT_ROI:
                            returnArray[key] = offer.getProfitRoiForCycle(cycleName)
                            break;
                        case EKPI_TYPE_RETAIL_ROI:
                            returnArray[key] = offer.getRetailRoiForCycle(cycleName)
                            break;
                        case EKPI_TYPE_CPUM:
                            returnArray[key] = offer.getCpumForCycle(cycleName)
                            break;
                        case EKPI_TYPE_CPIUM:
                            returnArray[key] = offer.getCpiumForCycle(cycleName)
                            break;
                        case MULT_TYPE_OCM:
                            returnArray[key] = offer.getOfferCostMultiplierForCycle(cycleName)
                            break;
                        default:
                            print("Unknown eKPI Type: \(type)")
                        }
                    }
                }
            }
        }
        return returnArray
    }
    
    /*!
     * @brief Cycles through each offer and cycle and adds the high and low range for the slider.
     */
    fileprivate func buildEkpiContraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, kpiValue: Double, eKpiType:Int) {
        let cycleValues = loadEKpiCycleArray(scenario, type: eKpiType)
        let kpiValueHigh: Double = ( round(kpiValue * 100)+(RANGE_EKPI*100/2))/100 - 0.0000001
        let kpiValueLow: Double = ( round(kpiValue * 100)-(RANGE_EKPI*100/2))/100
        
//        print("Input: \(kpiValue)")
//        print("High:  \(kpiValueHigh)")
//        print("Low:   \(kpiValueLow)")
        
//        print(cycleValues)
        let kpiFormulaHigh = Row()
        let kpiFormulaLow = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        let key = offer.name.rawValue + cycleName
                        kpiFormulaHigh.add("\(offer.name)?\(cycleName)", cycleValues[key]! - kpiValueHigh)
                        kpiFormulaLow.add("\(offer.name)?\(cycleName)", cycleValues[key]! - kpiValueLow)
                        
                    }
                }
            }
        }
        formulaSystem.addConstraint(kpiFormulaHigh, constraint: "<=", constant: 0)
        formulaSystem.addConstraint(kpiFormulaLow, constraint: ">=", constant: 0)
    }
    
    
    func addRetailRoiConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, retailRoi: Double) {
        buildEkpiContraint(formulaSystem, scenario: scenario, kpiValue: retailRoi, eKpiType: EKPI_TYPE_RETAIL_ROI)
    }
    
    func addProfitRoiConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, profitRoi: Double) {
        buildEkpiContraint(formulaSystem, scenario: scenario, kpiValue: profitRoi, eKpiType: EKPI_TYPE_PROFIT_ROI)
    }
    
    func addCpumConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, cpum: Double) {
        buildEkpiContraint(formulaSystem, scenario: scenario, kpiValue: cpum, eKpiType: EKPI_TYPE_CPUM)
    }
    
    func addCpiumConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, cpium: Double) {
        buildEkpiContraint(formulaSystem, scenario: scenario, kpiValue: cpium, eKpiType: EKPI_TYPE_CPIUM)
    }
    
    fileprivate func addPercentConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, budgetPercent: Double, offerType: IPOfferType) {
        let cycleOCMs = loadEKpiCycleArray(scenario, type: MULT_TYPE_OCM)
        let newFormulaHigh = Row()
        let newFormulaLow = Row()
        
        let percentHigh: Double = ( round(budgetPercent * 100)+(RANGE_PERCENTAGE*100/2))/100 - 0.0000001
        let percentLow: Double = ( round(budgetPercent * 100)-(RANGE_PERCENTAGE*100/2))/100
        
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        let offerCostMultiplier = cycleOCMs[offer.name.rawValue + cycleName]!
                        if(offer.type == offerType){
                            newFormulaHigh.add("\(offer.name)?\(cycleName)", offerCostMultiplier - (offerCostMultiplier * (percentHigh / 100)))
                            newFormulaLow.add("\(offer.name)?\(cycleName)", offerCostMultiplier - (offerCostMultiplier * (percentLow / 100)))
                        }else{
                            newFormulaHigh.subtract("\(offer.name)?\(cycleName)", (offerCostMultiplier * (percentHigh / 100)))
                            newFormulaLow.subtract("\(offer.name)?\(cycleName)", (offerCostMultiplier * (percentLow / 100)))
                        }
                    }
                }
            }
        }
        formulaSystem.addConstraint(newFormulaHigh, constraint: "<=", constant: 0)
        formulaSystem.addConstraint(newFormulaLow, constraint: ">=", constant: 0)
        
    }
    
    func addTrialPercentConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, budgetPercent: Double) {
        addPercentConstraint(formulaSystem, scenario: scenario, budgetPercent: budgetPercent, offerType: IPOfferType.Trial)
    }
    
    func addLoyaltyPercentConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, budgetPercent: Double) {
        addPercentConstraint(formulaSystem, scenario: scenario, budgetPercent: budgetPercent, offerType: IPOfferType.Loyalty)
    }
    
    func addVolumePercentConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, budgetPercent: Double) {
        addPercentConstraint(formulaSystem, scenario: scenario, budgetPercent: budgetPercent, offerType: IPOfferType.Volume)    }
    
    fileprivate func determineRangeForInvestmentSlider(_ investment: Double) -> Double {
        if(investment >= 1000000) {
            return RANGE_INVESTMENT_M
        }
        if(investment >= 500000) {
            return RANGE_INVESTMENT_500K
        }
        return RANGE_INVESTMENT_50K
    }
    
    func addInvestmentConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario, investment: Double) {
        let formulaHigh = Row()
        let formulaLow = Row()
        
        let investRange = determineRangeForInvestmentSlider(investment)
        
        let investmentHigh: Double = ( round(investment * 100)+(investRange*100/2))/100 - 0.0000001
        let investmentLow: Double = ( round(investment * 100)-(investRange*100/2))/100
        
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        formulaHigh.add("\(offer.name)?\(cycleName)", offer.getOfferCostMultiplierForCycle(cycleName))
                        formulaLow.add("\(offer.name)?\(cycleName)", offer.getOfferCostMultiplierForCycle(cycleName))
                    }
                }
            }
        }
        formulaSystem.addConstraint(formulaHigh, constraint: "<=", constant: investmentHigh)
        formulaSystem.addConstraint(formulaLow, constraint: ">=", constant: investmentLow)
        
    }
    
    func addMinBudgetConstraint(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        // Determine the minimum budget
        var minBudget = 999999999.0
        var minOutuOCM = 100.0
        let formula = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        if offer.type == .Trial {
                            minBudget = min(minBudget, MINIMUM_TRIAL_EACH * offer.getOfferCostMultiplierForCycle(cycleName))
                        } else if( offer.type == .Loyalty) {
                            minOutuOCM = min( minOutuOCM, offer.getOfferCostMultiplierForCycle(cycleName))
                        } else if( offer.type == .Volume) {
                            minBudget = min(minBudget, MINIMUM_VOLUME_EACH)
                        } else {
                            print("Unknown offer type: \(offer.type)")
                        }
                        
                        formula.add("\(offer.name)?\(cycleName)", offer.getOfferCostMultiplierForCycle(cycleName))
                        
                    }
                }
            }
        }
        minBudget = min(minBudget, MINIMUM_OUTU_TOTAL * minOutuOCM)
        formulaSystem.addConstraint(formula, constraint: ">=", constant: minBudget)
    }
    
    func addMaxDistributionConstraints(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let CCMMode = determineCCMMode(scenario)
        for offer in scenario.offers {
            if offer.enabled {
                if offer.type == .Trial || offer.type == .Loyalty || ( offer.type == .Volume && CCMMode == 0) {
                    if let firstCoupon = offer.coupons.first {
                        for (cycleName, _) in firstCoupon.distributionCycles {
                            
                            let kpiFormula = Row()
                            kpiFormula.add("\(offer.name)?\(cycleName)", 1)
                            formulaSystem.addConstraint(kpiFormula, constraint: "<=", constant: round(offer.getMaxDistributionForCycle(cycleName)))
                            
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func setupCCM(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let runCCM = shouldRunCCM(formulaSystem, scenario: scenario)
        
        // IF we are not running CCM, then fix the variables to 0.
        if !runCCM {
            for offer in scenario.offers {
                if offer.enabled {
                    if offer.type == .Volume {
                        if let firstCoupon = offer.coupons.first {
                            for (cycleName, _) in firstCoupon.distributionCycles {
                                if let column = formulaSystem.getColumnByName("\(offer.name)?\(cycleName)") {
                                    column.setFixedValue(0.0)
                                }
                                if let column = formulaSystem.getColumnByName("\(offer.name)F\(cycleName)") {
                                    column.setFixedValue(0.0)
                                }
                            }
                        }
                    }
                }
            }
        } else {
            // We are running CCM.  Lets determine the mode.
            let CCMMode = determineCCMMode(scenario)
            for offer in scenario.offers {
                if offer.enabled {
                    if offer.type == .Volume {
                        if let firstCoupon = offer.coupons.first {
                            for (cycleName, _) in firstCoupon.distributionCycles {
                                if( CCMMode == 1) {
                                    // CCM is all-or-nothing
                                    if(offer.getMaxDistributionForCycle(cycleName)*offer.getOfferCostMultiplierForCycle(cycleName) >= MINIMUM_VOLUME_EACH) {
                                        let AllOrNothing = Row()
                                        AllOrNothing.add("\(offer.name)?\(cycleName)", 1)
                                        AllOrNothing.subtract("\(offer.name)F\(cycleName)", offer.getMaxDistributionForCycle(cycleName))
                                        formulaSystem.addConstraint(AllOrNothing, constraint: "=", constant: 0)
                                    } else {
                                        // The cycle cant hit the minimum, so just make it zero.
                                        if let column = formulaSystem.getColumnByName("\(offer.name)?\(cycleName)") {
                                            column.setFixedValue(0.0)
                                        }
                                        if let column = formulaSystem.getColumnByName("\(offer.name)F\(cycleName)") {
                                            column.setFixedValue(0.0)
                                        }
                                    }
                                } else {
                                    // Setup Link to F variable
                                    let newFormula = Row()
                                    newFormula.add("\(offer.name)?\(cycleName)", 1)
                                    newFormula.subtract("\(offer.name)F\(cycleName)", offer.getMaxDistributionForCycle(cycleName))
                                    formulaSystem.addConstraint(newFormula, constraint: "<=", constant: 0)
                                    
                                    let newFormula2 = Row()
                                    newFormula2.add("\(offer.name)?\(cycleName)", 1)
                                    newFormula2.subtract("\(offer.name)F\(cycleName)", 1)
                                    formulaSystem.addConstraint(newFormula2, constraint: ">=", constant: 0)
                                    
                                    // Set the minimum
                                    let CostFormula = Row()
                                    let ocm = offer.getOfferCostMultiplierForCycle(cycleName)
                                    CostFormula.add("\(offer.name)?\(cycleName)",ocm)
                                    // Add the non-retailer specific portions of constraint formulas
                                    CostFormula.subtract("\(offer.name)F\(cycleName)", MINIMUM_VOLUME_EACH)
                                    // Add constraints to solver
                                    formulaSystem.addConstraint(CostFormula, constraint: ">=", constant: 0)
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func shouldRunCCM(_ formulaSystem: FormulaSystem, scenario: IPScenario) -> Bool {
        var shouldRunCcm = true
        for offer in scenario.offers {
            if offer.enabled {
                if offer.type == .Volume {
                    if let firstCoupon = offer.coupons.first {
                        for (cycleName, distribution) in firstCoupon.distributionCycles {
                            
                            if distribution > (offer.getMaxDistributionForCycle(cycleName) * 0.90) {
                                shouldRunCcm = false
                            }
                            
                        }
                    }
                }
            }
        }
        return shouldRunCcm
    }
    
    
    func roundToDecimal(_ value:Double, decimals:Int) -> Double {
        let mult = pow(10.0, Double(decimals))
        let temp = Double(Float(round(mult * value)/mult))
        return temp
    }
    
    func setDistributionObjective(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let objective = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        objective.add("\(offer.name)?\(cycleName)", 1)
                        
                    }
                }
            }
        }
        formulaSystem.setObjective(objective)
    }
    
    func setInvestmentObjective(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let objective = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        objective.add("\(offer.name)?\(cycleName)", roundToDecimal(offer.getOfferCostMultiplierForCycle(cycleName),decimals: 4))
                        
                    }
                }
            }
        }
        formulaSystem.setObjective(objective)
    }
    
    func setProfitRoiObjective(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let objective = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        objective.add("\(offer.name)?\(cycleName)", roundToDecimal(offer.getProfitRoiForCycle(cycleName) * offer.getOfferCostMultiplierForCycle(cycleName), decimals: 4))
                        
                    }
                }
            }
        }
        formulaSystem.setObjective(objective)
    }
    
    func setRetailRoiObjective(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let objective = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        objective.add("\(offer.name)?\(cycleName)", roundToDecimal(offer.getRetailRoiForCycle(cycleName) * offer.getOfferCostMultiplierForCycle(cycleName), decimals: 4))
                        
                    }
                }
            }
        }
        formulaSystem.setObjective(objective)
    }
    
    func setCpumObjective(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let objective = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        objective.add("\(offer.name)?\(cycleName)", roundToDecimal(offer.getCpumForCycle(cycleName) * offer.getOfferCostMultiplierForCycle(cycleName), decimals: 4))
                        
                    }
                }
            }
        }
        formulaSystem.setObjective(objective)
    }
    
    func setCpiumObjective(_ formulaSystem: FormulaSystem, scenario: IPScenario) {
        let objective = Row()
        for offer in scenario.offers {
            if offer.enabled {
                if let firstCoupon = offer.coupons.first {
                    for (cycleName, _) in firstCoupon.distributionCycles {
                        
                        objective.add("\(offer.name)?\(cycleName)", roundToDecimal(offer.getCpiumForCycle(cycleName) * offer.getOfferCostMultiplierForCycle(cycleName), decimals: 4))
                        
                    }
                }
            }
        }
        formulaSystem.setObjective(objective)
    }
    
    func setTrialDeduplicationConstraint(_ formulaSystem:FormulaSystem, scenario:IPScenario){
        let setup = scenario.program.report.data.setup
//        print(setup["DM Target"])
//        print(setup["Shoppers (Heavy Category and Never Buy)"])
        
        let dedup = min((setup["DM Target"] as! Double) * 0.411, (setup["Shoppers (Heavy Category and Never Buy)"] as! Double))
        
        // Add columns for dedup values
        _ = formulaSystem.addColumn("\(IPOfferName.DataMining)Dfirst", isBinary: false)
        _ = formulaSystem.addColumn("\(IPOfferName.DataMining)Dsecond", isBinary: false)
        _ = formulaSystem.addColumn("\(IPOfferName.HeavyNeverBuy)Dfirst", isBinary: false)
        _ = formulaSystem.addColumn("\(IPOfferName.HeavyNeverBuy)Dsecond", isBinary: false)
        
        
        // Tie Dedup cycles together
        let cycle1Formula = Row()
        let cycle2Formula = Row()
        
        cycle1Formula.add("\(IPOfferName.DataMining)Dfirst", 1)
        cycle1Formula.add("\(IPOfferName.HeavyNeverBuy)Dfirst", 1)
        
        cycle2Formula.add("\(IPOfferName.DataMining)Dsecond", 1)
        cycle2Formula.add("\(IPOfferName.HeavyNeverBuy)Dsecond", 1)
        
        formulaSystem.addConstraint(cycle1Formula, constraint: "=", constant: round(dedup))
        formulaSystem.addConstraint(cycle2Formula, constraint: "=", constant: round(dedup))
        
        
        // Set MaxDist >= Dedup + SolvedDistribution for each relevant offer(cycle
        
        for offer in scenario.offers {
            if offer.enabled {
                if offer.name == .DataMining || offer.name == .HeavyNeverBuy {
                    if let firstCoupon = offer.coupons.first?.getTemplateCoupon() {
                        for (cycleName, maxDistribution) in firstCoupon.distributionCycles {
                            // Add distribution max limit constraints
                            let newFormula = Row()
                            newFormula.add("\(offer.name)?\(cycleName)", 1)
                            newFormula.add("\(offer.name)D\(cycleName)", 0.65)
                            formulaSystem.addConstraint(newFormula, constraint: "<=", constant:  (maxDistribution))
                            
                            // Add deduplication factor less than maxUniverse Constraint
                            let newFormula2 = Row()
                            newFormula2.add("\(offer.name)D\(cycleName)", 1)
                            formulaSystem.addConstraint(newFormula2, constraint: "<=", constant: (maxDistribution))
                            
                        }
                    }
                }
            }
        }
    }
    
    func setupTrialMinimums(_ formulaSystem:FormulaSystem, scenario:IPScenario) {
        for offer in scenario.offers {
            if offer.enabled {
                if offer.type == .Trial {
                    if let firstCoupon = offer.coupons.first {
                        for (cycleName, _) in firstCoupon.distributionCycles {
                            let newFormula = Row()
                            newFormula.add("\(offer.name)?\(cycleName)", 1)
                            newFormula.subtract("\(offer.name)F\(cycleName)", MINIMUM_TRIAL_EACH)
                            formulaSystem.addConstraint(newFormula, constraint: ">=", constant: 0)
                        }
                    }
                }
            }
        }
    }
    
    func setupOnOffLinkage(_ formulaSystem:FormulaSystem, scenario:IPScenario) {
        for offer in scenario.offers {
            if offer.enabled {
                if offer.type == .Trial || offer.type == .Loyalty {
                    if let coupon = offer.coupons.first {
                        for (cycleName, _) in coupon.distributionCycles {
                            
                            let newFormula = Row()
                            newFormula.add("\(offer.name)?\(cycleName)", 1)
                            newFormula.subtract("\(offer.name)F\(cycleName)", offer.getMaxDistributionForCycle(cycleName))
                            formulaSystem.addConstraint(newFormula, constraint: "<=", constant: 0)
                            
                            let newFormula2 = Row()
                            newFormula2.add("\(offer.name)?\(cycleName)", 1)
                            newFormula2.subtract("\(offer.name)F\(cycleName)", 1)
                            formulaSystem.addConstraint(newFormula2, constraint: ">=", constant: 0)
                        }
                    }
                }
            }
        }
    }
    
    func setupOUTUMinimum(_ formulaSystem:FormulaSystem, scenario:IPScenario) {
        
        //      Add new binary column
        _ = formulaSystem.addColumn("L", isBinary: true)
        
        //      Create linkage: IF any cycle has distribution THEN "L" must be 1
        let outuMinimumLinkage1 = Row()
        let outuMinimumLinkage2 = Row()
        
        for offer in scenario.offers {
            if offer.enabled {
                if offer.name == .OUTU {
                    if let coupon = offer.coupons.first?.getTemplateCoupon() {
                        for (cycleName, _) in coupon.distributionCycles {
                            outuMinimumLinkage1.add("\(offer.name)F\(cycleName)", 1)
                            outuMinimumLinkage2.add("\(offer.name)F\(cycleName)", 1)
                        }
                    }
                }
            }
        }
        
        outuMinimumLinkage1.subtract("L", 13)
        formulaSystem.addConstraint(outuMinimumLinkage1, constraint: "<=", constant: 0)
        
        outuMinimumLinkage2.subtract("L", 1)
        formulaSystem.addConstraint(outuMinimumLinkage2, constraint: ">=", constant: 0)
        
        //      Create minimum: If "L" is 1 THEN total OUTU distribution must be >= 250,000
        let outuMinimumConstraint = Row()
        
        for offer in scenario.offers {
            if offer.enabled {
                if offer.name == .OUTU {
                    if let coupon = offer.coupons.first {
                        for (cycleName, _) in coupon.distributionCycles {
                            outuMinimumConstraint.add("\(offer.name)?\(cycleName)", 1)
                        }
                    }
                }
            }
        }
        
        outuMinimumConstraint.subtract("L", MINIMUM_OUTU_TOTAL)
        formulaSystem.addConstraint(outuMinimumConstraint, constraint: ">=", constant: 0)
    }
    
    /*!
     * @brief When there are fewer than 7 retailers active OR the percent attributed to a
     * single retailer is greater than 25%, Then each volume cycle is all-or-nothing.
     */
    func determineCCMMode(_ scenario:IPScenario) -> Int {
        let retailers = scenario.getAdjustedRetailers()
        var count: Int = 0
        var highPercent: Double = 0.0
        for retailer in retailers {
            count += 1
            highPercent = max(highPercent, retailer.percentTrips)
        }
        if(count < 7)||(highPercent > 0.70) {
            return 1;
        }
        return 0;
        
    }
    
    func setupCCMandOUTUdedup(_ formulaSystem:FormulaSystem, scenario:IPScenario) {
        for offer in scenario.offers {
            if offer.enabled {
                if offer.name == .CCM {
                    if let coupon = offer.coupons.first?.getTemplateCoupon() {
                        for (cycleName, maxDistribution) in coupon.distributionCycles {
                            if(maxDistribution > 0) {
                                let formula = Row()
                                formula.add("\(IPOfferName.OUTU)F\(cycleName)",1)
                                formula.add("\(offer.name)F\(cycleName)", 1)
                                formulaSystem.addConstraint(formula, constraint: "<=", constant: 1)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func checkLastSolveWasSuccessful() -> Bool{
        return (lastSuccessfulSolveId == currentSolveId)
    }
    
    func getLastValidSolution() -> [String: Double] {
        return lastSolution
    }
    
    fileprivate func mapSolutionToScenario(_ scenario: IPScenario) {
        let solution = getLastValidSolution()
        for offer in scenario.offers {
            for coupon in offer.coupons {
                let templateCoupon = coupon.getTemplateCoupon()
                for (cycleName, distribution) in templateCoupon.distributionCycles {
                    let cycleMaxDistribution = templateCoupon.offer.getMaxDistributionForCycle(cycleName)
                    let buyLevelWeight = cycleMaxDistribution > 0 ? distribution / cycleMaxDistribution : 0
                    var totalCycleDistribution:Double = 0.0
                    totalCycleDistribution = solution["\(offer.name)?\(cycleName)"] ?? 0
                    if totalCycleDistribution < 100 || offer.enabled == false {
                        totalCycleDistribution = 0
                    }
                    coupon.distributionCycles[cycleName] = totalCycleDistribution * buyLevelWeight
                    
                }
            }
        }
    }
    
}
 

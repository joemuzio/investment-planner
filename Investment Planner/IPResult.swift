//
//  IPResult.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/20/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPResult: IPModelObject {
    
    var target: String = ""
    var incrementalityShorttermIntercept: Double = 0
    var incrementalityShorttermXdiscountPercent: Double = 0
    var redemptionIntercept: Double = 0
    var redemptionXdiscountPercent: Double = 0
    var redemptionXdiscountAmount: Double = 0
    var redemptionXbuylevel: Double = 0
    var repeatIntercept: Double = 0
    var repeatXdiscountPercent: Double = 0
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "target",
            "incrementalityShorttermIntercept",
            "incrementalityShorttermXdiscountPercent",
            "redemptionIntercept",
            "redemptionXdiscountPercent",
            "redemptionXdiscountAmount",
            "redemptionXbuylevel",
            "repeatIntercept",
            "repeatXdiscountPercent"])
        return keys
    }
}

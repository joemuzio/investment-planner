//
//  FinancialSummaryViewController.swift
//  Investment Planner
//
//  Created by Douglas Wall on 8/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

enum ComponentType {
    case trial, loyalty, volume, total
}

enum MetricCategory {
    case `return`, roi, investment
}

class FinancialMetric {
    
    var displayName: String!
    var category: MetricCategory!
    
    init(displayName: String, category: MetricCategory) {
        self.displayName = displayName
        self.category = category
    }
    
    func metricValueForObject(_ scenario: IPScenario, _ componentType: ComponentType) -> String {
        if displayName == "Total Sales Dollars" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        result += offer.totalDollars
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        result += offer.totalDollars
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        result += offer.totalDollars
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .total {
                return scenario.totalDollars.currencyString(0)
            }
        }
        else if displayName == "Incremental Sales Dollars" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons{
                            result += coupon.totalIncrementalDollars
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        result += offer.totalIncrementalDollars
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        result += offer.totalIncrementalDollars
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .total {
                return scenario.totalIncrementalDollars.currencyString(0)
            }
        }
        else if displayName == "% Incremental" {
            if componentType == .trial {
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        return offer.incrementalRatio.percentString(0)
                    }
                }
            }
            else if componentType == .loyalty {
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        return offer.incrementalRatio.percentString(0)
                    }
                }
            }
            else if componentType == .volume {
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        return offer.incrementalRatio.percentString(0)
                    }
                }
            }
            else if componentType == .total {
                let scenarioIncRatio: Double = scenario.totalUnits != 0 ? scenario.totalIncrementalUnits / scenario.totalUnits : 0
                return scenarioIncRatio.percentString(0)
            }
        }
        else if displayName == "Total Units" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        result += offer.totalUnits
                    }
                }
                return result.numberWithCommaString()
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        result += offer.totalUnits
                    }
                }
                return result.numberWithCommaString()
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        result += offer.totalUnits
                    }
                }
                return result.numberWithCommaString()
            }
            else if componentType == .total {
                return scenario.totalUnits.numberWithCommaString()
            }
        }
        else if displayName == "Incremental Units" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        result += offer.totalIncrementalUnits
                    }
                }
                return result.numberWithCommaString()
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        result += offer.totalIncrementalUnits
                    }
                }
                return result.numberWithCommaString()
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        result += offer.totalIncrementalUnits
                    }
                }
                return result.numberWithCommaString()
            }
            else if componentType == .total {
                return scenario.totalIncrementalUnits.numberWithCommaString()
            }
        }
        else if displayName == "Incremental Profit $" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons{
                            result += coupon.totalIncrementalDollars * scenario.program.profitMargin
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        result += offer.totalIncrementalDollars * scenario.program.profitMargin
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        result += offer.totalIncrementalDollars * scenario.program.profitMargin
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .total {
                return (scenario.totalIncrementalDollars * scenario.program.profitMargin).currencyString(0)
            }
        }
        else if displayName == "Profit ROI" {
            if componentType == .trial {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalIncrementalDollars += coupon.totalIncrementalDollars * coupon.offer.scenario.program.profitMargin
                        }
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            }
            else if componentType == .loyalty {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalIncrementalDollars += coupon.totalIncrementalDollars * coupon.offer.scenario.program.profitMargin
                        }
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            }
            else if componentType == .volume {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalIncrementalDollars += coupon.totalIncrementalDollars * coupon.offer.scenario.program.profitMargin
                        }
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()            }
            else if componentType == .total {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        totalInvestment += coupon.totalInvestment
                        totalIncrementalDollars += coupon.totalIncrementalDollars * coupon.offer.scenario.program.profitMargin
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            }
        }
        else if displayName == "Profit Margin %" {
            return scenario.program.profitMargin.percentString(0)
        }
        else if displayName == "Retail ROI" {
            if componentType == .trial {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalIncrementalDollars += coupon.totalIncrementalDollars
                        }
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            }
            else if componentType == .loyalty {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalIncrementalDollars += coupon.totalIncrementalDollars
                        }
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            }
            else if componentType == .volume {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalIncrementalDollars += coupon.totalIncrementalDollars
                        }
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()            }
            else if componentType == .total {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        totalInvestment += coupon.totalInvestment
                        totalIncrementalDollars += coupon.totalIncrementalDollars
                    }
                }
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            }
        }
        else if displayName == "CPUM" {
            if componentType == .trial {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalUnits += coupon.totalUnits
                        }
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
            else if componentType == .loyalty {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalUnits += coupon.totalUnits
                        }
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
            else if componentType == .volume {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalUnits += coupon.totalUnits
                        }
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
            else if componentType == .total {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        totalInvestment += coupon.totalInvestment
                        totalUnits += coupon.totalUnits
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
        }
        else if displayName == "CPIUM" {
            if componentType == .trial {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalUnits += coupon.totalIncrementalUnits
                        }
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
            else if componentType == .loyalty {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalUnits += coupon.totalIncrementalUnits
                        }
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
            else if componentType == .volume {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            totalInvestment += coupon.totalInvestment
                            totalUnits += coupon.totalIncrementalUnits
                        }
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
            else if componentType == .total {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        totalInvestment += coupon.totalInvestment
                        totalUnits += coupon.totalIncrementalUnits
                    }
                }
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            }
        }
        else if displayName == "Media & Measurement Investment" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            result += (coupon.totalDistribution * coupon.offer.printCost)
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            result += (coupon.totalDistribution * coupon.offer.printCost)
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            result += (coupon.totalDistribution * coupon.offer.printCost)
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .total {
                var result = 0.0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        result += (coupon.totalDistribution * coupon.offer.printCost)
                    }
                }
                return result.currencyString(0)
            }
        }
        else if displayName == "Redemption & Handling Investment" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            result += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            result += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            result += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .total {
                var result = 0.0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        result += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
                    }
                }
                return result.currencyString(0)
            }
        }
        else if displayName == "Total Investment" {
            if componentType == .trial {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Trial {
                        for coupon in offer.coupons {
                            result += coupon.totalInvestment
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .loyalty {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Loyalty {
                        for coupon in offer.coupons {
                            result += coupon.totalInvestment
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .volume {
                var result = 0.0
                for offer in scenario.offers {
                    if offer.type == .Volume {
                        for coupon in offer.coupons {
                            result += coupon.totalInvestment
                        }
                    }
                }
                return result.currencyString(0)
            }
            else if componentType == .total {
                var result = 0.0
                for offer in scenario.offers {
                    for coupon in offer.coupons {
                        result += coupon.totalInvestment
                    }
                }
                return result.currencyString(0)
            }
        }
        else if displayName == "Weighting By Strategy" {
            if componentType == .trial {
                return scenario.trialPercent.percentString(0)
            }
            else if componentType == .loyalty {
                return scenario.loyaltyPercent.percentString(0)
            }
            else if componentType == .volume {
                return scenario.volumePercent.percentString(0)
            }
            else if componentType == .total {
                return 1.percentString(0)
            }
        }
        
        return "--"
    }
}

class FinancialSummaryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var scenario: IPScenario?
    var backButton: UIButton!
    var expandButton: UIButton!
    
    var sortedMetrics: [[FinancialMetric]] = [[FinancialMetric]]()
    
    var selectedMetrics: [FinancialMetric]? {
        
        willSet {
            
        }
        
        didSet {
            
            if let metrics = selectedMetrics {
                
                sortedMetrics.removeAll()
                
                var selectedReturnMetrics = [FinancialMetric]()
                var selectedInvestMetrics = [FinancialMetric]()
                var selectedROIMetrics = [FinancialMetric]()
                
                for metric in metrics {
                    if metric.category == .return {
                        selectedReturnMetrics.append(metric)
                    } else if metric.category == .investment {
                        selectedInvestMetrics.append(metric)
                    } else if metric.category == .roi {
                        selectedROIMetrics.append(metric)
                    }
                }
                
                sortedMetrics.append([FinancialMetric]())
                sortedMetrics.append(selectedReturnMetrics)
                sortedMetrics.append(selectedInvestMetrics)
                sortedMetrics.append(selectedROIMetrics)
            }
            if let collView = collectionView {
                collView.reloadData()
            }
            
        }
    }
    var availableMetrics: [FinancialMetric] = [
        FinancialMetric(displayName: "Total Sales Dollars", category: .return),
        FinancialMetric(displayName: "% Incremental", category: .return),
        FinancialMetric(displayName: "Incremental Sales Dollars", category: .return),
        FinancialMetric(displayName: "Total Units", category: .return),
        FinancialMetric(displayName: "Incremental Units", category: .return),
        FinancialMetric(displayName: "Profit Margin %", category: .return),
        FinancialMetric(displayName: "Incremental Profit $", category: .return),
        
        FinancialMetric(displayName: "Total Investment", category: .investment),
        FinancialMetric(displayName: "Media & Measurement Investment", category: .investment),
        FinancialMetric(displayName: "Redemption & Handling Investment", category: .investment),
        FinancialMetric(displayName: "Weighting By Strategy", category: .investment),
        
        FinancialMetric(displayName: "Profit ROI", category: .roi),
        FinancialMetric(displayName: "Retail ROI", category: .roi),
        FinancialMetric(displayName: "CPUM", category: .roi),
        FinancialMetric(displayName: "CPIUM", category: .roi)]
    
    
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var header: FinancialHeaderView!
    
    var isExpanded: Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        backButton = UIButton()
        backButton.setImage(UIImage(named: "icon-back-dark-bg"), for: UIControlState())
        backButton.sizeToFit()
        backButton.addTarget(self, action: #selector(self.backButtonPressed), for: .touchUpInside)
        
        header = FinancialHeaderView()
        header.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 0)
        header.titleLabel.text = scenario!.program.report.brand + " Investment Summary" //late decision to remove titles
        header.leftButtons = [backButton]
        header.sizeToFit()
        self.view.addSubview(header)
        
        expandButton = UIButton()
        expandButton.setImage(UIImage(named: "icon-left-arrow"), for: UIControlState())
        expandButton.sizeToFit()
        expandButton.addTarget(self, action: #selector(self.expandButtonPressed), for: .touchUpInside)
        expandButton.frame = CGRect(x: self.view.frame.width - (expandButton.frame.width + 21), y: header.frame.height + 16, width: expandButton.frame.width, height: expandButton.frame.height)
        self.view.addSubview(expandButton)
        
        
        // Collection View
        
        layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.bounds.width, height: FinancialSummaryCell.rowHeight)
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0
        layout.sectionHeadersPinToVisibleBounds = true
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.headerReferenceSize = CGSize(width: 0, height:FinancialSummarySectionHeaderView.requiredHeight())
        
        collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.register(FinancialSummaryCell.self, forCellWithReuseIdentifier: "PropertyCell")
        collectionView.register(FinancialSummarySectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderView")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(collectionView)
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: header, attribute: .bottom, multiplier: 1, constant: 64))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        
        self.loadSelectedMetrics()
    }
    
    
    // MARK: - UICollectionViewDataSource Implementation
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return sortedMetrics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        else {
            return sortedMetrics[section].count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let iPath:NSIndexPath = indexPath as NSIndexPath
        let item = sortedMetrics[iPath.index(atPosition: 0)][iPath.index(atPosition: 1)]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertyCell", for: indexPath) as! FinancialSummaryCell
        cell.propertyNameLabel.text = item.displayName
        if let displayedScenario = self.scenario {
            cell.propertyValueLabel1.text = item.metricValueForObject(displayedScenario, .trial)
            cell.propertyValueLabel2.text = item.metricValueForObject(displayedScenario, .loyalty)
            cell.propertyValueLabel3.text = item.metricValueForObject(displayedScenario, .volume)
            cell.propertyValueLabel4.text = item.metricValueForObject(displayedScenario, .total)
        }
        cell.backgroundColor = UIColor.catalinaWhiteColor()
        
        if isExpanded {
            cell.setConstraintsForExpanded()
        } else {
            cell.setConstraintsForCollapsed()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let iPath:NSIndexPath = indexPath as NSIndexPath
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderView", for: indexPath) as! FinancialSummarySectionHeaderView
        if iPath.index(atPosition: 0) == 0 {
            let accessoryButton = UIButton()
            accessoryButton.setImage(UIImage(named: "icon-more-light-bg"), for: UIControlState())
            accessoryButton.addTarget(self, action: #selector(self.moreButtonPressed(_:)), for: .touchUpInside)
            accessoryButton.sizeToFit()
            accessoryButton.translatesAutoresizingMaskIntoConstraints = false
            header.accessoryButton = accessoryButton
        }
        header.setupForIndex(iPath.index(atPosition: 0))
        
        
        if isExpanded {
            header.setConstraintsForExpanded()
        } else {
            header.setConstraintsForCollapsed()
        }
        
        return header
    }
    
    // MARK: - Actions
    
    func moreButtonPressed(_ sender: UIButton) {
        
        let picker = FinancialMetricPicker()
        picker.prnt = self
        picker.modalPresentationStyle = .popover
        if let popover = picker.popoverPresentationController {
            popover.sourceView = sender
            popover.sourceRect = sender.bounds
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    func backButtonPressed() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func expandButtonPressed() {
        isExpanded = !isExpanded
        if isExpanded {
            self.expandButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        }
        else {
            self.expandButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
        collectionView.reloadData()
    }
    
    // MARK: - Helper Methods
    
    //MARK: - Internal Helpers
    
    fileprivate func loadSelectedMetrics() {
        
        if let metricNames = SettingsController.sharedSettingsController.financialScreenSelectedMetrics {
            var temp = [FinancialMetric]()
            for metricName in metricNames {
                for metric in availableMetrics {
                    if metric.displayName == metricName {
                        temp.append(metric)
                    }
                }
            }
            selectedMetrics = temp
        } else {
            selectedMetrics = [availableMetrics[0], availableMetrics[1], availableMetrics[2], availableMetrics[5], availableMetrics[6], availableMetrics[7], availableMetrics[10], availableMetrics[11], availableMetrics[12]]
        }
    }
    
    fileprivate func saveSelectedMetrics() {
        
        if let metrics = selectedMetrics {
            var temp = [String]()
            for metric in metrics {
                temp.append(metric.displayName)
            }
            SettingsController.sharedSettingsController.financialScreenSelectedMetrics = temp
        }
    }
}

class FinancialMetricPicker: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var prnt: FinancialSummaryViewController!
    var tableView = UITableView()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let frame = CGRect(x: 0, y: 0, width: 320, height: 600)
        self.preferredContentSize = CGSize(width: frame.size.width, height: frame.size.height)
        tableView.frame = frame
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return prnt.availableMetrics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let metric = prnt.availableMetrics[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = metric.displayName
        
        if prnt.selectedMetrics!.contains(where: { $0 === metric }){
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let metric =  prnt.availableMetrics[(indexPath as NSIndexPath).row]
        
        if prnt.selectedMetrics!.contains(where: { $0 === metric }){
            let index = prnt.selectedMetrics!.index(where: { $0 === metric })!
            prnt.selectedMetrics!.remove(at: index)
        } else {
            prnt.selectedMetrics!.append(metric)
        }
        prnt.saveSelectedMetrics()
        
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        
    }
}

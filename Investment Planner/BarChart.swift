//
//  BarChart.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/9/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPTextLayer: CATextLayer {
    
    var shouldAnimatePositionUpdates = true
    
    override func action(forKey event: String) -> CAAction? {
        
        if event == "position" {
            if shouldAnimatePositionUpdates {
                let animation = CABasicAnimation(keyPath: event)
                animation.duration = CATransaction.animationDuration()
                animation.timingFunction = CATransaction.animationTimingFunction()
                return animation
            } else {
                return nil
            }
        }
        return super.action(forKey: event)
    }
}

class IPShapeLayer: CAShapeLayer {
    
    var shouldAnimatePathUpdates = true
    
    override func action(forKey event: String) -> CAAction? {

        if shouldAnimatePathUpdates &&  event == "path" {
            let animation = CABasicAnimation(keyPath: event)
            animation.duration = CATransaction.animationDuration()
            animation.timingFunction = CATransaction.animationTimingFunction()
            return animation
        }
        return super.action(forKey: event)
    }
}

class BarChartBarLayer: CALayer {
    
    var value: Double = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    var offset: Double = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    var color: UIColor = UIColor.black
    var title: String?
    var text: String?
    
    var shouldAnimateUpdates = true {
        didSet {
            barLayer.shouldAnimatePathUpdates = shouldAnimateUpdates
            baselineLayer.shouldAnimatePathUpdates = shouldAnimateUpdates
            titleLayer.shouldAnimatePositionUpdates = shouldAnimateUpdates
            textLayer.shouldAnimatePositionUpdates = shouldAnimateUpdates
        }
    }
    
    override class func needsDisplay(forKey key: String) -> Bool {
        
        if key == "value" || key == "offset" || key == "color" || key == "title" || key == "text" {
            return true
        }
        return super.needsDisplay(forKey: key)
    }
    
    fileprivate(set) var parent: BarChart?
    fileprivate var barLayer = IPShapeLayer()
    fileprivate var baselineLayer = IPShapeLayer()
    fileprivate var titleLayer = IPTextLayer()
    fileprivate var textLayer = IPTextLayer()
    
    init(parent: BarChart) {
        super.init()
        textLayer.contentsScale = UIScreen.main.scale
        titleLayer.contentsScale = UIScreen.main.scale
        self.parent = parent
        self.addSublayer(barLayer)
        self.addSublayer(baselineLayer)
        self.addSublayer(titleLayer)
        self.addSublayer(textLayer)
        self.setNeedsDisplay()
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
        if let bar = layer as? BarChartBarLayer {
            self.parent = bar.parent
            self.value = bar.value
            self.color = bar.color
            self.title = bar.title
            self.text = bar.text
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func display() {
        
        if self.superlayer == nil {
            return
        }
        
        if self.bounds.width == 0 || self.bounds.height == 0 {
            return
        }
        
        if let parent = self.parent {
            
            //title
            if let title = title {
                let titleColor = parent.titleColor != nil ? parent.titleColor! : color
                let attributedTitle = NSAttributedString(string: title, attributes: [NSFontAttributeName : parent.titleFont, NSForegroundColorAttributeName : titleColor])
                titleLayer.string = attributedTitle
                titleLayer.isWrapped = true
                titleLayer.alignmentMode = kCAAlignmentCenter
            } else {
                titleLayer.string = nil
            }
            
            //baseline
            let baseLineRect = baselineLayer.bounds.insetBy(dx: parent.baselineWidth/2, dy: parent.baselineWidth/2)
            let baselinePath = UIBezierPath()
            baselinePath.move(to: CGPoint(x: baseLineRect.minX, y: baseLineRect.minY + 8))
            baselinePath.addLine(to: CGPoint(x: baseLineRect.minX, y: baseLineRect.minY))
            baselinePath.addLine(to: CGPoint(x: baseLineRect.maxX, y: baseLineRect.minY))
            baselinePath.addLine(to: CGPoint(x: baseLineRect.maxX, y: baseLineRect.minY + 8))
            baselineLayer.lineWidth = parent.baselineWidth
            let baselineColor = parent.baselineColor != nil ? parent.baselineColor! : color
            baselineLayer.strokeColor = baselineColor.cgColor
            baselineLayer.fillColor = UIColor.clear.cgColor
            baselineLayer.path = baselinePath.cgPath
            
            //bar
            let barRect = self.calculateBarRect()
            let barPath = UIBezierPath(rect: barRect)
            barLayer.fillColor = color.cgColor
            barLayer.path = barPath.cgPath
            
            //text
            if let text = text {
                let textColor = parent.textColor != nil ? parent.textColor! : color
                let attributedText = NSAttributedString(string: text, attributes: [NSFontAttributeName : parent.textFont, NSForegroundColorAttributeName : textColor])
                textLayer.string = attributedText
                textLayer.isWrapped = true
                textLayer.alignmentMode = kCAAlignmentCenter
            } else {
                textLayer.string = nil
            }
        }
    }
    
    override func layoutSublayers() {
        
        if let parent = self.parent {
            barLayer.frame = self.bounds
            let (titleRect, _) = self.bounds.divided(atDistance: parent.titleHeight, from: .maxYEdge)
            titleLayer.frame = titleRect
            let (baseRect, _) = self.bounds.divided(atDistance: parent.titleHeight + parent.titlePadding, from: .maxYEdge)
            baselineLayer.frame = baseRect
            let barRect = self.calculateBarRect()
            let textRect = CGRect(x: self.bounds.minX, y: barRect.minY - parent.textHeight - parent.textPadding, width: self.bounds.width, height: parent.textHeight)
            textLayer.frame = textRect
        }
    }
    
    func calculateBarRect() -> CGRect {
        
        if let parent = self.parent {
            
            let barAreaRect = CGRect(x: parent.barSpacing/2, y: parent.textHeight + parent.textPadding, width: self.bounds.maxX - parent.barSpacing, height: self.bounds.maxY - (parent.textHeight + parent.textPadding + parent.titleHeight + parent.titlePadding))
            let barHeight = (value > 0 && parent.maxBarValue > 0) ? barAreaRect.height * CGFloat(value/parent.maxBarValue) : 0
            var (barRect, _) = barAreaRect.divided(atDistance: barHeight, from: .maxYEdge)
            if offset > 0 {
                let offsetDistance = barAreaRect.height * CGFloat(offset/parent.maxBarValue)
                barRect.offsetInPlace(dx: 0, dy: -offsetDistance)
            }
            return barRect
        }
        return CGRect.zero
    }
}

class BarChart: UIView {
    
    //bars
    var bars = [BarChartBarLayer]() {
        didSet {
            for bar in bars {
                bar.shouldAnimateUpdates = self.shouldAnimateUpdates
            }
        }
    }
    var barSpacing: CGFloat = 8
    var maxBarValue: Double = 0
    var shouldUseHighestBarValueForMax = true
    
    //baseline
    var baselineColor: UIColor?
    var baselineWidth: CGFloat = 0
    
    //titles
    var titleFont = UIFont.systemFont(ofSize: 14)
    var titleColor: UIColor?
    var titlePadding: CGFloat = 8
    fileprivate var titleHeight: CGFloat = 0
    
    //text values
    var textFont = UIFont.boldSystemFont(ofSize: 14)
    var textColor: UIColor?
    var textPadding: CGFloat = 8
    fileprivate var textHeight: CGFloat = 0
    
    var shouldAnimateUpdates = true {
        didSet {
            for bar in bars {
                bar.shouldAnimateUpdates = shouldAnimateUpdates
            }
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.clear
    }
    
    override func layoutSublayers(of layer: CALayer) {
        if layer == self.layer {
            let barWidth: CGFloat = self.bounds.width/CGFloat(bars.count)
            var barPosition: CGFloat = 0
            for bar in bars {
                bar.frame = CGRect(x: barPosition, y: 0, width: barWidth, height: self.bounds.height)
                barPosition += barWidth
            }
        }
    }
    
    func update() {
        
        if shouldUseHighestBarValueForMax {
            var maxValue: Double = 0
            for bar in bars {
                if bar.value > maxValue {
                    maxValue = bar.value
                }
            }
            self.maxBarValue = maxValue
        }
        
        self.titleHeight = self.calculateTitleHeight()
        self.textHeight = self.calculateTextHeight()
        
        for bar in bars {
            bar.setNeedsDisplay()
            bar.setNeedsLayout()
        }
        
        self.setNeedsLayout()
    }
    
    func addBarWithValue(_ value: Double, color: UIColor?, title: String?, text: String?) -> BarChartBarLayer {
        
        let bar = BarChartBarLayer(parent: self)
        bar.value = value
        bar.color = color == nil ? UIColor.black : color!
        bar.title = title
        bar.text = text
        
        self.layer.addSublayer(bar)
        self.bars.append(bar)
        self.update()
        
        return bar
    }
    
    func removeBar(_ bar: BarChartBarLayer) {
        
        if let index = bars.index(of: bar) {
            bars.remove(at: index)
            bar.removeFromSuperlayer()
            self.update()
        }
    }
    
    func barAtIndex(_ index: Int) -> BarChartBarLayer? {
        
        if self.bars.count >= index + 1 {
            return self.bars[index]
        }
        return nil
    }
    
    func barWithTitle(_ title: String) -> BarChartBarLayer? {
        
        for bar in bars {
            if title == bar.title {
                return bar
            }
        }
        return nil
    }
    
    fileprivate func calculateTextHeight() -> CGFloat {
        
        var maxHeight: CGFloat = 0
        for bar in bars {
            if let text = bar.text {
                let height = self.calculateStringHeight(text, withFont: titleFont, andMaxWidth: (self.bounds.width/CGFloat(bars.count)) - 10)
                if height > maxHeight {
                    maxHeight = height
                }
            }
        }
        return maxHeight
    }
    
    fileprivate func calculateTitleHeight() -> CGFloat {
        
        var maxHeight: CGFloat = 0
        for bar in bars {
            if let title = bar.title {
                let height = self.calculateStringHeight(title, withFont: titleFont, andMaxWidth: (self.bounds.width/CGFloat(bars.count)) - 10)
                if height > maxHeight {
                    maxHeight = height
                }
            }
        }
        return maxHeight
    }
    
    fileprivate func calculateStringHeight(_ string: String, withFont font: UIFont, andMaxWidth width: CGFloat) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = string.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
}

//extension to simplify creating and working with TLV graphs
extension BarChart {
    
    class func tlvGraphWithTrial(_ trial: Double, loyalty: Double, volume:Double) -> BarChart {
        
        let barGraph = BarChart()
        _ = barGraph.addBarWithValue(trial, color: UIColor.catalinaDarkBlueColor(), title: "TRIAL", text: nil)
        _ = barGraph.addBarWithValue(loyalty, color: UIColor.catalinaLightBlueColor(), title: "LOYALTY", text: nil)
        _ = barGraph.addBarWithValue(volume, color: UIColor.catalinaLightGreenColor(), title: "VOLUME", text: nil)
        return barGraph
    }
    
    var trialBar: BarChartBarLayer? {
        
        get {
            return self.barAtIndex(0)
        }
    }
    
    var loyaltyBar: BarChartBarLayer? {
        
        get {
            return self.barAtIndex(1)
        }
    }
    
    var volumeBar: BarChartBarLayer? {
        
        get {
            return self.barAtIndex(2)
        }
    }
}

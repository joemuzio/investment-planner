//
//  ReportListViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/26/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ReportListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var reports: [SearchToken] = [SearchToken]()
    
    var headerView: UIView!
    var topBarView: UIView!
    var cancelButton: UIButton!
    var titleLabel: UILabel!
    var tabView: TabView!
    var searchBar: UISearchBar!
    var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
    
    lazy var reportTypesDictionary: [String:[IPReportType]] = {
        
        var typesDictionary = [String:[IPReportType]]()
        typesDictionary["MY REPORTS"] = IPReportType.listManagerTypes
        typesDictionary["CMS BRANDS"] = IPReportType.brandCategoryTypes
//        for type in IPReportType.searchableTypes.reversed() {
//            typesDictionary[type.rawValue.uppercased()] = [type]
//        }
        return typesDictionary
    }()
    
    var keyboardWillShowObserver: NSObjectProtocol?
    var keyboardWillHideObserver: NSObjectProtocol?
    var reportPersistenceObserver: NSObjectProtocol?
    var dynamicTableViewBottomConstraint: NSLayoutConstraint!
    
    var listUpdateTimer: Timer!
    var needsListUpdate: Bool = false
    
    // MARK: - UIViewController Overrides

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        
        topBarView = UIView()
        topBarView.backgroundColor = UIColor.catalinaDarkBlueColor()
        topBarView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(topBarView)
        self.view.addConstraint(NSLayoutConstraint(item: topBarView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topBarView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topBarView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: topBarView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 72))
        
        cancelButton = UIButton()
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.setTitleColor(UIColor.catalinaWhiteColor(), for: UIControlState())
        cancelButton.setTitleColor(UIColor.lightGray, for: .disabled)
        cancelButton.sizeToFit()
        cancelButton.addTarget(self, action: #selector(ReportListViewController.cancelButtonPressed), for: .touchUpInside)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        topBarView.addSubview(cancelButton)
        topBarView.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .left, relatedBy: .equal, toItem: topBarView, attribute: .left, multiplier: 1, constant: 20))
        topBarView.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .top, relatedBy: .equal, toItem: topBarView, attribute: .top, multiplier: 1, constant: 20))
        
        searchBar = UISearchBar()
        searchBar.placeholder = "Find a Report"
        searchBar.barTintColor = UIColor.catalinaDarkBlueColor()
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.catalinaDarkBlueColor().cgColor
        searchBar.delegate = self
        searchBar.autocapitalizationType = UITextAutocapitalizationType.none
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        topBarView.addSubview(searchBar)
        topBarView.addConstraint(NSLayoutConstraint(item: searchBar, attribute: .centerY, relatedBy: .equal, toItem: cancelButton, attribute: .centerY, multiplier: 1, constant: 0))
        topBarView.addConstraint(NSLayoutConstraint(item: searchBar, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        topBarView.addConstraint(NSLayoutConstraint(item: searchBar, attribute: .right, relatedBy: .equal, toItem: topBarView, attribute: .right, multiplier: 1, constant: -10))
        
        headerView = UIView()
        headerView.backgroundColor = UIColor.catalinaWhiteColor()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(headerView)
        self.view.addConstraint(NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: topBarView, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: headerView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: headerView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: headerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 96))
        
        titleLabel = UILabel()
        titleLabel.text = "Available Reports"
        titleLabel.font = UIFont.catalinaMediumFontOfSize(28)
        titleLabel.textColor = UIColor.catalinaLightBlueColor()
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(titleLabel)
        headerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1, constant: 20))
        headerView.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: headerView, attribute: .centerX, multiplier: 1, constant: 0))
        
        tabView = TabView()
        tabView.tintColor = UIColor.catalinaDarkBlueColor()
        tabView.tabs = [String](reportTypesDictionary.keys).sorted().reversed()
        tabView.addTarget(self, action: #selector(ReportListViewController.selectedTabChanged), for: .valueChanged)
        tabView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(tabView)
        headerView.addConstraint(NSLayoutConstraint(item: tabView, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 10))
        headerView.addConstraint(NSLayoutConstraint(item: tabView, attribute: .centerX, relatedBy: .equal, toItem: headerView, attribute: .centerX, multiplier: 1, constant: 0))
        
        tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 50
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: 20))
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 200))
        self.view.addConstraint(NSLayoutConstraint(item: tableView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: -200))
        dynamicTableViewBottomConstraint = NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        self.view.addConstraint(dynamicTableViewBottomConstraint)
        
        self.updateReportsList()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ReportListViewController.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.dynamicTableViewBottomConstraint.constant = 0
    }
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        keyboardWillShowObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            if let userInfo = (notification as NSNotification).userInfo {
                let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue
                self.dynamicTableViewBottomConstraint.constant = -(self.view.bounds.height - (keyboardEndFrame?.origin.y)!)
                UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        keyboardWillHideObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            self.dynamicTableViewBottomConstraint.constant = 0
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
        
        reportPersistenceObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ReportDataPersistenceNotification), object: nil, queue: OperationQueue.main, using: { (notification) -> Void in
            
            let info = (notification as NSNotification).userInfo!
            let records: [ReportDataPersistenceRecord] = info["records"] as! [ReportDataPersistenceRecord]
            for record in records {
                if record.action != .Delete { //we do not need to update for deletes because they are driven by this view and the update has already happened
                    self.needsListUpdate = true
                    break;
                }
            }
        })
        
        listUpdateTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ReportListViewController.listUpdateTimerFired), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        listUpdateTimer.invalidate()
        listUpdateTimer = nil
        
        super.viewWillDisappear(animated)
        if let observer = keyboardWillShowObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        if let observer = keyboardWillHideObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        if let observer = reportPersistenceObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    // MARK: - Actions
    
    func cancelButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func selectedTabChanged() {
        if let selectedType = tabView.selectedTab{
        AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Filter Reports By Type", action: "\(selectedType)", label: "", value: 0)
        }
        self.updateReportsList()
    }
    
    func listUpdateTimerFired() {
        
        if needsListUpdate {
            needsListUpdate = false
            self.updateReportsList()
        }
    }
    
    func refresh(_ refreshControl: UIRefreshControl) {
        
        if !WebservicesController.sharedWebservicesController.isCheckingForNewReports {
            WebservicesController.sharedWebservicesController.checkForNewReports({ (error) in
                if let error = error {
                    let alert = UIAlertController(title: "Reports Service Error", message: "An error occurred while attempting to check for new Report data from the Reports Service.\n\nError Details:\n\(error.localizedDescription)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                refreshControl.endRefreshing()
            })
        } else {
            refreshControl.endRefreshing()
        }
    }
    
    //MARK: - UITableViewDataSource Implementation
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //dequeue or create cell
        var dequeued = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if dequeued == nil {
            dequeued = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
            dequeued?.textLabel?.textColor = UIColor.catalinaDarkBlueColor()
            dequeued?.detailTextLabel?.textColor = UIColor.gray
        }
        let cell = dequeued!
        
        //get data
        let token = reports[(indexPath as NSIndexPath).item]
        
        //update cell
        if let catDescription = token.category{
            cell.textLabel?.text = "\(token.name!) \(catDescription)"
        }
        else{
            cell.textLabel?.text = token.name
        }
        if let tokenB = token.brand{
            cell.detailTextLabel?.text = "\(tokenB) - \(dateFormatter.string(from: token.lastModified! as Date))"
        }
        
        if token.type == IPReportType.Custom.rawValue {
            cell.imageView?.image = UIImage(named: "icon-report-search-custom")
        } else if token.type == IPReportType.Shared.rawValue {
            cell.imageView?.image = UIImage(named: "icon-report-search-shared")
        } else {
            cell.imageView?.image = UIImage(named: "icon-report-search-standard")
        }
        cell.accessoryView = nil
        if let downloading = token.downloading {
            if downloading.boolValue {
                let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                activityIndicator.startAnimating()
                cell.accessoryView = activityIndicator
            }
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate Implementation
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let token = reports[(indexPath as NSIndexPath).row]
        if let type = token.type{
            AnalyticsController.sharedAnalyticsController.sendEventToAnalytics("Report Selected", action: type, label: "", value: 0)
        }
        
        //check to see if we have a local copy
        if let report = DataController.sharedDataController.openReportForToken(token) {
            //check to see if the last modified date of from the metadata index is more recent than the version we have locally and, if it is, download the latest version from the server
            if token.lastModified?.compare(report.lastModifiedDate) == .orderedDescending {
                self.downloadAndShowReportForToken(token)
            } else {
                self.showReport(report)
            }
        } else {
            //we do not have a local copy at all so download it now
            self.downloadAndShowReportForToken(token)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let token = reports[(indexPath as NSIndexPath).row]
        if token.type == IPReportType.Standard.rawValue {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let token = reports[(indexPath as NSIndexPath).row]
            if let report = DataController.sharedDataController.openReportForToken(token) {
                let success = DataController.sharedDataController.deleteReport(report)
                if success {
                    self.tableView.beginUpdates()
                    self.reports.remove(at: (indexPath as NSIndexPath).row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    self.tableView.endUpdates()
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if searchBar.isFirstResponder {
            self.view.endEditing(true)
        }
    }
    
    // MARK: - UISearchBarDelegate Implementation
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.updateReportsList()
    }
    
    //MARK: - Internal Helper Methods
    
    func updateReportsList() {
        
        if let selectedTab = tabView.selectedTab {
            let types = reportTypesDictionary[selectedTab]!
            var reportsArray = DataController.sharedDataController.searchReports(searchBar.text, types: types)
            reportsArray.sort(by: { (token1, token2) -> Bool in
                return token1.lastModified!.compare(token2.lastModified!) == .orderedDescending
            })
            // removing duplicates from the reports array because of an issue with NSManagedContext not updating properly when a new report is downloaded
            reports.removeAll()
            for report in reportsArray{
                var doesContain = false
                let name = report.name
                let type = report.type
                for r in reports{
                    if name == r.name && type == r.type{
                        doesContain = true
                    }
                }
                if(doesContain == false){
                    reports.append(report)
                }
            }
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }
    
    func downloadAndShowReportForToken(_ token: SearchToken) {
        
        //get variables we need for the download from the token
        let id = token.id!
        let stringType = token.type!
        let type = IPReportType(rawValue: stringType)!
        
        //set the token's transient "downloading" property to true and reload the table so the cell will display an activity indicator while it is downloading
        token.downloading = true
        tableView.reloadData()
        
        //download the report
        WebservicesController.sharedWebservicesController.downloadReportWithID(id, type: type, completion: { (error) in
            
            //hide the activity indicator
            token.downloading = false
            self.tableView.reloadData()
            
            if error != nil { //if an error occurred during the download alert the user
                let alert = UIAlertController(title: "Download Failed", message: "Unable to download the selected report. Please try again later.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else { //otherwise, open the report file and show the report
                if let report = DataController.sharedDataController.openReportForToken(token) {
                    self.showReport(report)
                }
            }
        })
    }
    
    func showReport(_ report: IPReport) {
        
        //when we get ready to show a report we need to make sure it has a program
        if report.program == nil {
            report.program = report.createProgram()
        }
        
        //we also need to check the type, if this is a standard report we need to change the type to custom and give it a new id so it will be saved as a copy of the original if the user changes it in any way
        if report.displayType == .Standard {
            report.displayType = .Custom
            report.reportType = "LIST_MANAGER"
            report.owner = WebservicesController.sharedWebservicesController.userName!
            report.fileExtension = "IMPORT"
            report.fileName = "IMPORT"
            report.filePath = "IMPORT"
            report.productList = "IMPORT"
            report.reportDate = "IMPORT"
            report.reportNumber = "IMPORT"
            report.id = UUID().uuidString
        }
        
        //set the report as the selecet report for the application and dismiss this view, revealing the Insights screen which is now showing this report
        SettingsController.sharedSettingsController.selectedReport = report
        self.dismiss(animated: true, completion: nil)
    }
}

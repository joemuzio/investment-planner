//
//  SettingsController.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/29/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

private let sharedSettingsControllerInstance = SettingsController()
private let selectedReportKey = "pythagoras.app.selectedReport"
private let compareScreenSelectedMetricsKey = "pythagoras.app.compareScreenSelectedMetrics"
private let financialScreenSelectedMetricsKey = "pythagoras.app.financialScreenSelectedMetrics"
private let onboardingScreenShownKey = "pythagoras.app.onboardingScreenShownKey"
private let vitoStatementSelectedMetricsKey = "pythagoras.app.vitoStatementSelectedMetrics"
private let userMathConstantsKey = "pythagoras.app.userMathConstants"

class SettingsController: NSObject {
    
    static let selectedReportChangedNotification = "pythagoras.app.selectedReportChanged"
    
    class var sharedSettingsController: SettingsController {
        
        return sharedSettingsControllerInstance
    }
    
    var selectedReport: IPReport? = {
        if let reportKey = UserDefaults.standard.dictionary(forKey: selectedReportKey) {
            let reportID = reportKey["id"] as! String
            let reportType = IPReportType(rawValue: reportKey["type"] as! String)!
            if let report = DataController.sharedDataController.openReportWithID(reportID, andType: reportType) {
                return report
            } else {
                UserDefaults.standard.setValue(nil, forKey: selectedReportKey)
            }
        }
        return nil
        }() {
        didSet {
            if let report = selectedReport {
                let reportKey: [String: AnyObject] = ["id" : report.id as AnyObject, "type" : report.displayType.rawValue as AnyObject]
                UserDefaults.standard.setValue(reportKey, forKey: selectedReportKey)
            } else {
                UserDefaults.standard.setValue(nil, forKey: selectedReportKey)
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: SettingsController.selectedReportChangedNotification), object: self, userInfo: nil)
        }
    }
    
    var compareScreenSelectedMetrics: [String]? {
        get {
            return UserDefaults.standard.stringArray(forKey: compareScreenSelectedMetricsKey)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: compareScreenSelectedMetricsKey)
        }
    }
    
    var financialScreenSelectedMetrics: [String]? {
        get {
            return UserDefaults.standard.stringArray(forKey: financialScreenSelectedMetricsKey)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: financialScreenSelectedMetricsKey)
        }
    }
    
    var onboardingScreenShown: Bool {
        get {
            return UserDefaults.standard.bool(forKey: onboardingScreenShownKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: onboardingScreenShownKey)
        }
    }
    
    var vitoStatementSelectedMetrics: [String: AnyObject]? {
        get {
            return UserDefaults.standard.dictionary(forKey: vitoStatementSelectedMetricsKey) as [String : AnyObject]?
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: vitoStatementSelectedMetricsKey)
        }
    }
    
    var userMathConstants: [String: AnyObject]? {
        get {
            return UserDefaults.standard.dictionary(forKey: userMathConstantsKey) as [String : AnyObject]?
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: userMathConstantsKey)
        }
    }
    
}

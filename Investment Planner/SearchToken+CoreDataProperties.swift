//
//  SearchToken+CoreDataProperties.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 6/22/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SearchToken {

    @NSManaged var id: String?
    @NSManaged var name: String?
    @NSManaged var brand: String?
    @NSManaged var lastModified: Date?
    @NSManaged var type: String?
    @NSManaged var user: String?
    @NSManaged var category: String?
    @NSManaged var downloading: NSNumber?

}

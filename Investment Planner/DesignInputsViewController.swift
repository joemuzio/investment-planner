//
//  DesignInputsViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class DesignInputsColumnGroup {
    
    var headerText = ""
    var columns = [DesignInputsColumn]()
    
    init(headerText: String) {
        self.headerText = headerText
    }
}

class DesignInputsColumn {
    
    var headerText = ""
    var referenceObject: AnyObject?
    
    init(headerText: String, referenceObject: AnyObject?) {
        self.headerText = headerText
        self.referenceObject = referenceObject
    }
}

class DesignInputsRow {
    
    var headerText = ""
    var cellValueFormatter: ((_ column: DesignInputsColumn) -> String)?
    
    init(headerText: String, cellValueFormatter: ((_ column: DesignInputsColumn) -> String)?) {
        self.headerText = headerText
        self.cellValueFormatter = cellValueFormatter
    }
    
    func cellValueForColumn(_ column: DesignInputsColumn) -> String {
        
        if let formatter = cellValueFormatter {
            return formatter(column)
        }
        return "--"
    }
}

class DesignInputsViewController: UIViewController, UICollectionViewDataSource, DesignInputsCollectionViewLayoutDelegate {
    
    var doneButton: UIButton!
    var titleLabel: UILabel!
//    var moreButton: UIButton!
    var layout: DesignInputsCollectionViewLayout!
    var collectionView: UICollectionView!
    
    var scenario: IPScenario!
    
    var rows = [DesignInputsRow]()
    var columnGroups = [DesignInputsColumnGroup]()

    var primaryRowColor = UIColor.catalinaWhiteColor()
    var alternateRowColor = UIColor(hexColor: 0xDBF5FD)
    
    init(scenario: IPScenario) {
        
        super.init(nibName: nil, bundle: nil)
        self.scenario = scenario
        self.initializeRowsAndColumns()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        doneButton = UIButton()
        doneButton.setTitleColor(UIColor.catalinaDarkBlueColor(), for: UIControlState())
        doneButton.setTitle("Done", for: UIControlState())
        doneButton.addTarget(self, action: #selector(DesignInputsViewController.doneButtonPressed), for: .touchUpInside)
        doneButton.frame = CGRect(x: self.view.bounds.width - 60, y: 15, width: 60, height: 24)
        self.view.addSubview(doneButton)
        
//        moreButton = UIButton()
//        moreButton.setImage(UIImage(named: "icon-more-dark-bg"), forState: .Normal)
//        moreButton.addTarget(self, action: "moreButtonPressed", forControlEvents: .TouchUpInside)
//        moreButton.sizeToFit()
        
        titleLabel = UILabel()
        titleLabel.text = "Program Data for \(scenario.program.focusBrand)"
        titleLabel.textColor = UIColor.catalinaDarkBlueColor()
        titleLabel.font = UIFont.catalinaLightFontOfSize(36)
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(titleLabel)
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 60))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 80))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
        
        layout = DesignInputsCollectionViewLayout(delegate: self)
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.register(DesignInputsCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(collectionView)
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: collectionView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0))
    }
    
    //MARK: - Actions
    
    func doneButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
   
    // MARK: - UICollectionViewDataSource Implementation
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.rows.count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return self.columnGroups.count + 1
        } else {
            return self.getTotalColumnCount() + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! DesignInputsCollectionViewCell
        
        if (indexPath as NSIndexPath).section == 0 { //column group headers
            
            cell.backgroundColor = UIColor.catalinaDarkBlueColor()
            cell.contentView.layer.borderColor = UIColor.catalinaWhiteColor().cgColor
            cell.textLabel.textColor = UIColor.catalinaWhiteColor()
            cell.textLabel.font = UIFont.catalinaBoldFontOfSize(18)
            cell.textLabel.textAlignment = .center
            if (indexPath as NSIndexPath).item == 0 { //top-left corner piece, no text
                cell.textLabel.text = ""
            } else {
                let group = self.columnGroups[(indexPath as NSIndexPath).item - 1]
                cell.textLabel.text = group.headerText
            }
            
        } else if (indexPath as NSIndexPath).section == 1 { //column headers
            
            cell.backgroundColor = primaryRowColor
            cell.contentView.layer.borderColor = UIColor.catalinaDarkBlueColor().cgColor
            cell.textLabel.textColor = UIColor.catalinaLightGreenColor()
            cell.textLabel.font = UIFont.catalinaBoldFontOfSize(18)
            cell.textLabel.textAlignment = .center
            if (indexPath as NSIndexPath).item == 0 { //top-left corner piece, no text
                cell.textLabel.text = ""
            } else {
                if let column = self.getColumnAtIndex((indexPath as NSIndexPath).item - 1) {
                    cell.textLabel.text = column.headerText
                } else {
                    cell.textLabel.text = "" //this should never happen
                }
            }
            
        } else { //all other rows
            
            if (indexPath as NSIndexPath).section % 2 == 0 {
                cell.backgroundColor = alternateRowColor
            } else {
                cell.backgroundColor = primaryRowColor
            }
            cell.contentView.layer.borderColor = UIColor.catalinaDarkBlueColor().cgColor
            cell.textLabel.textColor = UIColor.catalinaDarkBlueColor()
            cell.textLabel.font = UIFont.catalinaBoldFontOfSize(18)
            
            let row = self.rows[(indexPath as NSIndexPath).section - 2]
            if (indexPath as NSIndexPath).item == 0 { //row headers
                cell.textLabel.text = row.headerText
                cell.textLabel.textAlignment = .left
            } else { //other cells
                if let column = self.getColumnAtIndex((indexPath as NSIndexPath).item - 1) {
                    cell.textLabel.text = row.cellValueForColumn(column)
                } else {
                    cell.textLabel.text = "" //this should never happen
                }
                cell.textLabel.textAlignment = .center
            }
            
        }
        
        return cell
    }
    
    //MARK: - DesignInputsCollectionViewLayoutDelegate Implementation
    
    func layout(_ layout: DesignInputsCollectionViewLayout, sizeOfItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if (indexPath as NSIndexPath).section == 0 { //column group headers
            if (indexPath as NSIndexPath).item == 0 {
                return CGSize(width: 240, height: 40)
            } else {
                let columnGroup = self.columnGroups[(indexPath as NSIndexPath).item - 1]
                return CGSize(width: 180 * columnGroup.columns.count, height: 40)
            }
        } else if (indexPath as NSIndexPath).section == 1 { //column headers
            if (indexPath as NSIndexPath).item == 0 {
                return CGSize(width: 240, height: 80)
            } else {
                return CGSize(width: 180, height: 80)
            }
        } else { //other rows
            if (indexPath as NSIndexPath).item == 0 {
                return CGSize(width: 240, height: 40)
            } else {
                return CGSize(width: 180, height: 40)
            }
        }
    }
    
    //MARK: - Helper Methods
    
    func getTotalColumnCount() -> Int {
        
        var count = 0
        for group in columnGroups {
            count += group.columns.count
        }
        return count
    }
    
    func getColumnAtIndex(_ index: Int) -> DesignInputsColumn? {
        
        var count = 0
        for group in columnGroups {
            for column in group.columns {
                if index == count {
                    return column
                }
                count += 1
            }
        }
        return nil
    }
    
    func initializeRowsAndColumns() {
        
        //MARK: --------------------------------------------  COLUMNS  -------------------------------------------------------
    
        //trial offers
        let trialOfferGroup = DesignInputsColumnGroup(headerText: "TRIAL")
        var trialCoupons = [IPCoupon]()
        for offer in scenario.offers {
            if offer.type.rawValue.uppercased() == trialOfferGroup.headerText {
                for coupon in offer.coupons {
                    let column = DesignInputsColumn(headerText: offer.name.rawValue, referenceObject: coupon)
                    trialOfferGroup.columns.append(column)
                    trialCoupons.append(coupon)
                }
            }
        }
        let trialTotalsColumn = DesignInputsColumn(headerText: "Total Trial", referenceObject: trialCoupons as AnyObject?)
        trialOfferGroup.columns.append(trialTotalsColumn)
        self.columnGroups.append(trialOfferGroup)
        
        //loyalty offers
        let loyaltyOfferGroup = DesignInputsColumnGroup(headerText: "LOYALTY")
        var loyaltyCoupons = [IPCoupon]()
        for offer in scenario.offers {
            if offer.type.rawValue.uppercased() == loyaltyOfferGroup.headerText {
                let highestTrigger = offer.highestTriggerQuantity
                for coupon in offer.coupons {
                    var name = "\(offer.name.rawValue.capitalized)\nBuy \(coupon.triggerQuantity.numberString())"
                    if coupon.triggerQuantity == highestTrigger {
                        name += "+"
                    }
                    let column = DesignInputsColumn(headerText: name, referenceObject: coupon)
                    loyaltyOfferGroup.columns.append(column)
                    loyaltyCoupons.append(coupon)
                }
            }
        }
        let loyaltyTotalsColumn = DesignInputsColumn(headerText: "Total Loyalty", referenceObject: loyaltyCoupons as AnyObject?)
        loyaltyOfferGroup.columns.append(loyaltyTotalsColumn)
        self.columnGroups.append(loyaltyOfferGroup)
        
        //volume offers
        let volumeOfferGroup = DesignInputsColumnGroup(headerText: "VOLUME")
        let announcementColumn = DesignInputsColumn(headerText: "CCM Announcement", referenceObject: "Announcement" as AnyObject?)
        volumeOfferGroup.columns.append(announcementColumn)

        var volumeCoupons = [IPCoupon]()
        for offer in scenario.offers {
            if offer.type.rawValue.uppercased() == volumeOfferGroup.headerText {
                let highestTrigger = offer.highestTriggerQuantity
                for coupon in offer.coupons {
                    var name = "\(offer.name.rawValue.uppercased())\n Buy \(coupon.triggerQuantity.numberString())"
                    if coupon.triggerQuantity == highestTrigger {
                        name += "+"
                    }
                    let column = DesignInputsColumn(headerText: name, referenceObject: coupon)
                    volumeOfferGroup.columns.append(column)
                    volumeCoupons.append(coupon)
                }
            }
        }
        let volumeTotalsColumn = DesignInputsColumn(headerText: "Total Volume", referenceObject: volumeCoupons as AnyObject?)
        volumeOfferGroup.columns.append(volumeTotalsColumn)
        self.columnGroups.append(volumeOfferGroup)

        //create column for overall totals and add it to its own group
        let totalsColumn = DesignInputsColumn(headerText: "Annual Plan Total", referenceObject: scenario.getOrderedCoupons() as AnyObject?)
        let totalsColumnGroup = DesignInputsColumnGroup(headerText: "")
        totalsColumnGroup.columns.append(totalsColumn)
        self.columnGroups.append(totalsColumnGroup)
        
        //MARK: ----------------------------------------------------------------------------------------------------------------
        
        //MARK: -----------------------------------------------  ROWS  ---------------------------------------------------------
        
        self.rows.append(DesignInputsRow(headerText: "#Cycles", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.numberOfCycles.numberWithCommaString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var multipleTypesFound = false
                var offerType:IPOfferType?
                var loyaltyCycleTotal = 0
                var volumeCycleTotal = 0
                var trialCycleTotal = 0
                var biggestCycleTotal = 0
                for coupon in coupons{
                    let total = coupon.numberOfCycles
                    if(total > biggestCycleTotal){
                        biggestCycleTotal = total
                    }
                    if(offerType == nil){
                        offerType = coupon.offer.type
                    }
                    else{
                        if(offerType != coupon.offer.type){
                            multipleTypesFound = true
                        }
                    }
                    if(coupon.offer.type == .Trial){
                        if(total > trialCycleTotal){
                            trialCycleTotal = total
                        }
                    }
                    else if(coupon.offer.type == .Loyalty){
                        if(total > loyaltyCycleTotal){
                            loyaltyCycleTotal = total
                        }
                    }
                    else if(coupon.offer.type == .Volume){
                        if(total > volumeCycleTotal){
                            volumeCycleTotal = total
                        }
                    }
                }
                if(multipleTypesFound == false){
                    return biggestCycleTotal.numberWithCommaString()
                }
                else{
                    var annualCycleTotal = 0
                    let volumePlusCCMCycleTotal = loyaltyCycleTotal + volumeCycleTotal
                    if(volumePlusCCMCycleTotal > trialCycleTotal){
                        if(volumePlusCCMCycleTotal > 13){
                            annualCycleTotal = 13
                        }
                        else{
                            annualCycleTotal = volumePlusCCMCycleTotal
                        }
                    }
                    else{
                        annualCycleTotal = trialCycleTotal
                    }
                    return annualCycleTotal.numberWithCommaString()
                }

            } else {
                return "--"
            }

        }))
        self.rows.append(DesignInputsRow(headerText: "Estimated Distribution", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalDistribution.numberWithCommaString()
            } else if let _ = column.referenceObject as? String {
                let total: Double = self.scenario.getTotalDistributionForType(IPOfferType.Volume) * 4
                return total.numberWithCommaString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                for coupon in coupons {
                    total += coupon.totalDistribution
                    if coupon.offer.type == .Volume {
                        total += coupon.totalDistribution * 4
                    }
                }
                return total.numberWithCommaString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Estimated Redemption", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalRedemption.numberWithCommaString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalRedemption
                }
                return total.numberWithCommaString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Total Units", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalUnits.numberWithCommaString()
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                for coupon in coupons {
                    total += coupon.totalUnits
                }
                return total.numberWithCommaString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Total Dollars", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return coupon.totalDollars.currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalDollars
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Incremental Units", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    return coupon.totalIncrementalUnits.numberWithCommaString()
                }
                else {
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                for coupon in coupons {
                    total += coupon.totalIncrementalUnits
                }
                return total.numberWithCommaString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Incremental Dollars", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
             //       print(coupon.totalIncrementalDollars.currencyString(0))
                    return coupon.totalIncrementalDollars.currencyString(0)
                }
                else {
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalIncrementalDollars
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Profit Margin", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if(coupon.totalDistribution == 0){
                    return 0.percentString()
                }
                else{
                    return coupon.offer.scenario.program.profitMargin.percentString()
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                for coupon in coupons {
                    if(coupon.totalDistribution != 0){
                        return coupon.offer.scenario.program.profitMargin.percentString()
                    }
                }
                return 0.percentString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Retail ROI", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else {
                        return coupon.retailRoi.currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalIncrementalDollars += coupon.totalIncrementalDollars
                }
                
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Profit ROI", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else {
                        return coupon.profitRoi.currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalIncrementalDollars: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalIncrementalDollars += coupon.totalIncrementalDollars * coupon.offer.scenario.program.profitMargin
                }
                
                return (totalInvestment > 0 ? totalIncrementalDollars / totalInvestment : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Acquired Buyers", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    return coupon.totalAcquiredBuyers.numberWithCommaString()
                }
                else {
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total: Double = 0
                var type: IPOfferType = .Volume
                for coupon in coupons {
                    total += coupon.totalAcquiredBuyers
                    type = coupon.offer.type
                }
                if(total > 0 || type == .Trial){
                    return total.numberWithCommaString()
                }
                else{
                     return "--"
                }
//                if(type == .Trial){
//                    return total.numberWithCommaString()
//                }
//                else{
//                    return "--"
//                }
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Cost per Acquisition", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if(coupon.offer.type == .Trial){
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else{
                        return (coupon.totalInvestment / coupon.totalAcquiredBuyers).currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var trialTypeFound = false
                var totalAcquired: Double = 0
                for coupon in coupons {
                    if(coupon.offer.type == .Trial){
                        totalInvestment += coupon.totalInvestment
                        totalAcquired += coupon.totalAcquiredBuyers
                        trialTypeFound = true
                    }
                }
                
                
                if trialTypeFound == true {
                    return (totalAcquired > 0 ? totalInvestment / totalAcquired : 0).currencyString()
                }
                else {
                    return "--"
                }
                

            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "CPUM", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if(coupon.totalDistribution == 0){
                    return 0.currencyString()
                }
                else {
                    if(coupon.offer.type == .Volume || coupon.offer.type == .Loyalty){
                        return "--"
                    }
                    return coupon.cpum.currencyString()
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalUnits += coupon.totalUnits
                }
                
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "CPIUM", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Trial {
                    if(coupon.totalDistribution == 0){
                        return 0.currencyString()
                    }
                    else {
                        return coupon.cpium.currencyString()
                    }
                }
                else{
                    return "--"
                }
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var totalInvestment: Double = 0
                var totalUnits: Double = 0
                for coupon in coupons {
                    totalInvestment += coupon.totalInvestment
                    totalUnits += coupon.totalIncrementalUnits
                }
                
                return (totalUnits > 0 ? totalInvestment / totalUnits : 0).currencyString()
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Total Investment", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Volume {
                    let totalInvestment = coupon.totalDistribution * (self.scenario.program.printCostTransactional + (coupon.redemptionRate * (coupon.promotionValue + self.scenario.program.handlingFee)))
                    return totalInvestment.currencyString(0)
                }
                else {
                    return coupon.totalInvestment.currencyString(0)
                }
            } else if let _ = column.referenceObject as? String {
                let total: Double = (self.scenario.getTotalDistributionForType(IPOfferType.Volume) * 4) * self.scenario.program.ccmAnnouncementCost
                return total.currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += coupon.totalInvestment
                }
                
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Distribution Investment", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                if coupon.offer.type == .Volume {
                    return (coupon.totalDistribution * self.scenario.program.printCostTransactional).currencyString(0)
                }
                else {
                    return (coupon.totalDistribution * coupon.offer.printCost).currencyString(0)
                }
            } else if let _ = column.referenceObject as? String {
                let total: Double = (self.scenario.getTotalDistributionForType(IPOfferType.Volume) * 4) * self.scenario.program.ccmAnnouncementCost
                return total.currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += (coupon.totalDistribution * coupon.offer.printCost)
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Redemption Investment", cellValueFormatter: { (column) -> String in
            if let coupon = column.referenceObject as? IPCoupon {
                return (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue)).currencyString(0)
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var total:Double = 0
                for coupon in coupons {
                    total += (coupon.totalDistribution * coupon.redemptionRate * (coupon.offer.scenario.program.handlingFee + coupon.promotionValue))
                }
                return total.currencyString(0)
            } else {
                return "--"
            }
        }))
        self.rows.append(DesignInputsRow(headerText: "Strategic Weighting", cellValueFormatter: { (column) -> String in
            if let _ = column.referenceObject as? IPCoupon {
                return "--"
            } else if let coupons = column.referenceObject as? [IPCoupon] {
                var foundOfferType:IPOfferType?
                var multipleTypes = false
                var total: Double = 0
                for coupon in coupons {
                    if(foundOfferType == nil){
                        foundOfferType = coupon.offer.type
                    }
                    else{
                        if(foundOfferType != coupon.offer.type){
                            multipleTypes = true
                        }
                    }
                    total += coupon.totalInvestment / coupon.offer.scenario.totalBudget
                }
                if(multipleTypes == true){
                    return "--"
                }
                else{
                    return total.percentString()
                }
            } else {
                return "--"
            }
        }))
        
        //MARK: ----------------------------------------------------------------------------------------------------------------
    }
}

//MARK: - DesignInputsCollectionViewLayoutDelegate

protocol DesignInputsCollectionViewLayoutDelegate: class {
    
    func layout(_ layout: DesignInputsCollectionViewLayout, sizeOfItemAtIndexPath indexPath: IndexPath) -> CGSize
}

//MARK: - DesignInputsCollectionViewLayout

class DesignInputsCollectionViewLayout: UICollectionViewLayout {
    
    var delegate: DesignInputsCollectionViewLayoutDelegate!
    
    var lockColumnHeaders = true {
        didSet {
            needsFullLayout = true
        }
    }
    var lockRowHeaders = true {
        didSet {
            needsFullLayout = true
        }
    }
    
    var needsFullLayout = true
    
    fileprivate var attributesDictionary = [IndexPath:UICollectionViewLayoutAttributes]()
    fileprivate var contentSize = CGSize.zero
    
    init(delegate: DesignInputsCollectionViewLayoutDelegate) {
        
        super.init()
        self.delegate = delegate
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var collectionViewContentSize : CGSize {
        
        return contentSize
    }
    
    override func prepare() {
        
        if let collectionView = self.collectionView {
            
            if needsFullLayout { //we need to update the size and position of all items in the collection view
                
                var x:CGFloat = 0
                var y:CGFloat = 0
                var maxX:CGFloat = 0
                var maxY:CGFloat = 0
                
                //loop through each section in the collection view
                for section in 0..<collectionView.numberOfSections {
                    
                    //reset the x position and maxY values each time we start a new row
                    x = 0
                    maxY = 0
            
                    //loop through each item in the section
                    for item in 0..<collectionView.numberOfItems(inSection: section) {
                        
                        //create a UICollectionVieLayoutAttributes for the item
                        let indexPath = IndexPath(item: item, section: section)
                        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                        let size = delegate.layout(self, sizeOfItemAtIndexPath: indexPath)
                        
                        //update the frame for the cell
                        attributes.frame = CGRect(x: x, y: y, width: size.width, height: size.height)
                        
                        //update the zIndex based on cell type
                        if (section == 0 || section == 1) && item == 0 { //top-left corner cell
                            attributes.zIndex = 4
                        } else if (section == 0 || section == 1) { //column headers
                            attributes.zIndex = 3
                        } else if item == 0 { //row headers
                            attributes.zIndex = 2
                        } else { //regular cells
                            attributes.zIndex = 1
                        }
                        
                        //save the attributes in our dictionary for later use
                        attributesDictionary[indexPath] = attributes
                        
                        //as we move through the items, update the x position
                        x += size.width
                        if x > maxX {
                            maxX = x
                        }
                        if size.height > maxY {
                            maxY = size.height
                        }
                        
                    }
                    
                    //as we move through the sections, update the y position
                    y += maxY
                }
                
                // Update content size.
                self.contentSize = CGSize(width: maxX, height: y)
                
            } else { //we only need to update the positions of the row and column headers if they are locked
                
                if lockColumnHeaders || lockRowHeaders {
                    
                    //get current content offset
                    let xOffset = collectionView.contentOffset.x
                    let yOffset = collectionView.contentOffset.y
                    
                    let defaultYForSection0:CGFloat = 0
                    let defaultYForSection1:CGFloat = 40
                    
                    //loop through sections(rows)
                    for section in 0..<collectionView.numberOfSections {
                        
                        if (section == 0 || section == 1) && lockColumnHeaders { // Update all items in the first section (column headers)
                            
                            for item in 0..<collectionView.numberOfItems(inSection: section) {
                                let indexPath = IndexPath(item: item, section: section)
                                if let attributes = attributesDictionary[indexPath] {
                                    var frame = attributes.frame
                                    if yOffset > 0 {
                                        // Update y-position to match content offset of the collection view
                                        if section == 0 {
                                            frame.origin.y = defaultYForSection0 + yOffset
                                        } else {
                                            frame.origin.y = defaultYForSection1 + yOffset
                                        }
                                    } else {
                                        if section == 0 {
                                            frame.origin.y = defaultYForSection0
                                        } else {
                                            frame.origin.y = defaultYForSection1
                                        }
                                    }
                                    if item == 0 && lockRowHeaders { // Also update x-position for the first (corner) cell
                                        if xOffset > 0 {
                                            frame.origin.x = xOffset
                                        } else {
                                            frame.origin.x = 0
                                        }
                                    }
                                    attributes.frame = frame
                                }
                            }
                        
                        } else if lockRowHeaders { // For all other sections, we only need to update the x-position for the fist item (row headers)
                            
                            let indexPath = IndexPath(item: 0, section: section)
                            if let attributes = attributesDictionary[indexPath] {
                                var frame = attributes.frame
                                if xOffset > 0 {
                                    frame.origin.x = xOffset // Update x-position to match content offset of the collection view
                                } else {
                                    frame.origin.x = 0
                                }
                                attributes.frame = frame
                            }
                            
                        }
                    }
                }
            }
            needsFullLayout = false
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var attributesToReturn = [UICollectionViewLayoutAttributes]()
        for attributes in attributesDictionary.values {
            if rect.intersects(attributes.frame) {
                attributesToReturn.append(attributes)
            }
        }
        return attributesToReturn
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        return attributesDictionary[indexPath]
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        
        return true
    }
}

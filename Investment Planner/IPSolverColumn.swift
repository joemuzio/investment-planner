
class IPSolverColumn {
    
    var name: String = ""
    var type: Int32 = GLP_CV
    var upperBound: Double = 0.0
    var lowerBound: Double = 0.0
    /*
    GLP_FR - free variable
    GLP_LO - variable with lower bound
    GLP_UP - variable with upper bound
    GLP_DB - double-bounded variable
    GLP_FX - fixed variable
    */
    var valueType: Int32 = GLP_LO
    
    func setFixedValue(_ value: Double) {
        lowerBound = value
        valueType = GLP_FX
    }
}

//
//  OfferCollectionViewCell.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/3/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

let DO_COLOR_INTERPOLATION_ON_SCALE = true
let SHOW_DROP_SHADOW_ON_SCALE = false

class OfferCollectionViewCell: UICollectionViewCell {
    
    var nameLabel: UILabel!
    var promoLabel: UILabel!
    fileprivate var gradientLayer: CAGradientLayer!
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.contentView.backgroundColor = UIColor.catalinaWhiteColor()
        
        gradientLayer = CAGradientLayer()
        if SHOW_DROP_SHADOW_ON_SCALE {
            gradientLayer.frame = self.contentView.bounds.insetBy(dx: 6, dy: 6)
        } else {
            gradientLayer.frame = self.contentView.bounds
        }
        gradientLayer.colors = [UIColor.catalinaLightBlueColor().cgColor, UIColor.catalinaDarkBlueColor().cgColor]
        gradientLayer.cornerRadius = 6
        self.contentView.layer.addSublayer(gradientLayer)
        
        nameLabel = UILabel()
        nameLabel.font = UIFont.catalinaBookFontOfSize(24)
        nameLabel.text = "Offer Name"
        nameLabel.textColor = UIColor.white
        nameLabel.sizeToFit()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(nameLabel)
        self.contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: -20))
        
        promoLabel = UILabel()
        promoLabel.font = UIFont.catalinaMediumFontOfSize(32)
        promoLabel.text = "Buy 1"
        promoLabel.textColor = UIColor.white
        promoLabel.sizeToFit()
        promoLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(promoLabel)
        self.contentView.addConstraint(NSLayoutConstraint(item: promoLabel, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: promoLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 20))
    }
    
    //transforms the content view of the cell based on its proximity (or relative position) to the center of the collection view
    func transformContentViewForRelativeXPosition(_ position: CGFloat) {
        
        //if we are left of center relative position is negative, right of center it is positive
        let startPosition: CGFloat = 10 //start transforming at this number of points from center
        let endPosition: CGFloat = 300 //finish transforming at this number of points from center
        let minXScale: CGFloat = 0.98
        let minYScale: CGFloat = 0.85
        let minZScale: CGFloat = 0.9
        let absolutePosition: CGFloat = fabs(position)
    
        if absolutePosition < startPosition { //cell is in the center position on our collection view
            
            //shadow
            if SHOW_DROP_SHADOW_ON_SCALE {
                gradientLayer.shadowOffset = CGSize(width: 1, height: 1)
                gradientLayer.shadowRadius = 4
                gradientLayer.shadowOpacity = 0.5
            }
            
            //scale transform
            self.contentView.layer.transform = CATransform3DIdentity
            
            //color interpolation
            if DO_COLOR_INTERPOLATION_ON_SCALE {
                gradientLayer.colors = [UIColor.catalinaLightBlueColor().cgColor, UIColor.catalinaDarkBlueColor().cgColor]
            }
            
        } else if absolutePosition > endPosition { //cell is fully offset from the center either to the left or right
            
            //shadow
            if SHOW_DROP_SHADOW_ON_SCALE {
                gradientLayer.shadowOffset = CGSize.zero
                gradientLayer.shadowRadius = 0
                gradientLayer.shadowOpacity = 0
            }
            
            //scale transform
            let transform = CATransform3DMakeScale(minXScale, minYScale, minZScale)
            self.contentView.layer.transform = transform
            
            //color interpolation
            if DO_COLOR_INTERPOLATION_ON_SCALE {
                gradientLayer.colors = [UIColor.catalinaLightGreenColor().cgColor, UIColor.catalinaDarkGreenColor().cgColor]
            }
            
        } else { //cell is somewhere between the center position and one of the two offset positions, we need to calculate an offset percentage to adjust its scale and color by
            
            //shadow
            if SHOW_DROP_SHADOW_ON_SCALE {
                gradientLayer.shadowOffset = CGSize(width: 1, height: 1)
                gradientLayer.shadowRadius = 4
                gradientLayer.shadowOpacity = 0.5
            }
            
            //scale transform
            let percent = (absolutePosition - startPosition) / (endPosition - startPosition)
            let xScale = 1 - ((1 - minXScale) * percent)
            let yScale = 1 - ((1 - minYScale) * percent)
            let zScale = 1 - ((1 - minZScale) * percent)
            let transform = CATransform3DMakeScale(xScale, yScale, zScale)
            self.contentView.layer.transform = transform
            
            //color interpolation
            if DO_COLOR_INTERPOLATION_ON_SCALE {
                let color1 = UIColor.catalinaLightBlueColor().interpolateRGBColorTo(UIColor.catalinaLightGreenColor(), fraction: percent)
                let color2 = UIColor.catalinaDarkBlueColor().interpolateRGBColorTo(UIColor.catalinaDarkGreenColor(), fraction: percent)
                gradientLayer.colors = [color1.cgColor, color2.cgColor]
            }
        }
    }
}

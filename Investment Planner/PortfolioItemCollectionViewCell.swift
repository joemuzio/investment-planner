//
//  PortfolioItemCollectionViewCell.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/5/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class PortfolioItemCollectionViewCell: UICollectionViewCell {
    
    fileprivate(set) var deleteButton: UIButton!
    fileprivate(set) var duplicateButton: UIButton!
    var scenario: IPScenario!
    var viewController:ScenarioBuilderDeepDiveViewController!
    
    var longPressGesture: UILongPressGestureRecognizer!
    var longPressGestureHandler: ((UILongPressGestureRecognizer) -> Void)? {
        didSet {
            if longPressGestureHandler == nil {
                self.removeGestureRecognizer(longPressGesture)
            } else {
                self.addGestureRecognizer(longPressGesture)
            }
        }
    }
    
    var borderColor = UIColor.catalinaLightBlueColor() {
        didSet {
            self.contentView.layer.borderColor = borderColor.cgColor
        }
    }
    
    var deleteActionHandler: ((PortfolioItemCollectionViewCell) -> Void)? {
        didSet {
            deleteButton.alpha = (deleteActionHandler == nil) ? 0 : 1
        }
    }
    
    var duplicateActionHandler: ((PortfolioItemCollectionViewCell) -> Void)? {
        didSet {
            duplicateButton.alpha = (duplicateActionHandler == nil) ? 0 : 1
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.contentView.backgroundColor = UIColor.catalinaWhiteColor()
        self.contentView.layer.cornerRadius = 6
        self.contentView.layer.borderColor = UIColor.catalinaLightBlueColor().cgColor
        self.contentView.layer.borderWidth = 2
        self.contentView.layer.masksToBounds = true
        
        let deleteButtonImage = UIImage(named: "icon-delete-card")
        deleteButton = UIButton()
        deleteButton.setImage(deleteButtonImage, for: UIControlState())
        deleteButton.sizeToFit()
        deleteButton.alpha = 0
        deleteButton.addTarget(self, action: #selector(PortfolioItemCollectionViewCell.deleteButtonPressed), for: .touchUpInside)
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(deleteButton)
        self.addConstraint(NSLayoutConstraint(item: deleteButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: -12))
        self.addConstraint(NSLayoutConstraint(item: deleteButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: -12))
        
        let duplicateButtonImage = UIImage(named: "icon-duplicate-card")
        duplicateButton = UIButton()
        duplicateButton.setImage(duplicateButtonImage, for: UIControlState())
        duplicateButton.sizeToFit()
        duplicateButton.alpha = 0
        duplicateButton.addTarget(self, action: #selector(PortfolioItemCollectionViewCell.duplicateButtonPressed), for: .touchUpInside)
        duplicateButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(duplicateButton)
        self.addConstraint(NSLayoutConstraint(item: duplicateButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: -12))
        self.addConstraint(NSLayoutConstraint(item: duplicateButton, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 12))
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(PortfolioItemCollectionViewCell.handleLongPress(_:)))
    }
    
    func setupViewController(_ vc:ScenarioBuilderDeepDiveViewController){
        viewController = vc
        scenario = vc.displayedScenario
        viewController.view.frame = self.contentView.frame
        self.contentView.subviews.forEach({ $0.removeFromSuperview() })
        self.contentView.addSubview(vc.view)
    }
    
    
    // MARK: - Actions
    
    internal func deleteButtonPressed() {
        
        if let delete = deleteActionHandler {
            delete(self)
        }
    }
    
    internal func duplicateButtonPressed() {
        
        if let duplicate = duplicateActionHandler {
            duplicate(self)
        }
        
    }
    
    func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        
        if let handler = self.longPressGestureHandler {
            handler(gesture)
        }
    }
    
    func setupPorfolioModeConstraintsWithAnimation(_ showAnimation: Bool){
        viewController.setupConstraintsPortfolioMode(showAnimation)
    }
    func setupNormalModeConstraintsWithAnimation(_ showAnimation: Bool){
        viewController.setupConstraintsNormalMode(showAnimation)
    }
    
    // MARK: - Dashed Outline
    
    fileprivate var dashedOutlineLayer: CAShapeLayer?
    
    var shouldShowDashedOutline = false {
        
        didSet {
            if shouldShowDashedOutline != oldValue { //make sure this has actually changed so we don't unintentionally duplicate the outline layer
                if shouldShowDashedOutline {
                    let shapeLayer = CAShapeLayer()
                    shapeLayer.lineWidth = 2
                    shapeLayer.lineDashPattern = [10,5]
                    shapeLayer.fillColor = UIColor.clear.cgColor
                    shapeLayer.strokeColor = UIColor.catalinaDarkBlueColor().cgColor
                    shapeLayer.frame = self.bounds.insetBy(dx: -6, dy: -6)
                    shapeLayer.path = UIBezierPath(roundedRect: shapeLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 6, height: 6)).cgPath
                    self.layer .addSublayer(shapeLayer)
                    dashedOutlineLayer = shapeLayer
                } else {
                    if let shapeLayer = dashedOutlineLayer {
                        shapeLayer.removeFromSuperlayer()
                        dashedOutlineLayer = nil
                    }
                }
            }
        }
    }
    
    // MARK: - Solid Line
    
    fileprivate var solidOutlineLayer: CAShapeLayer?
    
    var shouldShowSolidOutline = false {
        
        didSet {
            if shouldShowSolidOutline != oldValue { //make sure this has actually changed so we don't unintentionally duplicate the outline layer
                if shouldShowSolidOutline {
                    let shapeLayer = CAShapeLayer()
                    shapeLayer.lineWidth = 2
           //         shapeLayer.lineDashPattern = [10,5]
                    shapeLayer.fillColor = UIColor.clear.cgColor
                    shapeLayer.strokeColor = UIColor.catalinaDarkBlueColor().cgColor
                    shapeLayer.frame = self.bounds.insetBy(dx: -6, dy: -6)
                    shapeLayer.path = UIBezierPath(roundedRect: shapeLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 6, height: 6)).cgPath
                    self.layer .addSublayer(shapeLayer)
                    solidOutlineLayer = shapeLayer
                } else {
                    if let shapeLayer = solidOutlineLayer {
                        shapeLayer.removeFromSuperlayer()
                        solidOutlineLayer = nil
                    }
                }
            }
        }
    }
    
    // MARK: - Active (i.e. Color vs. Grayscale) Appearance
    
    
    var active = true {
        
        didSet {
            if active {
                self.borderColor = UIColor.catalinaLightBlueColor()
                self.alpha = 1
            } else {
                self.borderColor = UIColor.lightGray
                self.alpha = 0.75
            }
        }
    }
    
}

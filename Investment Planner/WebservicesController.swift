
//  WebservicesController.swift
//  Investment Planner
//
//  Created by Joseph Muzio on 3/1/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit
import KeychainAccess

private let SharedWebServicesInstance = WebservicesController()

private let userNameKey = "pythagoras.user.userName"
private let accessTokenKey = "pythagoras.user.accessToken"
private let refreshTokenKey = "pythagoras.user.refreshToken"

#if DEBUG
    private let hostName = "ipservices.catalinamarketing.com"
#else
    private let hostName = "ipservices.catalinamarketing.com"
#endif
private let authURL = "https://\(hostName):8101/uaa/oauth/token"
private let usersServiceURL = "https://\(hostName):8101/uaa/group"
private let reportsServiceURL = "https://\(hostName):8102"
private let reportsServiceMetadataURL = "\(reportsServiceURL)/meta"
private let authClientID = "ip_app"

private enum ReportsServiceEndpoint: String {
    case ListManager = "listManager"
    case BrandCategory = "categoryBrand"
}

let UserAccessErrorDomain = "pythagoras.error.UsersAccessErrorDomain"
let AuthenticationFailureCode = 101
let AccessTokenRejectedErrorCode = 102

let ReportsServiceErrorDomain = "pythagoras.error.ReportsServiceErrorDomain"
let ReportSearchFailureCode = 201
let ReportUpdateFailureCode = 202

let GenericServiceErrorDomain = "pythagoras.error.GenericServiceErrorDomain"
let UnknownServiceErrorCode = 300
let InvalidServiceResponseCode = 301
let InternalServiceErrorCode = 302

class WebservicesController: NSObject {
    
    class var sharedWebservicesController: WebservicesController {
        return SharedWebServicesInstance
    }
    
    fileprivate(set) var reachability: Reachability!
    
    //MARK: - Initialization
    
    override init() {
        
        super.init()
        
        do {
            reachability = Reachability(hostname: hostName)
        }
        
        reachability.whenReachable = { reachability in
            if self.userName != nil && !self.needsLogout {
                print("Reports Service is now reachable. Resuming service updates.")
                self.reportsServiceUpdateOperationQueue.isSuspended = false
            }
        }
        
        reachability.whenUnreachable = { reachability in
            print("Reports Service is no longer reachable. Suspending service updates.")
            self.reportsServiceUpdateOperationQueue.isSuspended = true
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        //add an observer for persistence notifications from other parts of the application
        reportPersistenceObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ReportDataPersistenceNotification), object: DataController.sharedDataController, queue: OperationQueue.main, using: { (notification) -> Void in
            
            let info = (notification as NSNotification).userInfo!
            let records: [ReportDataPersistenceRecord] = info["records"] as! [ReportDataPersistenceRecord]
            for record in records {
                if record.success {
                    self.enqueueUpdateOperationForReport(record.report, withAction: record.action)
                }
            }
        })
    }
    
    deinit {
        
        if let observer = reportPersistenceObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    //MARK: - Session Management
    
    fileprivate(set) var userName: String? {
        get {
            return UserDefaults.standard.string(forKey: userNameKey)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: userNameKey)
        }
    }
    
    fileprivate var accessToken: String? {
        get {
            let bundle = Bundle.main
            let info = bundle.infoDictionary
            let appID = info!["CFBundleIdentifier"] as! String
            let keychain = Keychain(service:appID)
            return keychain[accessTokenKey]
        }
        set {
            let bundle = Bundle.main
            let info = bundle.infoDictionary
            let appID = info!["CFBundleIdentifier"] as! String
            let keychain = Keychain(service:appID)
            keychain[accessTokenKey] = newValue
        }
    }
    fileprivate var refreshToken: String? {
        get {
            let bundle = Bundle.main
            let info = bundle.infoDictionary
            let appID = info!["CFBundleIdentifier"] as! String
            let keychain = Keychain(service:appID)
            return keychain[refreshTokenKey]
        }
        set {
            let bundle = Bundle.main
            let info = bundle.infoDictionary
            let appID = info!["CFBundleIdentifier"] as! String
            let keychain = Keychain(service:appID)
            keychain[refreshTokenKey] = newValue
        }
    }
    
    var ipGroupUsers: [IPUser] = [IPUser]()
    
    fileprivate(set) var needsLogout: Bool = false
    
    //this method can help with debugging access token issues
    fileprivate func getRemainingTimeForAccessToken(_ token: String?) -> TimeInterval {
        
        if let token = token {
            let tokenComponents = token.components(separatedBy: ".")
            if tokenComponents.count == 3 {
                let payload = tokenComponents[1]
                let paddedLength = payload.characters.count + (4 - (payload.characters.count % 4))
                let paddedPayload = payload.padding(toLength: paddedLength, withPad: "=", startingAt: 0)
                if let decodedPayloadData = Data(base64Encoded: paddedPayload, options: .ignoreUnknownCharacters) {
                //    if let payloadJSON = try? JSONSerialization.jsonObject(with: decodedPayloadData, options: .mutableContainers as [String: AnyObject]) {
                    if let payloadJSON = try? JSONSerialization.jsonObject(with: decodedPayloadData, options:.mutableContainers) as? [String: AnyObject]{
                        if let expirationTimeInterval = payloadJSON?["exp"] as? Double {
                            let currentTimeInterval = Date().timeIntervalSince1970
                            return expirationTimeInterval - currentTimeInterval
                        }
                    }
                }
            }
        }
        return 0
    }
    
    //MARK: - Users Service
    
    func login(_ userName: String, password: String, completion: @escaping ((_ error: NSError?) -> Void)) {
        
        let url = URL(string: authURL)!
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
   //    let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
        request.httpMethod = "POST"
        let formDataString = "grant_type=password&username=\(userName)&password=\(password)"
        let updatedFormDataString = formDataString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)

        request.httpBody = updatedFormDataString?.data(using: String.Encoding.utf8)
        let basicAuthData: Data = "\(authClientID):\(authClientID)".data(using: String.Encoding.utf8)!
        let base64LoginString = basicAuthData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64LoginString)"]
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
       // session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let token = result {
                self.userName = userName
                self.accessToken = token["access_token"] as? String
                self.refreshToken = token["refresh_token"] as? String
                self.needsLogout = false
                self.reportsServiceUpdateOperationQueue.isSuspended = false
                print("Login Succeeded for user \(userName). Access token will expire in \(self.getRemainingTimeForAccessToken(self.accessToken)) seconds. Token:\n\(self.accessToken)")
                // check if session has a username
                if let currentUser = AnalyticsController.sharedAnalyticsController.getCurrentUserID(){
                    print(currentUser)
                    // check if user changed
                    if(userName != currentUser){
                        AnalyticsController.sharedAnalyticsController.sendUserIdToAnalytics(userName)
                    }
                }
                else{
                    AnalyticsController.sharedAnalyticsController.sendUserIdToAnalytics(userName)
                }
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                //special check for 400 error here which means authentication failed
                if let response = response as? HTTPURLResponse {
                    if response.statusCode == 400 {
                        let error = NSError(domain: UserAccessErrorDomain, code: AuthenticationFailureCode, userInfo: [NSLocalizedDescriptionKey:"Invalid username or password."])
                        print("Login Failed for user \(userName). Invalid username or password.")
                        completion(error)
                        return
                    }
                }
                if let error = error {
                    print("Login Failed for user \(userName) with error:\n\(error)\nService Response:\n\(response)")
                }
                completion(error)
            })
            
        }).resume()
    }
    
    func logout() {
        
        self.userName = nil
        self.accessToken = nil
        self.refreshToken = nil
    }
    
    func searchIPUsers(_ searchText: String) -> [IPUser] {
        
        var searchResults = [IPUser]()
        
        //search first for names that start with the search text
        var primaryResults = [IPUser]()
        for user in ipGroupUsers {
            if user.name.uppercased().hasPrefix(searchText.uppercased()) {
                primaryResults.append(user)
            }
        }
        primaryResults.sort { (user1, user2) -> Bool in
            return user1.name < user2.name
        }
        
        //secondarily we search for names that match the text in any way
        var secondaryResults = [IPUser]()
        for user in ipGroupUsers {
            if !primaryResults.contains(user) && user.name.uppercased().contains(searchText.uppercased()) {
                secondaryResults.append(user)
            }
        }
        secondaryResults.sort { (user1, user2) -> Bool in
            return user1.name < user2.name
        }
        
        //now add both to our results set and return
        searchResults.append(contentsOf: primaryResults)
        searchResults.append(contentsOf: secondaryResults)
        
        return searchResults
    }
    
    func updateIPUserGroup(_ completion: @escaping ((_ error: NSError?) -> Void)) {

        self.userServiceUpdateIPUserGroup(completion)
    }
    
    //MARK: - Users Service Private Methods
    
    fileprivate func refreshAccessToken(_ completion: @escaping ((_ error: NSError?) -> Void)) {
        
        let url = URL(string: authURL)!
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        let formDataString = "grant_type=refresh_token&refresh_token=\(refreshToken!)&client_id=\(authClientID)"
        request.httpBody = formDataString.data(using: String.Encoding.utf8)
        let basicAuthData: Data = "\(authClientID):\(authClientID)".data(using: String.Encoding.utf8)!
        let base64LoginString = basicAuthData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Basic \(base64LoginString)"]
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let token = result {
                self.accessToken = token["access_token"] as? String
                print("Access token refresh for user \(self.userName!) Succeeded. New access token will expire in \(self.getRemainingTimeForAccessToken(self.accessToken)) seconds. Token:\n\(self.accessToken)")
            }
            
            if let error = error {
                //we tried to refresh our access token an it failed, probably because the refresh token was rejected. we need to suspend our update queue and set the needsLogout flag to true to let the app know it should force the user to log out and back in
                self.reportsServiceUpdateOperationQueue.isSuspended = true
                self.needsLogout = true
                print("Access token refresh for user \(self.userName!) Failed with error:\n\(error)\nDisabling service update queue.\nService Response:\n\(response)")
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                completion(error)
            })
            
        }).resume()
    }
    
    fileprivate func userServiceUpdateIPUserGroup(_ completion: @escaping ((_ error: NSError?) -> Void)) {
        
        let url = URL(string: usersServiceURL)!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Bearer \(accessToken!)"]
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let JSONObject = result as? [[String: AnyObject]] {
                
                var users = [IPUser]()
                for userJSON in JSONObject {
                    let user = IPUser()
                    user.updateWithJSONObject(userJSON)
                    users.append(user)
                }
                self.ipGroupUsers = users
                
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(error)
                })
                
            } else if let error = error {
                
                if error.domain == UserAccessErrorDomain && error.code == AccessTokenRejectedErrorCode { //if the error was caused by a bad access token, refresh the token and try again
                
                    self.refreshAccessToken({ (error) in
                        if let error = error { //could not refresh the access token
                            //we can't really do anything about it here, just have to invoke the completion block and pass the error along
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(error)
                            })
                        } else {
                            //we were able to refresh the access token so try again
                            self.userServiceUpdateIPUserGroup(completion)
                        }
                    })
                    
                } else {//something else happened, invoke the completion block and pass the error along
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(error)
                    })
                }
                
            }
            
        }).resume()
        
    }
    
    //MARK: - Reports Service
    
    var isCheckingForNewReports: Bool = false
    
    fileprivate var reportPersistenceObserver: NSObjectProtocol?
    
    fileprivate lazy var reportsServiceUpdateOperationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    fileprivate lazy var standardUTCDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formatter
    }()
    
    func checkForNewReports(_ completion: @escaping ((_ error: NSError?) -> Void)) {
    
        //prevent multiple executions of this method. also, we know this will always be called from main UI thread so we're not worrying about thread safety here
        assert(!isCheckingForNewReports)
        isCheckingForNewReports = true
        
        let startDate = Date()
        
        //get the most recent last modified date for  list manager report reference date from user defaults
        let owner = userName!
        let data = DataController()
        data.userName = owner
        let modifiedAfter = data.getLastModifiedDateOfMostRecentReportInTypes(IPReportType.listManagerTypes)
        
        //search for list manager reports
        self.reportsServiceGetReports(.ListManager, owner: owner, modifiedAfter: modifiedAfter) { (error) in
            
            if let error = error {
                //if an error occurred go ahead and bail now
                completion(error)
                self.isCheckingForNewReports = false
            } else {
                //update the category brand metadata index
                self.updateCategoryBrandReportMetadata({ (error) in
                    completion(error)
                    self.isCheckingForNewReports = false
                    let endDate = Date()
                    let time = endDate.timeIntervalSince1970 - startDate.timeIntervalSince1970
                    print("checkForNewReports completed after \(time) seconds")
                })
            }
        }
    }
    
    func shareReport(_ report: IPReport, withUser user: IPUser, completion: @escaping ((_ error: NSError?) -> Void)) {
        
        let temp = report.duplicate()
        let originalOwner = report.owner
        let newOwner = user.userName.lowercased()
        temp.owner = newOwner
        temp.fileName = report.fileName.replacingOccurrences(of: originalOwner, with: newOwner)
        temp.filePath = report.filePath.replacingOccurrences(of: originalOwner, with: newOwner)
        temp.displayType = .Shared
        temp.fileLastModified = Date()
        self.reportsServiceUpdate(temp, requiredAction: .Create, completion: completion)
    }
    
    func downloadReportWithID(_ id: String, type: IPReportType, completion: @escaping ((_ error: NSError?) -> Void)) {
        
        if IPReportType.brandCategoryTypes.contains(type) {
            self.reportsServiceGetReport(ReportsServiceEndpoint.BrandCategory, id: id, completion: completion)
        } else {
            self.reportsServiceGetReport(ReportsServiceEndpoint.ListManager, id: id, completion: completion)
        }
    }
    
    func getServiceInfo() -> String {
        
        if let user = userName {
            let data = DataController()
            data.userName = user
            let lmDate = data.getLastModifiedDateOfMostRecentReportInTypes(IPReportType.listManagerTypes)
            //            let bcDate = data.getLastModifiedDateOfMostRecentReportInTypes(IPReportType.brandCategoryTypes)
            //            return "Logged in User: \(user)\nList Manager Reference Date: \(lmDate)\nBrand Category Reference Date: \(bcDate)"
            return "Logged in User: \(user)\nList Manager Reference Date: \(lmDate)"
        }
        return "No logged in user"
    }
    
    //MARK: - Reports Service Private Methods
    
    fileprivate func updateCategoryBrandReportMetadata(_ completion: @escaping ((_ error: NSError?) -> Void)) {
        
        let url = URL(string: "\(reportsServiceURL)/meta/categoryBrand?size=15000&sort=reportName,asc")!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30.0)
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Bearer \(accessToken!)"]
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let JSONObject = result as? [String: AnyObject] {
                
                let dataController = DataController()
                dataController.userName = self.userName
                dataController.replaceCategoryBrandSearchIndexWithNewIndexFromMetadata(JSONObject)
                completion(nil)
            
            } else if let error = error {
                
                if error.domain == UserAccessErrorDomain && error.code == AccessTokenRejectedErrorCode { //if the error was caused by a bad access token, refresh the token and try again
                    
                    self.refreshAccessToken({ (error) in
                        if let error = error { //could not refresh the access token
                            //we can't really do anything about it here, just have to invoke the completion block and pass the error along
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(error)
                            })
                        } else {
                            //we were able to refresh the access token so try again
                            self.updateCategoryBrandReportMetadata(completion)
                        }
                    })
                    
                } else {//something else happened, invoke the completion block and pass the error along
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(error)
                    })
                }
                
                print("Reports Service GET Category Brand Report metadata Failed with error:\n\(error)\nService Response:\n\(response)")
                
            }
            
        }).resume()
        
    }
    
    ///The reportsServiceGetReport function will get a specific report with a given id on a given endpoint. If an error occurres, that error will be passed back in the completion block, otherwise, the report data will be saved locally.
    fileprivate func reportsServiceGetReport(_ endpoint: ReportsServiceEndpoint, id: String, completion: @escaping ((_ error: NSError?) -> Void)) {
        
        let url = URL(string: "\(reportsServiceURL)/\(endpoint.rawValue)/\(id)")!
        print(url)
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Bearer \(accessToken!)"]
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let JSONObject = result as? [String: AnyObject] {
                
                let report = IPReport()
                report.updateWithJSONObject(JSONObject)
                //create a local data controller instance here to use when saving the report so the save will not trigger the notification to update the report service
                let dataController = DataController()
                dataController.userName = self.userName
                _ = dataController.saveReport(report)
                
                print("Reports Service GET Report at endpoint \(endpoint.rawValue) with ID \(id) Succeeded")
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(nil)
                })
                
            } else if let error = error {
                
                if error.domain == UserAccessErrorDomain && error.code == AccessTokenRejectedErrorCode { //if the error was caused by a bad access token, refresh the token and try again
                    
                    self.refreshAccessToken({ (error) in
                        if let error = error { //could not refresh the access token
                            //we can't really do anything about it here, just have to invoke the completion block and pass the error along
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(error)
                            })
                        } else {
                            //we were able to refresh the access token so try again
                            self.reportsServiceGetReport(endpoint, id: id, completion: completion)
                        }
                    })
                    
                } else {//something else happened, invoke the completion block and pass the error along
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(error)
                    })
                }
                
                print("Reports Service GET Report at endpoint \(endpoint.rawValue) with id '\(id)' Failed with error:\n\(error)\nService Response:\n\(response)")
                
            }
            
        }).resume()
    }
    
    ///The reportsServiceGetReports function hits the search/latest/ endpoint on the reports service and gets all reports for the specified 'owner' that have a fileLastMofdified date after the 'modifiedAfter' date value. A page number can optionally be set, though the page number is intendted to be used by the reportsServiceSearchLatest function itself when it recursively requests each page of the results set. When the last entry of the last page is reached, the completion block will be invoked. If an error occurred, that error will be passed back in the completion block.
    fileprivate func reportsServiceGetReports(_ endpoint: ReportsServiceEndpoint, owner: String, modifiedAfter: Date, page: Int = 0, completion: @escaping ((_ error: NSError?) -> Void)) {
        
        var referenceDate = standardUTCDateFormatter.string(from: modifiedAfter)
        let characterSet = (CharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy()
        (characterSet as AnyObject).removeCharacters(in: ":+")
        referenceDate = referenceDate.addingPercentEncoding(withAllowedCharacters: characterSet as! CharacterSet)!
        var urlString = ""
        if endpoint == .BrandCategory {
            urlString = "\(reportsServiceURL)/\(endpoint.rawValue)/search/latest?lastModifiedAfter=\(referenceDate)&page=\(page)&sort=lastModifiedDate,asc"
        } else {
            urlString = "\(reportsServiceURL)/\(endpoint.rawValue)/search/latest?owner=\(owner.lowercased())&lastModifiedAfter=\(referenceDate)&page=\(page)&sort=lastModifiedDate,asc"
        }
        let url = URL(string: urlString)!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Bearer \(accessToken!)"]
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let JSONObject = result as? [String: AnyObject] {
            
                //validate JSON object and store data in local variables
                let embedded = JSONObject["_embedded"] as! [String: AnyObject]
                let JSONReportObjects = embedded["\(endpoint.rawValue)s"] as! [[String: AnyObject]]
                let pageInfo = JSONObject["page"] as! [String: AnyObject]
                let pageNumber = pageInfo["number"] as! Int
                let totalPages = pageInfo["totalPages"] as! Int
                
                //create IPReports from the returned results
                var reports = [IPReport]()
                for JSONReportObject in JSONReportObjects {
                    let report = IPReport()
                    report.updateWithJSONObject(JSONReportObject)
                    if let userConstantsDictionary = SettingsController.sharedSettingsController.userMathConstants {
                        if(userConstantsDictionary.count > 0){
                            for(key, value) in userConstantsDictionary{
                                if let string = value as? String {
                                    
                                    if key == "profitMargin" {
                                        let stringWithNoCharsOrSymbols = string.stringWithNoCharsOrSymbols()
                                        report.program!.profitMargin = Double(percentString: stringWithNoCharsOrSymbols)
                                    }
                                    if key == "printCostTransactional" {
                                        let stringWithNoCharsOrSymbols = string.stringWithNoCharsOrSymbols()
                                        report.program!.printCostTransactional = Double(currencyString: stringWithNoCharsOrSymbols)
                                    }
                                    if key == "ccmAnnouncementCost" {
                                        let stringWithNoCharsOrSymbols = string.stringWithNoCharsOrSymbols()
                                        report.program!.ccmAnnouncementCost = Double(currencyString: stringWithNoCharsOrSymbols)
                                    }
                                    if key == "printCostId" {
                                        let stringWithNoCharsOrSymbols = string.stringWithNoCharsOrSymbols()
                                        report.program!.printCostId = Double(currencyString: stringWithNoCharsOrSymbols)
                                    }
                                    if key == "handlingFee" {
                                        let stringWithNoCharsOrSymbols = string.stringWithNoCharsOrSymbols()
                                        report.program!.handlingFee = Double(currencyString: stringWithNoCharsOrSymbols)
                                    }
                                    if key == "defaultInvestment" {
                                        let stringWithNoCharsOrSymbols = string.stringWithNoCharsOrSymbols()
                                        report.program!.defaultInvestment = Double(currencyString: stringWithNoCharsOrSymbols)
                                    }
                                }
                            }
                        }
                    }
                    reports.append(report)
                }
                
                //create a local data controller instance here to use when saving the reports so the save will not trigger the notification to update the report service
                let dataController = DataController()
                dataController.userName = owner
                _ = dataController.saveReports(reports)
        
                print("Reports Service GET Reports at endpoint \(endpoint.rawValue) with owner '\(owner)' modified after \(modifiedAfter) page number \(page) Succeeded")
                //check the page information to see if there are more pages that need to be loaded
                if pageNumber < totalPages - 1 { //there are more pages so recursively invoke the method to load the next page
                    self.reportsServiceGetReports(endpoint, owner: owner, modifiedAfter: modifiedAfter, page: pageNumber + 1, completion: completion)
                } else { //that was the last page so invoke the completion block to indicate we're finished
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(nil)
                    })
                }
                
            } else if let error = error {
            
                if error.domain == UserAccessErrorDomain && error.code == AccessTokenRejectedErrorCode { //if the error was caused by a bad access token, refresh the token and try again
                    
                    self.refreshAccessToken({ (error) in
                        if let error = error { //could not refresh the access token
                            //we can't really do anything about it here, just have to invoke the completion block and pass the error along
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(error)
                            })
                        } else {
                            //we were able to refresh the access token so try again
                            self.reportsServiceGetReports(endpoint, owner: owner, modifiedAfter: modifiedAfter, page: page, completion: completion)
                        }
                    })
                    
                } else {//something else happened, invoke the completion block and pass the error along
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(error)
                    })
                }
                
            }
            
        }).resume()
    }
    
    ///The reportsServiceUpdate function invokes a POST, PATCH, or DELETE on the reports service for a given 'report' based on the 'requiredAction' for that report (Create, Update, or Delete) and notifies the caller of any errors that may have occurred via the completion block. In most cases, this method will be called by this class as a result of a notification from the data controller that changes to a report have been persisted. It can also be called by the "shareReport" function above.
    fileprivate func reportsServiceUpdate(_ report: IPReport, requiredAction: ReportDataPersistenceAction, completion: @escaping ((_ error: NSError?) -> Void)) {
            
        //create service url
        var urlString = "\(reportsServiceURL)/\(ReportsServiceEndpoint.ListManager.rawValue)"
        if requiredAction != .Create {
            urlString = "\(reportsServiceURL)/\(ReportsServiceEndpoint.ListManager.rawValue)/\(report.id)"
        }
        let url = URL(string: urlString)!
        
        //create service request
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval:10.0)
        request.httpMethod = requiredAction.rawValue
        if requiredAction != .Delete {
            var JSONData: Data!
            if requiredAction == .Create {
                let temp = report.duplicate()
                temp.id = ""
                JSONData = try! JSONSerialization.data(withJSONObject: temp.JSONObject(), options: JSONSerialization.WritingOptions(rawValue: 0))
            } else {
                JSONData = try! JSONSerialization.data(withJSONObject: report.JSONObject(), options: JSONSerialization.WritingOptions(rawValue: 0))
            }
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("\(JSONData.count)", forHTTPHeaderField: "Content-Length")
            request.httpBody = JSONData
        }
        
        //configure session
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.httpAdditionalHeaders = ["Authorization" : "Bearer \(accessToken!)"]
        let session = URLSession(configuration: sessionConfiguration)
        
        //create data task
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            
            let (result, error) = self.processServiceResponse(data, response: response, error: error as NSError?)
            
            if let JSONObject = result as? [String: AnyObject] {
                
                let temp = IPReport()
                temp.updateWithJSONObject(JSONObject)
                //create a local data controller instance to save the report so the save will not trigger the notification to update the report service
                let dataController = DataController()
                dataController.userName = temp.owner
                if report.id != temp.id { //if the id does match the id we have saved locally we need to delete and re-save the report. this can happen if the report was created locally with a temporary id.
                    let success = dataController.deleteReport(report)
                    if success {
                        _ = dataController.saveReport(temp)
                    }
                    //we also need to check for any queued updates that may be affected by this id change and make sure they're referencing the new report id
                    let oldID = report.id, newID = temp.id
                    for operation in self.reportsServiceUpdateOperationQueue.operations {
                        if let operation = operation as? ReportsServiceUpdateOperation {
                            if operation.report.id == oldID {
                                operation.report.id = newID
                            }
                        }
                    }
                    report.id = newID
                } else { //if the ids match, we just save the report locally to update the last modified date and any other values the server may have updated
                    _ = dataController.saveReport(temp)
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(nil)
                })
                
                print("Reports Service \(requiredAction.rawValue) for Report with ID '\(report.id)' Succeeded")
                
            } else if let error = error {
                
                if error.domain == UserAccessErrorDomain && error.code == AccessTokenRejectedErrorCode { //if the error was caused by a bad access token, refresh the token and try again
                    
                    self.refreshAccessToken({ (error) in
                        if let error = error { //could not refresh the access token
                            //we can't really do anything about it here, just have to invoke the completion block and pass the error along
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(error)
                            })
                        } else {
                            //we were able to refresh the access token so try again
                            self.reportsServiceUpdate(report, requiredAction: requiredAction, completion: completion)
                        }
                    })
                    
                } else {//something else happened, invoke the completion block and pass the error along
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(error)
                    })
                }
                
//                print("Reports Service \(requiredAction.rawValue) for Report with ID '\(report.id)' Failed with error:\n\(error)\nService Response:\n\(response)")
                
            }
            
        }).resume()
    }
    
    ///Enqueues an update operation for a given report with a given required action. This method is meant to be called when a report update is not directly user driven, i.e. updates scheduled by the data persistence notification handler. Methods that are directly user driven like the ShareReport method should directly invoke the reportsServiceUpdate method and either directly handle or notify the user of any errors that occur.
    fileprivate func enqueueUpdateOperationForReport(_ report: IPReport, withAction action: ReportDataPersistenceAction) {
        
        //create the operation
        let operation = ReportsServiceUpdateOperation(report: report, requiredAction: action)
        
        operation.willFinishBlock = { //this block will execute just before the operation is marked finished. we need to check for any errors that may have occurred and handle them here.
            
            if let error = operation.error {
                
                var shouldReenqueue = false
                
                if error.domain == UserAccessErrorDomain {
                    //our access token was rejected and we were unable to refresh it automatically, we need the user to log in but we do not have a UI to propigate the error up to. the update queue was suspended when the error the refresh token was rejected so we will re-enqueue the operation and wait for the queue to resume
                    shouldReenqueue = true
                }
                
                if error.domain == NSURLErrorDomain {
                    // the most likely cause of an nsurlerrordomain error is a connection error caused by a lost internet connection or a server time out. if we get such an error, we can just re-enqueue it to try it again.
                    shouldReenqueue = true
                }
                
                //any other error is completely unexpected and, unfortunately, cannot really be handled here. if we try to re-enqueue such errors we will likely end up trying to post the update to the server indefinitely and consuming a large amount of resources. we will let the update fail and not re-enqueue. this should be an extremely rare occurance if it ever happens at all
                
                if shouldReenqueue {
                    //we need to cancel all operations related to that report and remove them from the queue so they can be recreated and re-enqueued in the proper order for execution when the connection is restored.
                    //go through the queue and get all operations related to the report which are not executing and have not yet finished or been cancelled. this will include the operation that generated the error because this block will run after the executing flag has been set to false but before the finished flag is set true.
                    var relatedOperations = [ReportsServiceUpdateOperation]()
                    for operation in self.reportsServiceUpdateOperationQueue.operations {
                        if let operation = operation as? ReportsServiceUpdateOperation {
                            if operation.report.id == report.id && !operation.isExecuting && !operation.isFinished && !operation.isCancelled {
                                operation.cancel()
                                relatedOperations.append(operation)
                            }
                        }
                    }
                    
                    //now create new operations for each of those cancelled operations and re-enqueue them for later execution
                    for operation in relatedOperations {
                        self.enqueueUpdateOperationForReport(operation.report, withAction: operation.requiredAction)
                    }
                }
            }
        }
        
        //add it to the operation queue
        reportsServiceUpdateOperationQueue.addOperation(operation)
    }
    
    ///Processes a service response and returns either a JSON object result or an error. The error and result return values are both optional but one of the two will always have a value and the other will always be nil. It is not possible for both values to be nil or for both to have a value.
    fileprivate func processServiceResponse(_ data: Data?, response: URLResponse?, error: NSError?) -> (result: AnyObject?, error: NSError?) {
        
        //API generated error, most likely NSURLErrorDomain
        if error != nil {
            return (nil, error)
        }
        
        //handle normal service response
        if let response = response as? HTTPURLResponse {
            if response.statusCode >= 200 && response.statusCode <= 299 { //good response
                if let data = data {
                    do {
                        let result = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        return (result as AnyObject?, nil)
                    }
                    catch {
                        let error = NSError(domain: GenericServiceErrorDomain, code: InvalidServiceResponseCode, userInfo: [NSLocalizedDescriptionKey:"Invalid service response."])
                        return (nil, error)
                    }
                } else {
                    let error = NSError(domain: GenericServiceErrorDomain, code: InvalidServiceResponseCode, userInfo: [NSLocalizedDescriptionKey:"Invalid service response."])
                    return (nil, error)
                }
            } else if response.statusCode == 401 { //access denied
                let error = NSError(domain: UserAccessErrorDomain, code: AccessTokenRejectedErrorCode, userInfo: [NSLocalizedDescriptionKey:"Access Denied."])
                return (nil, error)
            } else if response.statusCode == 500 { //internal service error
                let error = NSError(domain: GenericServiceErrorDomain, code: InternalServiceErrorCode, userInfo: [NSLocalizedDescriptionKey:"Internal Service Error."])
                return (nil, error)
            } else { //unknown error
                let error = NSError(domain: GenericServiceErrorDomain, code: UnknownServiceErrorCode, userInfo: [NSLocalizedDescriptionKey:"Unrecognized Service Error."])
                return (nil, error)
            }
        }
        
        //it should not be posible to reach this point because this would mean there was no error and also no response from the server
        let error = NSError(domain: GenericServiceErrorDomain, code: UnknownServiceErrorCode, userInfo: [NSLocalizedDescriptionKey:"Unrecognized Service Error."])
        return (error, nil)
    }
}

class ReportsServiceUpdateOperation: Operation {

    fileprivate(set) var report: IPReport!
    fileprivate(set) var requiredAction: ReportDataPersistenceAction!
    fileprivate var _finished = false
    fileprivate var _executing = false
    
    var willFinishBlock: (() -> Void)?
    
    override var isExecuting:Bool {
        get { return _executing }
        set {
            willChangeValue(forKey: "isExecuting")
            _executing = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isFinished:Bool {
        get { return _finished }
        set {
            willChangeValue(forKey: "isFinished")
            _finished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isAsynchronous:Bool {
        get { return true }
    }
        
    var error: NSError?
    
    init(report: IPReport, requiredAction: ReportDataPersistenceAction) {
        
        super.init()
        self.report = report
        self.requiredAction = requiredAction
    }
    
    override func start() {
        
        if isCancelled {
            if let block = self.willFinishBlock {
                block()
            }
            isFinished = true
            return
        }
        
        isExecuting = true
        
        WebservicesController.sharedWebservicesController.reportsServiceUpdate(report, requiredAction: requiredAction) { (error) -> Void in
            
            self.error = error
            self.isExecuting = false
            
            if let block = self.willFinishBlock {
                block()
            }
            
            self.isFinished = true
            
        }
    }
}

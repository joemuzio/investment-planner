//
//  IPCoupon.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/21/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class IPCoupon: IPModelObject {
    
    var promotionValue: Double = 0
    var purchaseRequirement: Double = 0
    var triggerQuantity: Int = 0
    var redemptionRate: Double = 1
    var distributionCycles: [String:Double] = [String:Double]()
    
    var offer: IPOffer!
    
    init(offer: IPOffer) {
        super.init()
        self.offer = offer
    }
    
    override func JSONPropertyKeys() -> [String] {
        
        var keys = super.JSONPropertyKeys()
        keys.append(contentsOf: [
            "promotionValue",
            "purchaseRequirement",
            "triggerQuantity",
            "redemptionRate",
            "distributionCycles"])
        return keys
    }
}
extension IPCoupon {
    
    var numberOfCycles: Int {
        get {
            var result: Int = 0
            for (_, distribution) in distributionCycles {
                if distribution > 100 {
                    result += 1
                }
            }
            return result
        }
    }
    
    var discountPercent: Double {
        get {
            if offer.type == .Volume {
                return 0
            }
            else {
                return promotionValue / (offer.scenario.program.retailPricePerUnit * purchaseRequirement)
            }
        }
    }
    
    var totalDistribution: Double {
        get {
            var result: Double = 0
            for (_, distributionValue) in distributionCycles {
                result += distributionValue
            }
            return result
        }
    }
    
    var totalRedemption: Double {
        get {
            return totalDistribution * redemptionRate
        }
    }
    
    var totalInvestment: Double {
        get {
            return totalDistribution * printCostMultiplier
        }
    }
    
    var totalUnits: Double {
        get {
            if offer.type == .Trial || offer.type == .Loyalty {
                return totalRedemption * purchaseRequirement
            }
            else {
                return totalDistribution * purchaseRequirement
            }
        }
    }
    
    var totalDollars: Double {
        get {
            return totalUnits * offer.scenario.program.retailPricePerUnit
        }
    }
    
    var totalIncrementalUnits: Double {
        get {
            if offer.scenario.program.shortTermROI {
                return totalUnits * offer.incrementalRatio
            }
            else {
                return totalUnits * offer.incrementalRatio * offer.repeatFactor
            }
        }
    }
    
    var totalIncrementalDollars: Double {
        get {
            return totalIncrementalUnits * offer.scenario.program.retailPricePerUnit
        }
    }
    
    var totalAcquiredBuyers: Double {
        get {
            if offer.type == .Trial {
                if offer.scenario.program.shortTermROI {
                    return totalIncrementalUnits / offer.scenario.program.assumedUnitsPerTrialTrip
                }
                else {
                    return (totalIncrementalUnits / offer.repeatFactor) / offer.scenario.program.assumedUnitsPerTrialTrip
                }
            }
            else {
                return 0
            }
        }
    }
    
    var retailRoi: Double {
        get {
            return totalIncrementalDollars / totalInvestment
        }
    }
    
    var profitRoi: Double {
        get {
            return retailRoi * offer.scenario.program.profitMargin
        }
    }
    
    var cpum: Double {
        get {
            var printRate: Double = 0
            
            if offer.type == .Trial {
                printRate = offer.scenario.program.printCostId
            }
            else {
                printRate = offer.scenario.program.printCostTransactional
            }
            
            var investment: Double = 0
            for (_, distribution) in distributionCycles {
                investment += (printRate + (redemptionRate * (promotionValue + offer.scenario.program.handlingFee))) * distribution
            }
            
            return investment / totalUnits
        }
    }
    
    var cpium: Double {
        get {
            return totalInvestment / totalIncrementalUnits
        }
    }
    
    var printCostMultiplier: Double {
        get {
            return offer.printCost + (redemptionRate * (promotionValue + offer.scenario.program.handlingFee))
        }
    }
    
    func getCycleWeighting() -> [String: Double] {
        let templateCoupon = getTemplateCoupon()
        var cycleWeighting: [String: Double] = [String: Double]()
        for (cycleName, maxDistribution) in templateCoupon.distributionCycles {
            cycleWeighting[cycleName] = maxDistribution / templateCoupon.totalDistribution
        }
        return cycleWeighting
    }
    
    func getTemplateCoupon() -> IPCoupon {
        if let templateScenario = offer.scenario.program.templateScenario {
            for templateOffer in templateScenario.offers {
                if templateOffer.name == offer.name {
                    for coupon in templateOffer.coupons {
                        if coupon.triggerQuantity == self.triggerQuantity {
                            return coupon
                        }
                    }
                }
            }
        }
        return IPCoupon(offer: offer)
    }
}

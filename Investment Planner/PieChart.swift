//
//  PieChart.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/31/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class PieChartSectionInfo {
    
    var value: Double = 0
    var scaleAmount: CGFloat = 0
    var translateAmount: CGFloat = 0
    var color: UIColor = UIColor.black
    var title: String?
    var text: String?
    
    init() {}
    
    init(value: Double) {
        self.value = value
    }
    
    init(value: Double, color: UIColor, title: String) {
        self.value = value
        self.color = color
        self.title = title
    }
}

class PieChart: UIView {
    
    var sections = [PieChartSectionInfo]()
    var radius: CGFloat = 0
    var innerRadius: CGFloat = 0
    
    //titles
    var titleFont = UIFont.systemFont(ofSize: 14)
    var titleColor: UIColor?
    var titlePadding: CGFloat = 8
    
    //text values
    var textFont = UIFont.boldSystemFont(ofSize: 14)
    var textColor: UIColor?
    var textPadding: CGFloat = 8
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.backgroundColor = UIColor.clear
    }
    
    func addSectionWithValue(_ value: Double, color: UIColor?, title: String?, text: String?, scaleAmount: CGFloat?, translateAmount: CGFloat?) -> PieChartSectionInfo {
        
        let section = PieChartSectionInfo()
        section.value = value
        if let c = color {
            section.color = c
        }
        section.title = title
        section.text = text
        if let scale = scaleAmount {
            section.scaleAmount = scale
        }
        if let translate = translateAmount {
            section.translateAmount = translate
        }
        
        self.sections.append(section)
        self.update()
        
        return section
    }
    
    func sectionAtIndex(_ index: Int) -> PieChartSectionInfo? {
        
        if self.sections.count >= index + 1 {
            return self.sections[index]
        }
        return nil
    }
    
    func sectionWithTitle(_ title: String) -> PieChartSectionInfo? {
        
        for section in sections {
            if title == section.title {
                return section
            }
        }
        return nil
    }
    
    func update() {
        self.setNeedsLayout()
        self.setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        
        // Drawing code
        var totalValue: Double = 0
        for section in sections {
            totalValue += section.value
        }
        
        if totalValue == 0 {
            return
        }
        
        var currentValue: Double = 0
        for section in sections {
            
            let radius = self.radius + section.scaleAmount
            let innerRadius = max(0, self.innerRadius - section.scaleAmount)
            let startAngle = CGFloat((2 * M_PI) * (currentValue/totalValue))
            let endAngle = CGFloat((2 * M_PI) * ((currentValue + section.value)/totalValue))
            var center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
            var centralAngle = startAngle + ((endAngle - startAngle) * 0.5)
            if section.translateAmount > 0 {
                let tempPath = UIBezierPath()
                tempPath.move(to: center)
                tempPath.addArc(withCenter: center, radius: section.translateAmount, startAngle: startAngle, endAngle: centralAngle, clockwise: true)
                center = tempPath.currentPoint
            }
    
            let path = UIBezierPath()
            path.move(to: center)
            path.addArc(withCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            if innerRadius > 0 {
                path.addArc(withCenter: center, radius: innerRadius, startAngle: endAngle, endAngle: startAngle, clockwise: false)
            }
            section.color.setFill()
            path.fill()
            
            //draw title & text
            
            if section.title != nil || section.text != nil {
                
                let attributedText = NSMutableAttributedString()
                let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
                paragraphStyle.alignment = .center
                if let text = section.text {
                    let color = self.textColor != nil ? self.textColor! : section.color
                    attributedText.append(NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName : color, NSFontAttributeName : self.textFont, NSParagraphStyleAttributeName : paragraphStyle]))
                }
                if let title = section.title {
                    if attributedText.length > 0 {
                        attributedText.append(NSAttributedString(string: "\n"))
                    }
                    let color = self.titleColor != nil ? self.titleColor! : section.color
                    attributedText.append(NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName : color, NSFontAttributeName : self.titleFont, NSParagraphStyleAttributeName : paragraphStyle]))
                }
                
                //find a center point to draw the text outside the bounds of our circle
                centralAngle = startAngle + ((endAngle - startAngle) * 0.5)
                let tempPath = UIBezierPath()
                tempPath.move(to: center)
                tempPath.addArc(withCenter: center, radius: radius + 30, startAngle: startAngle, endAngle: centralAngle, clockwise: true)
                let textCenter = tempPath.currentPoint
                //create a rect to draw the text in that is centered on this point
                let textRect = CGRect(x: textCenter.x - 28, y: textCenter.y - 28, width: 56, height: 56)
                //the following code vertically centers the text inside the text rect
                let options = unsafeBitCast(NSStringDrawingOptions.usesLineFragmentOrigin.rawValue | NSStringDrawingOptions.usesFontLeading.rawValue, to: NSStringDrawingOptions.self)
                let adjustedRect = attributedText.boundingRect(with: CGSize(width: textRect.width, height: textRect.height), options: options, context: nil)
                let finalRect = CGRect(x: textRect.origin.x, y: textRect.origin.y + (textRect.size.height - adjustedRect.height)/2.0, width: textRect.size.width, height: adjustedRect.size.height)
                //draw the text
                attributedText.draw(in: finalRect)
            }
            
            currentValue += section.value
        }
    }
}

//
//  SwiftTypeExtensions.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/26/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import Foundation

extension String {
    func stringWithNoCharsOrSymbols()->String{
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: ".")
        let invertedCharSet = characterSet.inverted
        let arrayOfChars = self.components(separatedBy: invertedCharSet)
        let returnString = arrayOfChars.joined(separator: "")
  //     let returnString = arrayOfChars.componentsJoined(by: "")
        
        return returnString
    }
    func threeDecmialString()->Double{
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: ".")
        let invertedCharSet = characterSet.inverted
        let arrayOfChars = self.components(separatedBy: invertedCharSet)
        let newString = arrayOfChars.joined(separator: "")
        
        let doubleAmount = Double(round(Double(newString)! * 1000) / 1000)
    //    print(doubleAmount)
        
        return doubleAmount
    }
}

extension Float {
    
    static func random(min: Float, max: Float) -> Float {
        
        return (Float(arc4random()) / 0xFFFFFFFF) * (max - min) + min
    }
    
    func format(_ f: String) -> String {
        
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
}

extension Double {
    
    //convenience initializers
    
    init(currencyString: String?) {
        
        self = 0
        if var string = currencyString {
            
            if string.characters.count == 0 {
                return
            }
            
            //the following if statements check for currency suffixes, strips them out if they're found, creates a double from the resulting currency string, and then multiplies by the appropriate amount and
            string = string.uppercased()
            let suffix = string.substring(from: string.characters.index(before: string.endIndex))
            if suffix == "B" { //check for B (billion) suffix
                string = string.replacingOccurrences(of: "B", with: "")
                let value = Double(currencyString: string)
                self = value * 1000000000
                return
            } else if suffix == "M" { //check for M (million) suffix
                string = string.replacingOccurrences(of: "M", with: "")
                let value = Double(currencyString: string)
                self = value * 1000000
                return
            } else if suffix == "K" { //check for K (thousand) suffix
                string = string.replacingOccurrences(of: "K", with: "")
                let value = Double(currencyString: string)
                self = value * 1000
                return
            }
            
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.currency
            //strip out currency symbols and grouping separators to make this method work even if those were not entered by the user
            string = string.replacingOccurrences(of: formatter.currencySymbol, with: "")
            string = string.replacingOccurrences(of: formatter.currencyGroupingSeparator, with: "")
            formatter.currencySymbol = ""
            formatter.currencyGroupingSeparator = ""
            if let number = formatter.number(from: string) {
                self = number.doubleValue
            }
        }
    }
    
    init(percentString: String?) {
        
        self = 0
        if var string = percentString {
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.percent
            //strip out the percent symbol to make this method work even if that symbol was not entered by the user
            string = string.replacingOccurrences(of: formatter.percentSymbol, with: "")
            formatter.percentSymbol = ""
            if let number = formatter.number(from: string) {
                self = number.doubleValue
            }
        }
    }
    
    init(numberString: String?) {
        
        self = 0
        if let string = numberString {
            let formatter = NumberFormatter()
            if let number = formatter.number(from: string) {
                self = number.doubleValue
            }
        }
    }
    init(numberWithCommaString: String?) {
        
        self = 0
        if let string = numberWithCommaString {
            let formatter = NumberFormatter()
            if let number = formatter.number(from: string) {
                self = number.doubleValue
            }
        }
    }
    
    //other convenience methods
    
    static func random(min: Double, max: Double) -> Double {
        
        return (Double(arc4random()) / 0xFFFFFFFF) * (max - min) + min
    }
    
    func format(_ f: String) -> String {
        
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
    
    func percentString() -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.percent
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    func percentString(_ decimalPlaces: Int) -> String {
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = decimalPlaces
        formatter.maximumFractionDigits = decimalPlaces
        formatter.numberStyle = .percent
        formatter.numberStyle = NumberFormatter.Style.percent
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    
    func currencyString() -> String {
        
        return currencyString(2)
    }
    
    func currencyString(_ decimalPlaces: Int) -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.minimumFractionDigits = decimalPlaces
        formatter.maximumFractionDigits = decimalPlaces
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    
    func currencyString(_ minDecimalPlaces: Int, _ maxDecimalPlaces: Int) -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.minimumFractionDigits = minDecimalPlaces
        formatter.maximumFractionDigits = maxDecimalPlaces
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    
    func abbreviatedCurrencyString() -> String {
        
        return "$\(self.abbreviatedNumberString())"
    }
    
    func abbreviatedNumberString() -> String {
        
        var newAmountString = ""
        var newAmountPostFix = ""
        var newAmountInt = 0
        var newAmountDouble = 0.0
        if(self > 999 && self < 1000000){
            newAmountInt = Int(floor(self / 1000 + 0.5))
            newAmountPostFix = "K"
        }
        else if(self > 999999 && self < 1000000000){
            newAmountDouble =  self / 1000000
            newAmountPostFix = "M"
        }
        else if(self > 999999999){
            newAmountDouble =  self / 1000000000
            newAmountPostFix = "B"
        }
        if(self >= 1000000){
            newAmountString = "\(NSString(format: "%.01f", newAmountDouble))\(newAmountPostFix)"
        }
        else{
            newAmountString = "\(newAmountInt)\(newAmountPostFix)"
        }
        return newAmountString
    }
    func abbreviatedCurrencyStringRounded() -> String {
        
        return "$\(self.abbreviatedNumberStringRounded())"
    }
    func abbreviatedNumberStringRounded() -> String {
        
        var newAmountString = ""
        var newAmountPostFix = ""
        var newAmountInt = 0
        if(self > 999 && self < 1000000){
            newAmountInt = Int(floor(self / 1000 + 0.5))
            newAmountPostFix = "K"
        }
        else if(self > 999999 && self < 1000000000){
            newAmountInt =  Int(floor(self / 1000000 + 0.5))
            newAmountPostFix = "M"
        }
        else if(self > 999999999){
            newAmountInt =  Int(floor(self / 1000000000 + 0.5))
            newAmountPostFix = "B"
        }
        newAmountString = "\(newAmountInt)\(newAmountPostFix)"
        return newAmountString
    }
    
    func numberString() -> String {
        
        return numberString(0)
    }
    
    func numberString(_ decimalPlaces: Int) -> String {
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = decimalPlaces
        formatter.maximumFractionDigits = decimalPlaces
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    func numberWithCommaString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    func numberWithThreeDecimalsString() -> String {
        let string = self.numberString(3)
        let seperator = "."
        let decimalString = string.components(separatedBy: seperator)
        var string1 = decimalString[0]
        if(string1 == ""){
            string1 = "0"
        }
        print(string1)
        let string2 = decimalString[1]
        var returnString = ""
        if(string2.characters.last == "0"){
            let removedCharString = String(string2.characters.dropLast())
            returnString = "$\(string1).\(removedCharString)"
        }
        else{
            returnString = "$\(string1).\(string2)"
        }
        return(returnString)
    }
}

extension Int {
    
    //convenience initializers
    
    init(currencyString: String?) {
        
        self = 0
        if var string = currencyString {
            
            if string.characters.count == 0 {
                return
            }
            
            string = string.uppercased()
            let suffix = string.substring(from: string.characters.index(before: string.endIndex))
            if suffix == "B" { //check for B (billion) suffix
                string = string.replacingOccurrences(of: "B", with: "")
                let value = Int(currencyString: string)
                self = value * 1000000000
                return
            } else if suffix == "M" { //check for M (million) suffix
                string = string.replacingOccurrences(of: "M", with: "")
                let value = Int(currencyString: string)
                self = value * 1000000
                return
            } else if suffix == "K" { //check for K (thousand) suffix
                string = string.replacingOccurrences(of: "K", with: "")
                let value = Int(currencyString: string)
                self = value * 1000
                return
            }
            
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.currency
            //strip out currency symbols and grouping separators to make this method work even if those were not entered by the user
            string = string.replacingOccurrences(of: formatter.currencySymbol, with: "")
            string = string.replacingOccurrences(of: formatter.currencyGroupingSeparator, with: "")
            formatter.currencySymbol = ""
            formatter.currencyGroupingSeparator = ""
            if let number = formatter.number(from: string) {
                self = number.intValue
            }
        }
    }
    
    init(numberString: String?) {
        
        self = 0
        if let string = numberString {
            let formatter = NumberFormatter()
            if let number = formatter.number(from: string) {
                self = number.intValue
            }
        }
    }
    
    init(percentString: String?) {
        
        self = 0
        if var string = percentString {
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.percent
            //strip out the percent symbol to make this method work even if that symbol was not entered by the user
            string = string.replacingOccurrences(of: formatter.percentSymbol, with: "")
            formatter.percentSymbol = ""
            if let number = formatter.number(from: string) {
                self = number.intValue
            }
        }
    }
    init(numberWithCommaString: String?) {
        
        self = 0
        if let string = numberWithCommaString {
            let formatter = NumberFormatter()
            if let number = formatter.number(from: string) {
                self = number.intValue
            }
        }
    }
    
    //other convenience methods
    
    static func random(min: Int = 0, max: Int) -> Int {
        assert(min >= 0)
        assert(min < max)
        return Int(arc4random_uniform(UInt32((max - min) + 1))) + min
    }
    
    func percentString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.percent
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    
    func currencyString() -> String {
        
        return currencyString(2)
    }
    
    func currencyString(_ decimalPlaces: Int) -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.minimumFractionDigits = decimalPlaces
        formatter.maximumFractionDigits = decimalPlaces
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    
    func abbreviatedCurrencyString() -> String {
        
        return "$\(self.abbreviatedNumberString())"
    }
    
    func abbreviatedNumberString() -> String {
        
        return Double(self).abbreviatedNumberString()
    }
    
    func numberString() -> String {
        
        let formatter = NumberFormatter()
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
    func numberWithCommaString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        if let string = formatter.string(from: NSNumber(value: self)) {
            return string
        }
        return ""
    }
}

extension CGRect {
    mutating func offsetInPlace(dx: CGFloat, dy: CGFloat) {
        self = self.offsetBy(dx: dx, dy: dy)
    }
}

//
//  ShareViewController.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/7/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    enum ShareResult {
        case waiting
        case succeeded
        case failed
    }
    
    var scenarioToShare: IPScenario!
    var shareButton: UIButton!
    var cancelButton: UIButton!
    var titleLabel: UILabel!
    var instructionsLabel: UILabel!
    var userNameTextField: UITextField!
    var shareWithLabel: UILabel!
    var userListTableView: UITableView!
    var activityIndicator: UIActivityIndicatorView!
    
    var searchResults: [IPUser] = [IPUser]() {
        didSet {
            userListTableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }
    var selectedUsers: [IPUser] = [IPUser]() {
        didSet {
            userListTableView.reloadSections(IndexSet(integer: 0), with: .automatic)
            shareButton.isEnabled = selectedUsers.count > 0
            shareButton.backgroundColor = shareButton.isEnabled ? UIColor.catalinaDarkBlueColor() : UIColor(white: 0.9, alpha: 1)
        }
    }
    var shareResults: [IPUser:ShareResult] = [IPUser:ShareResult]()
    
    init(scenarioToShare: IPScenario) {
        
        super.init(nibName: nil, bundle: nil)
        self.scenarioToShare = scenarioToShare
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.catalinaWhiteColor()
        self.preferredContentSize = CGSize(width: 600, height: 540)
        
        titleLabel = UILabel()
        titleLabel.font = UIFont.catalinaMediumFontOfSize(24)
        titleLabel.text = "SHARE GROWTH PLAN"
        titleLabel.textColor = UIColor.catalinaDarkBlueColor()
        titleLabel.sizeToFit()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(titleLabel)
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 40))
        
        instructionsLabel = UILabel()
        instructionsLabel.font = UIFont.catalinaMediumFontOfSize(16)
        instructionsLabel.text = "Share this growth plan with another Investment Planner user"
        instructionsLabel.textColor = UIColor.catalinaDarkBlueColor()
        instructionsLabel.sizeToFit()
        instructionsLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(instructionsLabel)
        self.view.addConstraint(NSLayoutConstraint(item: instructionsLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: instructionsLabel, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 20))
        
        userNameTextField = UITextField()
        userNameTextField.placeholder = "user name"
        userNameTextField.clearButtonMode = .whileEditing
        userNameTextField.borderStyle = .roundedRect
        userNameTextField.delegate = self
        userNameTextField.keyboardType = .default
        userNameTextField.autocapitalizationType = .none
        userNameTextField.autocorrectionType = .no
        userNameTextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(userNameTextField)
        self.view.addConstraint(NSLayoutConstraint(item: userNameTextField, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: userNameTextField, attribute: .top, relatedBy: .equal, toItem: instructionsLabel, attribute: .bottom, multiplier: 1, constant: 40))
        self.view.addConstraint(NSLayoutConstraint(item: userNameTextField, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        
        shareWithLabel = UILabel()
        shareWithLabel.font = UIFont.catalinaBoldFontOfSize(16)
        shareWithLabel.text = "Share with:"
        shareWithLabel.textColor = UIColor.catalinaDarkBlueColor()
        shareWithLabel.sizeToFit()
        shareWithLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(shareWithLabel)
        self.view.addConstraint(NSLayoutConstraint(item: shareWithLabel, attribute: .centerY, relatedBy: .equal, toItem: userNameTextField, attribute: .centerY, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: shareWithLabel, attribute: .right, relatedBy: .equal, toItem: userNameTextField, attribute: .left, multiplier: 1, constant: -10))
        
        userListTableView = UITableView()
        userListTableView.dataSource = self
        userListTableView.delegate = self
        userListTableView.separatorStyle = .none
        userListTableView.layer.borderWidth = 1
        userListTableView.layer.borderColor = UIColor.lightGray.cgColor
        userListTableView.layer.cornerRadius = 6
        userListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        userListTableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(userListTableView)
        self.view.addConstraint(NSLayoutConstraint(item: userListTableView, attribute: .top, relatedBy: .equal, toItem: userNameTextField, attribute: .bottom, multiplier: 1, constant: 20))
        self.view.addConstraint(NSLayoutConstraint(item: userListTableView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: userListTableView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300))
        self.view.addConstraint(NSLayoutConstraint(item: userListTableView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 200))
        
        cancelButton = UIButton()
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.layer.cornerRadius = 6
        cancelButton.backgroundColor = UIColor.catalinaLightBlueColor()
        cancelButton.setTitleColor(UIColor.catalinaWhiteColor(), for: UIControlState())
        cancelButton.addTarget(self, action: #selector(ShareViewController.cancelButtonPressed), for: .touchUpInside)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(cancelButton)
        self.view.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 20))
        self.view.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .top, relatedBy: .equal, toItem: userListTableView, attribute: .bottom, multiplier: 1, constant: 40))
        self.view.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 130))
        self.view.addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 40))
        
        shareButton = UIButton()
        shareButton.setTitle("Share", for: UIControlState())
        shareButton.layer.cornerRadius = 6
        shareButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
        shareButton.setTitleColor(UIColor.catalinaWhiteColor(), for: UIControlState())
        shareButton.addTarget(self, action: #selector(ShareViewController.shareButtonPressed), for: .touchUpInside)
        shareButton.isEnabled = false
        shareButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(shareButton)
        self.view.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: -20))
        self.view.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .top, relatedBy: .equal, toItem: userListTableView, attribute: .bottom, multiplier: 1, constant: 40))
        self.view.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 130))
        self.view.addConstraint(NSLayoutConstraint(item: shareButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 40))
        
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.catalinaDarkBlueColor()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(activityIndicator)
        self.view.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        activityIndicator.startAnimating()
        userNameTextField.isEnabled = false
        WebservicesController.sharedWebservicesController.updateIPUserGroup { (error) -> Void in
            self.activityIndicator.stopAnimating()
            self.userNameTextField.isEnabled = true
            if let error = error {
                var message = ""
                if WebservicesController.sharedWebservicesController.ipGroupUsers.count > 0 {
                    message = "Unable to update user group. \(error.localizedDescription)"
                } else {
                    message = "Unable to update user group. Please try again later. \(error.localizedDescription)"
                }
                let alert = UIAlertController(title: "User Service Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true, completion: nil)
                    })
                self.present(alert, animated: true, completion: nil)
            }            
        }
    }
    
    //MARK: - Actions
    
    func cancelButtonPressed() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func shareButtonPressed() {
        
        activityIndicator.startAnimating()
        shareButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
        cancelButton.backgroundColor = UIColor(white: 0.9, alpha: 1)
        shareButton.isEnabled = false
        cancelButton.isEnabled = false
        shareResults.removeAll()
        
        //make a copy of the report
        let reportCopy = scenarioToShare.program.report.duplicate()
        //remove all the scenarios because we only want to share one
        reportCopy.program?.scenarios.removeAll()
        //now make a copy of the scenario we want to share and add it back to the report copy
        let scenarioCopy = scenarioToShare.duplicate()
        reportCopy.program?.scenarios.append(scenarioCopy)
        //now we can share the report with the selected users
        for user in selectedUsers {
            //for each user, add a result to the share results dictionary
            self.shareResults[user] = .waiting
            self.userListTableView.reloadData()
            WebservicesController.sharedWebservicesController.shareReport(reportCopy, withUser: user, completion: { (error) -> Void in
                //when the share has completed, evaluate the results
                self.shareResults[user] = error == nil ? .succeeded : .failed
                self.userListTableView.reloadData()
                var finished = true //if no users are still waiting then we're finished
                for (_, result) in self.shareResults {
                    if result == .waiting {
                        finished = false
                    }
                }
                if finished { // if we're finished, we can remove all of the successes from the list
                    
                    self.activityIndicator.stopAnimating()
                    self.shareButton.backgroundColor = UIColor.catalinaDarkBlueColor()
                    self.cancelButton.backgroundColor = UIColor.catalinaLightBlueColor()
                    self.shareButton.isEnabled = true
                    self.cancelButton.isEnabled = true
                    
                    for (user, result) in self.shareResults {
                        if result == .succeeded {
                            self.selectedUsers.remove(at: self.selectedUsers.index(of: user)!)
                        }
                    }
                    if self.selectedUsers.count > 0 { //if any users are left we need to alert the user and let them decide what to do
                        let alert = UIAlertController(title: "Share Failed", message: "Unable to share with all users. Please try again later.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            self.userListTableView.reloadData()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    } else { //otherwise we can dismiss the view
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    //MARK: - UITextFieldDelegate Implementation
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let searchText = (text as NSString).replacingCharacters(in: range, with: string).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.searchResults = WebservicesController.sharedWebservicesController.searchIPUsers(searchText)
        } else {
            searchResults = [IPUser]()
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        searchResults = [IPUser]()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if searchResults.count > 0 {
            self.selectUser(searchResults.first!)
        }
        return true
    }
    
    //MARK: - UITableViewDataSource Implementation
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchResults.count > 0 {
            return searchResults.count
        }
        return selectedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.accessoryView = nil
        
        var user: IPUser
        if searchResults.count > 0 {
            user = searchResults[(indexPath as NSIndexPath).row]
        } else {
            user = selectedUsers[(indexPath as NSIndexPath).row]
            if shareResults.keys.contains(user) {
                if let result = shareResults[user] {
                    if result == .succeeded {
                        let imageView = UIImageView(image: UIImage(named: "yes")?.withRenderingMode(.alwaysTemplate))
                        imageView.tintColor = UIColor.catalinaDarkGreenColor()
                        cell.accessoryView = imageView
                    } else if result == .failed {
                        let imageView = UIImageView(image: UIImage(named: "no")?.withRenderingMode(.alwaysTemplate))
                        imageView.tintColor = UIColor.catalinaDarkRedColor()
                        cell.accessoryView = imageView
                    }
                }
            }
        }
        cell.textLabel?.text = user.name
        
        if (indexPath as NSIndexPath).row % 2 == 0 {
            cell.backgroundColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor(white: 0.9, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if searchResults.count > 0 {
            let user = searchResults[(indexPath as NSIndexPath).row]
            self.selectUser(user)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if searchResults.count > 0 {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            selectedUsers.remove(at: (indexPath as NSIndexPath).row)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        
        return "Remove"
    }
    
    //MARK: - Helper Methods
    
    fileprivate func selectUser(_ user: IPUser) {
        
        var userAlreadySelected = false
        for alreadySelectedUser in selectedUsers {
            if user.userName == alreadySelectedUser.userName {
                userAlreadySelected = true
                break
            }
        }
        if !userAlreadySelected {
            selectedUsers.append(user)
        }
        searchResults = [IPUser]()
        userNameTextField.text = ""
        userNameTextField.endEditing(true)
    }
}

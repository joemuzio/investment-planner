//
//  PromotionsCollectionViewLayout.swift
//  Investment Planner
//
//  Created by Joe Helton on 3/2/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//
// Custom UICollectionViewFlowLayout used to make the horizontal scrolling and paging effect of the promotions list on the Program Settings View.

import UIKit

class PromotionsCollectionViewLayout: UICollectionViewFlowLayout {
    
    var allowFreeScrolling = false
    
    //Slightly modified version of the item paging implementation found here: http://stackoverflow.com/questions/13492037/targetcontentoffsetforproposedcontentoffsetwithscrollingvelocity-without-subcla/14291208#14291208
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let collectionView = self.collectionView {
            
            var adjustedContentOffset = proposedContentOffset
            var offSetAdjustment = CGFloat.greatestFiniteMagnitude
            let horizontalCenter = CGFloat(proposedContentOffset.x + (collectionView.bounds.size.width/2))
            
            let x = self.allowFreeScrolling ? proposedContentOffset.x : collectionView.contentOffset.x
            let targetRect = CGRect(x: x, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
            
            let array: [UICollectionViewLayoutAttributes] = self.layoutAttributesForElements(in: targetRect)! as [UICollectionViewLayoutAttributes]
            for layoutAttributes: UICollectionViewLayoutAttributes in array {
                if (layoutAttributes.representedElementCategory == UICollectionElementCategory.cell) {
                    let itemHorizontalCenter: CGFloat = layoutAttributes.center.x
                    if (abs(itemHorizontalCenter - horizontalCenter) < abs(offSetAdjustment)) {
                        offSetAdjustment = itemHorizontalCenter - horizontalCenter
                    }
                }
            }
            
            var nextOffset: CGFloat = proposedContentOffset.x + offSetAdjustment
            
            repeat {
                adjustedContentOffset.x = nextOffset
                let deltaX = proposedContentOffset.x - collectionView.contentOffset.x
                let velX = velocity.x
                
                if (deltaX == 0 || velX == 0 || (velX > 0 && deltaX > 0) || (velX < 0 && deltaX < 0)) {
                    break
                }
                
                if (velocity.x > 0) {
                    nextOffset = nextOffset + self.snapStep()
                } else if (velocity.x < 0) {
                    nextOffset = nextOffset - self.snapStep()
                }
            } while self.isValidOffset(nextOffset)
            
            adjustedContentOffset.y = 0
            
            return adjustedContentOffset
            
        }
        
        return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
    }
    
    func isValidOffset(_ offset: CGFloat) -> Bool {
        
        return (offset >= CGFloat(self.minContentOffset()) && offset <= CGFloat(self.maxContentOffset()))
    }
    
    func minContentOffset() -> CGFloat {
        
        return -CGFloat(self.collectionView!.contentInset.left)
    }
    
    func maxContentOffset() -> CGFloat {
        
        return CGFloat(self.minContentOffset() + self.collectionView!.contentSize.width - self.itemSize.width)
    }
    
    func snapStep() -> CGFloat {
        
        return self.itemSize.width + self.minimumLineSpacing;
    }
}

//
//  UIKitExtensions.swift
//  Investment Planner
//
//  Created by Joe Helton on 2/9/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit
import ObjectiveC

// MARK: - UIColor

extension UIColor {
    
    class func catalinaLightBlueColor() -> UIColor {
        
        return UIColor(hexColor: 0x00ADEF)
    }
    
    class func catalinaDarkBlueColor() -> UIColor {
        
        return UIColor(hexColor: 0x274A80)
    }
    
    class func catalinaLightGreenColor() -> UIColor {
        
        return UIColor(hexColor: 0xA5DA00)
    }
    
    class func catalinaDarkGreenColor() -> UIColor {
        
        return UIColor(hexColor: 0x008245)
    }
    
    class func catalinaLightRedColor() -> UIColor {
        
        return UIColor(hexColor: 0xE6266C)
    }
    
    class func catalinaDarkRedColor() -> UIColor {
        
        return UIColor(hexColor: 0xB1262D)
    }
    
    class func catalinaOrangeColor() -> UIColor {
        
        return UIColor(hexColor: 0xFF6900)
    }
    
    class func catalinaYellowColor() -> UIColor {
        
        return UIColor(hexColor: 0xF1D54E)
    }
    
    class func catalinaBlackColor() -> UIColor {
        
        return UIColor(hexColor: 0x000000)
    }
    
    class func catalinaWhiteColor() -> UIColor {
        
        return UIColor(hexColor: 0xFFFFFF)
    }
    
    convenience init(hexColor: UInt) {
        
        self.init(
            red: CGFloat((hexColor & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hexColor & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hexColor & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    convenience init(hexString: String) {
        
        var hex: UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hex)
        self.init(hexColor:UInt(hex))
    }
    
    class func randomColor() -> UIColor { 
        
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    
    func interpolateRGBColorTo(_ end:UIColor, fraction:CGFloat) -> UIColor {
 
        var f = max(0, fraction)
        f = min(1, fraction)
        
        let c1 = self.cgColor.components!
        let c2 = end.cgColor.components!
        
        let r = (c1[0] + (c2[0] - c1[0]) * f)
        let g = (c1[1] + (c2[1] - c1[1]) * f)
        let b = (c1[2] + (c2[2] - c1[2]) * f)
        let a = (c1[3] + (c2[3] - c1[3]) * f)
        
        return UIColor(red:r, green:g, blue:b, alpha:a)
    }
    
}

// MARK: - UIFont

extension UIFont {
    
    class func catalinaLightFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-Light", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    class func catalinaLightObliqueFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-LightOblique", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    class func catalinaMediumFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-Medium", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    class func catalinaMediumObliqueFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-MediumOblique", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    class func catalinaBookFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-Book", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    class func catalinaBookObliqueFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-BookOblique", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    class func catalinaHeavyFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-Heavy", size: size) {
            return font
        } else {
            return UIFont.boldSystemFont(ofSize: size)
        }
    }
    
    class func catalinaHeavyObliqueFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-HeavyOblique", size: size) {
            return font
        } else {
            return UIFont.boldSystemFont(ofSize: size)
        }
    }
    
    class func catalinaBoldFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-Bold", size: size) {
            return font
        } else {
            return UIFont.boldSystemFont(ofSize: size)
        }
    }
    
    class func catalinaBoldObliqueFontOfSize(_ size: CGFloat) -> UIFont {
        
        if let font = UIFont(name: "FuturaStd-BoldOblique", size: size) {
            return font
        } else {
            return UIFont.boldSystemFont(ofSize: size)
        }
    }    
}

// MARK: - UIView

extension UIView {
    
    var isRaised: Bool {
        
        get {
            return self.layer.shadowRadius == 5 && self.layer.shadowOpacity == 0.5 && self.layer.shadowOffset.width == 1 && self.layer.shadowOffset.height == 1
        }
        set {
            if newValue {
                self.layer.shadowOffset = CGSize(width: 1, height: 1)
                self.layer.shadowRadius = 5
                self.layer.shadowOpacity = 0.5
            } else {
                self.layer.shadowOffset = CGSize.zero
                self.layer.shadowRadius = 0
                self.layer.shadowOpacity = 0
            }
        }
    }
    
    var isBouncing: Bool {
        
        get {
            return self.layer.animation(forKey: "pythagoras.animation.bounce") != nil && self.layer.animation(forKey: "pythagoras.animation.rotation") != nil
        }
        set {
            if newValue {
                CATransaction.begin()
                CATransaction.setDisableActions(false)
                self.layer.add(rotationAnimation(), forKey: "pythagoras.animation.rotation")
                self.layer.add(bounceAnimation(), forKey: "pythagoras.animation.bounce")
                CATransaction.commit()
            } else {
                self.layer.removeAnimation(forKey: "pythagoras.animation.rotation")
                self.layer.removeAnimation(forKey: "pythagoras.animation.bounce")
            }
        }
    }
    
    fileprivate func rotationAnimation() -> CAKeyframeAnimation {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        let angle = CGFloat(0.03)
        let duration = TimeInterval(0.15)
        let variance = Double(0.03)
        animation.values = [angle, -angle]
        animation.autoreverses = true
        animation.duration = self.randomizeInterval(duration, withVariance: variance)
        animation.repeatCount = .infinity
        return animation
    }
    
    fileprivate func bounceAnimation() -> CAKeyframeAnimation {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        let bounce = CGFloat(2.0)
        let duration = TimeInterval(0.2)
        let variance = Double(0.025)
        animation.values = [bounce, -bounce]
        animation.autoreverses = true
        animation.duration = self.randomizeInterval(duration, withVariance: variance)
        animation.repeatCount = .infinity
        return animation
    }
    
    fileprivate func randomizeInterval(_ interval: TimeInterval, withVariance variance:Double) -> TimeInterval {
        
        let random = (Double(arc4random_uniform(1000)) - 500.0) / 500.0
        return interval + variance * random;
    }
    
}

class SmoothSlider: UISlider {
    
    override func setValue(_ value: Float, animated: Bool) {
        
        if animated {
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: { () -> Void in
                super.setValue(value, animated: animated)
            }) 
        } else {
            super.setValue(value, animated: animated)
        }
    }
}

class GradientView: UIView {
    
    override class var layerClass : AnyClass {
        return CAGradientLayer.self
    }
    
    var colors: [CGColor]? {
        didSet {
            let gradientLayer = self.layer as! CAGradientLayer
            gradientLayer.colors = colors
        }
    }
    
    var locations: [NSNumber]? {
        didSet {
            let gradientLayer = self.layer as! CAGradientLayer
            gradientLayer.locations = locations
        }
    }
}

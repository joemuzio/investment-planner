//
//  DesignInputsCollectionViewCell.swift
//  Investment Planner
//
//  Created by Joe Helton on 4/1/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class DesignInputsCollectionViewCell: UICollectionViewCell {
    
    static let cellPadding: CGFloat = 10
    
    var textLabel: UILabel!
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        self.contentView.layer.borderColor = UIColor.catalinaDarkBlueColor().cgColor
        self.contentView.layer.borderWidth = 0.5
        
        textLabel = UILabel()
        textLabel.backgroundColor = UIColor.clear
        textLabel.numberOfLines = 0
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(textLabel)
        self.contentView.addConstraint(NSLayoutConstraint(item: textLabel, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: DesignInputsCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: textLabel, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1, constant: -DesignInputsCollectionViewCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: textLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
    }
}

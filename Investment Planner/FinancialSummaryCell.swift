//
//  FinancialSummaryCell.swift
//  Investment Planner
//
//  Created by Douglas Wall on 8/28/16.
//  Copyright © 2016 Pythagoras.io. All rights reserved.
//

import UIKit

class FinancialSummaryCell: UICollectionViewCell {
    
    static let rowHeight: CGFloat = 28
    static let cellPadding: CGFloat = 20
    static let textFont = UIFont.catalinaBoldFontOfSize(14)
    
    var propertyNameLabel: UILabel!
    var propertyValueLabel1: UILabel!
    var propertyValueLabel2: UILabel!
    var propertyValueLabel3: UILabel!
    var propertyValueLabel4: UILabel!
    
    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit() {
        
        propertyNameLabel = UILabel()
        propertyNameLabel.font = FinancialSummaryCell.textFont
        propertyNameLabel.backgroundColor = UIColor.clear
        propertyNameLabel.textColor = UIColor.catalinaBlackColor()
        propertyNameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyNameLabel)
        
        propertyValueLabel4 = UILabel()
        propertyValueLabel4.textAlignment = .center
        propertyValueLabel4.font = FinancialSummaryCell.textFont
        propertyValueLabel4.backgroundColor = UIColor.clear
        propertyValueLabel4.textColor = UIColor.catalinaBlackColor()
        propertyValueLabel4.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel4)
        
        propertyValueLabel3 = UILabel()
        propertyValueLabel3.textAlignment = .center
        propertyValueLabel3.font = FinancialSummaryCell.textFont
        propertyValueLabel3.backgroundColor = UIColor.clear
        propertyValueLabel3.textColor = UIColor.catalinaBlackColor()
        propertyValueLabel3.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel3)
        
        propertyValueLabel2 = UILabel()
        propertyValueLabel2.textAlignment = .center
        propertyValueLabel2.font = FinancialSummaryCell.textFont
        propertyValueLabel2.backgroundColor = UIColor.clear
        propertyValueLabel2.textColor = UIColor.catalinaBlackColor()
        propertyValueLabel2.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel2)
        
        propertyValueLabel1 = UILabel()
        propertyValueLabel1.textAlignment = .center
        propertyValueLabel1.font = FinancialSummaryCell.textFont
        propertyValueLabel1.backgroundColor = UIColor.clear
        propertyValueLabel1.textColor = UIColor.catalinaBlackColor()
        propertyValueLabel1.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(propertyValueLabel1)
        
        setConstraintsForCollapsed()
    }
    
    func setConstraintsForExpanded() {
        self.contentView.removeConstraints(self.contentView.constraints)
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: FinancialSummaryCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.40, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1, constant: -FinancialSummaryCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.12, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel3, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.12, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel2, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.12, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel1, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.12, constant: 0))
    }
    
    func setConstraintsForCollapsed() {
        self.contentView.removeConstraints(self.contentView.constraints)

        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1, constant: FinancialSummaryCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyNameLabel, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.40, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1, constant: -FinancialSummaryCell.cellPadding))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel3, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel3, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel2, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel2, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel1, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .right, relatedBy: .equal, toItem: propertyValueLabel1, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: propertyValueLabel4, attribute: .width, relatedBy: .equal, toItem: self.contentView, attribute: .width, multiplier: 0.48, constant: 0))
    }
}
